/**
 * CustomRMLoginAltFlowServiceGenerateAuthAccessCodePostProcess.java
 * @since Oct 10, 2018 - 2:21:19 PM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.user.custom;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.VOFactory;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.common.ICustomHook;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.EmailId;
import com.infosys.fentbase.types.primitives.OTP;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.user.CUSRTableUtility;

/**
 * @author Sahanaboggaram_N
 *
 */
public class CustomForgotPasswordServiceGenerateAuthAccessCodePostProcess implements ICustomHook {

	@Override
	public void execute(IContext objContext, Object objInputOutput)
			throws BusinessException, BusinessConfirmation, CriticalException {

		FEBATransactionContext febaTxnContext = (FEBATransactionContext) objContext;
		EmailId email = null;
		OTP smsOTP = null;

		String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", febaTxnContext);

		if ((pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.PINANG))
				|| (pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.SAHABAT))) {

			LoginAltFlowVO loginAltFlowVO = (LoginAltFlowVO) objInputOutput;
			if (loginAltFlowVO.getRegistrationStatus().toString().equals("N")
					&& loginAltFlowVO.getForgotPasswordOfflineVO().getRequestType().toString().equals("FPONU")) {
				CustomSendSMSUtil smsUtil = new CustomSendSMSUtil();
				smsUtil.sendSMS(febaTxnContext, objInputOutput);
			}
		}

		if (pinangOrSahabat.equalsIgnoreCase(CustomEBConstants.SAHABAT)) {

			LoginAltFlowVO loginAltFlowVO = (LoginAltFlowVO) objInputOutput;
			if (loginAltFlowVO.getRegistrationStatus().toString().equals("Y")
					&& loginAltFlowVO.getForgotPasswordOfflineVO().getRequestType().toString().equals("FPONU")) {

				CUSRInfo cusrInfo = CUSRTableUtility.getInstance().getUserRecord((FBATransactionContext) febaTxnContext,
						loginAltFlowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().getUserId(),
						loginAltFlowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().getCorpId(),
						febaTxnContext.getBankId());
				if (cusrInfo != null) {
					email = cusrInfo.getCEmailId();
				}
				smsOTP = loginAltFlowVO.getLoginAltFlowUserDetailsVO().getOtp();

				CustomEmailDetailsVO emailDetailsVO = (CustomEmailDetailsVO) VOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomEmailDetailsVO);
				if (email != null) {
					emailDetailsVO.setToAddress(email.toString());
				}
				emailDetailsVO.setSubject(getMessage(febaTxnContext, null, CustomEBConstants.EMAIL_SUBJECT));
				emailDetailsVO.setMessageContent(getMessage(febaTxnContext, smsOTP, CustomEBConstants.EMAIL_CONTENT));

				CustomSendEmailUtil emailUtil = new CustomSendEmailUtil();
				emailUtil.sendEmail(febaTxnContext, emailDetailsVO);
			}

		}

	}

	private String getMessage(FEBATransactionContext febaTxnContext, OTP smsOTP, String strId) {

		DescriptionMedium strDesc = new DescriptionMedium("");
		try {
			IFEBAType returnedValue = AppDataManager.getValue(febaTxnContext, FBAAppDataConstants.STRINGCONSTANTS_CACHE,
					FBAAppDataConstants.COLUMN_STR_ID + AppDataConstants.EQUALS + strId);
			if (null != returnedValue) {
				strDesc = (DescriptionMedium) returnedValue;
			}
		} catch (CriticalException e) {
			strDesc.setValue("");
		}
		if (smsOTP != null) {
			return strDesc.toString().replace("<OTP>", smsOTP.toString());
		} else {
			return strDesc.toString();
		}

	}

}

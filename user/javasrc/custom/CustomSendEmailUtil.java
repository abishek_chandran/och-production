/**
 * CustomSendEmailUtil.java
 * @since Oct 9, 2018 - 3:57:46 PM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.user.custom;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.util.ValidationsUtil;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;

/**
 * @author Tarun_Kumar18
 *
 *         Common Utility to send Email
 * 
 *         Fields to be passed in CustomEmailDetailsVO object
 * 
 *         Mandatory : toAddress, subject, messageContent.
 * 
 *         Optional : debitAccount, ccAddress, attachment.
 * 
 *         Attachment: Name of the file to be transferred, as well as the
 *         display method on the email (separated by ";").
 * 
 *         "Picture.jpg;0": shown as a view inside the email body content.
 *         "Picture.jpg;1": shown as inserted attachment
 *
 * 
 */

public final class CustomSendEmailUtil {

	public static void sendEmail(FEBATransactionContext txnContext, Object objInputOutput)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomEmailDetailsVO emailDetailsVO = (CustomEmailDetailsVO) objInputOutput;

		/* Validate Mandatory Fields */
		final FEBAValItem[] valitems = new FEBAValItem[] {
				new FEBAValItem(emailDetailsVO.getToAddress(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.TO_ADDRESS_MANDATORY),
				new FEBAValItem(emailDetailsVO.getSubject(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.SUBJECT_MANDATORY),
				new FEBAValItem(emailDetailsVO.getMessageContent(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.MESSAGE_CONTENT_MANDATORY) };
		ValidationsUtil.callValEngine(txnContext, emailDetailsVO, valitems);
		EBHostInvoker.processRequest(txnContext, CustomEBRequestConstants.CUSTOM_SEND_EMAIL, emailDetailsVO);
	}
}

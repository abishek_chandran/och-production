package com.infosys.custom.ebanking.user.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.ReverseComparator;

import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.pojo.UploadedFile;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.formsgroup.ContentUploadDetails;
import com.infosys.feba.framework.formsgroup.FEBAFileUploadUtility;
import com.infosys.feba.framework.formsgroup.FEBAInputWrapper;
import com.infosys.feba.framework.formsgroup.FileUploadedFileInfo;
import com.infosys.feba.framework.types.primitives.FileUploadFormFieldName;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoListVO;
import com.infosys.feba.utils.insulate.ArrayList;

public class CustomDocumentUploadUtil {
	
	private String tempFileName;
	
	protected HashMap files = new HashMap();
	
	File dir = null;
	/**
	 * Creates a temporary file and uploads the file if the validations are successful.
	 * 
	 * @param fileName
	 * @param mimeType
	 * @param opcontext
	 * @param data
	 * @param request
	 * @return
	 * @throws BusinessException
	 * @throws CriticalException
	 * @throws BusinessConfirmation
	 * @throws IOException 
	 */
	public CustomDocumentInputDetailsVO createFileInSharedDirAndUploadEncryptedFile(CustomDocumentInputDetailsVO documentDetails,IOpContext opcontext,HttpServletRequest request) throws BusinessException, CriticalException, BusinessConfirmation, IOException{
		
		String saveDirectory =PropertyUtil.getProperty("FEBA_SHARED_SYS_PATH", null);
		//String  saveDirectory = FEBAConstants.FEBA_SHARED_SYS_PATH;
		HashMap fileNameMap = new HashMap();
		String data = documentDetails.getFile();
		String[] fileData = null;
		if(data.startsWith("data:image/jpeg;base64,")){
			fileData = data.split(",");
			data = fileData[1];
		}
		byte[] bytedata = Base64.getDecoder().decode(data);
		
		dir = new File(saveDirectory+"/"+documentDetails.getApplicationId());
		
		if (!dir.exists()) {
			dir.mkdirs();
		}
		
		// Check saveDirectory is truly a directory
		if (!dir.isDirectory()){
		
			throw new CriticalException(
					null,
					FEBAIncidenceCodes.IS_NOT_DIR,
					ErrorCodes.IS_NOT_DIR);
		}
		// Check saveDirectory is writable
		if (!dir.canWrite()){
			throw new CriticalException(
					null,
					FEBAIncidenceCodes.IS_NOT_WRITABLE,
					ErrorCodes.IS_NOT_WRITABLE);
		}
	
	
		OpContext context = (OpContext) request.getAttribute("OP_CONTEXT");
		
		FileNameMap mimeTypeMap = URLConnection.getFileNameMap();
		
		String mimeType = mimeTypeMap.getContentTypeFor(documentDetails.getFileName());
		
		if(!RestCommonUtils.isNotNullorEmpty(mimeType)){
			MimetypesFileTypeMap tempmimeTypeMap = new MimetypesFileTypeMap();
			mimeType = tempmimeTypeMap.getContentType(documentDetails.getFileName());
			
		}
		
		String type = mimeType.split("/")[0];
		
		if(type.equals("image")){
		for (File file: dir.listFiles()){
		    if (!file.isDirectory()){		    	
		    	MimetypesFileTypeMap tempmimeTypeMap = new MimetypesFileTypeMap();
				mimeType = tempmimeTypeMap.getContentType(file);
		
				type = mimeType.split("/")[0];
				if(type.equals("image")){
		
					file.delete();
				}
		    }
		}
		}
		
		String userprincipal = "";
		UserPrincipal userPrincipal = (UserPrincipal)opcontext.getFromContextData(EBankingConstants.USER_PRINCIPAL);
		if(userPrincipal!=null) {
			userprincipal = userPrincipal.toString();
		}
		if(null != userprincipal)
			try {
				writeToXservice(dir,userprincipal, documentDetails.getFileName(),bytedata);
			} catch (IOException e) {
				throw new CriticalException(null, FEBAIncidenceCodes.FILE_READ_ERROR, ErrorCodes.FILE_READ_ERROR, e);
			}

		request.setAttribute("fileNameinDir", tempFileName);

		fileNameMap.put(documentDetails.getFileName(), tempFileName);

		request.setAttribute("fileNameMap", fileNameMap);

		files.put(tempFileName, new UploadedFile(dir.toString(), tempFileName, documentDetails.getFileName(),
				mimeType, documentDetails.getFileName()));


		
		FEBAInputWrapper[] inputWrapper = setFilesToRequest(documentDetails.getFileName(),tempFileName,mimeType);
		request.setAttribute(FEBAConstants.LIST_OF_UPLOADED_FILES, inputWrapper);
		
		return uploadFile( context, documentDetails,request);


	}

	public long writeToXservice(File fileOrDirectory, String userprincipal, String fileName, byte[] bytedata) throws IOException {
		
		long written = 0;

		try{
			// Only do something if this part contains a file
			if (fileName != null) {
		
				// FDK 11.2 reconcilation
				String currTimeValue=getDateTime();
				int index =fileName.lastIndexOf('.'); 
				
				if(index==-1) {
					index = fileName.length();
				} 
				
				String   fileName1 = fileName.substring(0, index);
				
			//	fileName=fileName.substring(0, index)+FEBAConstants.UNDER_SCORE+userprincipal+currTimeValue+fileName.substring(index,fileName.length());
				tempFileName = fileName;
				// Check if user supplied directory
				File file;
				// Fortify Fix : Path Manipulation  : Added by Piyasha
				if (fileOrDirectory.isDirectory() && isValidFileName(fileName1)) {
					// Write it to that dir the user supplied, 
					// with the filename it arrived with
		
					file = new File(fileOrDirectory, fileName);
				}
				else {
		
					// Write it to the file the user supplied,
					// ignoring the filename it arrived with
					file = fileOrDirectory;
					
				}

				FileOutputStream fileOut = new FileOutputStream(file);
		
				written = write(fileOut,bytedata);
			}
		}catch(Exception e){
		
			LogManager.logError(null, e);
		}
		return written;
	}
	
	/**
	 * This  method validates the FileName
	 * @author Piyasha_Roy 
	 * @param text
	 * @return boolean
	 */
	private boolean isValidFileName(String fileName) {     
		Pattern pattern = Pattern.compile("^[^/\\:*?\"<>|]+$");     
		Matcher matcher = pattern.matcher(fileName);     
		return  matcher.matches();  
	} 
	
	private static   String getDateTime()  
	{  
		DateFormat df = new SimpleDateFormat("_dd_MM_yyyy_hh_mm_ss");  
		df.setTimeZone(TimeZone.getTimeZone("PST"));

		return df.format(new Date());  


	} 

	/**
	 * Internal method to write this file part; doesn't check to see
	 * if it has contents first.
	 * @param bytedata 
	 *
	 * @return number of bytes written.
	 * @exception IOException	if an input or output exception has occurred.
	 */
	long write(OutputStream out, byte[] bytedata) throws IOException {
		long size=0;
		if(bytedata!=null) {
			out.write(bytedata);
			out.close();
			size = bytedata.length;
		}		
		
		return size;
	}
	
	/**
	 * Uploads the file after performing validations.
	 * 
	 * @param fileDetails2
	 * @param context
	 * @param fileNameinReq
	 * @param request 
	 * @return List<GFPFileUploadDetailsVO>
	 * @throws BusinessException
	 * @throws CriticalException
	 * @throws BusinessConfirmation
	 */
	private CustomDocumentInputDetailsVO uploadFile(OpContext context,CustomDocumentInputDetailsVO documentDetails,
			 HttpServletRequest request) throws BusinessException, CriticalException, BusinessConfirmation {
		
		FEBAInputWrapper[] filesList = (FEBAInputWrapper[]) request
				.getAttribute(FEBAConstants.LIST_OF_UPLOADED_FILES);
		// Added if fileList is blank then user can navigate back
		if (filesList == null) {
			return null;
		}
		int length = filesList.length;
	
		//TO DD:Make a for loop for VO list and validate if febafile is null,thrown exception
		FEBAInputWrapper inputWrapper = null;

		
		HashMap fileNameMap = (HashMap)request.getAttribute("fileNameMap");
		ArrayList bVOListException = new ArrayList();
		
		if(length==1){
			BusinessExceptionVO bVOException = null;
			inputWrapper =  filesList[length-1];
			ContentUploadDetails fileContentDetails=inputWrapper.getFileContentDetails();
			if (!(fileContentDetails == null || fileContentDetails.getContentLength()<0)) {
		
				FileUploadedFileInfo fileDetails;
				try {
					fileDetails = new FEBAFileUploadUtility().uploadFileFromFileSystem(context, fileContentDetails);
		
					if(fileDetails!=null) {
		
						documentDetails.setFileSeqNo(Long.toString(fileDetails.getSequenceNumber()));
						documentDetails.setFileUploadPath(fileContentDetails.getFileStoragePath()+"/"+documentDetails.getFileName());
						
					}
				} catch (BusinessException be) {
		
					bVOException = be.getExceptionList().get(0);           	
					bVOListException.add(bVOException);
				}
			}
			
		}

		/*If any business exception is thrown while uploading a file, then clear the success message of valid file as well as valid file
		 * never gets atatched in case of business exception. */
		if(!bVOListException.isEmpty() ){ 
			if(context.getBusinessInfo()!=null){
				context.getBusinessInfo().set(new BusinessInfoListVO());
			}	 

			//throw a generic message asking used to upload again all the files in case of exception in any of the uplaoded files
			BusinessExceptionVO bCommonMessageVO = new BusinessExceptionVO(context,FEBAIncidenceCodes.FU_FILE_FILE_UPLOAD_VAL_FAILURE,
					"Please select the proper files and attach again.",ErrorCodes.FILE_ATTACH_FAILURE_MESSAGE);
			
			bVOListException.add(bCommonMessageVO);

			throw new BusinessException(context, bVOListException);	        
		} 
		return documentDetails;
	}
	/**
	 * Sets the files to the reserved field called "FILE_ARRAY" in the request.
	 * @param fileName 
	 * @param mimeType 
	 * @param actualFileName 
	 *
	 * @param multi the MultipartRequest
	 * @param webRequest the IFebaRequest
	 * @return 
	 * @revised revised by Soumya Banerjee 
	 * @revision_changes see class comments
	 * @since May14 2010
	 */
	public FEBAInputWrapper[] setFilesToRequest(String originalFileName, String fileName, String mimeType) {

		FileUploadFormFieldName fuf=null;
		ContentUploadDetails fileContentDetails=null;
		com.infosys.feba.utils.insulate.ArrayList fileDataList = new com.infosys.feba.utils.insulate.ArrayList();


		FEBAInputWrapper inputWrapper = null;
		inputWrapper = new FEBAInputWrapper();


		fuf= new FileUploadFormFieldName(originalFileName);
		inputWrapper.setFileName(fuf);

		File file = getFile(fileName);
		if (fileName != null) {

			if(file != null){
				// put the file details into ContentUploadDetails. This will be used to get the details of the files to be uploaded.
				fileContentDetails=new ContentUploadDetails(file,originalFileName,mimeType);
				fileContentDetails.setClientFilePath(getClientFilePath(fileName));
				fileContentDetails.setFileStoragePath(getFileStoragePath(fileName));
				inputWrapper.setFileContentDetails(fileContentDetails);
			}

			fileDataList.add(inputWrapper);

		}
		Comparator<FEBAInputWrapper> comp = new BeanComparator("fileName",new ReverseComparator(new ComparableComparator()));
		Collections.sort(fileDataList, comp);

		// Use the temp one to fill the array set in the request object
		int finalSize = fileDataList.size();
		FEBAInputWrapper[] filesList = new FEBAInputWrapper[finalSize];

		for (int i = 0; i < finalSize; i++) {
			filesList[i] =  (FEBAInputWrapper) fileDataList.get(i);

		}

		return filesList;

	}
	
	/**
	 * Returns a File object for the specified file saved on the server's
	 * filesystem, or null if the file was not included in the upload.
	 *
	 * @param name
	 *            the html page's file parameter name.
	 * @return a File object for the named file.
	 */
	private File getFile(String name) {

		try {
			UploadedFile file = (UploadedFile) files.get(name);
			return file.getFile(); // may be null
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Returns the path where the file is stored in local file system
	 *
	 * @param file
	 *            name
	 * @return path of the file.
	 */
	public String getFileStoragePath(String name) {

		try {
			UploadedFile file = (UploadedFile) files.get(name);
			return file.getFileStoragePath(); // may be null
		} catch (Exception e) {
			return null;
		}
	}
	
	public String getClientFilePath(String name) {

		try {
			UploadedFile file = (UploadedFile) files.get(name);
			return file.getClientFilePath(); // may be null
		} catch (Exception e) {
			return null;
		}
	}
	


}

package com.infosys.custom.ebanking.user.util;

import com.infosys.custom.ebanking.tao.CLPMConstants;
import com.infosys.custom.ebanking.tao.CLTIConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;

/**
 * This class is used to populate the workflow VO
 * 
 */
public class CustomLoanProductMaintenancePopulateWfUtil {
	/**
	 *
	 * <b>Description</b>:This method is used to populate the workflow VO
	 * 
	 * @param objContext
	 * @param wfVOPopulator
	 * @param objInputOutput
	 * @param objTxnWM
	 * @param operation
	 *            <br>
	 * 
	 */

	public static void populateWorkflowVO(FEBATransactionContext objContext, IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, char operation) {
		// Cast the Transaction context into EBanking transaction context
		EBTransactionContext ebcontext = (EBTransactionContext) objContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		// Using the setter methods in the utility set the input parameters
		wfVOPopulator.setCorpId(ebcontext.getBankId().toString());
		wfVOPopulator.setEntityType("CLPM");
		wfVOPopulator.setWfOperation(String.valueOf(operation));
		wfVOPopulator.setModuleID(EBankingConstants.BANK_USER_ADMIN);
		wfVOPopulator.setTransactionType("LPM");
		wfVOPopulator.setKeyField(TxnTypeConstants.BANK_ID, ebcontext.getBankId().toString());
		wfVOPopulator.setKeyField(CLPMConstants.LN_REC_ID, customLoanProductDetailsVO.getLnRecId().toString());
	}

	/**
	 *
	 * <b>Description</b>:This method is used to populate the workflow VO
	 * 
	 * @param objContext
	 * @param wfVOPopulator
	 * @param objInputOutput
	 * @param objTxnWM
	 * @param operation
	 *            <br>
	 * 
	 */

	public static void populateLoanInterestWorkflowVO(FEBATransactionContext objContext,
			IPopulateWorkflowVOUtility wfVOPopulator, IFEBAValueObject objInputOutput, char operation) {

		// Cast the Transaction context into EBanking transaction context
		EBTransactionContext ebcontext = (EBTransactionContext) objContext;
		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;

		wfVOPopulator.setCorpId(ebcontext.getBankId().toString());
		wfVOPopulator.setEntityType("CLTI");
		wfVOPopulator.setWfOperation(String.valueOf(operation));
		wfVOPopulator.setModuleID(EBankingConstants.BANK_USER_ADMIN);
		wfVOPopulator.setTransactionType("LID");
		wfVOPopulator.setKeyField(TxnTypeConstants.BANK_ID, ebcontext.getBankId().toString());
		wfVOPopulator.setKeyField(CLTIConstants.LN_REC_ID, customLoanTenureInterestDetailsVO.getLnRecId().toString());
		wfVOPopulator.setKeyField(CLTIConstants.TENOR, customLoanTenureInterestDetailsVO.getTenor().toString());
	}

	/**
	 *
	 * <b>Description</b>:This method is used to populate the workflow VO
	 * 
	 * @param objContext
	 * @param wfVOPopulator
	 * @param objInputOutput
	 * @param objTxnWM
	 * @param operation
	 * @param functionCode
	 *            <br>
	 */
	public void populateWorkflowVO(FEBATransactionContext objContext, IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, char operation, char functionCode) {
		// calling the overloaded method
		populateWorkflowVO(objContext, wfVOPopulator, objInputOutput, operation);
		wfVOPopulator.setFuncCode(String.valueOf(functionCode));
	}
}

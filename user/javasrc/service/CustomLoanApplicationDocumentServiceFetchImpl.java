/**
 * CustomLoanApplicationDocumentServiceFetchImpl.java
 * @since Oct 4, 2018 - 4:27:16 PM
 *
 * COPYRIGHT NOTICE:
 * Copyright (c) 2018 Infosys Technologies Limited, Electronic City,
 * Hosur Road, Bangalore - 560 100, India.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDocumentCriteriaVO;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

/**
 * @author Tarun_Kumar18
 *
 */
public class CustomLoanApplicationDocumentServiceFetchImpl extends AbstractLocalListInquiryTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_APPLICATION_DOCUMENT_FETCH;
	}

	@Override
	protected void associateQueryParameters(FEBATransactionContext objContext, IFEBAValueObject objQueryCrit,
			IFEBAValueObject objTxnWM, QueryOperator queryOperator) throws CriticalException {

		CustomLoanApplicationDocumentCriteriaVO criteriaVO = (CustomLoanApplicationDocumentCriteriaVO) objQueryCrit;
		queryOperator.associate("bankId", criteriaVO.getBankId());
		queryOperator.associate("userId", criteriaVO.getUserId());
		queryOperator.associate("applicationStatus", getCommonCodes(new String[]{"LOAN_CREATED","LOAN_PAID"}));
		
		// populating document list to fetch for loan application
		String documentTypes = PropertyUtil.getProperty(CustomEBConstants.LOAN_APPLN_DOC_FETCH_PROPERTY, objContext);
		if (documentTypes.isEmpty()) {
			throw new CriticalException(true, objContext, CustomEBankingIncidenceCodes.LOAN_APPLICATION_FETCH,
					"Property configuration to fetch loan application documents types empty.",
					CustomEBankingErrorCodes.LOAN_APPLICATION_FETCH, null, (AdditionalParam) null);
		} else {
			String[] documentTypesArray = documentTypes.split(EBankingConstants.PIPE_SPLIT_PATTERN);
			FEBAArrayList<DocumentType> documentTypeList = new FEBAArrayList<>();
			for (String documentType : documentTypesArray) {
				documentTypeList.addObject(new DocumentType(documentType));
			}
			criteriaVO.setDocumentTypeList(documentTypeList);
			queryOperator.associate("documentTypeList", criteriaVO.getDocumentTypeList());
		}
	}

	private FEBAArrayList getCommonCodes(String[] statusCodes) {
		FEBAArrayList commonCodes = new FEBAArrayList();
		for (int cntr = 0; cntr < statusCodes.length ; cntr ++){
			commonCodes.addObject(new CommonCode(statusCodes[cntr]));
		}
		return commonCodes;
	}

	@Override
	protected void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			if (be.getErrorCode() == ErrorCodes.RECORD_NOT_FOUND) {
				throw new BusinessException(true, objContext,
						CustomEBankingIncidenceCodes.LOAN_APPLICATION_FETCH_NO_DOCUMENTS_FOUND,
						"Loan Application documents not found", "",
						CustomEBankingErrorCodes.LOAN_APPLICATION_FETCH_NO_DOCUMENTS_FOUND, null,
						(AdditionalParam) null);
			} else {
				throw be;
			}
		}
	}

}

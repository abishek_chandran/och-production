package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeCriteriaVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomPayrollDateServiceStatusFetchImpl extends AbstractLocalListInquiryTran {

	@Override
	protected void associateQueryParameters(FEBATransactionContext objTrnContxt, IFEBAValueObject inOutVo, IFEBAValueObject arg2,
			QueryOperator query) throws CriticalException {

		CustomPayrollDateChangeCriteriaVO criteriaVO =(CustomPayrollDateChangeCriteriaVO) inOutVo;
		query.associate("bankId", objTrnContxt.getBankId());
		if (criteriaVO.getUserId()!=null) {
			query.associate("userId", criteriaVO.getUserId());
		}
		if ( criteriaVO.getFromDate()!=null) {
			query.associate("fromTime", criteriaVO.getFromDate());
		}
		if (criteriaVO.getToDate()!=null) {
			query.associate("toTime", criteriaVO.getToDate());
		}
		
	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return null;
	}
    public String getQueryIdentifier(FEBATransactionContext febatransactioncontext, IFEBAValueObject ifebavalueobject, IFEBAValueObject ifebavalueobject1)
    {
        return "CustomPayrollDateChangeStatusDAL";
    }

}



package com.infosys.custom.ebanking.user.service;

import java.util.Date;

import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomOCRUploadVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;


/**
 * This service is used for ocr upload and getting the data..

 */

public class CustomOCRServiceUploadAndDataImpl
extends
AbstractHostUpdateTran {


	public FEBAValItem[] prepareValidationsList(

			FEBATransactionContext objContext,

			IFEBAValueObject objInputOutput,

			IFEBAValueObject objTxnWM)

					throws BusinessException,

					BusinessConfirmation,

					CriticalException {

		return new FEBAValItem[0];

	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	public void processHostData(

			FEBATransactionContext objContext,

			IFEBAValueObject objInputOutput,

			IFEBAValueObject objTxnWM)

					throws BusinessException,

					BusinessConfirmation,

					CriticalException {    	    	
		CustomOCRUploadVO ocrUploadVO = (CustomOCRUploadVO) objInputOutput;
		validateMandatory(objContext, ocrUploadVO);
		try {

			String noOfRetries = PropertyUtil.getProperty("OCR_DATA_FETCH_NO_RETRIES", objContext);
			String inProgressError = PropertyUtil.getProperty("OCR_DATA_FETCH_IN_PROGRESS_ERROR", objContext);
			String stringTimeout = PropertyUtil.getProperty("OCR_DATA_FETCH_TIMEOUT_BEFORE_RETRY", objContext);
			int timeout = Integer.parseInt(stringTimeout);
			int loopVar = Integer.parseInt(noOfRetries);

			EBHostInvoker.processRequest(
					objContext,
					CustomEBRequestConstants.CUSTOM_OCR_UPLOAD_REQUEST,
					ocrUploadVO);
			do{
				try {
		        	 System.out.println("Sleep Time Recording..." + new Date());
		        	 System.out.println("timeout- "+timeout);
					Thread.sleep(timeout);
					System.out.println("Sleep Time Recording..." + new Date());
				} catch (InterruptedException iex) {
					iex.printStackTrace();
				}
				EBHostInvoker.processRequest(
						objContext,
						CustomEBRequestConstants.CUSTOM_OCR_DATA_REQUEST,
						ocrUploadVO);
				System.out.println("ocrUploadVO - "+ocrUploadVO);
				System.out.println("ocrUploadVO.getErrorRemarks().getValue() - "+ocrUploadVO.getErrorRemarks().getValue());
				if(ocrUploadVO.getErrorRemarks().getValue().equals(inProgressError)){
					System.out.println("loopVar - " + loopVar);
					--loopVar;
				}else{
					System.out.println("break - ");
					break;
				}
			}while(loopVar>0);

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}
	private void validateMandatory(FEBATransactionContext ebContext, CustomOCRUploadVO ocrUploadVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		if (!FEBATypesUtility.isNotNullOrBlank(ocrUploadVO.getFile())) {
			valError.add(getBusinessExceptionVO(ebContext,"File is mandatory"));
		}


		if (!(valError.isEmpty())) {
			throw new BusinessException(ebContext, valError);
		}
	}
	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

}




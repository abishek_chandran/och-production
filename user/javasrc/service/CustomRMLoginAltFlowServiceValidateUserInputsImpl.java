package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.interceptor.xservice.XServiceConstants;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBARecordUserId;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.SequenceID;
import com.infosys.feba.framework.types.primitives.Status;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.UserPrincipal;
import com.infosys.feba.framework.types.primitives.UserType;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.BlobArrayObjectConversionUtil;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.LAFRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.DBApplicationId;
import com.infosys.fentbase.types.primitives.Remarks;
import com.infosys.fentbase.types.primitives.UserSignOnStatusFlag;
import com.infosys.fentbase.types.valueobjects.ForgotPasswordOfflineVO;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.user.LoginAltFlowConstants;
import com.infosys.fentbase.user.service.LoginAltFlowNavigationUtility;

/**
 *
 * @author renita_pinto
 * @since 20127:27:16 PM
 */
public class CustomRMLoginAltFlowServiceValidateUserInputsImpl extends AbstractLocalUpdateTran {

	@SuppressWarnings("rawtypes")
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

		final ILoginAltFlowVO loginAltFlowVO = (ILoginAltFlowVO) objInputOutput;

		final FBATransactionContext ebContext = (FBATransactionContext) objContext;
		UserId userId = ebContext.getUserId();
		CorporateId orgId = ebContext.getOrgId();
		UserPrincipal principalId = null;

		if (!loginAltFlowVO.getForgotPasswordOfflineVO().getRequestType().toString()
				.equals(LoginAltFlowConstants.FORGOT_PWD_UX3_ONLINE_REQUEST)) {
			MobileNumber phoneNo = loginAltFlowVO.getForgotPasswordOfflineVO().getMobileNumber();

			userId = new UserId(phoneNo.toString());
			orgId = new CorporateId(phoneNo.toString());
			principalId = new UserPrincipal(phoneNo.toString());

			try {
				QueryOperator pQueryOperator = QueryOperator.openHandle(ebContext,
						CustomEBQueryIdentifiers.CUSTOM_PRINCIPAL_ID_INQUIRY);
				pQueryOperator.associate("bankId", ebContext.getBankId());
				pQueryOperator.associate("principalId", principalId);
				FEBAArrayList principalIdList = pQueryOperator.fetchList(ebContext);
				if (principalIdList.size() > 0) {
					throw new BusinessException(ebContext, EBIncidenceCodes.MOBILE_NUMBER_ALREADY_REGISTERED,
							"The mobile number is already registered.",
							EBankingErrorCodes.MOBILE_NUMBER_ALREADY_REGISTERED);
				}

			} catch (DALException ex) {
				if (ex.getErrorCode() != ErrorCodes.NO_RECORD_FOUND_IN_DAL) {
					throw new CriticalException(ebContext, EBIncidenceCodes.RECORD_NOT_FOUND_ERROR_IN_GRPM,
							"Record not found", EBankingErrorCodes.RECORD_NOT_FOUND, ex);
				}

			}
			CUSRInfo cusrInfo = null;
			try {
				cusrInfo = CUSRTAO.select(objContext, objContext
						.getBankId(), orgId, userId);
			} catch (FEBATableOperatorException e) {
				e.printStackTrace();
			}
			if(null!=cusrInfo){
				throw new BusinessException(true, objContext, "User Mobile Number cannot be registered.", "error", null,
						CustomEBankingErrorCodes.USER_NUM_CANNOT_BE_REGISTERED, null);
			}
	
		}

		final FEBARecordUserId actualRecordUserID = new FEBARecordUserId(userId.toString().toUpperCase()
				+ EBankingConstants.USER_ID_DELIMITER + userId.toString().toUpperCase());

		ebContext.setUserId(userId);
		ebContext.setCorpId(orgId);
		ebContext.setRecordUserId(actualRecordUserID);
		ebContext.setActualUserId(actualRecordUserID);
		// 11010 user id case sensitive enhancement
		ebContext.setUserPrincipal(new UserPrincipal(userId.toString()));
		ebContext.setDBApplicationId(new DBApplicationId(userId.toString()));
		ebContext.setUserType(new UserType(EBankingConstants.STRING_RETAIL_USER));
		// setting status in LGIN to V if user principal is valid
		loginAltFlowVO.getLoginAltFlowNavigationVO()
				.setForgotPwdStatusFlag(new UserSignOnStatusFlag(FBAConstants.FORGOT_PWD_STATUS_USER_VALIDATED_CHAR));

		ForgotPasswordOfflineVO frtPwdOfflineVO = loginAltFlowVO.getForgotPasswordOfflineVO();

		SequenceID sequenceNo = new SequenceID(
				(SequenceGeneratorUtil.getNextSequenceNumber(LoginAltFlowConstants.FP_REQID_SEQNUM, ebContext)));

		ebContext.setRequestId(new DBApplicationId(sequenceNo.toString()));

		Object obj = frtPwdOfflineVO.getFpSectionVO();
		byte[] toBytes = BlobArrayObjectConversionUtil.toBytes(obj);
		FEBABinary bin = new FEBABinary();
		bin.setValue(toBytes);
		final LAFRTAO lafrTAO = new LAFRTAO(ebContext);

		lafrTAO.associateBankId(ebContext.getBankId());
		lafrTAO.associateRemarks(new Remarks(FBAConstants.BLANK));
		lafrTAO.associateRequestDate(new FEBADate(DateUtil.getCurrentDate(ebContext)));
		lafrTAO.associateRequestDetails(bin);
		lafrTAO.associateRequestId(new DBApplicationId(sequenceNo.toString()));
		lafrTAO.associateRequestType(ebContext.getRequestType());
		lafrTAO.associateRequestStatus(new Status(LoginAltFlowConstants.REQUEST_STATUS_SAVED));
		lafrTAO.associateUserPrincipal(new UserPrincipal(userId.toString()));
		if (loginAltFlowVO.getIsServiceExRequest().equals(FBAConstants.YNFLAG_Y)) {
			lafrTAO.associateRequestStatus(new Status(LoginAltFlowConstants.RM_APPROVED));
		}

		try {
			lafrTAO.insert(ebContext);
			if (loginAltFlowVO.getIsServiceExRequest().equals(FBAConstants.YNFLAG_Y)) {
				frtPwdOfflineVO.setRequestID(sequenceNo.toString());
			}

		} catch (FEBATableOperatorException toe) {
			throw new CriticalException(ebContext, FBAIncidenceCodes.INSERT_INTO_FPDT_FAILED,
					"Could not insert into LAFR", FBAErrorCodes.RECORD_INSERTION_FAILED);

		}
		if (ebContext.getContextMap().containsKey(XServiceConstants.XSERVICEREQUEST) && ebContext.getContextMap()
				.get(XServiceConstants.XSERVICEREQUEST).toString().equals(FBAConstants.YES)) {
			ebContext.setUserId(ebContext.getDefaultRelationshipUserId());
			ebContext.setCorpId(ebContext.getDefaultRelationshipCorpId());
		}
		LoginAltFlowNavigationUtility.getInstance().createApplicationSessionEntry(loginAltFlowVO, ebContext);

	}

}

package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomSchemeCodeWLEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomSchemeCodeWLServiceFetchImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {		
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		System.out.println("CustomSchemeCodeWLServiceFetchImpl - calling CustomWhiteListSchmCodesFetchRequest");
		CustomSchemeCodeWLEnquiryVO enquiryVO = (CustomSchemeCodeWLEnquiryVO) objInputOutput;	    
	    	try {
	    		EBHostInvoker.processRequest(objContext, "CustomWhiteListSchmCodesFetchRequest",
						enquiryVO);
	    	   }catch (BusinessException be) {
	    			    if(be.getErrorCode() == 211051){
	    			    	
	    			    	throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
	    							"No partner data found", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
	    			    	
	    			    }else{
	    			    	throw new BusinessException(objContext, "LNINQ003",
		    						"An unexpected exception occurred during white list call.",
		    						110882, be);
		    			    }
	    			    }
	    				
	    	if(enquiryVO.getResultList().size()==0)
	    	{
	    		throw new BusinessException(objContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
						"No Scheme codes found for the partner ID from white list", EBankingErrorCodes.NO_RECORDS_FOUND);	    		
	    	}
	    	
	    	int respCode=Integer.valueOf(enquiryVO.getDetails().getRespCode().getValue());
			if (respCode == 102) {
				throw new BusinessException(objContext, EBIncidenceCodes.RETAIL_FIM_RECORD_INSERTION_FAILED_IN_EAUSR,
						"invalid request: missing partnerId", EBankingErrorCodes.UNABLE_TO_PROCESS_REQUEST);
			}
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To write some code		
	}

}

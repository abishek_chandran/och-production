package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CommonCodeType;
import com.infosys.feba.framework.types.primitives.DescriptionMedium;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.COCDTAO;
import com.infosys.fentbase.tao.info.COCDInfo;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.user.CUSRTableUtility;

public class CustomCAMSCallServiceFetchAccountNumberImpl extends AbstractHostInquiryTran {

	DescriptionMedium expDateCheckFlag = null;
	String cardStatusAvailable = "AA";

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomCAMSCallEnquiryVO enquiryVO = (CustomCAMSCallEnquiryVO) objInputOutput;

		EBHostInvoker.processRequest(context, CustomEBRequestConstants.CAMS_INQUIRY, enquiryVO);

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomCAMSCallEnquiryVO enquiryVO = (CustomCAMSCallEnquiryVO) objInputOutput;
		CustomCAMSCallCriteriaVO criteriaVO = enquiryVO.getCriteria();
		CustomCAMSCallDetailsVO detailsVO = enquiryVO.getDetails();
		String critExpDate = criteriaVO.getExpiryDate().toString();
		String detExpDate = detailsVO.getExpiryDate().toString();
		String reversedDate = "";
		if (critExpDate.length() > 0) {
			String splittedDate[] = critExpDate.split("/");
			if (splittedDate.length > 1) {
				reversedDate = splittedDate[1] + splittedDate[0];
			}
		}

		COCDInfo cocdInfo = null;
		try {
			cocdInfo = COCDTAO.select(objContext, objContext.getBankId(), new CommonCodeType("CABL"),
					new CommonCode(detailsVO.getCardType().toString()), objContext.getLangId());

		} catch (FEBATableOperatorException e) {
			// intentionally left blank
		}
		if (cocdInfo != null) {
			expDateCheckFlag = cocdInfo.getCdDesc();
			// expiry date validation
			if (expDateCheckFlag.toString().equals("Y") && !(detExpDate.equals(reversedDate))) {
				throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.EXPIRY_DATE_INCORRECT,
						"Expiry date mismatch.", null, CustomEBankingErrorCodes.EXPIRY_DATE_INCORRECT, null);
			}
		}

		// card status validation
		FEBAUnboundString cardStatus = detailsVO.getCardStatus();
		if (!(cardStatus.toString().equals(cardStatusAvailable))) {
			throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.CARD_NOT_ACTIVE,
					"Card is not active.", null, CustomEBankingErrorCodes.CARD_NOT_ACTIVE, null);
		}
		if(criteriaVO.getIsKTPCheckRequired().getValue()=='Y'){
			CUSRInfo cusrInfo = CUSRTableUtility.getInstance().getCUSRInfo(objContext);
			if (!(criteriaVO.getKtp().getValue().equalsIgnoreCase(cusrInfo.getPanNationalId().getValue()))) {
				throw new BusinessException(true, objContext, CustomEBankingIncidenceCodes.KTP_NUMBER_MISMATCH,
						"KTP Number mismatch.", null, CustomEBankingErrorCodes.KTP_NUMBER_MISMATCH, null);
			}
		}

	}

}

package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanCallEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostInquiryTran;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomLoanListServiceFetchImpl extends AbstractHostInquiryTran {
		
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {					
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext febaContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext context = (EBTransactionContext) febaContext;
		CustomLoanCallEnquiryVO enquiryVO = (CustomLoanCallEnquiryVO) objInputOutput;
		CustomLoanCallCriteriaVO criteriaVO = enquiryVO.getCriteria();

		FEBAUnboundString userId = criteriaVO.getUserId();
		
		try {
			CUSRInfo cusrInfo = CUSRTAO.select(context, context.getBankId(),new CorporateId(userId.toString()),new UserId(userId.toString()));
			if (cusrInfo == null) {
				throw new BusinessException(true, context, "LNINQ002",
						"User Id is invalid", null, 211082, null);								
			} 
			enquiryVO.getCriteria().setCifId(new FEBAUnboundString(cusrInfo.getCustId().toString()));
			EBHostInvoker.processRequest(context, "CustomLoanListInqRequest", enquiryVO);
			
		} catch (FEBATypeSystemException | FEBATableOperatorException e) {
			throw new BusinessException(true, context, "LNINQ002",
					"User Id is invalid", null, 211082, null);
		}catch (BusinessException be) {
		throw new BusinessException(context, "LNINQ003",
				"An unexpected exception occurred during loan list retrieval",
				211082, be);
	    }catch (Exception e) {
			throw new BusinessException(context, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
					EBankingErrorCodes.HOST_NOT_AVAILABLE);
		}
		if(enquiryVO.getResultList().size()==0)
		{
			throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, 
					"No Account Linked to user", EBankingErrorCodes.NO_RECORDS_FOUND);		
		}
	}
	

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// To Do Code
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject bjTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {	
		// To Do Code
	}

}

package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CNLDTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailBatchDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.transaction.util.ValidationsUtil;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomBatchEmailNotificationMaintenanceServiceSendEmailImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		
			EBTransactionContext context = (EBTransactionContext) arg0;
			CustomEmailBatchDetailsVO emailBatchDetailsVO = (CustomEmailBatchDetailsVO) arg1;
			try {
				if (null != emailBatchDetailsVO) {
					/* Validate Mandatory Fields */
					final FEBAValItem[] valitems = new FEBAValItem[] {
							new FEBAValItem(emailBatchDetailsVO.getDestEmail(), FEBAValEngineConstants.MANDATORY,
									FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.TO_ADDRESS_MANDATORY),
							new FEBAValItem(emailBatchDetailsVO.getEmailSubject(), FEBAValEngineConstants.MANDATORY,
									FEBAValEngineConstants.INDEPENDENT, CustomEBankingErrorCodes.SUBJECT_MANDATORY),
							new FEBAValItem(emailBatchDetailsVO.getShortMessage(), FEBAValEngineConstants.MANDATORY,
									FEBAValEngineConstants.INDEPENDENT,
									CustomEBankingErrorCodes.MESSAGE_CONTENT_MANDATORY) };
					ValidationsUtil.callValEngine(context, emailBatchDetailsVO, valitems);
	
					if (null != emailBatchDetailsVO) {
						emailBatchDetailsVO.setMessageContent(emailBatchDetailsVO.getShortMessage().getValue());
					}
					// Recording Email entry for mobile APP notification.
					/*
					 * Creating App notification i.e. DB Entry in CNLD
					 */
					EBHostInvoker.processRequest(context, CustomEBRequestConstants.CUSTOM_BATCH_AGRO_SEND_EMAIL_REQUEST,
							emailBatchDetailsVO);
					
					if(null !=emailBatchDetailsVO && emailBatchDetailsVO.getStatusCode().getValue().equalsIgnoreCase("00")){
						inserCNLD(context, emailBatchDetailsVO);
					}
				}

		} catch (CriticalException ce) {
			ce.printStackTrace();
			Logger.logError("Exception e" + ce);
		}
	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

	}

	private void inserCNLD(EBTransactionContext ebContext, CustomEmailBatchDetailsVO emailBatchDetailsVO)
			throws BusinessException {

		CNLDTAO cnldTAO = new CNLDTAO(ebContext);
		try {
			cnldTAO.associateBankId(ebContext.getBankId());
			cnldTAO.associateUserId(new UserId(emailBatchDetailsVO.getUserId().getValue()));
			cnldTAO.associateNotificationId(new FEBAUnboundLong(SequenceGeneratorUtil
					.getNextSequenceNumber(CustomEBConstants.NEXT_APP_NOTIFICATION_ID_SEQ, ebContext)));
			cnldTAO.associateAppNotifStatus(emailBatchDetailsVO.getApplicationStatus());
			cnldTAO.associateAppNotifType(emailBatchDetailsVO.getNotificationType());
			cnldTAO.associateMessageHeader(new FEBAUnboundString(emailBatchDetailsVO.getEmailSubject().getValue()));
			cnldTAO.associateShortMessage(emailBatchDetailsVO.getShortMessage());
			cnldTAO.associateIsNew(new YNFlag("Y"));
			cnldTAO.associateFreeText1(new FEBAUnboundString());
			cnldTAO.associateFreeText2(new FEBAUnboundString());
			cnldTAO.associateFreeText3(new FEBAUnboundString());

			cnldTAO.insert(ebContext);

		} catch (FEBATableOperatorException ftoe) {
			ftoe.printStackTrace();
			//throw new BusinessException(ebContext, "Record does not exist.", ftoe.getMessage(), ftoe.getErrorCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

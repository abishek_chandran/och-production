package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLPATAO;
import com.infosys.custom.ebanking.tao.CPCTTAO;
import com.infosys.custom.ebanking.tao.info.CLPAInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomPayrollDateServiceChangeImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return null;
	}

	@Override
	protected void processHostData(FEBATransactionContext objTrnCtx, IFEBAValueObject objInOut, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomPayrollDateChangeVO inOutVO = (CustomPayrollDateChangeVO) objInOut;
		CLPAInfo clpaInfo;
		try {
			clpaInfo = CLPATAO.select(objTrnCtx, objTrnCtx.getBankId(), inOutVO.getApplicationId());
			inOutVO.setOldPayrollDate(clpaInfo.getAccPayrollDate());
		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		}
		System.out.println("inOutVO.toDebugString()-->"+inOutVO.toDebugString());
			EBHostInvoker.processRequest(objTrnCtx, "CustomPayrollDateChangeRequest", inOutVO);

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub

	}

	@Override
	public void postProcess(FEBATransactionContext objTrnCtx, IFEBAValueObject objInOut, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomPayrollDateChangeVO inOutVO = (CustomPayrollDateChangeVO) objInOut;

		try {
//			insertCPCTTable(objTrnCtx, inOutVO);
			updateCLPA(objTrnCtx, inOutVO);
		} catch (FEBATableOperatorException e) {
			 throw new FatalException(objTrnCtx, "Record could not be updated", "USERTX0016");
		}
	}

	public void updateCLPA(FEBATransactionContext objTrnCtx, CustomPayrollDateChangeVO inOutVO)
			throws FEBATableOperatorException {
		if (!inOutVO.getStatus().getValue().equals("SUCCESS")) {
			return;
		}
		CLPAInfo clpaInfo = CLPATAO.select(objTrnCtx, objTrnCtx.getBankId(), inOutVO.getApplicationId());
		CLPATAO clpatao = new CLPATAO(objTrnCtx);
		clpatao.associateBankId(clpaInfo.getBankId());
		clpatao.associateApplicationId(inOutVO.getApplicationId());
		clpatao.associateAccPayrollDate(inOutVO.getNewPayrollDate());
		clpatao.associateCookie(clpaInfo.getCookie());
		clpatao.update(objTrnCtx);

	}

	public void insertCPCTTable(FEBATransactionContext objTrnCtx, CustomPayrollDateChangeVO inOutVO)
			throws FEBATableOperatorException {
		CPCTTAO cpcttao = new CPCTTAO(objTrnCtx);
		cpcttao.associateApplicationId(inOutVO.getApplicationId());
		cpcttao.associateBankId(objTrnCtx.getBankId());
		cpcttao.associateOldpayrolldate(inOutVO.getOldPayrollDate());
		cpcttao.associateNewpayrolldate(inOutVO.getNewPayrollDate());
		cpcttao.associateUserid(inOutVO.getUserId());
		cpcttao.associateBatchreferenceid(new FEBAUnboundLong(SequenceGeneratorUtil.getNextSequenceNumber("NXT_PAY_DATE_CHANGE_SEQ", objTrnCtx)));
		cpcttao.associateStatus(inOutVO.getStatus());
		cpcttao.insert(objTrnCtx);
	}

}

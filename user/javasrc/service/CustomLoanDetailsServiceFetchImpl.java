
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanDetailResultVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

/**
 * This service is used for fetching document text
 * 
 */

public class CustomLoanDetailsServiceFetchImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext context, IFEBAValueObject inOutVO,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		//CustomDocTextMaintVO docTextVO = (CustomDocTextMaintVO) inOutVO;
		return null;
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject inOutVO, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanDetailEnquiryVO docTextVO = (CustomLoanDetailEnquiryVO) inOutVO;
		docTextVO.getResults().setList(fetchDocumentText(context));
	}

	private FEBAArrayList<CustomLoanDetailResultVO> fetchDocumentText(FEBATransactionContext context) {
		QueryOperator queryOperator = QueryOperator.openHandle(context,
				CustomEBQueryIdentifiers.CUSTOM_LOAN_DETAILS_FETCH);
		queryOperator.associate("bankId", context.getBankId());
		FEBAArrayList<CustomLoanDetailResultVO> list = new FEBAArrayList<CustomLoanDetailResultVO>();
		try {
			list = queryOperator.fetchList(context);
		} catch (DALException e) {
			e.printStackTrace();
		}
		queryOperator.closeHandle(context);
		return list;
	}

}

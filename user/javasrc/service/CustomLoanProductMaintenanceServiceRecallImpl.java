
package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.applicationmaintenance.validators.CustomLoanMinAmtVal;
import com.infosys.custom.ebanking.contentmanagement.util.CustomContentMgmtUtil;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.custom.ebanking.user.util.CustomLoanProductMaintenancePopulateWfUtil;
import com.infosys.custom.ebanking.user.validator.CustomLoanProductValidator;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;

public class CustomLoanProductMaintenanceServiceRecallImpl extends AbstractLocalUpdateTran {

	@Override
	public void process(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
			throws BusinessException, BusinessConfirmation, CriticalException {

		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
		FBATransactionContext objContext = (FBATransactionContext) pObjContext;
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;
		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(MaintenanceReqdUtility.getMaintenanceReqd(pObjContext));
		criteria.setDependencyFlag(FBAConstants.NO);

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLPM(pObjContext, customLoanProductDetailsVO,
					FEBATMOConstants.TMO_OPERATION_CANCEL);

			criteria.setDependencyFlag(FBAConstants.YES);

		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_CANCEL + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.USER_RECALL_TMO_EXCEPTION, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_RECALL, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}
		objContext.addBusinessInfo(
				CustomContentMgmtUtil.getSuccessMessage(ebContext, EBankingErrorCodes.REQUEST_SUCESSFULLY_RECALLED));
	}

	/**
	 * Method populateWorkflowVO() Description Populates workflow value object
	 * from context and InputOutputVO Input objContext, workflowVO,
	 * objInputOutput, objTxnWM
	 *
	 * @param objContext
	 * @param populateWorkflowVOUtility
	 * @param objInputOutput
	 * @param objTxnWM
	 */
	@Override
	protected void populateWorkflowVO(FEBATransactionContext objContext, IPopulateWorkflowVOUtility wfVOPopulator,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		CustomLoanProductMaintenancePopulateWfUtil.populateWorkflowVO(objContext, wfVOPopulator, objInputOutput,
				EBankingConstants.RECALL);
	}

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem(customLoanProductDetailsVO, new CustomLoanProductValidator(),
						FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem(customLoanProductDetailsVO.getMinAmount(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.ENT_MIN_BALANCE),

				new FEBAValItem(customLoanProductDetailsVO.getMaxAmount(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.ENT_MAX_BALANCE),

				new FEBAValItem(customLoanProductDetailsVO, new CustomLoanMinAmtVal(),
						FEBAValEngineConstants.INDEPENDENT),

		};

	}
}

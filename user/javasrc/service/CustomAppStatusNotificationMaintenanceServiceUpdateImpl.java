package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CNLDTAO;
import com.infosys.custom.ebanking.tao.info.CNLDInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomAppStatusNotificationDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;

public class CustomAppStatusNotificationMaintenanceServiceUpdateImpl extends AbstractLocalUpdateTran{

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext context, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomAppStatusNotificationDetailsVO inOutVO = (CustomAppStatusNotificationDetailsVO)arg1;

		try {
			FEBATMOCriteria criteria = new FEBATMOCriteria();
			
			criteria.setMaintenanceRequired(MaintenanceReqdUtility
					.getMaintenanceReqd(context));
			UserId userId = context.getUserId();
			BankId bankId = context.getBankId();
			FEBAUnboundLong notificationId = inOutVO.getNotificationId();
			CNLDInfo  CNLDInfo= null;
			CNLDTAO cnldTAO = new CNLDTAO(context);
			cnldTAO.associateBankId(bankId);
			cnldTAO.associateUserId(userId);
			cnldTAO.associateNotificationId(notificationId);
			cnldTAO.associateIsNew(new YNFlag("N"));
			CNLDInfo = cnldTAO.select(context, bankId, userId, notificationId);
			
			cnldTAO.associateCookie(CNLDInfo.getCookie());
			
			cnldTAO.update(context);
			
		} catch (FEBATableOperatorException e) {
			
			throw new BusinessException(context,
					"Record does not exist.",
					e.getMessage(),
					e.getErrorCode());
		
		} 
		
	} 

}



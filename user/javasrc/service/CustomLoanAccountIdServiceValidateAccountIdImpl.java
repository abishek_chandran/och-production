package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanHistoryCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanHistoryEnquiryVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanAccountIdServiceValidateAccountIdImpl extends AbstractLocalListInquiryTran {

    public CustomLoanAccountIdServiceValidateAccountIdImpl()
    {
    }

    protected void associateQueryParameters(FEBATransactionContext txnContext, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2, QueryOperator queryOperator)
        throws CriticalException
    {
        EBTransactionContext ebcontext = (EBTransactionContext)txnContext;
        CustomLoanHistoryCriteriaVO criteriaVO = (CustomLoanHistoryCriteriaVO)objQueryCrit;
        queryOperator.associate("bankId", ebcontext.getBankId());
        queryOperator.associate("userId", criteriaVO.getUserId());
        queryOperator.associate("accountID", criteriaVO.getAccountID());

        
    }

    public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
        throws BusinessException, BusinessConfirmation, CriticalException
    {
        return new FEBAValItem[0];
    }

    public String getQueryIdentifier(FEBATransactionContext pObjContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
    {
    	
    	System.out.println("Calling DAL::::::::::::::::::::::::");
        String queryIdentifier = "CustomValidateLoanAccountIdDAL";
        return queryIdentifier;
    }

    protected void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
        throws BusinessException, BusinessConfirmation, CriticalException
    {
    	CustomLoanHistoryEnquiryVO enquiryVO = (CustomLoanHistoryEnquiryVO)objInputOutput;
        try
        {
            super.executeQuery(objContext, enquiryVO, objTxnWM);
            
    		
    		System.out.println("AccountId match testing::::::::::::::::::::::::");
    		
    		System.out.println("HistoryList::::::::::::::::::::::::"+enquiryVO.getResultList());
            
        	if(enquiryVO.getResultList().size() == 0){
        		//throw business exception in the resource file.

        		enquiryVO.getDetails().setIsLoanAccountIdMatch("N");
        	}else{
        		enquiryVO.getDetails().setIsLoanAccountIdMatch("Y");
        	}
        }
        catch(BusinessException be)
        {
        	be.printStackTrace();
        	enquiryVO.getDetails().setIsLoanAccountIdMatch("N");

        }
    }

}

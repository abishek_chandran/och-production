package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomOfflineKTPFetchServiceRetrievalImpl extends AbstractLocalInquiryTran {

	
	static String exception = "Exception";
	
	@SuppressWarnings("rawtypes")
	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanApplicationDetailsVO detailsVO = (CustomLoanApplicationDetailsVO) objInputOutput;
				
		populateDocumentDetails(txnContext, detailsVO); 
		
	}
	
	private static void populateDocumentDetails(FEBATransactionContext objContext,
			CustomLoanApplicationDetailsVO detailsVO) {
				
		FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> docList = null;
		QueryOperator queryOperator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_DOCUMENT_LIST);
		
		queryOperator.associate("bankId", ((EBTransactionContext) objContext).getBankId());
		queryOperator.associate("applicationId", detailsVO.getLoanApplicantDetails().getApplicationId());
		queryOperator.associate("docType", new DocumentType("KTP"));
				
		try {
				docList = queryOperator.fetchList(objContext);
			    CustomLoanApplnDocumentsDetailsVO docDetVO = docList.get(0);
			    detailsVO.setLoanDocumentDetails(docDetVO);
								
		} catch (DALException e) {
			Logger.logError("Exception e" + e);
		} 
		finally {
			// Call the closeHandle method which Closes the handle
			queryOperator.closeHandle((EBTransactionContext) objContext);
		}
		
	}

}

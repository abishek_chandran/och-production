package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.tao.CNLDTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomBatchPopupNotificationInOutVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.SequenceGeneratorUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomBatchPopUpNotificationMaintenanceServiceSendPopUpImpl extends AbstractHostUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	protected void processHostData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext context = (EBTransactionContext) arg0;
		CustomBatchPopupNotificationInOutVO popUpBatchDetailsVO = (CustomBatchPopupNotificationInOutVO) arg1;
		System.out
				.println("Inside CustomBatchPopUpNotificationMaintenanceServicesendPopUpImpl:: ");
		try {
			if (null != popUpBatchDetailsVO) {
				// Recording POPUP entry for application notification.
				/*
				 * Creating App notification i.e. DB Entry in CNLD
				 */
				EBHostInvoker.processRequest(context,
						CustomEBRequestConstants.FIREBASE_PUSH_NOTIFICATION_APIGEE_REQUEST, popUpBatchDetailsVO);

				if (FEBATypesUtility.isNotNullOrBlank(popUpBatchDetailsVO.getCode())) {
					System.out.println("Notification Delivered - ");
					inserCNLD(context, popUpBatchDetailsVO);
				} else {
					System.out.println("Notification delivery Failed - ");
				}
			}

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}

	@Override
	protected void processLocalData(FEBATransactionContext arg0, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

	}

	private void inserCNLD(EBTransactionContext ebContext, CustomBatchPopupNotificationInOutVO popUpBatchDetailsVO)
			throws BusinessException {

		CNLDTAO cnldTAO = new CNLDTAO(ebContext);
		try {
			cnldTAO.associateBankId(ebContext.getBankId());
			cnldTAO.associateUserId(new UserId(popUpBatchDetailsVO.getUserId().getValue()));
			cnldTAO.associateNotificationId(new FEBAUnboundLong(SequenceGeneratorUtil
					.getNextSequenceNumber(CustomEBConstants.NEXT_APP_NOTIFICATION_ID_SEQ, ebContext)));
			cnldTAO.associateAppNotifStatus(popUpBatchDetailsVO.getApplicationStatus());
			cnldTAO.associateAppNotifType(popUpBatchDetailsVO.getNotificationType());
			cnldTAO.associateMessageHeader(new FEBAUnboundString(popUpBatchDetailsVO.getMessageHeader().getValue()));
			cnldTAO.associateShortMessage(popUpBatchDetailsVO.getShortMessage());
			cnldTAO.associateIsNew(new YNFlag("Y"));
			cnldTAO.associateFreeText1(new FEBAUnboundString());
			cnldTAO.associateFreeText2(new FEBAUnboundString());
			cnldTAO.associateFreeText3(new FEBAUnboundString());
			cnldTAO.insert(ebContext);

		} catch (FEBATableOperatorException ftoe) {
			ftoe.printStackTrace();
			//throw new BusinessException(ebContext, "Record does not exist.", ftoe.getMessage(), ftoe.getErrorCode());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

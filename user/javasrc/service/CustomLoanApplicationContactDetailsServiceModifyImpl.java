package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.tao.CLCDTAO;
import com.infosys.custom.ebanking.tao.info.CLCDInfo;

import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnContactDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;

import com.infosys.ebanking.common.validators.PhoneNumberPatternVal;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;

import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;

public class CustomLoanApplicationContactDetailsServiceModifyImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanApplnContactDetailsVO contactDetVO = (CustomLoanApplnContactDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem("emergencyContactPhoneNum", contactDetVO.getEmergencyContactPhoneNum(),
						new PhoneNumberPatternVal(), FEBAValEngineConstants.NON_MANDATORY,
						FEBAValEngineConstants.INDEPENDENT),

		};

	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		EBTransactionContext ebContext = (EBTransactionContext) txnContext;
		CustomLoanApplnContactDetailsVO loanApplContactDetlsVO = (CustomLoanApplnContactDetailsVO) objInputOutput;
		final CLCDTAO clcdTAO = new CLCDTAO(ebContext);
		CLCDInfo clcdInfo = null;
		try {
			clcdTAO.associateBankId(ebContext.getBankId());
			clcdTAO.associateApplicationId(loanApplContactDetlsVO.getApplicationId());
			clcdTAO.associateMaritalStatus(loanApplContactDetlsVO.getMaritalStatus());
			clcdTAO.associateSpouseId(loanApplContactDetlsVO.getSpouseId());
            clcdTAO.associatePartnernikid(loanApplContactDetlsVO.getPartnerNikId());
            clcdTAO.associatePartnername(loanApplContactDetlsVO.getPartnerName());
			clcdTAO.associateNoOfDependents(loanApplContactDetlsVO.getNoOfDependents());
			clcdTAO.associateMotherName(loanApplContactDetlsVO.getMotherName());
			clcdTAO.associateEmergencyContact(loanApplContactDetlsVO.getEmergencyContact());
			clcdTAO.associateEmrConAddress(loanApplContactDetlsVO.getEmergencyContactAddress());
			clcdTAO.associateEmrConRelation(loanApplContactDetlsVO.getEmergencyContactRelation());
			clcdTAO.associateEmrConPhoneNum(loanApplContactDetlsVO.getEmergencyContactPhoneNum());
			clcdTAO.associateEmrConPostalCd(loanApplContactDetlsVO.getEmergencyContactPostalCode());

			clcdInfo = CLCDTAO.select(ebContext, ebContext.getBankId(), loanApplContactDetlsVO.getApplicationId());
			clcdTAO.associateCookie(clcdInfo.getCookie());

			clcdTAO.update(ebContext);

		} catch (FEBATableOperatorException e) {

			throw new FatalException(ebContext, "Record could not be updated",
					FBAIncidenceCodes.USER_RECORD_COULD_NOT_BE_UPDATED);
		}

	}

}

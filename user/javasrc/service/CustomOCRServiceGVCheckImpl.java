

package com.infosys.custom.ebanking.user.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.infosys.custom.ebanking.hif.CustomEBRequestConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomOCRUploadVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.transaction.pattern.AbstractHostUpdateTran;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.feba.utils.insulate.ArrayList;


/**
 * This service is used for ocr upload and getting the data..

 */

public class CustomOCRServiceGVCheckImpl
extends
AbstractHostUpdateTran {


	public FEBAValItem[] prepareValidationsList(

			FEBATransactionContext objContext,

			IFEBAValueObject objInputOutput,

			IFEBAValueObject objTxnWM)

					throws BusinessException,

					BusinessConfirmation,

					CriticalException {

		return new FEBAValItem[0];

	}

	/**
	 *
	 * This method is used to process local data
	 *
	 */

	protected void processLocalData(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

	}

	/**
	 * This method is used to make the HOST calls
	 *
	 */

	public void processHostData(

			FEBATransactionContext objContext,

			IFEBAValueObject objInputOutput,

			IFEBAValueObject objTxnWM)

					throws BusinessException,

					BusinessConfirmation,

					CriticalException {    	    	
		CustomOCRUploadVO ocrUploadVO = (CustomOCRUploadVO) objInputOutput;
		
		String inputNik = ocrUploadVO.getInputNik().getValue();
		
		System.out.println("inputNik::"+inputNik);
		
		validateMandatory(objContext, ocrUploadVO);
		try {
			EBHostInvoker.processRequest(
					objContext,
					CustomEBRequestConstants.CUSTOM_GOOGLE_VISION_OCR_REQUEST,
					ocrUploadVO);
			boolean nikFound = false;
			
			//FEBAArrayList<FEBAUnboundString> responseTextList = (FEBAArrayList<FEBAUnboundString>)ocrUploadVO.getResponseFieldsList();
			
			String ocrDescription = ocrUploadVO.getDescriptionOCR().getValue();
			System.out.println("ocrDescription::"+ocrDescription);
			
			if(null != ocrDescription){
					String outputNik = getNikVal(ocrDescription);
					if(outputNik != null && inputNik.equalsIgnoreCase(outputNik)){
						ocrUploadVO.setSuccess(inputNik);
						nikFound=true;
					}	
			}
			
			System.out.println("ocrUploadVO - "+ocrUploadVO);
			if(!nikFound){
				ocrUploadVO.setSuccess("");
			}
			
			System.out.println("ocrUploadVO NIK - "+ocrUploadVO.getSuccess().toString());
			System.out.println("ocrUploadVO - "+ocrUploadVO);
			System.out.println("ocrUploadVO.ge"
					+ "tErrorRemarks().getValue() - "+ocrUploadVO.getErrorRemarks().getValue());

		} catch (CriticalException ce) {
			Logger.logError("Exception e" + ce);
		}

	}
	private void validateMandatory(FEBATransactionContext ebContext, CustomOCRUploadVO ocrUploadVO)
			throws BusinessException {
		ArrayList<BusinessExceptionVO> valError = new ArrayList<>();
		if (!FEBATypesUtility.isNotNullOrBlank(ocrUploadVO.getFile())) {
			valError.add(getBusinessExceptionVO(ebContext,"File is mandatory"));
		}
		if (!(valError.isEmpty())) {
			throw new BusinessException(ebContext, valError);
		}
	}
	private BusinessExceptionVO getBusinessExceptionVO(FEBATransactionContext context, String msg) {
		return new BusinessExceptionVO(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}

	private BusinessException getBusinessException(FEBATransactionContext context, String errorField, String msg) {
		return new BusinessException(false, context, EBIncidenceCodes.HOST_FAILURE_RESPONSE, msg, errorField,
				EBankingErrorCodes.HOST_CP_FI_ERROR_CODE, null,
				new AdditionalParam("message", msg));
	}
	
	// This method matches 16 digit in given description and returns substring
	private String getNikVal(String descriptionOCR){

		final String number;
		
	    final Matcher m = Pattern.compile("(?<!\\d)\\d{16}(?!\\d)").matcher(descriptionOCR.toString());
	    if(m.find())
	        number = m.group(); // retrieve the matched substring
	    else
	        number = null; // no match found
	    
	    return number;
	}

}




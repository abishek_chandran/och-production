package com.infosys.custom.ebanking.user.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.applicationmaintenance.validators.CustomLoanMinAmtVal;
import com.infosys.custom.ebanking.contentmanagement.util.CustomCMMaintenanceReqdUtility;
import com.infosys.custom.ebanking.tao.CLPMConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanProductDetailsVO;
import com.infosys.custom.ebanking.user.validator.CustomLoanProductValidator;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;

public class CustomLoanProductMaintenanceServiceCreateImpl extends AbstractLocalUpdateTran {

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2)

					throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem(customLoanProductDetailsVO, new CustomLoanProductValidator(),
						FEBAValEngineConstants.INDEPENDENT),

				new FEBAValItem(customLoanProductDetailsVO.getMinAmount(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.ENT_MIN_BALANCE),

				new FEBAValItem(customLoanProductDetailsVO.getMaxAmount(), FEBAValEngineConstants.MANDATORY,
						FEBAValEngineConstants.INDEPENDENT, EBankingErrorCodes.ENT_MAX_BALANCE),

				new FEBAValItem(customLoanProductDetailsVO, new CustomLoanMinAmtVal(),
						FEBAValEngineConstants.INDEPENDENT),

		};

	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;

		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility.getMaintenanceReqd(objContext));

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLPM(objContext, customLoanProductDetailsVO,
					FEBATMOConstants.TMO_OPERATION_INSERT);

			addSuccessMessageToContext(objContext);
		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_INSERT + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.TMO_EXP_USER_CREATE, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {
			LogManager.logError(objContext, tblOperExp);
			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_CREATE, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}

	}

	@Override
	protected void populateWorkflowVO(FEBATransactionContext pObjContext,
			IPopulateWorkflowVOUtility populateWorkflowVOUtility, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		CustomLoanProductDetailsVO customLoanProductDetailsVO = (CustomLoanProductDetailsVO) objInputOutput;

		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;

		populateWorkflowVOUtility.setKeyField(CLPMConstants.LN_REC_ID,
				customLoanProductDetailsVO.getLnRecId().toString());
		populateWorkflowVOUtility.setKeyField(TxnTypeConstants.BANK_ID, ebContext.getBankId().toString());
		populateWorkflowVOUtility.setModuleKey("CLPM");
		populateWorkflowVOUtility.setEntityType("CLPM");
		populateWorkflowVOUtility.setModuleID((EBankingConstants.BANK_USER_MODULE_ID).toString());
		populateWorkflowVOUtility.setCorpId(ebContext.getCorpId().getValue());
		populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
		populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_INSERT).toString());
		populateWorkflowVOUtility.setTransactionType("LPM");
		populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
	}

	/**
	 * 
	 * Adding Success Message to the Context to be displayed on the screen
	 * 
	 * @param pObjContext
	 * @author Sahanaboggaram_n
	 * @since Feb 23, 2011 - 1:34:21 PM
	 */
	public static void addSuccessMessageToContext(FEBATransactionContext pObjContext) {
		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
		// Getting Transaction Key generated for the transaction
		final TransactionKey txnKey = ebContext.getApprovalTxnKey();
		BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		// Checking if WorkFlow is enabled
		if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(EBankingConstants.NO)) {
			infoDetails.setCode(new FEBAUnboundInt(EBankingErrorCodes.RECORD_INSERTION_SUCCESSFUL));
		} else {
			infoDetails.setCode(new FEBAUnboundInt(EBankingErrorCodes.RECORD_SENT_FOR_APPROVAL));
			infoDetails.setParam(new AdditionalParam(EBankingConstants.TRAN_KEY,
					new FEBAUnboundString(String.valueOf(txnKey.getValue()))));
		}
		infoDetails.setDispMessage("Loan Product Create Successfully.");
		infoDetails.setLogMessage("Log:Loan Product Created Successfully.");
		pObjContext.addBusinessInfo(infoDetails);
	}

}

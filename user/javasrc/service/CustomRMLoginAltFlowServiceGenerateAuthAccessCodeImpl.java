package com.infosys.custom.ebanking.user.service;

import java.time.LocalDate;
import java.util.Date;

import com.infosys.ci.common.DateUtil;
import com.infosys.custom.ebanking.tao.CROCTAO;
import com.infosys.custom.ebanking.tao.info.CROCInfo;
import com.infosys.feba.framework.common.ApplicationConfig;
import com.infosys.feba.framework.common.FEBAConstants;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.TranCommitBusinessException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundLong;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.BusinessWarningVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;
import com.infosys.fentbase.types.primitives.EmailId;
import com.infosys.fentbase.types.valueobjects.ILoginAltFlowVO;
import com.infosys.fentbase.user.service.IHostCIFValidateUserDetailsUtility;
import com.infosys.fentbase.user.service.RMUserDetailsFetchUtility;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;

public class CustomRMLoginAltFlowServiceGenerateAuthAccessCodeImpl extends
		AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException,
			BusinessConfirmation, CriticalException {
		return new FEBAValItem[] {};
	}

	@Override
	public void process(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		final ILoginAltFlowVO loginAltFlowVO = (ILoginAltFlowVO) objInputOutput;
		
		validateAndSetOtpLimit(objContext, loginAltFlowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().getUserId().getValue());
		 
		 boolean haveConatctDetails=true;
		if( loginAltFlowVO.getForgotPasswordOfflineVO().getFpFieldDetailsVO().getSectionId().toString().equalsIgnoreCase("CLGOTP"))
		{
			RMUserDetailsFetchUtility userUtil = new RMUserDetailsFetchUtility();
        	userUtil.checkUserContactDetails(objContext);
        	FBATransactionContext ebContext = (FBATransactionContext) objContext;
        	
        	FEBAArrayList bwList = ebContext.getWarningList().getWarningList();
        	if( bwList != null && bwList.size()>0 ){
        		for(int i=0; i <= bwList.size(); i++ ){
        			BusinessWarningVO bw = (BusinessWarningVO)bwList.get(i);
        			if(bw.getCode().getValue() == FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE ||
        					bw.getCode().getValue() == FBAErrorCodes.ALERTS_EMAIL_DETAILS_NOT_AVAILABLE ||
        					bw.getCode().getValue() == FBAErrorCodes.ALERTS_MOBILE_DETAILS_NOT_AVAILABLE){
        				haveConatctDetails=false;
        				ebContext.getWarningList().initialize();
        				bw.setCode(new FEBAUnboundInt(FBAErrorCodes.OTP_CONTACT_DETAILS_NOT_AVAILABLE));
        				bw.setDispMessage("The contact details do not exist. Register the details.");
        				bw.setLogMessage("The contact details do not exist. Register the details.");
        				ebContext.getBussinessInfo().initialize();
        				ebContext.addWarning(bw);
        				break;
        			}
            	}
        		}
        	if(haveConatctDetails){
        		try{
        			checkCusrInfo(loginAltFlowVO,ebContext);
        			CustomRMLoginAltFlowUtility.getInstance().generateOTP(loginAltFlowVO,
            				ebContext);
        			
        		}catch(CriticalException e){
        			if (ebContext.getFromContextData(FEBAConstants.XSERVICEREQUEST) != null
        					&& "Y".equalsIgnoreCase(ebContext.getFromContextData(
        							FEBAConstants.XSERVICEREQUEST).toString())){
        				CustomRMLoginAltFlowUtility.getInstance().generateOTP(loginAltFlowVO,
            				ebContext);
        			}
        		}
        	}			
        		
			
			if(ebContext.getContextMap().containsKey("CUSTOMER_ID")){
				
				IHostCIFValidateUserDetailsUtility hostCIFValidateUserDetails = (IHostCIFValidateUserDetailsUtility) ApplicationConfig.getImplementationInstance("HOST_CIF_VALIDATE_USER_DETAILS_UTILITY");
				hostCIFValidateUserDetails.validateHostCIF(ebContext, loginAltFlowVO);
		}
	}
	}
	public void checkCusrInfo(ILoginAltFlowVO loginAltFlowVO,
			FBATransactionContext ebContext) {
		
		CUSRInfo cusrInfo = null;
		try {
			cusrInfo = CUSRTAO.select(ebContext,ebContext.getBankId() , ebContext.getCorpId(), ebContext.getUserId());
		} catch (FEBATableOperatorException e) {
			LogManager.logError(ebContext, e);					
		}
		if(null != cusrInfo){
			loginAltFlowVO.getLoginAltFlowUserDetailsVO().setMobileNumber(cusrInfo.getCMPhoneNo());
			loginAltFlowVO.getLoginAltFlowUserDetailsVO().setEmailID(new EmailId(cusrInfo.getCEmailId().getValue()));
		}
		
	}
	
	public void validateAndSetOtpLimit(FEBATransactionContext context,String mobileNo) throws TranCommitBusinessException {

		CROCTAO crocTao = new CROCTAO(context);

		try {
			String maxOtpStr = PropertyUtil.getProperty("MAX_OTP_PER_MOBILE_NO_PER_DAY", context);
			int maxOtp=1;
			if(maxOtpStr!=null){
				maxOtp=Integer.valueOf(maxOtpStr);
			}
			CROCInfo info =null;
			try {
				info = CROCTAO.select(context, context.getBankId(),
					new FEBAUnboundString(mobileNo),new FEBADate(DateUtil.stripTimeComponent(new FEBADate(new Date()).getTimestampValue())));
			} catch (FEBATableOperatorException e) {
				LogManager.logError(context, e);					
			}

			
			if (info!=null) {
			
					if(info.getOtpCounter().isValid() && info.getOtpCounter().getValue()>=maxOtp){
								
						throw new TranCommitBusinessException(context,
								FBAIncidenceCodes.AUTHENTICATION_NOT_SUCCESSFUL,
								"Authentication not successful for the user",
								FBAErrorCodes.AUTHENTICATION_FAILED);
					}
					
				crocTao.associateMobileNo(info.getMobileNo());
				crocTao.associateBankId(context.getBankId());
				crocTao.associateCounterDate(new FEBADate(DateUtil.stripTimeComponent(new FEBADate(new Date()).getTimestampValue())));
				crocTao.associateOtpCounter(new FEBAUnboundLong(info.getOtpCounter().getValue()+1));
				crocTao.associateCookie(info.getCookie());
				crocTao.update(context);
				
				
			
			} else {
				crocTao.associateMobileNo(new FEBAUnboundString(mobileNo));
				crocTao.associateBankId(context.getBankId());
				crocTao.associateCounterDate(new FEBADate(DateUtil.stripTimeComponent(new FEBADate(new Date()).getTimestampValue())));
				crocTao.associateOtpCounter(new FEBAUnboundLong(1));
				crocTao.insert(context);
				
				
				
			}

		} catch (FEBATableOperatorException e) {
			LogManager.logError(context, e);
					
				throw new TranCommitBusinessException(context,
						FBAIncidenceCodes.AUTHENTICATION_NOT_SUCCESSFUL,
						"Authentication not successful for the user",
						FBAErrorCodes.AUTHENTICATION_FAILED);
					}
		}

		
	
	
}
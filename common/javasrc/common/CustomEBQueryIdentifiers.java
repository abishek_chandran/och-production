package com.infosys.custom.ebanking.common;

public class CustomEBQueryIdentifiers {

	private CustomEBQueryIdentifiers() {
		super();
	}

	public static final String CUSTOM_STANDARD_TEXT_MAINTENANCE = "StandardTextDetailsQuery";
	public static final String CUSTOM_LOAN_PRODUCT_MAINTENANCE = "CustomLoanProductDetailsQuery";
	public static final String CUSTOM_LOAN_TENURE_INTEREST_MAINTENANCE = "CustomLoanTenureInterestListQuery";
	// added for Loan tenure fetch list - alternative loan offering.
	public static final String CUSTOM_LOAN_TENURE_MAINTENANCE = "CustomLoanTenureListDAL";
	public static final String CUSTOM_LOAN_TENURE_DETAILS_MAINTENANCE = "CustomLoanTenureDetailsQuery";
	public static final String CUSTOM_CONTENT_MANAGEMENT_DAL = "CustomContentManagementListQuery";
	public static final String CUSTOM_LOAN_APPLICATION_INQUIRY = "CustomLoanApplicationListInquiryDAL";

	public static final String CUSTOM_DOCUMENT_TEXT_MAINTENANCE = "CustomDocumentTextListDAL";

	public static final String CUSTOM_PAYROLL_ACCOUNT_DETAILS_FETCH = "CustomPayrollAccountFetchQuery";
	public static final String CUSTOM_NOTIFICATION_EVENT_BATCH_DAL = "CustomBatchNotificationFetchListQuery";

	// added by sowjanya
	public static final String CUSTOM_FETCH_LOAN_DOCUMENT_LIST = "CustomFetchLoanDocListQuery";

	public static final String CUSTOM_DEVICE_REGISTRATION_INQUIRY = "CustomDeviceRegistrationListInquiryDAL";
	public static final String CUSTOM_USER_CHANNEL_FETCH = "CustomUserChannelsFetchDAL";

	public static final String CUSTOM_NOTIFICATION_LIST_DETAILS_FETCH = "CustomFetchNotificationListQuery";
	public static final String CUSTOM_LOAN_APPLICATION_DOCUMENT_FETCH = "CustomLoanDocumentFetch";
	public static final String CUSTOM_CUSR_INFO_DAL = "CustomCUSRInfoFetchQuery";
	public static final String CUSTOM_LOAN_DETAILS_FETCH = "CustomLoanDetailListDAL";	
	public static final String CUSTOM_FETCH_LOAN_ORIGINATION_LIST = "CustomFetchLoanOrgListDAL";
	public static final String CUSTOM_FETCH_LOAN_ORIGINATION_HISTORY_LIST = "CustomFetchLoanOrgHistoryListDAL";
	

	public static final String CUSTOM_LOAN_TXN_DETAILS_INQUIRY = "CustomLoanAccountIdFetchDAL";
	
	public static final String CUSTOM_PRINCIPAL_ID_INQUIRY = "CustomValidatePrincipalIDDAL";
	public static final String CUSTOM_FETCH_APPLICATION_STATUS_LIST = "CustomApplicationStatusFetchListQuery";
	public static final String CUSTOM_CUSR_DEDUP_CHECK_DAL = "CustomCUSRPanOrNationalIdFetchQuery";
	public static final String CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_OCR = "CustomLoanApplicationListInquiryForOCRDAL";
	public static final String CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_LOAN_PAID = "CustomLoanApplicationListInquiryForLoanPaidDAL";
	public static final String CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_BATCH = "CustomLoanApplicationListInquiryForBatchDAL";
	
	public static final String CUSTOM_APP_STATUS_NOTIFICATION_LIST_DETAILS_FETCH = "CustomFetchAppStatusNotificationListDAL";

	

}

package com.infosys.custom.ebanking.contentmanagement.service;

import java.util.Date;

import com.infosys.ci.common.LogManager;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourceConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentManagementCriteriaVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAConstants;

public class CustomContentManagementServiceFetchImpl extends AbstractLocalListInquiryTran{

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit, IFEBAValueObject arg2,
			QueryOperator queryOperator) throws CriticalException {

		CustomContentManagementCriteriaVO criteriaVO = (CustomContentManagementCriteriaVO) objQueryCrit;
		queryOperator.associate("contentType",criteriaVO.getContentType());
		queryOperator.associate("bankId", context.getBankId());
		FEBADate date = new FEBADate(new Date());
		
		queryOperator.associate("currentDate", date);
	}

	@Override
	protected final void executeQuery(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
					throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logDebug(be.getDispMessage());
		}
	}
	
	@Override
	/**
	 * 
	 * Associating DAL CustomShoppingMallStatusEnquiryDAL
	 * @see CustomShoppingMallStatusEnquiryDAL.xml
	 */
	public String getQueryIdentifier(FEBATransactionContext objContext,
			IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_CONTENT_MANAGEMENT_DAL;

	}

	public FEBAValItem[] prepareValidationsList(
			FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException,
	BusinessConfirmation, CriticalException {

		CustomContentManagementCriteriaVO criteriaVO = (CustomContentManagementCriteriaVO) objInputOutput;
		FEBAValItem[] vls=null;

		if(null==criteriaVO.getContentType().getValue()||criteriaVO.getContentType().getValue().length()==0)
		{

			throw new BusinessException(
					true,
					objContext,
					CustomEBankingIncidenceCodes.CONTENT_TYPE_MANDATORY,
					"Content Type is mandatory",
					null,
					CustomEBankingErrorCodes.CONTENT_TYPE_MANDATORY,
					null);
		}else{
			FEBAHashList resultList1 = (FEBAHashList) AppDataManager.getList(
					objContext, FBAConstants.COMMONCODE_CACHE, 
					FBAConstants.codeType+FBAConstants.EQUAL_TO+CustomResourceConstants.LOAN_CONTENT_TYPE);
			CommonCodeVO commonCodeVO=null;
			String contentType = criteriaVO.getContentType().getValue();
			Boolean isValidMerchId = false;

			for (Object obj : resultList1) {
				commonCodeVO = (CommonCodeVO) obj;
				if (commonCodeVO.getCommonCode().toString().trim()
						.equalsIgnoreCase(contentType)){
					isValidMerchId=true;
					break;
				}
			}
			if(!isValidMerchId){
				throw new BusinessException(
						true,
						objContext,
						CustomEBankingIncidenceCodes.CONTENT_TYPE_INVALID,
						"Content Type is invalid",
						null,
						CustomEBankingErrorCodes.CONTENT_TYPE_INVALID,
						null);
			}
		}

		return new FEBAValItem[] {};
	}}

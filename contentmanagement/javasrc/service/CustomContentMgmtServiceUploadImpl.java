package com.infosys.custom.ebanking.contentmanagement.service;


import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentTypeFieldsValidator;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentUploadDateValidator;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentUploadFormatValidator;
import com.infosys.custom.ebanking.contentmanagement.validators.CustomContentUploadSizeValidator;
import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.FEBATypesConstants;
import com.infosys.feba.framework.types.primitives.DataLength;
import com.infosys.feba.framework.types.primitives.FEBABinary;
import com.infosys.feba.framework.types.primitives.FileContentSerialNumber;
import com.infosys.feba.framework.types.primitives.FileSequenceNumber;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.tao.FDTTTAO;
import com.infosys.fentbase.tao.FUCTTAO;
import com.infosys.fentbase.tao.info.FDTTInfo;
import com.infosys.fentbase.tao.info.FUCTInfo;


/**

 * This class is responsible for uploading content photo to CMS system or CMGT
 * @author Vidhi_Kapoor01
 */

public class CustomContentMgmtServiceUploadImpl extends AbstractLocalUpdateTran {

	/**
	 * This method performs necessary validations on the inputs
	 * @author Vidhi_Kapoor01
	 * @since Sept 4, 2018	
	 */

    int serialNo;
    boolean errorCMGTFlag= false;

	@Override

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0,
			IFEBAValueObject arg1, IFEBAValueObject arg2)

			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomContentMgmtDetailsVO photoVO = (CustomContentMgmtDetailsVO) arg1;
		
		FBATransactionContext ctx = (FBATransactionContext) arg0;
		CustomContentMgmtDetailsVO photoVO1=getFileDetails(photoVO, ctx);
		  	
		return new FEBAValItem [] {
							
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO1,
						new CustomContentUploadSizeValidator(),
						FEBAValEngineConstants.INDEPENDENT),				
				
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO1,
						new CustomContentUploadFormatValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO1,
						new CustomContentTypeFieldsValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				  
				new FEBAValItem(FEBATypesConstants.NO_TAG, photoVO1,
						new CustomContentUploadDateValidator(),
						FEBAValEngineConstants.INDEPENDENT),
				};
		
	}

	
	private CustomContentMgmtDetailsVO getFileDetails(CustomContentMgmtDetailsVO photoVO,
			FBATransactionContext ctx) throws BusinessException {
		// Getting file sequence number VO from input output VO
		FileSequenceNumber seqNum = new FileSequenceNumber(photoVO
				.getFileSequenceNumber().toString());
		FUCTInfo fuctInfo = new FUCTInfo();
		FDTTInfo fdttInfo = new FDTTInfo();
		StringBuilder  dataBuffer = new StringBuilder ();
		String fullData;
		// get the file file details from FDTT
		try {
			fdttInfo = FDTTTAO.select(ctx, seqNum, ctx.getBankId());
		} catch (FEBATableOperatorException e1) {
			throw new BusinessException(ctx, FBAIncidenceCodes.NO_FILE_UPLOADED,
					"No records fetched from FUCT",
					FBAErrorCodes.FU_FUCT_FETCH_FAILED, e1);
		}
		/*
		 * File upload framework uploads the file contents to FUCT in chunks.
		 * Loop through and get all chunks of data
		 */
		for (int serialCount = 1; serialCount <= fdttInfo.getFileSerialNo()
				.getValue(); serialCount++) {
			FileContentSerialNumber fileContSerNum = new FileContentSerialNumber(
					serialCount);
			try {
				// getting the file contents from FUCT table
				fuctInfo = FUCTTAO.select(ctx, seqNum, fileContSerNum, ctx
						.getBankId());
			} catch (FEBATableOperatorException e) {
				throw new BusinessException(ctx,
						FBAIncidenceCodes.NO_FILE_UPLOADED,
						"No records fetched from FUCT",
						FBAErrorCodes.FU_FUCT_FETCH_FAILED, e);
			}
			fullData = new String(fuctInfo.getFileData().getValue());
			/* Append the chunk data to buffer */
			dataBuffer.append(fullData);
		}
		/* Get the byte data from string */
		FEBABinary profilePhoto = new FEBABinary(dataBuffer.toString()
				.getBytes());
		/* setting the file meta data and data to VO from FDTT and FUCT */
		populateProfilePhotoVO(photoVO, seqNum, fuctInfo, fdttInfo,
				profilePhoto);

		/* Delete the file from FUCT and FDTT table */
		serialNo =  fdttInfo.getFileSerialNo().getValue();
		return photoVO;
	}

/**
 * This method is used to populate ProfilePhotoVO
 */
	private void populateProfilePhotoVO(CustomContentMgmtDetailsVO photoVO,
			FileSequenceNumber seqNum, FUCTInfo fuctInfo, FDTTInfo fdttInfo,
			FEBABinary profilePhoto) {
		if(photoVO.getObjectIdEx()==null||photoVO.getObjectIdEx().toString().isEmpty())
		{				 
		   photoVO.setObjectId(seqNum.getValue());
		}
		else
		{  
			photoVO.setObjectId(photoVO.getObjectId());
		}		
		photoVO.setContentType(fdttInfo.getFileMime());
		photoVO.setFileSize(new DataLength(profilePhoto.length()));
		photoVO.setProfilePhoto(profilePhoto);
		photoVO.setFileSize(fuctInfo.getDataLength());
		photoVO.setContentType(fdttInfo.getFileMime());
		photoVO.setFileName(fdttInfo.getFileName());		
	}
	
	/*	 
	 * This method populates CMSInputOutputVO to upload photo to CMS/ CMGT
	 */
	
	 @Override
	    public void process(FEBATransactionContext pObjContext,
	            IFEBAValueObject objInputOutput, IFEBAValueObject objTxnWM)
	            throws BusinessException, BusinessConfirmation, CriticalException {
		 throw new UnsupportedOperationException();

	    }
	   

	
}
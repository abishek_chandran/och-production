/**
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */

package com.infosys.custom.ebanking.contentmanagement.validators;



import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.DateUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.fentbase.common.FBATransactionContext;


public class CustomContentUploadDateValidator implements IFEBAStandardValidator {


	public void validate(FEBATransactionContext arg0, IFEBAType arg1)
			throws BusinessException, CriticalException {
		
		CustomContentMgmtDetailsVO vo = (CustomContentMgmtDetailsVO) arg1;
		FBATransactionContext ctx = (FBATransactionContext) arg0;
		
		FEBADate currentDate = null;
		currentDate = new FEBADate(DateUtil.currentDate(ctx));
		currentDate.setDateFormat(EBankingConstants.FEBA_DATE_FORMAT);
		
		
		if(!FEBATypesUtility.isNotBlankDate(vo.getStartDate())
				|| FEBATypesUtility.isDefaultDate(vo.getStartDate()))
		        {
			throw new BusinessException(ctx,"CMGT001",
					"Start Date is mandatory",
					103765);
		        }
		else {
			if (currentDate.compareTo(vo.getStartDate()) > 1)
				{
			throw new BusinessException(ctx,"CMGT002",
					"Start Date cannot be prior to the current date",
					100797);
				}
		if(!FEBATypesUtility.isNotBlankDate(vo.getEndDate())
				|| FEBATypesUtility.isDefaultDate(vo.getEndDate()))
		        {
			throw new BusinessException(ctx,"CMGT003",
					"End Date is mandatory",
					105468);
		        }
		   else
		    {					
			  if (vo.getStartDate().compareTo(vo.getEndDate()) > 0) {
				  throw new BusinessException(ctx,"CMGT004",
							"End date cannot be prior to the start date",
							103321);
			}
		}	
	}}
	

}


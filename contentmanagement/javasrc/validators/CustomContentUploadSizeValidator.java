/**
 * COPYRIGHT NOTICE:
 * Copyright (c) 2007 Infosys Technologies Limited.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Infosys Technologies Ltd. ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered
 * into with Infosys.
 */
package com.infosys.custom.ebanking.contentmanagement.validators;

import com.infosys.custom.ebanking.types.valueobjects.CustomContentMgmtDetailsVO;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.DataLength;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.valengine.IFEBAExtendedValidator;
import com.infosys.feba.framework.valengine.IFEBAStandardValidator;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.user.UserConstants;

/**
 * @author Vidhi_Kapoor01
 * @since Sept 5,2018
 */
public class CustomContentUploadSizeValidator implements IFEBAStandardValidator {

	
	public void validate(FEBATransactionContext arg0, IFEBAType arg1)
			throws BusinessException, CriticalException {
		CustomContentMgmtDetailsVO vo = (CustomContentMgmtDetailsVO) arg1;
		FBATransactionContext ctx = (FBATransactionContext) arg0;
		/*
		 * Getting PHOTO_MINIMUM_SIZE & PHOTO_MAXIMUM_SIZE parameters from PRPM
		 * table
		 */
		
		Long photoMinimumSize = Long.parseLong(PropertyUtil.getProperty(
				UserConstants.PHOTO_MINIMUM_SIZE, ctx));

		Long photoMaximumSize = Long.parseLong(PropertyUtil.getProperty(
				UserConstants.PHOTO_MAXIMUM_SIZE, ctx));
		
		DataLength fileSize = vo.getFileSize();
		if (fileSize.getValue() < photoMinimumSize) {
			/* Getting minimum size in Kilobytes */
			Long photoMinimumSizeInKB = (photoMinimumSize / 1024)+1;
			throw new BusinessException(ctx,
					FBAIncidenceCodes.PHOTO_LESS_THAN_MIN_SIZE,
					"Photo should be of at least of $MINIMUM_SIZE$ kb",
					FBAErrorCodes.PHOTO_LESS_THAN_MIN_SIZE,
					new AdditionalParam("MINIMUM_SIZE", new FEBAUnboundString(
							photoMinimumSizeInKB.toString())));
		}

		if (fileSize.getValue() > photoMaximumSize) {
					/* Getting maximum size in Kilobytes */
					Long photoMaximumSizeInKB = (photoMaximumSize / 1024)+1;
					throw new BusinessException(ctx,
							FBAIncidenceCodes.PHOTO_MORE_THAN_MAX_SIZE,
							"Photo size should not be more than $MAXIMUM_SIZE$ kb",
							FBAErrorCodes.PHOTO_MORE_THAN_MAX_SIZE,
							new AdditionalParam("MAXIMUM_SIZE", new FEBAUnboundString(
									photoMaximumSizeInKB.toString())));
		}
	}
	
}

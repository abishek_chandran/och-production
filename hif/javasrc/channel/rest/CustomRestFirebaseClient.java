package com.infosys.custom.hif.channel.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.util.EntityUtils;
import org.glassfish.jersey.SslConfigurator;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.infosys.feba.framework.batch.config.BatchConfig;
import com.infosys.feba.framework.batch.config.ReadOnlyBatchParams;
import com.infosys.feba.framework.common.ErrorCodes;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.hif.model.IHostMessage;
import com.infosys.feba.framework.types.FEBAStringBuilder;
import com.infosys.fentbase.common.FBAConstants;

/**
 * Class added as part of HIF Rest Service Integration Invokes the REST API and
 * gets the response from the REST service and passes the response back.
 * 
 * @author Suresh
 */
public class CustomRestFirebaseClient {

	private static final String CONNECT_TIMEOUT = "CONNECT_TIMEOUT";
	private static final String READ_TIMEOUT = "READ_TIMEOUT";
	private static final String METHOD_POST = "POST";
	private static final String APPLICATION_JSON = "POST";


	private static final String MULITPART_FORM_DATA_HTTP = "MULITPART_FORM_DATA/HTTP";

	// creation of Client instance is expensive, Methods to create instances of
	// WebTarget are thread-safe
	private static Client jersyClient = null;
	// =Client.create();

	public String sSSLCertName = null;

	/**
	 * Properties
	 */
	public static Properties properties;

	/**
	 * to set the properties
	 * 
	 * @param properties
	 */
	public static synchronized void setProperties(Properties pProperties) {
		if (jersyClient == null) {
			CustomRestClient.properties = pProperties;

			ClientConfig config = new ClientConfig();
			config.register(MultiPartFeature.class);
			config.property(ClientProperties.CONNECT_TIMEOUT, properties.getProperty(CONNECT_TIMEOUT));
			config.property(ClientProperties.READ_TIMEOUT, properties.getProperty(READ_TIMEOUT));

			jersyClient = ClientBuilder.newClient(config);

			SslConfigurator sslConfig = getSSlConfig();
			SSLContext sc = sslConfig.createSSLContext();
			jersyClient = ClientBuilder.newBuilder().build();
		}

	}

	/***
	 *
	 * Method used for POST REQUEST
	 * 
	 * @param pTransactionContext
	 ** @param pHostMessage
	 * @return
	 * @throws CriticalException
	 */

	@SuppressWarnings("deprecation")
	public static String post(FEBATransactionContext pTransactionContext, IHostMessage pHostMessage)
			throws CriticalException {
		String responseString = null;

		System.out.println("Rest Firebase Client post method");

		String URL = null;
		CloseableHttpClient httpclient = null;

		
		LogManager.logDebug(pTransactionContext, "HttpPostClient : In POST method ");

		try {

			System.out.println("Firebase Host Message - "+pHostMessage);

			URL = prepareURL(pHostMessage);

		
			HttpHost proxy = new HttpHost("172.18.104.20",1707);

			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
			httpclient = HttpClients.custom()
					.setRoutePlanner(routePlanner)
					.build();
			HttpPost httpPost = new HttpPost(URL);

			String authToken = getGoogleAuthToken();
			System.out.println("Firebase Auth Token - "+authToken);
			httpPost.setHeader("Authorization", "Bearer "+authToken);
			httpPost.setHeader("Content-Type","application/json");

			
			StringEntity entity = new StringEntity(pHostMessage.getMessage().toString(),
					ContentType.APPLICATION_JSON);

			LogManager.log(pTransactionContext,
					"JSONRestClient : POST Request details: " + getHTTPRequestDetails(METHOD_POST, pHostMessage.getMessage().toString(), URL),
					LogManager.HOST_MESSAGE);
			
			httpPost.setEntity(entity);

			HttpResponse response = httpclient.execute(httpPost);
			
			HttpEntity resEntity = response.getEntity();
			responseString = EntityUtils.toString(resEntity);
			System.out.println("Firebase Response String - "+responseString);


			LogManager.log(pTransactionContext, "HttpPostClient : POST Response details: "
					+ getHTTPResponseDetails(METHOD_POST, responseString, ""), LogManager.HOST_MESSAGE);


			

		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logDebug(pTransactionContext, "JSONRestClient : Error in HTTP transmission : " + e.getMessage());
			throw new CriticalException(pTransactionContext, FEBAIncidenceCodes.HTTP_POST_TRANSMISSION_FAILED,
					"JSON on HTTP transmission to[" + URL + "] failed.", ErrorCodes.HTTP_POST_TRANSMISSION_FAILED, e);
		}finally {
			httpclient.getConnectionManager().shutdown();
		}

		return responseString;
	}

	/**
	 * Method used for creating the final REQUEST log message
	 * 
	 * @param httpMethod
	 * @param webTarget
	 * @param pContent
	 * @param requestHeaders
	 * @return
	 */

	private static String getHTTPRequestDetails(String httpMethod, String pContent, String URL) {

		FEBAStringBuilder returnString = new FEBAStringBuilder("");
		returnString.append("\n--------------------------------------------\n");
		returnString.append("Request URL: " + URL);
		returnString.append("\n");
		returnString.append("Request method: " + httpMethod);
		returnString.append("\n");
		returnString.append("Request content: " + pContent);
		returnString.append("\n--------------------------------------------\n");

		return returnString.toString();
	}

	/**
	 * Method used for creating the final RESPONSE log message
	 * 
	 * @param httpMethod
	 * @param response
	 * @param pContent
	 * @return
	 */

	private static String getHTTPResponseDetails(String httpMethod, String response,

			String pContent) {
		FEBAStringBuilder returnString = new FEBAStringBuilder("");
		returnString.append("\n--------------------------------------------\n");
		returnString.append("Response method: " + httpMethod);
		returnString.append("\n");
		returnString.append("Response content: " + response);
		returnString.append("\n--------------------------------------------\n");

		return returnString.toString();
	}

	/**
	 *
	 * To handle different response headers
	 *
	 *
	 *
	 */
	private static void handleResponseHeader(FEBATransactionContext pTransactionContext, Response response)
			throws CriticalException {
		/*
		 * if(!(response.getStatus()==200 || response.getStatus()==201
		 * ||response.getStatus()==204)) { throw new
		 * CriticalException(pTransactionContext,
		 * FEBAIncidenceCodes.RESPONSE_NOT_200, ErrorCodes.RESPONSE_NOT_200); }
		 */

		/**
		 * TO-DO
		 */
	}

	/**
	 * Prepare the REST URL endpoint
	 *
	 * @param pHostMessage
	 * @return
	 */

	private static String prepareURL(IHostMessage pHostMessage) throws MalformedURLException, URISyntaxException

	{

		// getting the REST url configured in HIF_Message.xml
		String sURL = pHostMessage.getHostMessageConfig().getRootURL();
		// getting the url path set in the tranformer
		String urlPath = pHostMessage.getUrlPath();
		// getting the request url params set in tranformer
		Map<String, String> requestUrlParams = pHostMessage.getRequestUrlParams();

		if (urlPath != null) {
			sURL = sURL + urlPath;
		}

		String splChar = "?";// initially before setting the first param

		if (requestUrlParams != null) {

			for (Object key : requestUrlParams.keySet()) {
				sURL = sURL + splChar + key.toString() + "=" + requestUrlParams.get(key.toString());
				splChar = "&";// changed so that ampersand is used from second
				// param onwards
			}
		}

		validateURI(sURL);
		return sURL;

	}

	/**
	 * Prepare the Invocation Builder
	 *
	 * @param pHostMessage
	 * @param webTarget
	 * @return
	 */

	private static Invocation.Builder prepareBuilder(IHostMessage pHostMessage, Map reqHeaders, WebTarget webTarget) {
		Invocation.Builder builder = null;
		if (pHostMessage.getHostMessageConfig().getRouteName().equalsIgnoreCase(MULITPART_FORM_DATA_HTTP)) {
			builder = webTarget.request(MediaType.MULTIPART_FORM_DATA);
			builder.accept(MediaType.MULTIPART_FORM_DATA);
		} else {
			builder = webTarget.request(MediaType.TEXT_PLAIN);
			builder.accept(MediaType.TEXT_PLAIN);
		}
		// setting the request headers
		if (reqHeaders != null) {
			for (Object key : reqHeaders.keySet()) {
				builder.header(key.toString(), reqHeaders.get(key.toString()));
			}
		}

		return builder;

	}

	/**
	 * prepare the entity
	 * 
	 * @param requestMessage
	 * @param routeName
	 * @return
	 * @throws UnsupportedEncodingException
	 */


	/**
	 * URL validation
	 * 
	 * @param url
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */

	private static void validateURI(String url) throws MalformedURLException, URISyntaxException {
		URL u = null;

		try {
			u = new URL(url);
		} catch (MalformedURLException e) {
			throw e;
		}

		try {
			u.toURI();
		} catch (URISyntaxException e) {
			throw e;
		}

	}

	/**
	 * getting SSL Congiguration
	 * 
	 * @param url
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	private static SslConfigurator getSSlConfig() {
		// reading the property from WAS Custom properties

		String keyStoreProvider = PropertyUtil.getProperty(FBAConstants.ENCRYPTION_PROVIDER, null);
		String keyManagFactAlgo = PropertyUtil.getProperty(FBAConstants.KEY_MANAGER_FACT_ALGO, null);
		String clientProtocol = PropertyUtil.getProperty(FBAConstants.CLIENT_PROTOCOL, null);
		String trustStorePassword = PropertyUtil.getProperty(FBAConstants.STOREPASSWORD, null);
		String trustStoreType = PropertyUtil.getProperty(FBAConstants.TRUST_STORE_TYPE, null);
		String trustStoreFile = PropertyUtil.getProperty(FBAConstants.TRUST_STORE_PATH, null);
		String trustStoreMangFactAlgo = PropertyUtil.getProperty(FBAConstants.TRUST_MANAGER_FACT_ALGO, null);

		// configuring the sslconfig
		SslConfigurator sslConfig = SslConfigurator.newInstance().trustStoreFile(trustStoreFile)
				.trustStorePassword(trustStorePassword).trustStoreType(trustStoreType)
				.trustManagerFactoryAlgorithm(trustStoreMangFactAlgo)

				.keyStoreFile(trustStoreFile).keyPassword(trustStorePassword).keyStoreType(trustStoreType)
				.keyManagerFactoryAlgorithm(keyManagFactAlgo).keyStoreProvider(keyStoreProvider)
				.securityProtocol(clientProtocol);
		return sslConfig;
	}


	private static String getGoogleAuthToken() throws FileNotFoundException, IOException{
		ReadOnlyBatchParams fireBaseParams = BatchConfig.getInstance().getBatchParams(
				"GoogleFirebaseParams");
		if (fireBaseParams != null) {
			String messageScope = fireBaseParams.getParam("MESSSAGE_SCOPES");
			String googleServcieAccountFile = fireBaseParams.getParam("SERVICE_ACCOUNT_FILE");
			System.out.println("googleServcieAccountFile : "+googleServcieAccountFile);
			final String[] SCOPES = { messageScope };
			System.setProperty("https.proxyHost","172.18.104.20");
			System.setProperty("https.proxyPort","1707");
			GoogleCredential googleCredential = GoogleCredential
					.fromStream(
							new FileInputStream(googleServcieAccountFile))
					.createScoped(Arrays.asList(SCOPES));
			googleCredential.refreshToken();
			System.clearProperty("https.proxyHost");
			System.clearProperty("https.proxyPort");
			return googleCredential.getAccessToken();
		}
		return null;
	}
}

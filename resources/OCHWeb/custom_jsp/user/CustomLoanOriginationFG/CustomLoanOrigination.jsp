<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.LISTING_VISIBILITY', 'FormField', pageContext)}" var="isVis"></feba2:Set>
<feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.DEL_MULTIPLE','formfield', pageContext)}" var="deleteMode"></feba2:Set>
<feba2:Set value="${feba2:select('deleteMode#true@@!deleteMode#false',pageContext)}" var="allChk"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'true')}" var="delete"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'false')}" var="search"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('deleteMode#DeleteCountryParameter@@!deleteMode#CountryParameterMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "Loan Origination"/>
<feba2:Page action="Finacle" name="CustomLoanOriginationFG" forcontrolIDs="">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (!pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%}%>
<%@include file="/jsp/sidebar.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
	<%@include file="/jsp/parentTable.jsp"%>
<%} %>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  
  <feba2:Set value="${feba2:select('delete#false@@search#true',pageContext)}" var="visible" />
  <feba2:Set value="${feba2:select('delete#Delete Currency Parameter@@search#Currency Parameter Maintenance',pageContext)}" var="heading" />
  <feba2:Set value="${feba2:getValue('CustomLoanOriginationFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize" />
  <feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible" />
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="title" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeading" text="Application List" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>           
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay17845338" style="alerttable" />
  <feba2:Set value="true" var="SearchPanel.visible" />
  <feba2:Section id="SearchPanel" collapsibleDisplay="allCollapse" displaymode="N" identifier="SearchPanel" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="TableHeader" style="whtboldtxt" text="Search Criteria" visible="true" isDownloadLink="false" isMenu="false" textLC="LC74" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="width100percent">          
          <feba2:row identifier="Ra1" style="formrow">
			<feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanOriginationFG.BANK_ID" id="LabelForControl25619547" name="LabelForControl25619547" text="Bank Id:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1797" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.BANK_ID" name="CustomLoanOriginationFG.BANK_ID" title="Bank Id" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC97" maxlength="11" />
            </feba2:Col>
			</feba2:row>
		   <feba2:row identifier="Ra2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanOriginationFG.APPLICATION_STATUS" id="Application Status" name="Application Status" text="Application Status:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC9158" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field combomode="complex" id="CustomLoanOriginationFG.APPLICATION_STATUS" key="CODE_TYPE=APS" name="CustomLoanOriginationFG.APPLICATION_STATUS" select="false" title="Application Status" visible="true" all="false" disable="false" displayMode="N" empty="false" filter="CommonCodeFilter" preselect="true" preSelectValue="ALL" style="dropdownexpandalbe" tagHelper="ComboBoxTagHelper" titleLC="LC309" />
            </feba2:Col>
          </feba2:row>        
          <feba2:row identifier="Ra3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol displayDateFormat="True" forControlId="CustomLoanOriginationFG.FROM_DATE" id="FromDatelbl" name="FromDate_lbl"  text="Last Action Date From:" title="From Date" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1046" titleLC="LC61" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:compositefield id="toDateComponent" align="horizontal" name="ToDateComponent" visible="true">
                <feba2:field fldFormatter="DateFormatter" id="CustomLoanOriginationFG" name="CustomLoanOriginationFG.FROM_DATE" title="From Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC61" maxlength="" />
                <feba2:blankfield count="2" id="blankField1" visible="true" />
              	<feba2:labelforcontrol displayDateFormat="True" forControlId="CustomLoanOriginationFG.TO_DATE" id="ToDatelbl" name="ToDate_lbl" text="Last Action Date To:" title="To Date" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC1047" titleLC="LC63" />
                <feba2:blankfield id="blankfield2" visible="true" />
              <feba2:field fldFormatter="DateFormatter" id="CustomLoanOriginationFG.TO_DATE" name="CustomLoanOriginationFG.TO_DATE" title="To Date" disable="false" readonly="false" style="datetextbox" tagHelper="DateTagHelper" visible="true" titleLC="LC63" maxlength="" />
              </feba2:compositefield>
            </feba2:Col>
          </feba2:row>		  
		  <feba2:row identifier="Ra4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanOriginationFG.USER_HP_NUMBER" id="LabelForControl29892258" name="LabelForControl29892258" text="User HP Number:" visible="true" displayColonAfterDateFormat="true" style="simpletext" textLC="LC877" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanOriginationFG.USER_HP_NUMBER"  name="CustomLoanOriginationFG.USER_HP_NUMBER" style="querytextboxmedium" title="User HP Number" visible="true" copyAllowed="true" disable="false" pasteAllowed="true" readonly="false" tagHelper="TextTagHelper" titleLC="LC845" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section> 
  <feba2:Set value="true" var="NavPanel.visible" />
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Search" id="SEARCH" name="SEARCH" style="formbtn_top" title="Search" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC75" titleLC="LC75" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Clear" id="CLEAR" name="CLEAR" style="formbtn_last" title="Clear" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC76" titleLC="LC76" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="true" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Application List Details" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="listtopbigbg" headinglevel="h3" hideEmptyTable="true" id="CustomLoanOriginationListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="CustomLoanOriginationListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="${allChk}" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC20005" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
               <feba2:Header>
               	 <feba2:Heading id="column7145547" sort="true" sortOrder="ASC" style="slno" visible="true" text="Select" textLC="LC38" />
                 <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Application Id" textLC="LC14250" />
                 <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="App Created On" textLC="LC14250" />
             	 <feba2:Heading id="column28061596" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Last Action Date" textLC="LC20001" />
                 <feba2:Heading id="column6744085" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Application Status" textLC="LC20002" />
                 <feba2:Heading id="column1073282" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Days in Current Status" textLC="LC14252" />
                 <feba2:Heading id="column1073282" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Credit Scoring Result" textLC="LC14252" />
                 <feba2:Heading id="column1073282" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Approved Amount" textLC="LC14252" />              
               </feba2:Header>                 
                <feba2:field id="CustomLoanOriginationFG.SELECTED_INDEX" name="CustomLoanOriginationFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Loan Parameter" visible="true" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" titleLC="LC319" />
              	<feba2:field id="CustomLoanOriginationFG.APPLICATION_ID_ARRAY" name="CustomLoanOriginationFG.APPLICATION_ID_ARRAY"  link="FORMSGROUP_ID__=CustomLoanOriginationFG#__EVENT_ID__=VIEW_DETAILS #CustomLoanOriginationFG.SELECTED_INDEX={INDEX,page}" linkstyle="bluelink" tableColStyle="listgreyrowtxtleftline"  disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />               
                <feba2:field fldFormatter="DateFormatter" id="CustomLoanOriginationFG.APP_CREATED_ON_ARRAY" name="CustomLoanOriginationFG.APP_CREATED_ON_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true"  />
           	 	<feba2:field fldFormatter="DateFormatter" id="CustomLoanOriginationFG.LAST_ACTION_DATE_ARRAY" name="CustomLoanOriginationFG.LAST_ACTION_DATE_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true"  />
            	<feba2:field id="CustomLoanOriginationFG.APPLICATION_STATUS_ARRAY" key="CODE_TYPE=APS" name="CustomLoanOriginationFG.APPLICATION_STATUS_ARRAY" tableColStyle="listgreyrowtxtleftline" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper"  />
				<feba2:field id="CustomLoanOriginationFG.DAYS_IN_CURRENT_STATUS_ARRAY" name="CustomLoanOriginationFG.DAYS_IN_CURRENT_STATUS_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" /> 
				<feba2:field id="CustomLoanOriginationFG.CREDIT_SCORING_RESULT_ARRAY" name="CustomLoanOriginationFG.CREDIT_SCORING_RESULT_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" /> 
				<feba2:field id="CustomLoanOriginationFG.APPROVED_AMOUNT_ARRAY" name="CustomLoanOriginationFG.APPROVED_AMOUNT_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" /> 				
                <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="CustomLoanOriginationListing.GOTO_PAGE__" prevPage="CustomLoanOriginationListing.GOTO_PREV__" nextPage="CustomLoanOriginationListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset displaymode="N" identifier="Rowset2" logical="true" style="width100percent">
          <feba2:row identifier="Rb1" style="button_withoutmargin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="History"  id="VIEW_HISTORY" name="VIEW_HISTORY" visible="${isVis}" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" captionLC="LC78" />
            </feba2:Col>            
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>

          
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanOriginationFG" />
 <feba2:hidden name="CustomLoanOriginationFG.REPORTTITLE" id="CustomLoanOriginationFG.REPORTTITLE" value="CustomLoanOrigination" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:Set value="${feba2:getValue('CustomLoanInterestDetailsFG.DEL_MULTIPLE','formfield', pageContext)}" var="deleteMode"></feba2:Set>
<feba2:Set value="${feba2:select('deleteMode#true@@!deleteMode#false',pageContext)}" var="allChk"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'true')}" var="delete"></feba2:Set>
<feba2:Set value="${feba2:equals(deleteMode,'false')}" var="search"></feba2:Set>
<feba2:SetAlternateView alternateView="${feba2:select('deleteMode#DeleteCountryParameter@@!deleteMode#CountryParameterMaintenance',pageContext)}"></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "Loan Interest Details"/>
<feba2:Page action="Finacle" name="CustomLoanInterestDetailsFG" forcontrolIDs="Product Code:=CustomLoanInterestDetailsFG.PRODUCT_CODE@@Product Category:=CustomLoanInterestDetailsFG.PROD_CATEGORY@@Configuration Type:=CustomLoanInterestDetailsFG.CONFIG_TYPE@@">
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="mtb" printScreen="btn-printscreen.gif" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Set value="${feba2:select('delete#false@@search#true',pageContext)}" var="visible" />
  <feba2:Set value="${feba2:select('delete#Delete Currency Parameter@@search#Currency Parameter Maintenance',pageContext)}" var="heading" />
  <feba2:Set value="${feba2:getValue('CustomLoanInterestDetailsFG.RESULT_LIST_SIZE','formfield', pageContext)}" var="listSize" />
  <feba2:Set value="${feba2:equals(listSize,'0')}" var="isVisible" />
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow Image" id="ArrowImage" name="title" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC27" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="PageHeading" text="Loan Product Maintenance" headinglevel="h1" upperCase="false" visible="true" />
            </feba2:Col>
            <feba2:Col displaymode="N" identifier="C3" style="right">
              <feba2:field caption="Create New" id="ADD_INTEREST_DETAILS" name="ADD_INTEREST_DETAILS" style="formbtn_top" title="Create New" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC73" titleLC="LC73" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay17845338" style="alerttable" />
  <feba2:Set value="${!isVisible}" var="ListTableWithCtrls.visible" />
  <feba2:Section id="ListTableWithCtrls" identifier="ListingTable_With_Navigation_Controls_black_border" style="section_fourlinbrd">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="false" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="false" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="false" style="width100percent">
          <feba2:row identifier="Ra1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:Table caption="Add Loan Interest Details" captionstyle="" collapsableFlag="" collapsableId="" collapsible="" footerStyle="" headerStyle="listtopbigbg" headinglevel="h3" hideEmptyTable="true" id="CustomLnInterestDetailsListing" lastupdatedate="" listDisplay="Displaying$range$of$total$results$" listing="CustomLnInterestDetailsListing" pagination="true" paginationButtonCaption="Go" paginationFooter="Page$current$of$total" paginationFooterText="Go to Page" rowOneStyle="listgreyrow" rowTwoStyle="listwhiterow" selectAllCheck="${allChk}" summary="" tableDisplayStyle="" tableStyle="width100percent" visible="true" width="100" captionLC="LC20005" listDisplay1LC="LC22" listDisplay2LC="LC17" listDisplay3LC="LC23" paginationButtonCaptionLC="LC24" paginationFooter1LC="LC25" paginationFooter2LC="LC17" paginationFooterTextLC="LC26">
                <feba2:Header>
                  <feba2:Heading id="column7145547" sort="true" sortOrder="ASC" style="slno" visible="true" text="Select" textLC="LC38" />
                  <feba2:Heading id="column13549765" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Tenor" textLC="LC14250" />
                  <feba2:Heading id="column28061596" sort="true" sortOrder="ASC" style="listboldtxtwithline" text="Interest" textLC="LC20001" />
                </feba2:Header>
                
                <feba2:field id="CustomLoanInterestDetailsFG.SELECTED_INDEX" name="CustomLoanInterestDetailsFG.SELECTED_INDEX" tableColStyle="listgreyrowtxtwithoutline" title="Select Currency Parameter" visible="true" style="absmiddle" tagHelper="TableRadioTagHelper" usage="I" value="true" titleLC="LC319" />
                <feba2:field id="CustomLoanInterestDetailsFG.TENOR_ARRAY" name="CustomLoanInterestDetailsFG.TENOR_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:field id="CustomLoanInterestDetailsFG.INTEREST_ARRAY" name="CustomLoanInterestDetailsFG.INTEREST_ARRAY" tableColStyle="listgreyrowtxtleftline" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" />
                <feba2:Footer pageNo="REQUESTED_PAGE_NUMBER" isPinnable="False" goName="CustomLnInterestDetailsListing.GOTO_PAGE__" prevPage="CustomLnInterestDetailsListing.GOTO_PREV__" nextPage="CustomLnInterestDetailsListing.GOTO_NEXT__" />
              </feba2:Table>
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
        <feba2:rowset displaymode="N" identifier="Rowset2" logical="true" style="width100percent">
          <feba2:row identifier="Rb1" style="button_withoutmargin">
            <feba2:Col identifier="C2" style="">
              <feba2:field caption="Update" id="UPDATE_INTEREST_DETAILS" name="UPDATE_INTEREST_DETAILS" style="formbtn_top" title="Update" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC78" titleLC="LC78" />
            </feba2:Col>
            <feba2:Col identifier="C3" style="">
              <feba2:field caption="Delete" id="DELETE_INTEREST_DETAILS" name="DELETE_INTEREST_DETAILS" style="formbtn_last" title="Delete" visible="true" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC13" titleLC="LC13" />
            </feba2:Col>
            <feba2:Col identifier="C4" style="">
              <feba2:field id="Back17125369" linkstyle="submit" name="PREV_SCREEN__" style="submit" tagHelper="BackTagHelper" title="Back" visible="true" titleLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanInterestDetailsFG" />
 <feba2:hidden name="CustomLoanInterestDetailsFG.REPORTTITLE" id="CustomLoanInterestDetailsFG.REPORTTITLE" value="CustomLnInterestList" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="feba2" uri="/feba_taglib.tld" %>
<%@ page import="com.infosys.feba.framework.ui.util.FebaTagUtil"%>
<%@include file="/jsp/topbar.jsp"%>

<feba2:SetAlternateView alternateView=""></feba2:SetAlternateView>
<feba2:title pageTitle="true" text = "Preview Confirmation Details" textLC = "LC64" />
<feba2:Page action="Finacle" name="CustomLoanProdMaintenanceFG" forcontrolIDs="">

<feba2:Set value="${feba2:getValue('CustomLoanProdMaintenanceFG.VIEW_MODE', 'FormField', pageContext)}" var="viewMode"></feba2:Set>
<feba2:Set value="${feba2:equalsIgnoreCase(viewMode,'CONF') or feba2:equalsIgnoreCase(viewMode,'APPROVALS')}" var="isConfOrIsApprovals"></feba2:Set>
<feba2:Set value="${feba2:equals(viewMode,'CONF')}" var="isConf"></feba2:Set>
<% if(!isGroupletView){%>

	<%@include file="/jsp/header.jsp"%>
<%if (pageContext.getAttribute("isRMUser").toString().equals("true")){ %>
<%@include file="/jsp/sidebar.jsp"%>
	<%@include file="/jsp/parentTable.jsp"%>
<%} else {%>
	<%@include file="/jsp/parentTable.jsp"%>
	<%@include file="/jsp/sidebar.jsp"%>
<%}%>
<%}%>
  <feba2:Section id="BrdCrumbNImg" identifier="BreadCrumb" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C1" style="">
              <feba2:moduleTopBar id="moduleTopBar31593628" breadcrumb="true" helpImage="btn-help.gif" printImage="btn-print.gif" visible="true" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="PgHeading" identifier="PageHeader" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" logical="true" style="width100percent">
          <feba2:row identifier="Ra1">
            <feba2:Col identifier="C1" style="width10">
              <feba2:field alt="Arrow" id="Image22020175" name="Image22020175" src="arrow-pageheading.gif" isMenu="false" tagHelper="ImageTagHelper" visible="true" altLC="LC228" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="pageheadingcaps">
              <feba2:title id="title30000602" text="Preview Confirmation Details" upperCase="True" headinglevel="h1" visible="true" textLC="LC64" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:ErrorDisplay id="ErrorDisplay7979803" style="" />
  <%-- <feba2:Section id="NavMenu" displaymode="N" identifier="NavigationMenuControl" style="greybottomleft">
    <feba2:SubSectionSet identifier="SubSectionSet1" style="greybottomright">
      <feba2:SubSection identifier="SubSection1" style="greytopleft">
        <feba2:rowset identifier="Rowset1" style="greytopright">
          <feba2:row identifier="Ra1" style="greybg_margin">
            <feba2:Col identifier="C1" style="">
              <feba2:field caption="Option" id="CustomLoanProdMaintenanceFG.OPTION_ID" name="CustomLoanProdMaintenanceFG.OPTION_ID" title="Option" buttonCaption="OK" tagHelper="AccessControlComboBoxTagHelper" visible="true" captionLC="LC31" titleLC="LC31" buttonCaptionLC="LC33" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section> --%>
  <feba2:Section id="DispForm" identifier="DisplayForm" style="section_blackborder">
    <feba2:SectHeader identifier="Header">
      <feba2:caption id="Caption22821636" name="Caption22821636" text="Loan Product Details" title="Loan Product Details" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC7439" titleLC="LC7439" />
    </feba2:SectHeader>
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection6" style="width100percent">
        <feba2:rowset identifier="Rowset23" style="width100percent">
        <feba2:row identifier="Rw7" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption21781911" name="Caption21781911" text="Merchant ID:" title="Merchant ID" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC44" titleLC="LC69" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.MERCHANT_ID" key="CODE_TYPE=MID" name="CustomLoanProdMaintenanceFG.MERCHANT_ID" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw1" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption21781911" name="Caption21781911" text="Product Code:" title="Product Code" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC44" titleLC="LC69" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.PRODUCT_CODE" key="CODE_TYPE=PCO" name="CustomLoanProdMaintenanceFG.PRODUCT_CODE" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw2" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Product Category:" title="Product Category" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.PROD_CATEGORY" key="CODE_TYPE=PCA" name="CustomLoanProdMaintenanceFG.PROD_CATEGORY" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw3" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Product SubCategory:" title="Product SubCategory" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY" name="CustomLoanProdMaintenanceFG.PROD_SUBCATEGORY" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="11" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw4" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Configuration Type:" title="Configuration Type" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.CONFIG_TYPE" key="CODE_TYPE=CFT" name="CustomLoanProdMaintenanceFG.CONFIG_TYPE" displayEvenIfAbsent="false" resourceName="CommonCode" style="searchsimpletext" tagHelper="GenericDescriptionTagHelper" maxlength="3" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw5" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Minimum Amount:" title="Minimum Amount" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.MIN_AMOUNT" name="CustomLoanProdMaintenanceFG.MIN_AMOUNT" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="11" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw6" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Maximum Amount:" title="Maximum Amount" isDownloadLink="false" isMenu="false" style="simpletext" visible="true" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.MAX_AMOUNT" name="CustomLoanProdMaintenanceFG.MAX_AMOUNT" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="true" maxlength="11" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Rw8" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:caption id="Caption23197" name="Caption23197" text="Loan Purpose:" title="Loan Purpose" isDownloadLink="false" isMenu="false" style="simpletext" visible="false" textLC="LC2076" titleLC="LC2082" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.LN_PURPOSE" name="CustomLoanProdMaintenanceFG.LN_PURPOSE" disable="false" isDownloadLink="false" isMenu="false" readonly="false" style="searchsimpletext" tagHelper="OutputTextTagHelper" visible="false" />
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Ra9" style="formrow">
            <feba2:Col identifier="C1" style="querytextleft">
              <feba2:labelforcontrol forControlId="CustomLoanProdMaintenanceFG.CURRENCY" id="currency" name="currency" required="false" text="Currency:" displayColonAfterDateFormat="true" style="simpletext" visible="true" textLC="LC462" />
            </feba2:Col>
            <feba2:Col identifier="C2" style="querytextright">
              <feba2:field id="CustomLoanProdMaintenanceFG.CURRENCY"  name="CustomLoanProdMaintenanceFG.CURRENCY" style="querytextboxmedium" title="Currency" disable="false" readonly="false" tagHelper="OutputTextTagHelper" visible="true" titleLC="LC463" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="Authorization1" displaymode="N" identifier="InputForm_Authorization" style="section_grayborder">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection2" style="width100percent">
        <feba2:rowset identifier="Rowset5" style="width100percent">
          <feba2:row identifier="Re1" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="ApproverDetails17428736" name="ApproverDetails17428736" visible="true" hideAdditionalDetails="false" isRemarksRequired="true" tagHelper="ApproversDetailsTagHelper">
                <feba2:map>
                  <feba2:param name="isMandatory" value="false" />
                  <feba2:param name="isConfidential" value="N" />
                </feba2:map>
              </feba2:field>
            </feba2:Col>
          </feba2:row>
          <feba2:row identifier="Re2" style="listingrow">
            <feba2:Col identifier="C1" style="">
              <feba2:field id="Authorization222442" name="Authorization222442" visible="false" tagHelper="AuthenticationTagHelper" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
  <feba2:Section id="NavPanel" displaymode="N" identifier="NavigationPanel" style="section">
    <feba2:SubSectionSet identifier="SubSectionSet1" logical="true" style="width100percent">
      <feba2:SubSection identifier="SubSection1" logical="true" style="width100percent">
        <feba2:rowset identifier="Rowset1" style="right">
          <feba2:row identifier="Ra1" style="">
            <feba2:Col identifier="C11" style="">
              <feba2:field caption="Submit" id="Button8957047" name="LOAN_SUBMIT" disable="false" isDownloadAction="false" style="formbtn" tagHelper="ButtonTagHelper" visible="${isConfOrIsApprovals}" captionLC="LC21" />
            </feba2:Col>
            <feba2:Col identifier="C12" style="">
              <feba2:field caption="Back" id="BACK" name="BACK" style="formbtn_last" visible="${!isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
            <feba2:Col identifier="C13" style="">
              <feba2:field caption="Back" id="BACK_TO_CREATE_NEW" name="BACK_TO_CREATE_NEW" style="formbtn_last" visible="${isConf}" disable="false" isDownloadAction="false" tagHelper="ButtonTagHelper" captionLC="LC4" />
            </feba2:Col>
          </feba2:row>
        </feba2:rowset>
      </feba2:SubSection>
    </feba2:SubSectionSet>
  </feba2:Section>
 <feba2:hidden name="FORMSGROUP_ID__" id="FORMSGROUP_ID__" value="CustomLoanProdMaintenanceFG" />
 <feba2:hidden name="CustomLoanProdMaintenanceFG.REPORTTITLE" id="CustomLoanProdMaintenanceFG.REPORTTITLE" value="CustomLoanProductCreatePreview" />
<%if(!isGroupletView){%>
	<%@include file="/jsp/footer.jsp"%>
<%}%>
</feba2:Page>


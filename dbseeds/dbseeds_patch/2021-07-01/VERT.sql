delete from VERT where ERR_CODE  IN ('211150');
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211150,'CUST','001','0','Tranasaction Pending.Try After 24 hours',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
delete from VERT where ERR_CODE  IN ('211151');
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211151,'CUST','001','0','Failed to Process Tranasaction',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
delete from VERT where ERR_CODE  IN ('211152');
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211152,'CUST','001','0','Insufficient Balance in Account',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

-- VIDA liveness  APIGEE changes start
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','APIGEE_NEW_USERNAME','INnT5twnglGEoUNmELXmgGMvtA09wBxa','Vida APIGEE user name','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','APIGEE_NEW_PASSWORD','LOlG6EZihxdvcSzO','Vida APIGEE password','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BRI_VIDA_LIVENESS_APIGEE_NEW_URL','/v1.0/vida/biometrics/face/liveliness','Vida APIGEE Liveness URL','N','setup',sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BRI_VIDA_LIVENESS_SCORE','95','Vida Liveness config','N','setup',sysdate,'setup',sysdate);

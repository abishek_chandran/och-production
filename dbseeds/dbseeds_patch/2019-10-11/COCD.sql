-- Loan disbursement process stage
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','ASTG','DISB_IN_PROCESS','001','Loan disbursement is in process, please wait.','N','setup',sysdate,'setup',sysdate);
commit;
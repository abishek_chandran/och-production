update TSTM set standard_text = 'Surat Pengakuan Hutang (SPH) pinjaman berbasis teknologi ini (beserta segala dokumen pendukungnya dan setiap perubahannya disebut sebagai SPH) dibuat dan ditandatangani oleh Perwakilan Pejabat Kredit PT Bank Raya Indonesia Tbk serta Penerima Pinjaman (Debitur) penerima Fasilitas Pinjaman Dana Tunai PINANG melalui format, perantara dan autorisasi secara automasi.' where text_type = 'TXM' and text_name = 'DPG1';


update TSTM set standard_text = 'Pemberi Pinjaman dalam hal ini PT Bank Raya Indonesia Tbk dalam hal ini diwakili oleh Perwakilan Pejabat Kredit selanjutnya disebut (BANK)' where text_type = 'TXM' and text_name = 'DTX3';


update TSTM set standard_text = 'Tunduk pada syarat dan ketentuan produk fasilitas “PINANG” dan ketentuan lainnya yang berlaku di Bank Raya' where text_type = 'TXM' and text_name = 'ATX3';


update TSTM set standard_text = 'Seluruh data dan informasi serta dokumen yang saya berikan melalui Aplikasi Online Fasilitas PINANG adalah milik Bank Raya dan tidak dapat diminta kembali' where text_type = 'TXM' and text_name = 'ATX4';


update TSTM set standard_text = 'Melunasi seluruh fasilitas pinjaman yang diterima dengan cara mengangsur melalui pemotongan upah ke Bank Raya setiap bulan sesuai dengan ketentuan sampai dengan fasilitas pinjaman dinyatakan lunas' where text_type = 'TXM' and text_name = 'ATX5';


update TSTM set standard_text = 'Bersedia dan menyatakan setuju untuk memberikan hak kepada Bank Raya untuk mempergunakan hak-hak pekerja saya termasuk namun tidak terbatas pada upah/gaji, pesangon,uang ganti kerugian maupun penerimaan lainnya yang dapat dipersamakan dengan itu guna memenuhi pembayaran angsuran fasilitas pinjaman PINANG sampai dengan fasilitas pinjaman dimaksud dinyatakan lunas.' where text_type = 'TXM' and text_name = 'ATX6';


update TSTM set standard_text = 'Memblokir/mendebet/mencairkan Kartu, Giro, Deposito atau Tabungan saya yang berada di Bank Raya dan Grup Usahanya guna pelunasan Fasilitas Pinjaman “PINANG”, dengan mengesampingkan berlakunya ketentuan pasal 1813 KUHPerdata' where text_type = 'TXM' and text_name = 'ATX8';


update TSTM set standard_text = 'Memberikan kuasa kepada Bank Raya untuk melakukan pendebetan ke rekening tabungan saya sebagai berikut:' where text_type = 'TXM' and text_name = 'AT10';


update TSTM set standard_text = 'Untuk keperluan pembayaran angsuran atas fasilitas pinjaman PINANG yang telah disetujui oleh Bank Raya termasuk dalam hal ini membayar biaya-biaya lainnya yang mungkin timbul dikemudian hari sebagai bagian dari kewajiban saya dalam pelunasan fasilitas pinjaman PINANG. Kuasa yang saya berikan berlaku sampai dengan fasilitas pinjaman PINANG dinyatakan lunas oleh Bank Raya.' where text_type = 'TXM' and text_name = 'AT11';

update TSTM set standard_text ='Bunga dari Fasilitas Pinjaman PINANG merupakan bunga harian berdasarkan Bunga Efektif setahun dibagi 365 (tiga ratus enam puluh lima) hari. Perhitungan bunga dihitung setiap hari dimulai dari tanggal Fasilitas Pinjaman PINANG sampai dengan Nasabah melunasi Fasilitas Pinjaman PINANG sesuai dengan jangka waktu Fasilitas Pinjaman PINANG dalam Perjanjian Kredit Digital. Informasi lebih lanjut mengenai ilustrasi perhitungan suku bunga Fasilitas Pinjaman PINANG dapat dilihat dalam media komunikasi resmi yang dikeluarkan oleh Bank dan/atau website Bank berikut : www.bankraya.co.id.' where text_type = 'TXM' and text_name = 'TT44';



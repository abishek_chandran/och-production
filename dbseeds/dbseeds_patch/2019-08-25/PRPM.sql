Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','CAMS_ENDPOINT_URL','/bem/v1/cams','CAMS endpoint url','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','SILVERLAKE_ACCT_INQ_ENDPOINT_URL','/bem/v1/cbs/inquirySACADetail','Silverlake Account Inquiry endpoint url','N','setup',
sysdate,'setup',sysdate);


Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','SILVERLAKE_CIF_INQ_ENDPOINT_URL','/bem/v1/cbs/inquiryCIFNew','Silverlake endpoint url','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','BANKCODES_ENDPOINT_URL','/bem/v1/bank-codes/','Endpoint url to get list of bank code and the description based on productCode','N','setup',
sysdate,'setup',sysdate);

-- new
SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','COOLDOWN_PERIOD_FOR_PAYROLL_REJECTION','1','Cooldown period in days for a system rejected payroll application , post will user will be allowed to apply again','N','setup',sysdate,'setup',sysdate);

-- new
SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','CREDIT_SCORE_ENDPOINT_URL','/cs/v1','Endpoint url for credit score system','N','setup',sysdate,'setup',sysdate);

-- old
update PRPM set property_val = '5' where property_name = 'CREDIT_SCR_CAL_TIME_INTRVL';

SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BANKNAME_044','BRI','Bank name to display on document signing','N','setup',sysdate,'setup',sysdate);

SET DEFINE OFF;
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','BANKNAME_055','BRI AGRO','Bank name to display on document signing','N','setup',sysdate,'setup',sysdate);

delete from VERT where ERR_CODE  IN ('211099','211100','211101');
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211099,'CUST','001','0','Please select Bank from a drop down.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211100,'CUST','001','0','Bank details are not available for the given product code.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211101,'CUST','001','0','Verified payroll data is not found.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

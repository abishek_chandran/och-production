
Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription",
"del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','BWY','IS_CUSTOMER_DEDUP_CHECK_ENABLED',
'Y','Is Customer Dedup check enabled. N for UAT. Y for production','N','setup',
sysdate,'setup',sysdate);
COMMIT;

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','OCR_VERIFICATION_FAIL_STATUS_PERIOD','1','Application Status form expiry period','N','setup',
sysdate,'setup',sysdate);
COMMIT;

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','OCR_VERIFICATION_FAIL_ALLOWED_COUNTER','3','Number of times OCR being allowed to Fail before marking it success for offline validation','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','IS_EKYC_ENABLED','Y','EKYC/Elements enable or disable(Y/N)','N','setup',
sysdate,'setup',sysdate);

Insert into PRPM ("db_ts","bank_id","app_id","property_name","property_val","propertydescription","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time")
values (1,'01','BWY','OCR_GOOGLE_VISION_KEY','AIzaSyDMIBAmpRWRVFmDfNgKR8ZfD2a-JHetjg0','Google Vision Key APIGEE','N','setup',
sysdate,'setup',sysdate);
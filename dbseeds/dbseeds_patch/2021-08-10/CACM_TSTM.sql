-- NUMBER 1 Splash screrns
update CACM set header = 'Pencairan pinjaman masuk di rekening Anda' where content_type ='SPL' and photo_key ='5';
update CACM set description = 'Tanggal bayar pinjaman dan cicilan akan di autodebet dan langsung mengikuti tanggal gajian Anda' where content_type ='SPL' and photo_key ='5';

-- NUMBER 2 and 3 are JRXML changes.

--NUMBER 4 Terms and Condition Register
update TSTM set standard_text ='1.	Kecuali ditentukan lain, dalam istilah-istilah dalam Syarat dan Ketentuan Umum ini dapat diartikan dan memiliki arti sebagai berikut:
a.	Angka Keamanan adalah password yang bersifat sementara yang dikeluarkan dan dikirimkan oleh Bank kepada nomor handphone yang terdaftar di Bank dan hanya dapat digunakan 1 (satu) kali dalam waktu terbatas untuk melakukan verifikasi.
b.	Angsuran adalah suatu jumlah uang yang wajib dibayar oleh Nasabah pada setiap tanggal jatuh tempo, yang besarannya sesuai dengan tagihan yang disampaikan oleh Bank berdasarkan ketentuan penggunaan Fasilitas Pinjaman PINANG.
c.	Aplikasi Online Pinjaman PINANG adalah suatu program berbentuk perangkat lunak yang berjalan pada suatu sistem tertentu yang berguna untuk membantu berbagai kegiatan yang sedang atau akan dilakukan. 
d.	Bank adalah PT Bank Rakyat Indonesia Agroniaga, Tbk yang selanjutnya disebut BRI Agro.
e.	Fasilitas Pinjaman PINANG adalah produk pinjaman berbasis teknologi yang diterbitkan oleh PT Bank Rakyat Indonesia Agroniaga, Tbk. yang diajukan oleh nasabah melalui Aplikasi Pinjaman Online PINANG. Fasilitas Pinjaman PINANG ini ditujukan bagi nasabah yang memiliki rekening baik di BRI maupun di BRI AGRO untuk memenuhi kebutuhan hidup nasabah, baik yang bersifat konsumtif maupun produktif.
f.	Tanda Tangan Elektronik (Digital Signature) adalah tanda tangan yang terdiri atas Informasi Elektronik yang dilekatkan, terasosiasi, atau terkait dengan Informasi Elektronik lainnya yang digunakan sebagai alat verifikasi dan autentikasi. Dalam hal ini Bank menggunakan tanda tangan elektronik tersertifikasi yang dikeluarkan oleh Penyelenggara Tanda Tangan Elektronik yang telah bekerjsama dengan Bank.
g.	Nasabah adalah pengguna Aplikasi Pinjaman Online PINANG yang mengajukan Fasilitas Pinjaman PINANG dan telah disetujui oleh BANK melalui Credit Risk Scoring System pada Aplikasi Pinjaman Online PINANG. 
h.	Pengguna adalah pengguna Aplikasi Pinjaman Online PINANG baik yang telah menjadi Nasabah maupun non Nasabah yang mengakses Aplikasi Pinjaman Online PINANG. 
i.	Surat Pengakuan Hutang adalah surat yang dibuat untuk 2 (dua) belah pihak antara Bank dengan Nasabah yang dibuat dan ditanda tangani menggunakan digital signature melalui Aplikasi PINANG yang menimbulkan akibat hukum berupa hubungan hutang piutang dan mewajibkan nasabah untuk membayar kembali pinjaman sesuai dengan kesepakatan yang telah ditetapkan dalam ketentuan Fasilitas Pinjaman PINANG yang terdiri dari Pokok, Bunga dan Denda (bila ada) sampai dengan batas waktu yang telah disepakati.
j.	Persetujuan Pinjaman adalah informasi persetujuan permohonan pinjaman yang diajukan oleh Nasabah yang dikeluarkan berdasarkan hasil Credit Risk Scoring System pada Aplikasi Pinjaman Online PINANG.
k.	Sisa Pinjaman (Outstanding) adalah sisa pokok pinjaman yang harus dibayarkan oleh nasabah sesuai dengan jangka waktu (tenor) yang telah ditentukan oleh nasabah.
l.	Tagihan adalah jumlah yang harus dibayarkan seperti pokok, bunga dan denda (jika ada) oleh nasabah sesuai dengan jangka waktu yang telah ditentukan dalam Surat Pengakuan Hutang.
m.	Tanggal Jatuh Tempo adalah tanggal batas akhir dimana pembayaran angsuran harus sudah diterima oleh Bank.
2.	Jika tidak ditentukan lain, rujukan dan penunjukan terhadap peraturan perundang-undangan termasuk juga pada rujukan dan penunjukan terhadap peraturan perundang-undangan yang diberlakukan kembali dan perubahannya dan peraturan yang lebih rendah yang dibuat sebagai peraturan pelaksana atas peraturan perundang-undangan tersebut.'
where user_type = '4' and text_type = 'TNC' and text_name = 'A.	PENGERTIAN DAN PENAFSIRAN';



--NUMBER 5 Terms and Condition Register
update TSTM set standard_text ='"1.	Pembayaran Angsuran Fasilitas Pinjaman PINANG 
Atas Fasilitas Pinjaman PINANG yang telah disetujui oleh Bank dan dinikmati oleh Nasabah, maka Nasabah wajib membayar kembali angsuran atas pinjaman yang diberikan sesuai dengan jumlah tagihan yang dapat dilihat oleh nasabah melalui Aplikasi Pinjaman Online PINANG milik nasabah maupun reminder notification yang disampaikan oleh Bank baik melalui media Layanan Pesan Singkat (SMS), Desk Call, maupun media lainnya yang ditetapkan oleh Bank sampai dengan seluruh kewajiban angsuran nasabah lunas sesuai dengan jangka waktu yang telah ditentukan.
Pembayaran angsuran Fasilitas PINANG akan dilakukan secara autodebet kepada rekening milik Nasabah yang sudah terdaftar pada Aplikasi Online Pinjaman PINANG pada tanggal yang telah ditentukan setiap bulannya. 
Berdasarkan hal tersebut diatas Nasabah memberikan kuasa persetujuan kepada bank untuk melakukan pendebetan/autodebet kepada rekening milik yang telah terdaftar didalam aplikasi PINANG sebesar cicilan yang harus dibayarkan tiap bulan sampai pinjaman telah selesai.
2.	Pembayaran di Muka (Pelunasan Maju) 
Nasabah dapat melunasi sisa terhutang dari Fasilitas Pinjaman PINANG lebih awal dari jangka waktu yang ditentukan. Pengajuan pelunasan maju dapat diajukan melalui Aplikasi Pinjaman Online PINANG. Nasabah akan dikenakan biaya pelunasan maju, yang besaran biaya pelunasan maju dihitung dengan memperhatikan sisa kewajiban bunga yang harus dibayarkan oleh Nasabah hingga berakhirnya sisa jangka waktu Fasilitas Pinjaman PINANG yang telah ditetapkan dalam Perjanjian Kredit Digital.
3.	Denda Keterlambatan 
Dalam hal pada saat tanggal jatuh tempo nasabah tidak menyiapkan dana yang cukup untuk melakukan pembayaran angsuran sebagaimana dimaksud pada butir E.1, maka nasabah akan dikenakan denda/biaya keterlambatan. Besaran biaya keterlambatan pembayaran angsuran ditetapkan sebesar 50 persen (lima puluh persen) dari bunga terhadap plafond pinjaman yang dibebankan kepada Nasabah Keterlambatan angsuran Fasilitas PINANG dapat memberikan dampak yang negatif pada kolektibilitas kredit Nasabah sesuai dengan ketentuan Otoritas Jasa Keuangan dan/atau Bank Indonesia dan tercatat dalam Sistem Informasi Debitur dan Sistem Layanan Informasi Keuangan (SLIK), juga berpengaruh pada pengajuan Fasilitas Kredit yang sedang dan/atau akan diajukan Nasabah baik pada Bank maupun lembaga jasa keuangan lainnya. Disamping itu, Bank akan melakukan penagihan atas tunggakan Fasilitas Pinjaman PINANG sesuai dengan mekanisme penagihan yang diatur dalam ketentuan Bank termasuk menggunakan jasa pihak ketiga dalam melakukan penagihan maupun menempuh jalur litigasi jika diperlukan. Pembebanan dan/atau pembayaran denda keterlambatan tidak menghilangkan kewajiban nasabah untuk membayar angsuran Fasilitas Pinjaman PINANG sesuai dengan tagihan yang dibebankan kepada nasabah.

4.	Biaya-Biaya Lain
Dalam proses pengajuan Fasilitas Pinjaman PINANG, nasabah dapat dikenakan biaya untuk pengiriman Angka Keamanan (One Time Password/OTP) yang besarannya sesuai dengan provider layanan telekomunikasi yang digunakan ooleh nasabah, adapun OTP akan diterima oleh nasabah dalam hal: 
a)	Registrasi 
Beban biaya pengiriman angka keamanan pada saat nasabah melakukan registrasi pada Aplikasi Online Pinjaman PINANG akan menjadi beban nasabah. 
b)	Proses Pembubuhan Tanda Tangan Elektronik
Beban biaya pengiriman angka keamanan untuk melakukan verifikasi proses pembubuhan Tangan Elektronik menjadi beban penyelenggara tanda tangan elektronik yang telah bekerjasama dengan Bank."'
where user_type = '4' and text_type = 'TNC' and text_name = 'E.	KEWAJIBAN PEMBAYARAN ANGSURAN';


--Number 5 Terms and Condition Accepted Loan Preview
update TSTM set standard_text ='Pembayaran angsuran Fasilitas  PINANG akan dilakukan secara autodebet kepada rekening milik Nasabah yang sudah terdaftar pada Aplikasi Online Pinjaman PINANG pada tanggal yang telah ditentukan setiap bulannya.'
where user_type = '4' and text_type = 'TXM' and text_name = 'TT34';

--Number 5 Terms and Condition Accepted Loan Preview
update TSTM set standard_text ='Berdasarkan hal tersebut diatas Nasabah memberikan kuasa persetujuan kepada bank untuk melakukan pendebetan / autodebet kepada rekening milik nasabah yang telah terdaftar didalam aplikasi PINANG sebesar cicilan yang harus dibayarkan tiap bulan sampai pinjaman telah selesai.'
where user_type = '4' and text_type = 'TXM' and text_name = 'TT35';


-- Number 6 Terms and Condition Accepted Loan Preview 
update TSTM set standard_text ='Selain pengenaan biaya untuk Angka Keamanan, nasabah juga dapat dikenakan biaya apabila nasabah setuju untuk disertakan pada asuransi jiwa. Besaran yang harus dibayarkan oleh nasabahadalah Rp. 50.000, -(lima puluh  ribu rupiah). Bank akan melakukan autodebet sejumlah besaran biaya asuransi dari rekening milik nasabah, sehingga nasabah  harus menyiapkan dana tersebut pada rekening, berikut minimal saldo mengendap sebesar Rp. 50.000, -(lima puluh ribu rupiah)  pada rekening. Biaya asuransi ini hanya perlu dibayarkan sekali per fasilitas pinjaman' where user_type = '4' and text_type = 'TXM' and text_name = 'TT43';

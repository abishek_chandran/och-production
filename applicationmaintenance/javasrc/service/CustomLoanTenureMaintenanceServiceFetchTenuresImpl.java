package com.infosys.custom.ebanking.applicationmaintenance.service;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestCriteriaVO;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalListInquiryTran;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomLoanTenureMaintenanceServiceFetchTenuresImpl extends AbstractLocalListInquiryTran {

	@Override
	protected void associateQueryParameters(FEBATransactionContext context, IFEBAValueObject objQueryCrit,
			IFEBAValueObject arg2, QueryOperator queryOperator) throws CriticalException {

		CustomLoanTenureInterestCriteriaVO customLoanTenureInterestEnqVO = (CustomLoanTenureInterestCriteriaVO) objQueryCrit;
		queryOperator.associate("lnRecId", customLoanTenureInterestEnqVO.getLnRecId());
		queryOperator.associate("bankId", context.getBankId());
	}

	@Override
	protected final void executeQuery(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		try {
			super.executeQuery(objContext, objInputOutput, objTxnWM);
		} catch (BusinessException be) {
			LogManager.logError(objContext, be);

		}
	}

	@Override
	public void postProcess(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {
		// Do nothing .

	}

	/**
	 * 
	 * Associating DAL CustomLoanTenureInterestDetailsListDAL.xml
	 * 
	 * @see CustomLoanTenureInterestDetailsListDAL.xml
	 */
	@Override
	public String getQueryIdentifier(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		return CustomEBQueryIdentifiers.CUSTOM_LOAN_TENURE_MAINTENANCE;

	}

	public FEBAValItem[] prepareValidationsList(FEBATransactionContext objContext, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) throws BusinessException, BusinessConfirmation, CriticalException {

		return new FEBAValItem[] {

		};
	}
}

package com.infosys.custom.ebanking.applicationmaintenance.service;

import com.infosys.custom.ebanking.applicationmaintenance.util.CustomLoanMaintenanceTableOperationUtil;
import com.infosys.custom.ebanking.contentmanagement.util.CustomCMMaintenanceReqdUtility;
import com.infosys.custom.ebanking.tao.CLTIConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanTenureInterestDetailsVO;
import com.infosys.custom.ebanking.user.validator.CustomLoanTenureInterestValidator;
import com.infosys.ebanking.applicationmaintenance.common.TxnTypeConstants;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.tmo.FEBATMOConstants;
import com.infosys.feba.framework.tmo.FEBATMOException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.transaction.workflow.IPopulateWorkflowVOUtility;
import com.infosys.feba.framework.types.primitives.FEBAUnboundChar;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.TransactionKey;
import com.infosys.feba.framework.types.valueobjects.BusinessInfoVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.FEBATMOCriteria;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValEngineConstants;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.util.MaintenanceReqdUtility;
import com.infosys.fentbase.framework.common.CAFrameworkErrorMappingHelper;
import com.infosys.fentbase.framework.common.OperationModeConstants;
import com.infosys.fentbase.types.TypesCatalogueConstants;
import com.infosys.fentbase.types.primitives.WorkflowOperation;

public class CustomLoanInterestMaintenanceServiceAddInterestImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject objInputOutput,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {

		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;

		return new FEBAValItem[] {

				new FEBAValItem(customLoanTenureInterestDetailsVO, new CustomLoanTenureInterestValidator(),
						FEBAValEngineConstants.INDEPENDENT),

		};

	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject objInputOutput, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;

		FEBATMOCriteria criteria = new FEBATMOCriteria();
		criteria.setMaintenanceRequired(CustomCMMaintenanceReqdUtility.getMaintenanceReqd(objContext));

		try {
			CustomLoanMaintenanceTableOperationUtil.processCLTI(objContext, customLoanTenureInterestDetailsVO,
					FEBATMOConstants.TMO_OPERATION_INSERT);

			addSuccessMessageToContext(objContext);
		} catch (FEBATMOException tmoExp) {
			int errorCode = CAFrameworkErrorMappingHelper
					.getMappedErrorCode(OperationModeConstants.CA_INSERT + tmoExp.getErrorCode());
			throw new BusinessException(objContext, FBAIncidenceCodes.TMO_EXP_USER_CREATE, errorCode, tmoExp);
		} catch (FEBATableOperatorException tblOperExp) {

			throw new CriticalException(objContext, FBAIncidenceCodes.TAO_EXP_USER_CREATE, tblOperExp.getMessage(),
					tblOperExp.getErrorCode());
		}

	}

	@Override
	protected void populateWorkflowVO(FEBATransactionContext pObjContext,
			IPopulateWorkflowVOUtility populateWorkflowVOUtility, IFEBAValueObject objInputOutput,
			IFEBAValueObject objTxnWM) {

		CustomLoanTenureInterestDetailsVO customLoanTenureInterestDetailsVO = (CustomLoanTenureInterestDetailsVO) objInputOutput;

		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;

		populateWorkflowVOUtility.setKeyField(CLTIConstants.LN_REC_ID,
				customLoanTenureInterestDetailsVO.getLnRecId().toString());
		populateWorkflowVOUtility.setKeyField(TxnTypeConstants.BANK_ID, ebContext.getBankId().toString());
		populateWorkflowVOUtility.setKeyField(CLTIConstants.TENOR,
				customLoanTenureInterestDetailsVO.getTenor().toString());
		populateWorkflowVOUtility.setModuleKey("CLTI");
		populateWorkflowVOUtility.setEntityType("CLTI");
		populateWorkflowVOUtility.setModuleID((EBankingConstants.BANK_USER_MODULE_ID).toString());
		populateWorkflowVOUtility.setCorpId(ebContext.getCorpId().getValue());
		populateWorkflowVOUtility.setWfOperation(new WorkflowOperation(EBankingConstants.INITIATE).toString());
		populateWorkflowVOUtility.setFuncCode(new FEBAUnboundChar(FEBATMOConstants.FUNC_CODE_INSERT).toString());
		populateWorkflowVOUtility.setTransactionType("LID");
		populateWorkflowVOUtility.setRmApprovalRequired(EBankingConstants.NO);
	}

	/**
	 * 
	 * Adding Success Message to the Context to be displayed on the screen
	 * 
	 * @param pObjContext
	 * @author Sahanaboggaram_n
	 * @since Feb 23, 2011 - 1:34:21 PM
	 */
	public static void addSuccessMessageToContext(FEBATransactionContext pObjContext) {
		EBTransactionContext ebContext = (EBTransactionContext) pObjContext;
		// Getting Transaction Key generated for the transaction
		final TransactionKey txnKey = ebContext.getApprovalTxnKey();
		BusinessInfoVO infoDetails = (BusinessInfoVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.BusinessInfoVO);
		// Checking if WorkFlow is enabled
		if (MaintenanceReqdUtility.getMaintenanceReqd(pObjContext).equals(EBankingConstants.NO)) {
			infoDetails.setCode(new FEBAUnboundInt(EBankingErrorCodes.RECORD_INSERTION_SUCCESSFUL));
		} else {
			infoDetails.setCode(new FEBAUnboundInt(EBankingErrorCodes.RECORD_SENT_FOR_APPROVAL));
			infoDetails.setParam(new AdditionalParam(EBankingConstants.TRAN_KEY,
					new FEBAUnboundString(String.valueOf(txnKey.getValue()))));
		}
		infoDetails.setDispMessage("Loan Interest Created Successfully.");
		infoDetails.setLogMessage("Log:Loan Interest Created Successfully.");
		pObjContext.addBusinessInfo(infoDetails);
	}
}

delete from CNET where notif_event in('LON_CLO','LON_PMT_REM','LON_PMT_DUE','LON_PMT_SUC');

--LON_PMT_SUC
Insert into CNET values (1,'01','LON_PMT_SUC','PINANG','Bayar Angsuran Berhasil	','Nasabah Yth,
Pembayaran angsuran PINANG Anda sebesar Rp$$AMOUNT1$$ BERHASIL. Terima kasih.','Pembayaran cicilan telah berhasil diambil dari rekening pencairan Anda. Terima kasih atas pembayaran cicilan pinjaman Anda.','Y','N','N','BPD1',sysdate,'BPD1',sysdate);

--LON_PMT_REM
Insert into CNET values (1,'01','LON_PMT_REM','PINANG','Pengingat Bayar Angsuran','Nasabah Yth,
Tanggal $$DATE1$$ adalah jatuh tempo bayar angsuran PINANG Anda, sebesar Rp$$AMOUNT1$$ Pastikan jumlah saldo rekening payroll Anda mencukupi sebelum pukul 17.00 WIB. ','','N','Y','N','BPD1',sysdate,'BPD1',sysdate);


--LON_PMT_DUE
Insert into CNET values (1,'01','LON_PMT_DUE','PINANG','Keterlambatan Bayar Angsuran','Nasabah Yth,
Saat ini Anda terlambat angsuran $$OTHER1$$ Hari. Total kewajiban PINANG Anda sebesar Rp$$AMOUNT1$$ Pastikan jumlah saldo rekening payroll Anda mencukupi untuk pembayaran angsuran dan pinalti keterlambatan sebelum pukul 17.00 WIB. ','Penarikan cicilan dari rek.pencairan gagal karena tidak ada cukup saldo. Segera isi saldo rekening Anda untuk menghindari denda keterlambatan harian lebih banyak.
','Y','Y','N','BPD1',sysdate,'BPD1',sysdate);

--LON_CLO
Insert into CNET values (1,'01','LON_CLO','PINANG','Pelunasan Pinjaman Berhasil','Nasabah Yth,
Pembayaran pinjaman PINANG Anda sebesar Rp$$AMOUNT1$$ BERHASIL. Pinjaman Anda telah LUNAS. Terima kasih.','Terima kasih atas pelunasan seluruh pinjaman Anda! Sebagai rasa terima kasih kami berikan cashback yang telah dikirimkan ke rekening pencairan Anda. Saat ini Anda dapat mengajukan pinjaman baru.','Y','N','N','BPD1',sysdate,'BPD1',sysdate);
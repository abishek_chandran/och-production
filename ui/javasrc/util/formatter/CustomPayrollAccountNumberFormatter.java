package com.infosys.custom.ebanking.ui.util.formatter;

import org.apache.commons.lang.StringUtils;

import com.infosys.feba.framework.cachemanager.ICacheManagerView;
import com.infosys.feba.framework.common.IContext;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.ui.common.AbstractFormatter;

public class CustomPayrollAccountNumberFormatter extends AbstractFormatter {
    /**
    * This method is used to return formatted field
    * 
     * @param String
    *            the value of the field which needs to be formatted.
    * 
    */

    

    /**
    * 
     * @see com.infosys.ebanking.ui.common.AbstractFormatter#format(java.lang.Object,
    *      com.infosys.feba.framework.common.IContext)
    */
    public String format(Object value, IContext context) throws Exception {
                    return value.toString();
    }

    /**
    * 
     * @see com.infosys.ebanking.ui.common.AbstractFormatter#format(com.infosys.feba.framework.types.IFEBAType,
    *      com.infosys.feba.framework.cachemanager.ICacheManagerView)
    */
    public String format(IFEBAType value, ICacheManagerView cache) {
    	 return value.toString();
                  
    }

	@Override
	public String format(String arg0, ICacheManagerView arg1) throws Exception {
    	String finalFormattedValue=null; 
    	if(arg0!="" && !arg0.isEmpty()){
        	finalFormattedValue=StringUtils.leftPad(arg0, 15, '0') ;    	
    		return finalFormattedValue;
    	}
		return finalFormattedValue;
    	
	}
}


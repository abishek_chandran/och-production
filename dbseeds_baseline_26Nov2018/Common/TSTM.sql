update TSTM set standard_text = 'Dokumen Syarat dan Ketentuan' where text_name = 'TH01' and text_type = 'TXM';
update TSTM set standard_text = 'Fasilitas Pinjamin Tunai Pinang' where text_name = 'TH02' and text_type = 'TXM';

update TSTM set standard_text = 'Dokumen Permohonan Fasilitas' where text_name = 'AH01' and text_type = 'TXM';
update TSTM set standard_text = 'Pinjaman Tunai Pinang' where text_name = 'AH02' and text_type = 'TXM';

update TSTM set standard_text = 'Perjanjian Elektronik Pemberian Fasilitas Pinjaman Tunai Pinang' where text_name = 'DHD1' and text_type = 'TXM';

update TSTM set standard_text = 'Fasilitas Pinjaman Tunai Pinang' where text_name ='TH02' and text_type = 'TXM';
update TSTM set standard_text = 'Syarat dan Ketentuan Fasilitas Pinjaman PINANG ("Syarat dan Ketentuan Umum") ini berlaku bagi Nasabah yang permohonan Fasilitas Pinjaman PINANG telah disetujui oleh Credit Risk Scoring System Aplikasi PINANG. Mohon Nasabah dapat membaca Syarat dan Ketentuan Umum ini dengan teliti. Dengan menyetujui syarat dan ketentuan ini, Anda memahami, menerima, dan menyetujui seluruh Syarat dan Ketentuan dan informasi yang telah disampaikan oleh Bank. Syarat dan Ketentuan yang dimaksud adalah sebagai berikut:' where text_name ='TT01' and text_type = 'TXM';
update TSTM set standard_text = 'Nomor Perjanjian : SPH/PINANG/@#@#@SEQUNCE_NUMBER@#@#@/@#@#@DATE_MM@#@#@/@#@#@DATE_YYYY@#@#@' where text_name='DTX2' and text_type = 'TXM';
commit;

update TSTM set standard_text='Tanpa mengesampingkan ketentuan-ketentuan lain dalam Syarat dan Ketentuan Umum ini (termasuk namun tidak terbatas, Bank berhak untuk melakukan peninjauan kembali dan meminta pembayaran atas segala atas Fasilitas Pinjaman PINANG yang wajib dibayar kembali apabila dimintakan), jika terjadi salah satu atau lebih hal-hal sebagai berikut ("Peristiwa Cidera Janji"), maka Bank berhak tanpa peringatan atau pemberitahuan sebelumnya kepada nasabah, untuk mengakhiri dan menuntut pembayaran dan pelunasan penuh dengan seketika dan sekaligus dari Nasabah atas jumlah-jumlah uang yang terhutang dan kewajiban-kewajiban Nasabah baik karena hutang pokok, bunga, dan biaya-biaya lainnya (jika ada), karena itu pemberitahuan dengan surat juru sita atau surat-surat lain yang berkekuatan serupa itu tidak diperlukan lagi, apabila:' where text_name='TT45' ;

update TSTM set standard_text='KUSA-KUSA' where text_name='TH31'; 
update TSTM set standard_text='RISIKO-RISIKO PADA FASILITAS PINJAMAN PINANG' where text_name='TH28'; 	

update TSTM set standard_text='Jumlah Pinjaman (IDR) : @#@#@LOAN_SANCTION_AMOUNT@#@#@ | Tenor Pinjaman : @#@#@LOAN_TENURE@#@#@ | Jatuh Tempo Angsuran : Tanggal @#@#@MONTHLY_DUE_DATE@#@#@ Setiap Bulan | Bunga (Efektif Annuitas): @#@#@LOAN_INTEREST_RATE@#@#@% per Bulan | Angsuran Per bulan : @#@#@LOAN_EMI_AMOUNT@#@#@ | Penalti Keterlambatan : 0.50% per hari dari Bunga Harian | Fasilitas Otomatis Pembayaran Angsuran : @#@#@AUTO_INSTALLMENT_PAYMENT@#@#@ | (* automasi system) : ' where text_type='TXM' and text_name='DTB3';
commit;
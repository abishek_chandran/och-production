--Home Ownership

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES1','001','MilikSendiri','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES2','001','RumahDinas ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES3','001','RumahDinas ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES4','001','SewaKontrak  ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RESH','RES5','001','SewaKontrak  ','N','SETUP',sysdate,'SETUP',sysdate);

--Marital Status

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','MENIKAH','001','Menikah','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','BELUM KAWIN','001','BelumMenikah','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','NS','001','BelumMenikah ','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MSTH','JANDA/DUDA','001','JandaDuda ','N','SETUP',sysdate,'SETUP',sysdate);


Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DEPN','0','001','0','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','LTP','P','001','Fully Paid','N','SETUP',sysdate,'SETUP',sysdate);


Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SD','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SMP','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','SMA','001','SD/SMP/SMU/SMK','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D1','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D2','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','D3','001','Akademi/Diploma','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S1','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S2','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','S3','001','S1/S2/S3','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','EDUH','NONE','001','TidakSekolah','N','SETUP',to_timestamp('2018-10-08 04:01:05.0','null'),'SETUP',to_timestamp('2018-10-08 04:01:05.0','null'));



--custom code type for access scheme on bank user creation screen
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRC','001','BRI Bank Admin for Divisi Kepatuhan','N','setup',sysdate,'setup',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAMS','BRM','001','BRI Bank Admin','N','setup',sysdate,'setup',sysdate);

--custom code type for authentication scheme on bank user creation screen
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CAUS','BUR','001','Bank User','N','setup',sysdate,'setup',sysdate);

--Removing all other authentication modes except password
delete from cocd where cm_code!='SPWD' and code_type='BATN' and bank_id='01';

update COCD set CD_DESC='Bank ANZ Indonesia' where CODE_TYPE='CCBK' and CM_CODE='ANZ';
update COCD set CD_DESC='Bank Bukopin' where CODE_TYPE='CCBK' and CM_CODE='BUKO';
update COCD set CD_DESC='Bank ICB Bumiputera' where CODE_TYPE='CCBK' and CM_CODE='ICB';
update COCD set CD_DESC='Bank Central Asia' where CODE_TYPE='CCBK' and CM_CODE='BCA';
update COCD set CD_DESC='Bank CIMB Niaga' where CODE_TYPE='CCBK' and CM_CODE='CIMB';
update COCD set CD_DESC='Bank Danamon Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BDI';
update COCD set CD_DESC='Bank ICBC Indonesia' where CODE_TYPE='CCBK' and CM_CODE='ICBC';
update COCD set CD_DESC='Bank Internasional Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BII';
update COCD set CD_DESC='Bank ICB Bumiputera' where CODE_TYPE='CCBK' and CM_CODE='MNDR';
update COCD set CD_DESC='Bank Mandiri' where CODE_TYPE='CCBK' and CM_CODE='BCA';
update COCD set CD_DESC='Bank Mega' where CODE_TYPE='CCBK' and CM_CODE='MEGA';
update COCD set CD_DESC='Bank Negara Indonesia 1946' where CODE_TYPE='CCBK' and CM_CODE='BNI';
update COCD set CD_DESC='PAN Indonesia Bank' where CODE_TYPE='CCBK' and CM_CODE='PIB';
update COCD set CD_DESC='Bak Rakyat Indonesia' where CODE_TYPE='CCBK' and CM_CODE='BRI';
update COCD set CD_DESC='Bank Permata' where CODE_TYPE='CCBK' and CM_CODE='BP';
update COCD set CD_DESC='CITIBANK' where CODE_TYPE='CCBK' and CM_CODE='CITI';
update COCD set CD_DESC='Bank OCBC NISP' where CODE_TYPE='CCBK' and CM_CODE='OCBC';
update COCD set CD_DESC='Standard Chartered Bank' where CODE_TYPE='CCBK' and CM_CODE='SCB';
update COCD set CD_DESC='Bank UOB Indonesia' where CODE_TYPE='CCBK' and CM_CODE='UOB';
update COCD set CD_DESC='BNI Syariah' where CODE_TYPE='CCBK' and CM_CODE='BNIS';
update COCD set CD_DESC='Bank Sinarmas' where CODE_TYPE='CCBK' and CM_CODE='BSIN';
update COCD set CD_DESC='AEON Credit Services' where CODE_TYPE='CCBK' and CM_CODE='OCBC';
update COCD set CD_DESC= concat('The Hongkong ','&',' Shanghai Bank') where CODE_TYPE='CCBK' and CM_CODE='THSB';

Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BUA','LPM','001','Loan Product Maintenance','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','BUA','LID','001','Loan Interest Details','N','setup',sysdate,'setup',sysdate);


delete from cocd where code_type='CCBK' and cm_code='OCBC' and bank_id='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time",
"r_cre_id","r_cre_time") values (1,'01','CCBK','OCBC','001','Bank OCBC NISP','N','setup',
sysdate,'setup',sysdate);
delete from cocd where code_type='CCBK' and cm_code='AEON' and bank_id='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time",
"r_cre_id","r_cre_time") values (1,'01','CCBK','AEON','001','AEON Credit Services','N','setup',
sysdate,'setup',sysdate);

delete from cocd where code_type='MST' AND BANK_ID='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MST','BELUM KAWIN','001','BELUM KAWIN','N','SETUP',to_timestamp('2018-11-01 23:49:54.0','null'),'SETUP',to_timestamp('2018-11-01 23:49:54.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MST','JANDA/DUDA','001','JANDA/DUDA','N','SETUP',to_timestamp('2018-11-01 23:49:54.0','null'),'SETUP',to_timestamp('2018-11-01 23:49:54.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MST','MENIKAH','001','MENIKAH','N','SETUP',to_timestamp('2018-11-01 23:49:54.0','null'),'SETUP',to_timestamp('2018-11-01 23:49:54.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','MST','NS','001','NOT SPECIFIED','N','SETUP',to_timestamp('2018-11-01 23:49:54.0','null'),'SETUP',to_timestamp('2018-11-01 23:49:54.0','null'));


update COCD set cd_desc='BANDUNG KOT.' where code_type='0100' and cm_code='0191' and bank_id='01';
update COCD set cd_desc='BEKASI KOT.' where code_type='0100' and cm_code='0198' and bank_id='01';
update COCD set cd_desc='Kota Bogor' where code_type='0100' and cm_code='0192' and bank_id='01';
update COCD set cd_desc='CIREBON KOT.' where code_type='0100' and cm_code='0194' and bank_id='01';
update COCD set cd_desc='KOTA SUKABUMI' where code_type='0100' and cm_code='0193' and bank_id='01';
update COCD set cd_desc='KOTA TASIKMALAYA' where code_type='0100' and cm_code='0195' and bank_id='01';
update COCD set cd_desc='MAGELANG KOT.' where code_type='0900' and cm_code='0995' and bank_id='01';
update COCD set cd_desc='PEKALONGAN KOT.' where code_type='0900' and cm_code='0993' and bank_id='01';
update COCD set cd_desc='SEMARANG KOT.' where code_type='0900' and cm_code='0991' and bank_id='01';
update COCD set cd_desc='TEGAL KOT.' where code_type='0900' and cm_code='0994' and bank_id='01';
update COCD set cd_desc='BLITAR KOT.' where code_type='1200' and cm_code='1296' and bank_id='01';
update COCD set cd_desc='KEDIRI KOT.' where code_type='1200' and cm_code='1297' and bank_id='01';
update COCD set cd_desc='KOTA MADIUN' where code_type='1200' and cm_code='1298' and bank_id='01';
update COCD set cd_desc='KOTA MALANG' where code_type='1200' and cm_code='1293' and bank_id='01';
update COCD set cd_desc='KOTA MOJOKERTO' where code_type='1200' and cm_code='1292' and bank_id='01';
update COCD set cd_desc='KOTA PASURUAN' where code_type='1200' and cm_code='1294' and bank_id='01';
update COCD set cd_desc='KOTA PROBOLINGGO' where code_type='1200' and cm_code='1295' and bank_id='01';
update COCD set cd_desc='KOTA SOLOK' where code_type='3400' and cm_code='3495' and bank_id='01';
update COCD set cd_desc='GORONTALO KOT.' where code_type='6300' and cm_code='6391' and bank_id='01';
update COCD set cd_desc='KOTA BIMA' where code_type='7100' and cm_code='7192' and bank_id='01';
update COCD set cd_desc='KOTA KUPANG' where code_type='7400' and cm_code='7491' and bank_id='01';
update COCD set cd_desc='JAYAPURA KOT.' where code_type='8200' and cm_code='8291' and bank_id='01';
update COCD set cd_desc='KOTA SORONG' where code_type='8400' and cm_code='8491' and bank_id='01';

delete from cocd where code_type='A465' and cm_code='F7348' and bank_id='01';
delete from cocd where code_type='A465' and cm_code='F7355' and bank_id='01';
delete from cocd where code_type='A837' and cm_code='D3323' and bank_id='01';
delete from cocd where code_type='A856' and cm_code='D0847' and bank_id='01';
delete from cocd where code_type='A856' and cm_code='E0984' and bank_id='01';
delete from cocd where code_type='A884' and cm_code='G6213' and bank_id='01';
delete from cocd where code_type='A905' and cm_code='G7673' and bank_id='01';
delete from cocd where code_type='B093' and cm_code='G7758' and bank_id='01';
delete from cocd where code_type='B096' and cm_code='F2638' and bank_id='01';
delete from cocd where code_type='B096' and cm_code='F2646' and bank_id='01';
delete from cocd where code_type='B244' and cm_code='F7636' and bank_id='01';
delete from cocd where code_type='B316' and cm_code='G6087' and bank_id='01';
delete from cocd where code_type='B327' and cm_code='G0002' and bank_id='01';
delete from cocd where code_type='B509' and cm_code='F9055' and bank_id='01';
delete from cocd where code_type='B606' and cm_code='F2713' and bank_id='01';
delete from cocd where code_type='B633' and cm_code='C1278' and bank_id='01';
delete from cocd where code_type='B637' and cm_code='F9284' and bank_id='01';
delete from cocd where code_type='B652' and cm_code='F4006' and bank_id='01';
delete from cocd where code_type='B758' and cm_code='C2650' and bank_id='01';
delete from cocd where code_type='B758' and cm_code='H1470' and bank_id='01';
delete from cocd where code_type='B758' and cm_code='C2642' and bank_id='01';
delete from cocd where code_type='B768' and cm_code='C2942' and bank_id='01';
delete from cocd where code_type='B837' and cm_code='E0418' and bank_id='01';
delete from cocd where code_type='B939' and cm_code='F3250' and bank_id='01';
delete from cocd where code_type='B950' and cm_code='C5250' and bank_id='01';
delete from cocd where code_type='C007' and cm_code='E6004' and bank_id='01';
delete from cocd where code_type='C044' and cm_code='C5913' and bank_id='01';
delete from cocd where code_type='C045' and cm_code='C5893' and bank_id='01';
delete from cocd where code_type='C054' and cm_code='C6023' and bank_id='01';
delete from cocd where code_type='C054' and cm_code='C6025' and bank_id='01';
delete from cocd where code_type='C170' and cm_code='C7325' and bank_id='01';
delete from cocd where code_type='C170' and cm_code='C7328' and bank_id='01';
delete from cocd where code_type='C197' and cm_code='C7680' and bank_id='01';
delete from cocd where code_type='C230' and cm_code='G6282' and bank_id='01';
delete from cocd where code_type='C277' and cm_code='G1960' and bank_id='01';
delete from cocd where code_type='C277' and cm_code='G1964' and bank_id='01';
delete from cocd where code_type='C281' and cm_code='C8905' and bank_id='01';
delete from cocd where code_type='C404' and cm_code='H1200' and bank_id='01';
delete from cocd where code_type='C556' and cm_code='G5321' and bank_id='01';
delete from cocd where code_type='C559' and cm_code='D1957' and bank_id='01';
delete from cocd where code_type='C581' and cm_code='D2430' and bank_id='01';
delete from cocd where code_type='C592' and cm_code='D2507' and bank_id='01';
delete from cocd where code_type='C611' and cm_code='D2741' and bank_id='01';
delete from cocd where code_type='C617' and cm_code='D2828' and bank_id='01';
delete from cocd where code_type='C622' and cm_code='D2919' and bank_id='01';
delete from cocd where code_type='C622' and cm_code='D3066' and bank_id='01';
delete from cocd where code_type='C635' and cm_code='D3076' and bank_id='01';
delete from cocd where code_type='C642' and cm_code='G8859' and bank_id='01';
delete from cocd where code_type='C666' and cm_code='F2700' and bank_id='01';
delete from cocd where code_type='C673' and cm_code='F9015' and bank_id='01';
delete from cocd where code_type='C673' and cm_code='F9020' and bank_id='01';
delete from cocd where code_type='C695' and cm_code='G5907' and bank_id='01';
delete from cocd where code_type='C723' and cm_code='D4062' and bank_id='01';
delete from cocd where code_type='C758' and cm_code='D4421' and bank_id='01';
delete from cocd where code_type='C864' and cm_code='H3875' and bank_id='01';
delete from cocd where code_type='C866' and cm_code='D5843' and bank_id='01';
delete from cocd where code_type='C877' and cm_code='H6446' and bank_id='01';
delete from cocd where code_type='C880' and cm_code='D6056' and bank_id='01';
delete from cocd where code_type='C881' and cm_code='G1099' and bank_id='01';
delete from cocd where code_type='C881' and cm_code='G1100' and bank_id='01';
delete from cocd where code_type='C881' and cm_code='G1103' and bank_id='01';
delete from cocd where code_type='C921' and cm_code='H6443' and bank_id='01';
delete from cocd where code_type='C926' and cm_code='E0882' and bank_id='01';
delete from cocd where code_type='C947' and cm_code='F0323' and bank_id='01';
delete from cocd where code_type='CCBK' and cm_code='MNDR' and bank_id='01';
delete from cocd where code_type='D001' and cm_code='F8680' and bank_id='01';
delete from cocd where code_type='D057' and cm_code='D8040' and bank_id='01';
delete from cocd where code_type='D081' and cm_code='G7907' and bank_id='01';
delete from cocd where code_type='D124' and cm_code='F9448' and bank_id='01';
delete from cocd where code_type='D125' and cm_code='G3152' and bank_id='01';
delete from cocd where code_type='D128' and cm_code='H5012' and bank_id='01';
delete from cocd where code_type='D210' and cm_code='I1398' and bank_id='01';
delete from cocd where code_type='D214' and cm_code='F9626' and bank_id='01';
delete from cocd where code_type='D240' and cm_code='E8780' and bank_id='01';
delete from cocd where code_type='D268' and cm_code='H0444' and bank_id='01';
delete from cocd where code_type='D277' and cm_code='H9350' and bank_id='01';
delete from cocd where code_type='D320' and cm_code='G3094' and bank_id='01';
delete from cocd where code_type='D324' and cm_code='F5201' and bank_id='01';
delete from cocd where code_type='D656' and cm_code='E8218' and bank_id='01';
delete from cocd where code_type='D736' and cm_code='F7413' and bank_id='01';
delete from cocd where code_type='D798' and cm_code='E6640' and bank_id='01';
delete from cocd where code_type='D869' and cm_code='E7391' and bank_id='01';
delete from cocd where code_type='D998' and cm_code='G6092' and bank_id='01';
delete from cocd where code_type='E020' and cm_code='G2086' and bank_id='01';
delete from cocd where code_type='E087' and cm_code='F0171' and bank_id='01';
delete from cocd where code_type='E137' and cm_code='F0899' and bank_id='01';
delete from cocd where code_type='E166' and cm_code='F1357' and bank_id='01';
delete from cocd where code_type='E166' and cm_code='F1358' and bank_id='01';
delete from cocd where code_type='E175' and cm_code='H6662' and bank_id='01';
delete from cocd where code_type='E240' and cm_code='F2465' and bank_id='01';
delete from cocd where code_type='E241' and cm_code='F2482' and bank_id='01';
delete from cocd where code_type='E242' and cm_code='F2497' and bank_id='01';
delete from cocd where code_type='E243' and cm_code='G3122' and bank_id='01';
delete from cocd where code_type='E243' and cm_code='F2515' and bank_id='01';
delete from cocd where code_type='E244' and cm_code='F2523' and bank_id='01';
delete from cocd where code_type='E244' and cm_code='F2529' and bank_id='01';
delete from cocd where code_type='E246' and cm_code='F2546' and bank_id='01';
delete from cocd where code_type='E247' and cm_code='F2555' and bank_id='01';
delete from cocd where code_type='E247' and cm_code='F2564' and bank_id='01';
delete from cocd where code_type='E271' and cm_code='F2797' and bank_id='01';
delete from cocd where code_type='E338' and cm_code='I0324' and bank_id='01';
delete from cocd where code_type='E338' and cm_code='I0320' and bank_id='01';
delete from cocd where code_type='E344' and cm_code='F9929' and bank_id='01';
delete from cocd where code_type='E346' and cm_code='H7378' and bank_id='01';
delete from cocd where code_type='E363' and cm_code='F7807' and bank_id='01';
delete from cocd where code_type='E449' and cm_code='F4884' and bank_id='01';
delete from cocd where code_type='E480' and cm_code='F5304' and bank_id='01';
delete from cocd where code_type='E482' and cm_code='H7681' and bank_id='01';
delete from cocd where code_type='E496' and cm_code='F5519' and bank_id='01';
delete from cocd where code_type='E503' and cm_code='F5608' and bank_id='01';
delete from cocd where code_type='E591' and cm_code='F7059' and bank_id='01';
delete from cocd where code_type='E595' and cm_code='H3993' and bank_id='01';
delete from cocd where code_type='E636' and cm_code='F7675' and bank_id='01';
delete from cocd where code_type='E636' and cm_code='F7676' and bank_id='01';
delete from cocd where code_type='E640' and cm_code='H5226' and bank_id='01';
delete from cocd where code_type='E672' and cm_code='F8164' and bank_id='01';
delete from cocd where code_type='E672' and cm_code='F8167' and bank_id='01';
delete from cocd where code_type='E748' and cm_code='I1009' and bank_id='01';
delete from cocd where code_type='E756' and cm_code='H9551' and bank_id='01';
delete from cocd where code_type='E815' and cm_code='G6380' and bank_id='01';
delete from cocd where code_type='E865' and cm_code='G0638' and bank_id='01';
delete from cocd where code_type='E901' and cm_code='G1053' and bank_id='01';
delete from cocd where code_type='E902' and cm_code='G1075' and bank_id='01';
delete from cocd where code_type='E906' and cm_code='G1120' and bank_id='01';
delete from cocd where code_type='E906' and cm_code='G1124' and bank_id='01';
delete from cocd where code_type='E907' and cm_code='G1139' and bank_id='01';
delete from cocd where code_type='E973' and cm_code='H3228' and bank_id='01';
delete from cocd where code_type='F022' and cm_code='G2647' and bank_id='01';
delete from cocd where code_type='F122' and cm_code='H4207' and bank_id='01';
delete from cocd where code_type='F138' and cm_code='G4170' and bank_id='01';
delete from cocd where code_type='F138' and cm_code='G4111' and bank_id='01';
delete from cocd where code_type='F139' and cm_code='G4113' and bank_id='01';
delete from cocd where code_type='F139' and cm_code='G4118' and bank_id='01';
delete from cocd where code_type='F139' and cm_code='G4195' and bank_id='01';
delete from cocd where code_type='F202' and cm_code='H4076' and bank_id='01';
delete from cocd where code_type='F232' and cm_code='G4974' and bank_id='01';
delete from cocd where code_type='F242' and cm_code='G5002' and bank_id='01';
delete from cocd where code_type='F303' and cm_code='H4970' and bank_id='01';
delete from cocd where code_type='F385' and cm_code='G6222' and bank_id='01';
delete from cocd where code_type='F406' and cm_code='G6465' and bank_id='01';
delete from cocd where code_type='F407' and cm_code='G6479' and bank_id='01';
delete from cocd where code_type='F411' and cm_code='G6520' and bank_id='01';
delete from cocd where code_type='F420' and cm_code='G6660' and bank_id='01';
delete from cocd where code_type='F512' and cm_code='G7432' and bank_id='01';
delete from cocd where code_type='F512' and cm_code='G7435' and bank_id='01';
delete from cocd where code_type='F512' and cm_code='G7440' and bank_id='01';
delete from cocd where code_type='F513' and cm_code='G7451' and bank_id='01';
delete from cocd where code_type='F513' and cm_code='G7457' and bank_id='01';
delete from cocd where code_type='F513' and cm_code='G7462' and bank_id='01';
delete from cocd where code_type='F515' and cm_code='G7477' and bank_id='01';
delete from cocd where code_type='F516' and cm_code='G7492' and bank_id='01';
delete from cocd where code_type='F516' and cm_code='G7500' and bank_id='01';
delete from cocd where code_type='F516' and cm_code='G7501' and bank_id='01';
delete from cocd where code_type='F516' and cm_code='G7503' and bank_id='01';
delete from cocd where code_type='F516' and cm_code='G7506' and bank_id='01';
delete from cocd where code_type='F517' and cm_code='G7515' and bank_id='01';
delete from cocd where code_type='F517' and cm_code='G7517' and bank_id='01';
delete from cocd where code_type='F517' and cm_code='G7519' and bank_id='01';
delete from cocd where code_type='F517' and cm_code='G7526' and bank_id='01';
delete from cocd where code_type='F520' and cm_code='G7572' and bank_id='01';
delete from cocd where code_type='F520' and cm_code='G7575' and bank_id='01';
delete from cocd where code_type='F521' and cm_code='G7581' and bank_id='01';
delete from cocd where code_type='F522' and cm_code='G7593' and bank_id='01';
delete from cocd where code_type='F523' and cm_code='G7605' and bank_id='01';
delete from cocd where code_type='F523' and cm_code='G7609' and bank_id='01';
delete from cocd where code_type='F524' and cm_code='G7622' and bank_id='01';
delete from cocd where code_type='F524' and cm_code='G7631' and bank_id='01';
delete from cocd where code_type='F524' and cm_code='G7634' and bank_id='01';
delete from cocd where code_type='F525' and cm_code='G7650' and bank_id='01';
delete from cocd where code_type='F600' and cm_code='G8363' and bank_id='01';
delete from cocd where code_type='F625' and cm_code='H8989' and bank_id='01';
delete from cocd where code_type='F628' and cm_code='G8664' and bank_id='01';
delete from cocd where code_type='F674' and cm_code='H5091' and bank_id='01';
delete from cocd where code_type='F765' and cm_code='H6425' and bank_id='01';
delete from cocd where code_type='F821' and cm_code='H7794' and bank_id='01';
delete from cocd where code_type='F823' and cm_code='H0916' and bank_id='01';
delete from cocd where code_type='F948' and cm_code='H2185' and bank_id='01';
delete from cocd where code_type='G163' and cm_code='H4671' and bank_id='01';
delete from cocd where code_type='G163' and cm_code='H4672' and bank_id='01';
delete from cocd where code_type='G163' and cm_code='H4673' and bank_id='01';
delete from cocd where code_type='G167' and cm_code='H4730' and bank_id='01';
delete from cocd where code_type='G167' and cm_code='H4731' and bank_id='01';
delete from cocd where code_type='G167' and cm_code='H4732' and bank_id='01';
delete from cocd where code_type='G167' and cm_code='H4733' and bank_id='01';
delete from cocd where code_type='G211' and cm_code='H5460' and bank_id='01';
delete from cocd where code_type='G287' and cm_code='H6464' and bank_id='01';
delete from cocd where code_type='G287' and cm_code='H6465' and bank_id='01';
delete from cocd where code_type='G344' and cm_code='H6653' and bank_id='01';
delete from cocd where code_type='G350' and cm_code='H6703' and bank_id='01';
delete from cocd where code_type='G390' and cm_code='H6938' and bank_id='01';
delete from cocd where code_type='G452' and cm_code='H7833' and bank_id='01';
delete from cocd where code_type='G721' and cm_code='H9509' and bank_id='01';
delete from cocd where code_type='G721' and cm_code='H9508' and bank_id='01';
delete from cocd where code_type='G759' and cm_code='H9810' and bank_id='01';
delete from cocd where code_type='G759' and cm_code='H9811' and bank_id='01';
delete from cocd where code_type='G783' and cm_code='H9955' and bank_id='01';
delete from cocd where code_type='G836' and cm_code='I0390' and bank_id='01';
delete from cocd where code_type='G876' and cm_code='I0650' and bank_id='01';
delete from cocd where code_type='G882' and cm_code='I0684' and bank_id='01';
delete from cocd where code_type='G922' and cm_code='I0974' and bank_id='01';
delete from cocd where code_type='G954' and cm_code='I1225' and bank_id='01';
delete from cocd where code_type='A922' and cm_code='H4013' and bank_id='01';
delete from cocd where code_type='B382' and cm_code='F8788' and bank_id='01';
delete from cocd where code_type='B637' and cm_code='F9297' and bank_id='01';
delete from cocd where code_type='B957' and cm_code='G7700' and bank_id='01';
delete from cocd where code_type='C103' and cm_code='H2579' and bank_id='01';
delete from cocd where code_type='C273' and cm_code='H7123' and bank_id='01';
delete from cocd where code_type='D199' and cm_code='I0593' and bank_id='01';
delete from cocd where code_type='E471' and cm_code='H8392' and bank_id='01';
delete from cocd where code_type='E752' and cm_code='G7225' and bank_id='01';
delete from cocd where code_type='E815' and cm_code='H1256' and bank_id='01';
delete from cocd where code_type='A922' and cm_code='G5408' and bank_id='01';
delete from cocd where code_type='B382' and cm_code='C7834' and bank_id='01';
delete from cocd where code_type='B637' and cm_code='E7449' and bank_id='01';
delete from cocd where code_type='B957' and cm_code='G4813' and bank_id='01';
delete from cocd where code_type='C103' and cm_code='D3136' and bank_id='01';
delete from cocd where code_type='C273' and cm_code='F3548' and bank_id='01';
delete from cocd where code_type='D199' and cm_code='E8767' and bank_id='01';
delete from cocd where code_type='E471' and cm_code='G3141' and bank_id='01';
delete from cocd where code_type='E752' and cm_code='G5653' and bank_id='01';
delete from cocd where code_type='E815' and cm_code='G6377' and bank_id='01';
delete from cocd where code_type='B637' and cm_code='F3576' and bank_id='01';
delete from cocd where code_type='C273' and cm_code='F7206' and bank_id='01';
commit;

DELETE FROM COCD where code_type='SGMC';

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','1000000|3000000','001','D','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','3000001|5000000','001','C2','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','5000001|7000000','001','C1','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','7000001|10000000','001','B','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','10000001|15000000','001','A2','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','15000001','001','A1','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','APRC','CREATE_APP','001','3|10','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','APRC','ATM_INQUIRY','001','3|10','N','SETUP',sysdate,'SETUP',sysdate);
COMMIT;




--Residential Status
DELETE FROM COCD WHERE CODE_TYPE='RES' and BANK_ID='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','RES1','001','Milik Sendiri','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','RES2','001','Mililk Orang Tua','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','RES3','001','Milik Keluarga','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','RES4','001','Kontrak / Sewa','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RES','RES5','001','Lainnya','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));


--Relations

DELETE FROM COCD WHERE CODE_TYPE='RLT' and BANK_ID='01';
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT1','001','Kakak/Adik Kandung','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT2','001','Orang Tua','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT3','001','Sepupu','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT4','001','Kemenakan','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT5','001','Ipar','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','RLT','RLT6','001','Lainnya','N','SETUP',to_timestamp('0001-01-01 00:00:00.0','null'),'SETUP',to_timestamp('0001-01-01 00:00:00.0','null'));


--LoanStatusUpdate Batch
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','ASSF','KTP_SAVED|PER_SAVED|CON_SAVED','001','Application Status Saved Form Status','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','CSS','CR_SCORE_SUB|CR_SCORE_APR','001','Credit Score Status','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','DIS','DISB_ACC_CONF|EKYC_COM|DIG_SIGN_COM|DOCUMENT_SIGNED','001','Disbursement Incomplete Status','N','SETUP',sysdate,'SETUP',sysdate);

--OCR verification failed case handling
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','ASVS','OCR_VER_FAIL','001','1','N','setup',sysdate,'setup',sysdate);
COMMIT;

update COCD set cd_desc='19|TABLE|NULL|DHD4|LOAN_SANCTION_AMOUNT;LOAN_SCHEDULE_START_DATE;LOAN_SCHEDULE_END_DATE;MONTHLY_DUE_DATE;LOAN_INTEREST_RATE;LOAN_EMI_AMOUNT;LOAN_EMI_PENALTY;AUTO_INSTALLMENT_PAYMENT;LOAN_TENURE' where code_type='DBTL' and cm_code='DTB3';
COMMIT;
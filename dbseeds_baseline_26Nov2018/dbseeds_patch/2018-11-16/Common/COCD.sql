DELETE FROM COCD where code_type='SGMC';

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','1000000|3000000','001','D','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','3000001|5000000','001','C2','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','5000001|7000000','001','C1','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','7000001|10000000','001','B','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','10000001|15000000','001','A2','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','SGMC','15000001','001','A1','N','SETUP',sysdate,'SETUP',sysdate);

Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','APRC','CREATE_APP','001','3|10','N','SETUP',sysdate,'SETUP',sysdate);
Insert into COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','APRC','ATM_INQUIRY','001','3|10','N','SETUP',sysdate,'SETUP',sysdate);
COMMIT;



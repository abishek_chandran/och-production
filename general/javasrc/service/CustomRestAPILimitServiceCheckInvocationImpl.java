package com.infosys.custom.ebanking.general.service;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.infosys.custom.ebanking.tao.CRITTAO;
import com.infosys.custom.ebanking.tao.info.CRITInfo;
import com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.Number;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;

public class CustomRestAPILimitServiceCheckInvocationImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void process(FEBATransactionContext txnContext, IFEBAValueObject inOutObject, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {

		CRITTAO critTAO = new CRITTAO(txnContext);
		CustomRestAPILimitInOutVO customRestAPIInOutVO = (CustomRestAPILimitInOutVO) inOutObject;
		IFEBAType restrictionConfig = AppDataManager.getValue(txnContext, "CommonCodeCache",
				"CODE_TYPE=APRC|CM_CODE=" + customRestAPIInOutVO.getApplicationKey());
	 
		if (null != restrictionConfig) {
			String sRestrictionConfig = restrictionConfig.toString();
			String[] restConfigArray = sRestrictionConfig.split("\\|");
			if (restConfigArray.length == 2) {
				Number allowedTries = new Number(restConfigArray[0]);
				long cooldownMinutes = Long.parseLong(restConfigArray[1]);

				critTAO.associateUserId(txnContext.getUserId());
				critTAO.associateBankId(txnContext.getBankId());
				critTAO.associateApiKey(customRestAPIInOutVO.getApplicationKey());

				CRITInfo critInfo = null;
				try {
					critInfo = critTAO.select(txnContext, txnContext.getBankId(), txnContext.getUserId(),
							customRestAPIInOutVO.getApplicationKey());
				} catch (FEBATableOperatorException e) {
					// No records found for API Key
				}
				if (null != critInfo) {
					Date lastInvocationTime = critInfo.getCookie().getRModTime().getValue();
					Date currentTime = new java.util.Date(System.currentTimeMillis());
					long timeDifference = currentTime.getTime() - lastInvocationTime.getTime();
					long minLapsed = TimeUnit.MINUTES.convert(timeDifference, TimeUnit.MILLISECONDS);
					if (critInfo.getInvocationCnt().equals(allowedTries)) {
						if (minLapsed > cooldownMinutes) {
							critTAO.associateInvocationCnt(new Number(1));
						} else {
							throw new BusinessException(true, txnContext, "APIIC001",
									"You have exceeded the maximum allowed retries for this API call.Please try after some time",
									null, 211091, null, null);
						}
					} else {
						if (minLapsed > cooldownMinutes) {
							critTAO.associateInvocationCnt(new Number(1));
						} else {
							critInfo.getInvocationCnt().add(1);
							critTAO.associateInvocationCnt(critInfo.getInvocationCnt());
						}

					}
					critTAO.associateCookie(critInfo.getCookie());
				} else {
					critTAO.associateInvocationCnt(new Number(1));
				}
				try {
					if (null != critInfo) {
						critTAO.update(txnContext);
					} else {
						critTAO.insert(txnContext);
					}
				} catch (FEBATableOperatorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}

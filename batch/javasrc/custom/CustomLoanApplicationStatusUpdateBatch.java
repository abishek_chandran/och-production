package com.infosys.custom.ebanking.batch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationStatusInOutVO;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractBusinessProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.IFEBAList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;

public class CustomLoanApplicationStatusUpdateBatch extends AbstractBusinessProcessBatch {

	public static final String ID = "APPSTATUSUPDATE";
	public static final String BATCH_SERVICE = "CustomLoanApplicationMaintenanceService";
	public static final String BATCH_METHOD = "updateStatus";
	private final EBBatchUtility ebBatchUtil = new EBBatchUtility();
	
	FEBADate lastUpdatedDate = new FEBADate();

	public CustomLoanApplicationStatusUpdateBatch() {
		super();
		this.setId(ID);
		this.setRecordLevelCommit(true);
		this.setSingleThread(true);	
	}
	private void getLastModifiedDate(FEBATransactionContext objTxnContext, String prpmPeriod) {
		int statusPeriod = Integer.parseInt(prpmPeriod);
		GregorianCalendar cal = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		cal.add(Calendar.DAY_OF_MONTH, -statusPeriod);

		Date lastDate = cal.getTime();
		String pastDate = sdf.format(lastDate);

		Date modifiedDate = new Date();
		try {
			modifiedDate = (Date) sdf.parseObject(pastDate);

		} catch (ParseException e1) {
			LogManager.logError(objTxnContext, e1);

		}
		lastUpdatedDate = new FEBADate(modifiedDate);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void processBatch(FEBABatchContext context, IFEBAValueObject arg1, Object arg2, ReadOnlyUserParams arg3)
			throws CriticalException  {
		CustomApplicationStatusInOutVO applnStatusVO = (CustomApplicationStatusInOutVO)arg1;
		FEBATransactionContext objContext = ebBatchUtil.populateTransactionContext(context);
		try {
			System.out.println("Processing Batch Records for application Id - "+applnStatusVO.getApplicationId().getValue());
			LocalServiceUtil.invokeService(objContext,(BATCH_SERVICE),new FEBAUnboundString(BATCH_METHOD), applnStatusVO);
			System.out.println("Processing finished for application Id - "+applnStatusVO.getApplicationId().getValue());
		} catch (BusinessException | FEBATypeSystemException e) {
			LogManager.logError(objContext, e, "ERROR");
		} catch (BusinessConfirmation e) {
			LogManager.logDebug(objContext, e.getMessage());
		} 
	}

	@Override
	public void checkUsage(ReadOnlyUserParams arg0) throws CriticalException {
		// this methos not used
	}


	@Override
	protected IGenericInputData getListOfRecordsToProcess(FEBABatchContext objTxnContext, ReadOnlyUserParams arg1)
			throws BusinessConfirmation, CriticalException, BusinessException {
		IGenericInputData inputData = new GenericInputData();
		FEBATransactionContext objContext = ebBatchUtil.populateTransactionContext(objTxnContext);

		try{
			inputData.addItems(getApplicationRecords(objContext, "ASSF", "APP_STATUS_FORM_EXPIRY_PERIOD"));
			inputData.addItems(getApplicationRecords(objContext, "CSS", "CR_SCORE_STATUS_EXPIRY_PERIOD"));
			inputData.addItems(getApplicationRecords(objContext, "DIS", "DISBURSEMENT_INCOMPLETE_STATUS_PERIOD"));
			inputData.addItems(getApplicationRecords(objContext, "OCR", "OCR_VERIFICATION_FAIL_STATUS_PERIOD"));
			inputData.addItems(getApplicationRecords(objContext, "PRS", "PAYROLL_REJ_STATUS_EXPIRY_PERIOD"));
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		System.out.println("Final inputData - "+inputData.getItemList().toString());
		return inputData;
	}


	@SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	private FEBAArrayList<CustomApplicationStatusInOutVO> getApplicationRecords(FEBATransactionContext objContext, String codeType, String propertyName) throws CriticalException{

		CommonCodeVO cmCodeVO = (CommonCodeVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CommonCodeVO);
		FEBAArrayList<CustomApplicationStatusInOutVO> applicationStatusList = new FEBAArrayList();

		FEBAArrayList appStatusList = new FEBAArrayList();
		

		cmCodeVO.setBankID(objContext.getBankId());
		cmCodeVO.setLanguageID(objContext.getLangId());
		cmCodeVO.setCodeType(codeType);

		IFEBAList commonCodeCacheList = new FEBAArrayList();

		try {
			commonCodeCacheList = AppDataManager.getList(objContext, FBAAppDataConstants.COMMONCODE_CACHE, cmCodeVO);
		} catch (CriticalException e) {
			LogManager.log(objContext, "CriticalException thrown from HostCPMessageHelper.getCodeDesc",
					"CriticalException thrown from HostCPMessageHelper.getCodeDesc");
			LogManager.logDebug(objContext, "Business Confirmation thrown from HostCPMessageHelper.getCodeDesc");
		}
		cmCodeVO.setBankID(objContext.getBankId());
		cmCodeVO.setLanguageID(objContext.getLangId());
		CommonCodeVO codeVO = (CommonCodeVO) commonCodeCacheList.get(0);

		StringTokenizer tokens = new StringTokenizer(codeVO.getCommonCode().getValue(),"|");

		while (tokens.hasMoreTokens()) {
			CommonCode applicationStatusCode = new CommonCode(tokens.nextToken());
			appStatusList.add(applicationStatusCode);
		}
		
		System.out.println("getLastModifiedDate - "+propertyName);
		
		String appStatusPeriod = PropertyUtil.getProperty(propertyName, objContext);
		getLastModifiedDate(objContext, appStatusPeriod);

		QueryOperator operator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_FETCH_APPLICATION_STATUS_LIST);
		operator.associate("bankId", objContext.getBankId());
		operator.associate("applicationStatus", appStatusList);
		operator.associate("lastModifiedDate", lastUpdatedDate);

		try {
			applicationStatusList = operator.fetchList(objContext);
		} catch (DALException e) {
			LogManager.logError(objContext, e);
			e.printStackTrace();
		}
		System.out.println(cmCodeVO.getCommonCode().getValue()+" - applicationStatusList - "+applicationStatusList.toString());
		return applicationStatusList;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected IReportMessageVO getRecordErrorMessage(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List getRecordErrorMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {

		return new ArrayList<>();
	}
	@SuppressWarnings("rawtypes")
	@Override
	protected IReportMessageVO getRecordSuccessMessage(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {

		return new ArrayList<>();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected String getSuccessLogMessage(FEBABatchContext arg0, IFEBAValueObject arg1)
			throws BusinessException, CriticalException {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext arg0, IFEBAValueObject arg1, Object arg2)
			throws BusinessException, CriticalException {

		return new FEBAValItem[] {

		};
	}
}

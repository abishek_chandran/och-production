package com.infosys.custom.ebanking.batch;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.tao.CLATTAO;
import com.infosys.custom.ebanking.tao.CNCTTAO;
import com.infosys.custom.ebanking.tao.DVDTTAO;
import com.infosys.custom.ebanking.tao.info.CLATInfo;
import com.infosys.custom.ebanking.tao.info.CNCTInfo;
import com.infosys.custom.ebanking.tao.info.DVDTInfo;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationStatusInOutVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomBatchPopupNotificationInOutVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomEmailBatchDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSmsBatchDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.VOFactory;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.feba.framework.batch.FEBABatchContext;
import com.infosys.feba.framework.batch.IGenericInputData;
import com.infosys.feba.framework.batch.config.BatchConfig;
import com.infosys.feba.framework.batch.config.ReadOnlyBatchParams;
import com.infosys.feba.framework.batch.impl.GenericInputData;
import com.infosys.feba.framework.batch.impl.ReadOnlyUserParams;
import com.infosys.feba.framework.batch.pattern.AbstractBusinessProcessBatch;
import com.infosys.feba.framework.batch.util.EBBatchUtility;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.service.LocalServiceUtil;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.IFEBAList;
import com.infosys.feba.framework.types.primitives.CommonCode;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.FreeText;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.CommonCodeVO;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.types.valueobjects.IReportMessageVO;
import com.infosys.feba.framework.types.valueobjects.ReportMessageVO;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.fentbase.core.cache.FBAAppDataConstants;
import com.infosys.fentbase.tao.CUSRTAO;
import com.infosys.fentbase.tao.info.CUSRInfo;

public class CustomAppNotificationGenBatch extends AbstractBusinessProcessBatch {

	/*
	 * Batch Program Identifier
	 */
	public static final String ID = "CUSTOMAPPNOTOCH";
	private EBBatchUtility eBBatchUtil = new EBBatchUtility();

	private static final String BATCH_SMS_SERVICE = "CustomBatchSmsNotificationMaintenanceService";
	private static final String BATCH_EMAIL_SERVICE = "CustomBatchEmailNotificationMaintenanceService";
	private static final String BATCH_POPUP_SERVICE = "CustomBatchPopUpNotificationMaintenanceService";

	private static final String BATCH_SMS_METHOD = "sendSms";
	private static final String BATCH_EMAIL_METHOD = "sendEmail";
	private static final String BATCH_POPUP_METHOD = "sendPopUp";
	private final EBBatchUtility ebBatchUtil = new EBBatchUtility();

	int incompleteAppValPeriod = 0;

	public static final String MESSAGE = "No records fetched";

	/**
	 * Default Constructor instantiation
	 *
	 * 
	 */
	public CustomAppNotificationGenBatch() {
		super();
		this.setId(ID);
		this.setSingleThread(true);
	}

	/**
	 * This method is used to process batch based on VO details.
	 * 
	 * @return
	 * @throws CriticalException
	 * 
	 * 
	 */
	@Override
	protected void processBatch(FEBABatchContext batchContext, IFEBAValueObject VO, Object arg2,
			ReadOnlyUserParams userInputs) throws CriticalException {
		System.out.println("In CustomAppNotificationGenBatch Process batch");

		FEBATransactionContext objContext = eBBatchUtil.populateTransactionContext(batchContext);
		CustomLoanApplnMasterDetailsVO detailsVO = (CustomLoanApplnMasterDetailsVO) VO;
		
		CustomSmsBatchDetailsVO smsBatchDetailsVO = (CustomSmsBatchDetailsVO) VOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomSmsBatchDetailsVO);

		CustomEmailBatchDetailsVO emailBatchDetailsVO = (CustomEmailBatchDetailsVO) VOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomEmailBatchDetailsVO);

		CustomBatchPopupNotificationInOutVO popupBatchDetailsVO = (CustomBatchPopupNotificationInOutVO) VOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomBatchPopupNotificationInOutVO);

		String incompleteAppValidityPeriod = PropertyUtil.getProperty("ALLOWED_PERIOD_FOR_INCOMPLETE_APP",
				objContext);
		incompleteAppValPeriod = Integer.valueOf(incompleteAppValidityPeriod);

		CNCTInfo cnctInfo = null;

		try {

			if (null == detailsVO) {
				throw new BusinessException(objContext, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}
			
			System.out.println("In CustomAppNotificationGenBatch Process batch::detailsVO::Application ID::"+detailsVO.getApplicationId());

			CommonCode appId = new CommonCode(
					PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, objContext));

			Date rModTime = detailsVO.getRModTime().getValue();
			System.out.println("rModTime::" + rModTime);
			Date currentTime = new java.util.Date(System.currentTimeMillis());
			System.out.println("currentTime::" + currentTime);
			long diff = currentTime.getTime() - rModTime.getTime();
			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			System.out.println("days::" + days);
			//long days1 = TimeUnit.MILLISECONDS.toDays(days);
			//System.out.println("days1::" + days1);
			//FEBAUnboundInt daysInCurrentStatus = new FEBAUnboundInt((int) days1);
			//System.out.println("daysInCurrentStatus::" + daysInCurrentStatus);
			int numOfDays = (int) days;
			System.out.println("numOfDays::" + numOfDays);
			cnctInfo = CNCTTAO.select(objContext, objContext.getBankId(),
					new CommonCode(detailsVO.getApplicationStatus().getValue()), appId);			
			if (null != cnctInfo) {
				if (null !=detailsVO && detailsVO.getApplicationStatus().getValue().equalsIgnoreCase(cnctInfo.getAppNotifStatus().getValue())) {
					List<String> arraysList = fetchListOfConfigs(cnctInfo.getNumDaysNotifType().getValue());
					for (String numDaysStr : arraysList) {
						System.out.println("cnctarraysList::" + numDaysStr);
						String[] arrayOfConfigs = numDaysStr.split("\\|");
						System.out.println("arrayOfConfigs::" + arrayOfConfigs);
						int findNum = findNumInString(arrayOfConfigs[0]);
						System.out.println("findNum::" + findNum);
						for (int i = 1; i < arrayOfConfigs.length; i++) {
							// Check If number of days are equal to configuration.
							if (findNum == numOfDays) {
								// POPUP Delivery logic
								if (arrayOfConfigs[i].toString().equalsIgnoreCase("P")) {
										System.out.println("calling popup Service Impl");
										popupBatchDetailsVO = populatePopupUsers(objContext, popupBatchDetailsVO,
												detailsVO, cnctInfo);
										if (popupBatchDetailsVO != null) {
											popupBatchDetailsVO.setNotificationType(arrayOfConfigs[i].toString());	
											LocalServiceUtil.invokeService(objContext, (BATCH_POPUP_SERVICE),
													new FEBAUnboundString(BATCH_POPUP_METHOD), popupBatchDetailsVO);
										}
										updateCLATRmodTime(objContext, detailsVO);
								}
								
								// SMS Delivery logic
								if (arrayOfConfigs[i].toString().equalsIgnoreCase("S")) {
									System.out.println("calling SMS Service Impl");
										smsBatchDetailsVO = populateSMSDelivery(objContext, smsBatchDetailsVO, detailsVO, cnctInfo);
										if (smsBatchDetailsVO != null) {
											smsBatchDetailsVO.setNotificationType(arrayOfConfigs[i].toString());	
											LocalServiceUtil.invokeService(objContext, (BATCH_SMS_SERVICE),
													new FEBAUnboundString(BATCH_SMS_METHOD), smsBatchDetailsVO);
										}
										updateCLATRmodTime(objContext, detailsVO);
								}
								
								// Email Delivery logic
								if (arrayOfConfigs[i].toString().equalsIgnoreCase("E")) {
									System.out.println("calling Email Service Impl");
										emailBatchDetailsVO = populateEmailDelivery(objContext, emailBatchDetailsVO,
												detailsVO, cnctInfo);
										if (emailBatchDetailsVO != null) {
											emailBatchDetailsVO.setNotificationType(arrayOfConfigs[i].toString());
											LocalServiceUtil.invokeService(objContext, (BATCH_EMAIL_SERVICE),
													new FEBAUnboundString(BATCH_EMAIL_METHOD), emailBatchDetailsVO);
										}
										updateCLATRmodTime(objContext, detailsVO);
								}
							} // End of condition for number of days.
						}
					}
				}
			}
			// }
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logError(objContext, e, "ERROR");
		}finally {
			if (objContext != null) {
				try {
					objContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void checkUsage(ReadOnlyUserParams arg0) {
		/*
		 * 
		 */
	}

	@Override
	/**
	 * This method is used to get list of records to processed using the Service
	 * 
	 * @throws CriticalException
	 * @throws BusinessException
	 *             * @throws BusinessException
	 * 
	 */
	protected IGenericInputData getListOfRecordsToProcess(FEBABatchContext batchContext, ReadOnlyUserParams userInputs)
			throws CriticalException, BusinessException {

		FEBATransactionContext objContext = ebBatchUtil.populateTransactionContext(batchContext);
				
		IGenericInputData batchInputData = new GenericInputData();
		String codeType = "PABN";

		CommonCodeVO cmCodeVO = (CommonCodeVO) FEBAAVOFactory.createInstance(TypesCatalogueConstants.CommonCodeVO);
		FEBAArrayList<CustomApplicationStatusInOutVO> applicationStatusList = new FEBAArrayList();

		FEBAArrayList appStatusList = new FEBAArrayList();

		cmCodeVO.setBankID(objContext.getBankId());
		cmCodeVO.setLanguageID(objContext.getLangId());
		cmCodeVO.setCodeType(codeType);

		IFEBAList commonCodeCacheList = new FEBAArrayList();

		try {
			commonCodeCacheList = AppDataManager.getList(objContext, FBAAppDataConstants.COMMONCODE_CACHE, cmCodeVO);
		} catch (CriticalException e) {
			LogManager.log(objContext, "CriticalException thrown from HostCPMessageHelper.getCodeDesc",
					"CriticalException thrown from HostCPMessageHelper.getCodeDesc");
			LogManager.logDebug(objContext, "Business Confirmation thrown from HostCPMessageHelper.getCodeDesc");
		}
		cmCodeVO.setBankID(objContext.getBankId());
		cmCodeVO.setLanguageID(objContext.getLangId());
		CommonCodeVO codeVO = (CommonCodeVO) commonCodeCacheList.get(0);

		StringTokenizer tokens = new StringTokenizer(codeVO.getCommonCode().getValue(), "|");

		while (tokens.hasMoreTokens()) {
			CommonCode applicationStatusCode = new CommonCode(tokens.nextToken());
			appStatusList.add(applicationStatusCode);
		}

		// calling inquiry service
		CustomLoanApplicationEnquiryVO customAppEnquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomLoanApplicationEnquiryVO);

		QueryOperator queryOperator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_LOAN_APPLICATION_INQUIRY_FOR_BATCH);

		queryOperator.associate("bankId", objContext.getBankId());
		queryOperator.associate("userId", objContext.getUserId());
		if (customAppEnquiryVO.getCriteria().getApplicationId() != null
				&& customAppEnquiryVO.getCriteria().getApplicationId().getValue() != 0) {
			queryOperator.associate("applicationId", customAppEnquiryVO.getCriteria().getApplicationId());
		}
		queryOperator.associate("applicationStatus", appStatusList);

		FEBAArrayList<CustomLoanApplicationEnquiryVO> loanAppsVOList = new FEBAArrayList<>();

		try {
			loanAppsVOList = queryOperator.fetchList(objContext);
		} catch (DALException de) {

			throw new BusinessException(objContext, "Record does not exist.", de.getMessage(), de.getErrorCode());
		}

		batchInputData.addItems(loanAppsVOList);
		return batchInputData;
	}

	@Override
	protected IReportMessageVO getRecordErrorMessage(FEBATransactionContext arg0, IFEBAValueObject arg1) {

		return null;
	}

	@Override
	protected List getRecordErrorMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1) {

		return new ArrayList<>();
	}

	@Override
	protected IReportMessageVO getRecordSuccessMessage(FEBATransactionContext arg0, IFEBAValueObject arg1) {
		return new ReportMessageVO();
	}

	@Override
	protected List getRecordSuccessMessagesList(FEBATransactionContext arg0, IFEBAValueObject arg1) {
		return new ArrayList<>();
	}

	@Override
	protected String getSuccessLogMessage(FEBABatchContext arg0, IFEBAValueObject arg1) {
		return null;
	}

	@Override
	protected FEBAValItem[] prepareValidationsList(FEBABatchContext arg0, IFEBAValueObject arg1, Object arg2) {
		return null;
	}

	private CustomSmsBatchDetailsVO populateSMSDelivery(FEBATransactionContext objContext, CustomSmsBatchDetailsVO smsBatchDetailsVO,
			CustomLoanApplnMasterDetailsVO detailsVO, CNCTInfo cnctInfo) throws BusinessException {
		
			System.out.println("-------Inside populateSMSDelivery------");
			Date rModTime = detailsVO.getRModTime().getValue();
			Date currentTime = new java.util.Date(System.currentTimeMillis());
			long diff = currentTime.getTime() - rModTime.getTime();
			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			int numOfDays = (int) days;
			
			try {
				
					FreeText shortMessage = new FreeText();
					smsBatchDetailsVO.setMobileNumber(detailsVO.getUserId().getValue());
					smsBatchDetailsVO.setUserId(detailsVO.getUserId().getValue());
					smsBatchDetailsVO.setDaysInCurrentStatus(new FEBAUnboundInt(numOfDays));
					int statusExpDays = incompleteAppValPeriod - numOfDays;
					System.out.println("statusExpDays::" + statusExpDays);
					Calendar c = Calendar.getInstance();
					c.setTime(currentTime);
					c.add(Calendar.DATE, statusExpDays); // Adding days for expire date.
					System.out.println("expiry date::" + c.getTime());
					smsBatchDetailsVO.setExpiryDate(new FEBADate(c.getTime()));
					smsBatchDetailsVO.setApplicationStatus(detailsVO.getApplicationStatus());
					shortMessage = populateSmsMessage(cnctInfo.getShortMessage().getValue(), smsBatchDetailsVO);
					
					FreeText smsSubject = new FreeText();
					smsSubject = populateSmsSubject(objContext, cnctInfo.getMessageHeader().getValue(), smsBatchDetailsVO);
					
					smsBatchDetailsVO.setShortMessage(shortMessage);
					smsBatchDetailsVO.setMessageHeader(smsSubject);
					smsBatchDetailsVO.setMessage(new FEBAUnboundString(shortMessage.getValue()));
					System.out.println("-------Exit populateSMSDelivery------");
			
			} catch (FEBATypeSystemException e) {
				e.printStackTrace();
				//throw new BusinessException(true, objContext, "LNINQ002", "User Id is invalid", null, 211082, null);
			}
			
			return smsBatchDetailsVO;
	}

	
	private CustomEmailBatchDetailsVO populateEmailDelivery(FEBATransactionContext objContext,
			CustomEmailBatchDetailsVO emailBatchDetailsVO, CustomLoanApplnMasterDetailsVO detailsVO, CNCTInfo cnctInfo)
			throws BusinessException {

		System.out.println("-------Inside populateEmailDelivery------");

		Date rModTime = detailsVO.getRModTime().getValue();
		Date currentTime = new java.util.Date(System.currentTimeMillis());
		long diff = currentTime.getTime() - rModTime.getTime();
		long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		int numOfDays = (int) days;
		try {
			CUSRInfo cusrInfo = CUSRTAO.select(objContext, objContext.getBankId(),
					new CorporateId(detailsVO.getUserId().toString()), new UserId(detailsVO.getUserId().toString()));
			
			emailBatchDetailsVO.setDestEmail(cusrInfo.getCEmailId().toString());
			emailBatchDetailsVO.setUserId(detailsVO.getUserId().getValue());
			emailBatchDetailsVO.setMessageContent("Some Content");
			emailBatchDetailsVO.setDestEmailName(cusrInfo.getCFName().getValue());

			FreeText emailSubject = new FreeText();
			emailSubject = populateEmailSubject(cnctInfo.getMessageHeader().getValue(), emailBatchDetailsVO);
			
			emailBatchDetailsVO.setEmailSubject(emailSubject.getValue());
			emailBatchDetailsVO.setContentTitle(emailSubject.getValue());
			emailBatchDetailsVO.setEmailType(PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, objContext));
			
			int statusExpDays = incompleteAppValPeriod - numOfDays;
			System.out.println("statusExpDays::" + statusExpDays);
			Calendar c = Calendar.getInstance();
			c.setTime(currentTime);
			c.add(Calendar.DATE, statusExpDays); // Adding days for expire date.
			System.out.println("expiry date::" + c.getTime());

			emailBatchDetailsVO.setExpiryDate(new FEBADate(c.getTime()));
			emailBatchDetailsVO.setApplicationStatus(detailsVO.getApplicationStatus());
			emailBatchDetailsVO.setDaysInCurrentStatus(new FEBAUnboundInt(numOfDays));
			emailBatchDetailsVO
			.setShortMessage(populateEmailMessage(cnctInfo.getShortMessage().getValue(), emailBatchDetailsVO));
			emailBatchDetailsVO.setMessageHeader(new FreeText(cnctInfo.getMessageHeader().getValue()));
	
		} catch (FEBATypeSystemException | FEBATableOperatorException e) {
			e.printStackTrace();
			//throw new BusinessException(true, objContext, "LNINQ002", "User Id is invalid", null, 211082, null);
		}
		
		System.out.println("-------Exit populateEmailDelivery------");

		return emailBatchDetailsVO;
	}

	private CustomBatchPopupNotificationInOutVO populatePopupUsers(FEBATransactionContext objContext,
			CustomBatchPopupNotificationInOutVO popUpBatchDetailsVO, CustomLoanApplnMasterDetailsVO detailsVO,
			CNCTInfo cnctInfo) throws BusinessException {
		
		System.out.println("-------Inside populatePopupUsers------");
		
		Date rModTime = detailsVO.getRModTime().getValue();
		Date currentTime = new java.util.Date(System.currentTimeMillis());
		long diff = currentTime.getTime() - rModTime.getTime();
		long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		int numOfDays = (int) days;
		try {
			int statusExpDays = incompleteAppValPeriod - numOfDays;
			System.out.println("statusExpDays::" + statusExpDays);
			Calendar c = Calendar.getInstance();
			c.setTime(currentTime);
			c.add(Calendar.DATE, statusExpDays); // Adding days for expire date.
			System.out.println("expiry date::" + c.getTime());
			System.out.println("Firebase Push notifications");
			ReadOnlyBatchParams fireBaseParams = BatchConfig.getInstance().getBatchParams("GoogleFirebaseParams");
			System.out.println("fireBaseParams - " + fireBaseParams.getParamList());
			if (fireBaseParams != null) {
				popUpBatchDetailsVO.setDeviceToken(fetchDeviceToken(objContext, detailsVO.getUserId()));
				popUpBatchDetailsVO.setExpiryDate(new FEBADate(c.getTime()));
				popUpBatchDetailsVO.setUserId(detailsVO.getUserId().getValue());
				popUpBatchDetailsVO.setApplicationStatus(detailsVO.getApplicationStatus());
				popUpBatchDetailsVO.setDaysInCurrentStatus(new FEBAUnboundInt(numOfDays));
				popUpBatchDetailsVO
						.setShortMessage(populatePopUpMessage(cnctInfo.getShortMessage().getValue(), popUpBatchDetailsVO));
				popUpBatchDetailsVO.setMessageHeader(populatePopUpSubject(objContext,cnctInfo.getMessageHeader().getValue(),popUpBatchDetailsVO));
				String messageScope = fireBaseParams.getParam("MESSSAGE_SCOPES");
				String googleServcieAccountFile = fireBaseParams.getParam("SERVICE_ACCOUNT_FILE");
				System.out.println("messageScope : " + messageScope);
				System.out.println("googleServcieAccountFile : " + googleServcieAccountFile);
				final String[] SCOPES = { messageScope };
				System.out.println("SCOPES : " + SCOPES);
				System.setProperty("https.proxyHost", "172.18.104.20");
				System.setProperty("https.proxyPort", "1707");
				GoogleCredential googleCredential = GoogleCredential
						.fromStream(new FileInputStream(googleServcieAccountFile)).createScoped(Arrays.asList(SCOPES));
				System.out.println("Is google access token generated - " + googleCredential.refreshToken());
				popUpBatchDetailsVO.setApiToken(googleCredential.getAccessToken());
				System.clearProperty("https.proxyHost");
				System.clearProperty("https.proxyPort");
				System.out.println("Google Access Token - " + popUpBatchDetailsVO.getApiToken());
				System.out.println("-------Exit populatePopupUsers------");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return popUpBatchDetailsVO;
	}

	/*
	 * private int getExpNumOfDays(CustomLoanApplnMasterDetailsVO detailsVO){
	 * 
	 * Date rModTime = detailsVO.getRModTime().getValue(); Date currentTime =
	 * new java.util.Date(System.currentTimeMillis()); long diff =
	 * currentTime.getTime() - rModTime.getTime(); long days =
	 * TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS); long
	 * days1=TimeUnit.MILLISECONDS.toDays(days); FEBAUnboundInt
	 * daysInCurrentStatus=new FEBAUnboundInt((int) days1); int numOfDays =
	 * (int) days1;
	 * 
	 * System.out.println("Batch::numOfDays::"+numOfDays);
	 * System.out.println("Batch::daysInCurrentStatus::"+daysInCurrentStatus);
	 * 
	 * return numOfDays;
	 * 
	 * }
	 */

	public FEBAUnboundString fetchDeviceToken(FEBATransactionContext context, UserId userId)
			throws FEBATableOperatorException {

		System.out.println("In FetchDeviceToken ------");
		System.out.println("context.userId" + userId.getValue());
		DVDTInfo dvdtInfo = DVDTTAO.select(context, context.getBankId(), userId);

		return new FEBAUnboundString(dvdtInfo.getDeviceToken().toString());
	}

	public int findNumInString(String numOfDays) {

		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(numOfDays);
		if (m.find()) {
			int n = Integer.parseInt(m.group());
			return n;
		}
		return 0;
	}

	public List<String> fetchListOfConfigs(String splitArray) {

		List<String> arrayListConfig = new ArrayList<>();
		String[] arrOfStr = splitArray.split("\\,");

		for (String numDaysType : arrOfStr) {
			arrayListConfig.add(numDaysType);
		}
		return arrayListConfig;
	}

	private String[] getDaysConfigArray(String daysConfig) {

		if (daysConfig != null && daysConfig != "") {
			String splitArray[] = daysConfig.split("\\|");
			if (splitArray != null && splitArray.length > 0) {
				if (splitArray.length == 1) {
					return splitArray;
				} else if (splitArray.length == 2) {
					return splitArray;
				} else if (splitArray.length == 3) {
					return splitArray;
				}
			}
		}
		return null;
	}

	private FreeText populatePopUpMessage(String shortMessage, CustomBatchPopupNotificationInOutVO popUpBatchDetailsVO) {
		
		
		if (null != popUpBatchDetailsVO.getExpiryDate()
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, popUpBatchDetailsVO.getExpiryDate().toString());
		}

		//shortMessage = shortMessage.toString().replace("<EXPDATE>", popUpBatchDetailsVO.getExpiryDate().toString());
		return new FreeText(shortMessage);

	}
	
	private FreeText populateSmsMessage(String shortMessage, CustomSmsBatchDetailsVO smsBatchDetailsVO) {
		
		if (null != smsBatchDetailsVO.getExpiryDate()
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, smsBatchDetailsVO.getExpiryDate().toString());
		}

		return new FreeText(shortMessage);

	}

	private FreeText populateEmailMessage(String shortMessage, CustomEmailBatchDetailsVO emailBatchDetailsVO) {
		
		
		if (null != emailBatchDetailsVO.getExpiryDate()
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.EXPDATE
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, emailBatchDetailsVO.getExpiryDate().toString());
		}

		//shortMessage = shortMessage.toString().replace("<EXPDATE>", popUpBatchDetailsVO.getExpiryDate().toString());
		return new FreeText(shortMessage);

	}
	
	private FreeText populateEmailSubject(String shortMessage, CustomEmailBatchDetailsVO emailBatchDetailsVO) {
		
		
		if (null != emailBatchDetailsVO.getDestEmailName()
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, emailBatchDetailsVO.getDestEmailName().toString());
		}

		//shortMessage = shortMessage.toString().replace("<EXPDATE>", popUpBatchDetailsVO.getExpiryDate().toString());
		return new FreeText(shortMessage);

	}
	
	private FreeText populateSmsSubject(FEBATransactionContext objContext, String shortMessage, CustomSmsBatchDetailsVO smsBatchDetailsVO) throws BusinessException {
		
		try {
		CUSRInfo cusrInfo = CUSRTAO.select(objContext, objContext.getBankId(),
				new CorporateId(smsBatchDetailsVO.getUserId().toString()), new UserId(smsBatchDetailsVO.getUserId().toString()));
		
			if (null != cusrInfo.getCFName()
				&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

			shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
					+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, cusrInfo.getCFName().getValue());
			}
		}
		
		catch (FEBATypeSystemException | FEBATableOperatorException e) {
			e.printStackTrace();
		}

	return new FreeText(shortMessage);}
	
	private FreeText populatePopUpSubject(FEBATransactionContext objContext, String shortMessage, CustomBatchPopupNotificationInOutVO popUpBatchDetailsVO) throws BusinessException {
		
		try {
			CUSRInfo cusrInfo = CUSRTAO.select(objContext, objContext.getBankId(),
					new CorporateId(popUpBatchDetailsVO.getUserId().toString()), new UserId(popUpBatchDetailsVO.getUserId().toString()));
			
				if (null != cusrInfo.getCFName()
					&& shortMessage.contains(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
							+ CustomEBConstants.PLACEHOLDER_IDENTIFIER)) {

				shortMessage = shortMessage.replace(CustomEBConstants.PLACEHOLDER_IDENTIFIER + CustomEBConstants.CUSTOMER_NAME
						+ CustomEBConstants.PLACEHOLDER_IDENTIFIER, cusrInfo.getCFName().getValue());
				}
			}
			
			catch (FEBATypeSystemException | FEBATableOperatorException e) {
				throw new BusinessException(true, objContext, "LNINQ002", "User Id is invalid", null, 211082, null);
			}
	
		return new FreeText(shortMessage);

	}
	
	private void updateCLATRmodTime(FEBATransactionContext objContext, CustomLoanApplnMasterDetailsVO detailsVO) {
		
		System.out.println("-------------Inside updateCLATRmodTime-------------");

		CLATInfo clatData = new CLATInfo();
		CLATTAO clatTAO = new CLATTAO(objContext);
		try {
			
			clatData = CLATTAO.select(objContext, objContext.getBankId(), detailsVO.getApplicationId());
			clatTAO.associateCookie(clatData.getCookie());
			clatTAO.associateApplicationId(detailsVO.getApplicationId());
			clatTAO.associateBankId(clatData.getBankId());
			clatTAO.update(objContext);

		} catch (FEBATableOperatorException e) {
			e.printStackTrace();
		}

		System.out.println("-------------Exit updateCLATRmodTime-------------");
	}

}

package com.infosys.custom.ebanking.batch.service;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;
import com.infosys.custom.ebanking.common.CustomEBQueryIdentifiers;
import com.infosys.custom.ebanking.tao.CLDDTAO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCLDDRecAppDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomFDTTRecDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.ebanking.common.EBTransactionContext;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.DALException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.dal.QueryOperator;
import com.infosys.feba.framework.tao.FEBATableOperatorException;
import com.infosys.feba.framework.transaction.pattern.AbstractLocalUpdateTran;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.IFEBAValueObject;
import com.infosys.feba.framework.valengine.FEBAValItem;
import com.infosys.feba.tools.common.util.Logger;

public class CustomCLDDRecoveryServiceInsertImpl extends AbstractLocalUpdateTran {

	@Override
	public FEBAValItem[] prepareValidationsList(FEBATransactionContext arg0, IFEBAValueObject arg1,
			IFEBAValueObject arg2) throws BusinessException, BusinessConfirmation, CriticalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void process(FEBATransactionContext objContext, IFEBAValueObject arg1, IFEBAValueObject arg2)
			throws BusinessException, BusinessConfirmation, CriticalException {
		CustomCLDDRecAppDetailsVO detailsVO = (CustomCLDDRecAppDetailsVO) arg1;
		String sharedDocPath = PropertyUtil.getProperty("FEBA_SHARED_SYS_PATH", objContext);
		LogManager.logDebug(objContext, "sharedDocPath:" + sharedDocPath);
		File SharedFolder = new File(sharedDocPath);
		if (SharedFolder.exists() && SharedFolder.isDirectory()) {
			String applciationDocPath = sharedDocPath + "/" + detailsVO.getApplicationId();
			File applciationDocFolder = new File(applciationDocPath);
			if (applciationDocFolder.exists() && applciationDocFolder.isDirectory()) {
				LogManager.logDebug(objContext, "CustomCLDDRecInsertImpl:Application folder:" + applciationDocPath);
				File[] docsFileArray = applciationDocFolder.listFiles(new FileFilter() {
					@Override
					public boolean accept(File pathname) {
						return pathname.getName().toLowerCase().endsWith(".pdf")
								|| pathname.getName().toLowerCase().endsWith(".jpg");
					}
				});
				LogManager.logDebug(objContext,
						"CustomCLDDRecInsertImpl:list of files in folder:" + docsFileArray.length);
				FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> cldddata = populateDocumentDetails(objContext,
						detailsVO.getApplicationId().toString());

				List clddFileList = new ArrayList<String>();
				if (cldddata != null && cldddata.size() > 0) {
					cldddata.forEach(docdetails -> clddFileList.add(docdetails.getDocStorePath().toString()));
				}
				LogManager.logDebug(objContext, "CustomCLDDRecInsertImpl:No of cldd records:" + clddFileList.size());
				for (int i = 0; i < docsFileArray.length; i++) {
					File docfile = docsFileArray[i];
					if (!clddFileList.contains(docfile.getAbsolutePath())) {

						ProcessRecoveryForDocument(objContext, docfile.getName(), docfile.getAbsolutePath(),
								detailsVO.getApplicationId());

					}
				}

			}
		}

	}

	private void ProcessRecoveryForDocument(FEBATransactionContext objContext, String filename, String filepath,
			ApplicationNumber applicationId) {
		LogManager.logDebug(objContext,
				"CustomCLDDRecInsertImpl:In ProcessRecoveryForDocument records:" + applicationId + " File:" + filename);
		QueryOperator operator = QueryOperator.openHandle(objContext, "CustomfdttRecInquiryDAL");
		FEBAArrayList results = new FEBAArrayList<>();
		try {
			operator.associate("bankId", objContext.getBankId());
			operator.associate("file_name", new FEBAUnboundString(filename));
			results = operator.fetchList(objContext);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			operator.closeHandle((EBTransactionContext) objContext);
		}
		LogManager.logDebug(objContext, "CustomCLDDRecInsertImpl:in ProcessRecoveryForDocument records:" + applicationId
				+ " File:" + filename + " results:" + results.size());
		if (results.size() > 0) {
			CustomFDTTRecDetailsVO fdttrec = (CustomFDTTRecDetailsVO) results.get(0);
			CLDDTAO clddTAO = new CLDDTAO(objContext);
			clddTAO.associateBankId(objContext.getBankId());
			clddTAO.associateApplicationId(applicationId);
			if (filename.contains("KTP")) {
				clddTAO.associateDocType(new DocumentType("KTP"));
				String ktp = filename.split("_")[1];
				clddTAO.associateDocCode(new DocumentNumber(ktp));
				clddTAO.associateDocKey(new FEBAUnboundString(ktp));
			} else if (filename.contains("FPINANG")) {
				clddTAO.associateDocType(new DocumentType("DAOF"));
			} else if (filename.contains("SPH")) {
				clddTAO.associateDocType(new DocumentType("DBTL"));
			}

			clddTAO.associateDocStorePath(new FEBAUnboundString(filepath));
			clddTAO.associateFileSequenceNum(fdttrec.getFileSequenceNum());
			try {
				clddTAO.insert(objContext);
			} catch (FEBATableOperatorException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				LogManager.logError(objContext, e,
						"Error Processing:" + applicationId.toString() + " Filename:" + filename);
			}
			LogManager.logDebug(objContext,
					"CustomCLDDRecInsertImpl:insert success for:" + fdttrec.getFileSequenceNum());

		}

	}

	public static FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> populateDocumentDetails(
			FEBATransactionContext objContext, String applicationId) {

		final EBTransactionContext ebContext = (EBTransactionContext) objContext;
		FEBAArrayList<CustomLoanApplnDocumentsDetailsVO> docList = null;
		QueryOperator queryOperator = QueryOperator.openHandle(objContext,
				CustomEBQueryIdentifiers.CUSTOM_FETCH_LOAN_DOCUMENT_LIST);
		queryOperator.associate("bankId", ((EBTransactionContext) objContext).getBankId());
		queryOperator.associate("applicationId", new ApplicationNumber(applicationId));
		// queryOperator.associate("docType", new DocumentType("KTP"));

		try {
			docList = queryOperator.fetchList(objContext);

		} catch (DALException e) {
			Logger.logError("Exception e" + e);
		} finally {
			// Call the closeHandle method which Closes the handle
			queryOperator.closeHandle((EBTransactionContext) objContext);
		}
		return docList;

	}

}

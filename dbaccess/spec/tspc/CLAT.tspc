#<BWAYDOC>
#View Name                	: 	CLAT			
#Table Name               	: 	CUSTOM_LOAN_APPLN_MASTER_TABLE	
#Description              	: 	This table contains Loan Application related details			
#<BEGIN_COLUMNS>					
#DB_TS					    :	    NUMBER(5,0)	           : Database Time  stamp
#BANK_ID                  	: 	NVARCHAR2(11)             	: 	Valid Bank Code	
#USER_ID                		: 	NVARCHAR2(32)             	: 	User Id	
#APPLICATION_ID             : 	NUMBER(10,0)              	: 	Application Id (Unique Sequence number obtained through sequence generator)	 	
#REQUESTED_LOAN_AMT				:	NUMBER(18,3)				: Requested Loan Amount
#REQUESTED_TENOR					:	NUMBER(3)				: Requested Loan Tenor
#LOAN_INT_RATE					:	NUMBER(18,3)				:Loan Interest Rate
#MONHTLY_INSTALLMENT					:	NUMBER(18,3)				: Monthly Installment
#LEGACY_CIF					:	NVARCHAR2(32)				: Legacy CIF
#PRIVY_AGREE_DATE					:	DATE				: Privy agree date
#PRODUCT_TNC_AGREE_DATE					:	DATE				: Product Terms and Conditions Agree Date
#APPLICATION_STATUS					:	NVARCHAR2(32)				: Application Status
#PRODUCT_TYPE					:	NVARCHAR2(4)				: Product Type
#PRODUCT_CODE					:	NVARCHAR2(4)				: Product Code
#PRODUCT_CATEGORY					:	NVARCHAR2(4)				: Product Category
#PRODUCT_SUBCATEGORY					:	NVARCHAR2(56)				: Product Sub-Category
#FREE_TXT1					: NVARCHAR2(80)					: Free Text1
#FREE_TXT2					: NVARCHAR2(80)					: Free Text2
#FREE_TXT3					: NVARCHAR2(80)					: Free Text3
#FREE_DATE1					: DATE							: Free Date1
#FREE_DATE2					: DATE							: Free Date2
#DEL_FLG                    :   NCHAR(1)		    		: Valid DEL_FLG
#R_MOD_ID                   :   NCHAR(65)		    		: Record Modifier Id	
#R_MOD_TIME                 :   DATE			    		: Record Modified Time	
#R_CRE_ID                   :   NCHAR(65)	        		: Record Creator Id	
#R_CRE_TIME                 :   DATE		 	    		: Record Creation Time	
#EMAIL_VERIFIED				:	CHAR(1)						: Email Verified
#CREDIT_SCORE_UID			:	NVARCHAR2(100)				: credit score uid
#CREDIT_SCORE				:	NUMBER(10,2)				: credit score
#GRADE						:   NUMBER(2)					: grade
#APPROVED_AMOUNT			:	NUMBER(18,3)				: approved amount
#STATUS_DESC				:	NVARCHAR2(100)				: status desc
#FEEDBACK_STAR					:	NVARCHAR2(50)				: feedback
#REMARKS					:	NVARCHAR2(100)				: remarks
#LOAN_ACCOUNT_ID		: NVARCHAR2(16)            : loan account id
#IS_KTP_VERIFIED		: CHAR(1)						: KTP Verified
#PLAFOND_1			:	NUMBER(18,3)				: plafond 1
#TENOR_1			:	NUMBER(3)				: tenor 1
#PLAFOND_2			:	NUMBER(18,3)				: plafond 2
#TENOR_2			:	NUMBER(3)					: tenor 1
#TENOR_MAX			:	NUMBER(3)					: tenor Max
#BANK_CODE			:	NVARCHAR2(11)             	: 	Valid Bank Code	
#CR_RESP_CODE		: NUMBER(5)				: credit score response code
#KTP_REJ_CNT		: NUMBER(3)				: ktp rejection count
#IS_KTP_REUPLOADED		: CHAR(1)						: KTP Re Uploaded
#PARTNER_ID					:	NVARCHAR2(50)				: partner ID
#SCHEME_CODE				:	NVARCHAR2(100)				: Scheme Code
#PAY_LATER_PROCESS_FEE		:	NUMBER(18,3)				: Pay later Processing Fee 
#<END_COLUMNS>					
#Relationship with other tables					
#Master Table             	: 	None			
#Relation key with Master 	: 	None			
#Child Table              	: 	None			
#Relation key with Child  	: 	None			
#</BWAYDOC>
#------------------------------------------------------------------------------
#            Table specifications of CUSTOM_LOAN_APPLN_MASTER_TABLE 
#------------------------------------------------------------------------------


#----------- Table Name --------------------------
NAME = 'CUSTOM_LOAN_APPLN_MASTER_TABLE'

#----------- Table Short Name --------------------
SHORT_NAME = 'CLAT'

#----------- Keys --------------------------------
KEY_FIELDS =  [ 'BANK_ID','APPLICATION_ID'] 



#----------- Other Fields ------------------------
FIELDS =  [
{ 'name' : 'DB_TS',		    'type' : 'DBTS',				       		'febaType':'FEBADatabaseTimeStamp',	    'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'BANK_ID',      	'type' : 'BANK_ID',							'febaType':'BankId',					'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'USER_ID',               	'type' : 'STRING',  	'size' : '32',		'febaType':'UserId',					'modifiedThroughMaintenance':'Y',		'generalName':'User Id',                       					'userInfo.corpUser()':'N'},
{ 'name' : 'APPLICATION_ID', 'type' : 'NUMBER',		'size' : '10,0',	'febaType':'ApplicationNumber',		'modifiedThroughMaintenance':'Y'	},
{'name' : 'REQUESTED_LOAN_AMT',      		'type' : 'AMOUNT',	'size' : '18,3',				'febaType':'FEBAAmount','currencyCode':'HOME',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'REQUESTED_TENOR', 		'type' : 'NUMBER',	'size' : '3',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'LOAN_INT_RATE', 		'type' : 'NUMBER',	'size' : '18,3',	'febaType':'InterestRate',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'MONHTLY_INSTALLMENT', 		'type' : 'AMOUNT',		'size' : '18,3', 'febaType':'FEBAAmount','currencyCode':'HOME',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'LEGACY_CIF',                 	'type' : 'CUST_ID',	'size' : '32',			'febaType':'CustomerId',				'modifiedThroughMaintenance':'Y',		'generalName':'Bank Away Cust Id',                   	'read_only':'N'						},
{ 'name' : 'PRIVY_AGREE_DATE',	'type' : 'DATE',				            'febaType':'FEBADate',			    	'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PRODUCT_TNC_AGREE_DATE',	'type' : 'DATE',				            'febaType':'FEBADate',			    	'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'APPLICATION_STATUS',      		'type' : 'STRING',		'size' : '32',   	'febaType':'CommonCode',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PRODUCT_TYPE',      		'type' : 'STRING',		'size' : '4',   	'febaType':'CommonCode',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PRODUCT_CODE',      		'type' : 'STRING',		'size' : '4',   	'febaType':'CommonCode',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PRODUCT_CATEGORY',      		'type' : 'STRING',		'size' : '4',   	'febaType':'CommonCode',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PRODUCT_SUBCATEGORY',      		'type' : 'STRING',		'size' : '56',   	'febaType':'CommonCode',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FREE_TXT1',      		'type' : 'STRING',		'size' : '80',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FREE_TXT2',      		'type' : 'STRING',		'size' : '80',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FREE_TXT3',      		'type' : 'STRING',		'size' : '80',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FREE_DATE1',      		'type' : 'STRING',	 	'febaType':'FEBADate',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FREE_DATE2',      		'type' : 'STRING',		'febaType':'FEBADate',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'DEL_FLG',		'type' : 'CHAR',				            'febaType':'FEBADeleteFlag',			'modifiedThroughMaintenance':'N'	},
{ 'name' : 'R_MOD_ID',		'type' : 'USER_ID',				            'febaType':'FEBARecordUserId',			'modifiedThroughMaintenance':'N'	},
{ 'name' : 'R_MOD_TIME',	'type' : 'DATE',				            'febaType':'FEBADate',			    	'modifiedThroughMaintenance':'N'	},
{ 'name' : 'R_CRE_ID',		'type' : 'USER_ID',				            'febaType':'FEBARecordUserId',			'modifiedThroughMaintenance':'N'	},
{ 'name' : 'R_CRE_TIME',	'type' : 'DATE',				            'febaType':'FEBADate',			    	'modifiedThroughMaintenance':'N'	},
{ 'name' : 'EMAIL_VERIFIED',		'type' : 'CHAR',				            'febaType':'FEBAUnboundChar',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'CREDIT_SCORE_UID',      		'type' : 'STRING',		'size' : '100',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'CREDIT_SCORE', 		'type' : 'NUMBER',	'size' : '10',	'febaType':'FEBAUnboundDouble',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'GRADE', 		'type' : 'NUMBER',	'size' : '2',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'APPROVED_AMOUNT', 		'type' : 'AMOUNT',		'size' : '18,3', 'febaType':'FEBAAmount','currencyCode':'HOME',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'STATUS_DESC',      		'type' : 'STRING',		'size' : '100',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'LOAN_ACCOUNT_ID',      		'type' : 'STRING',		'size' : '16',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'FEEDBACK_STAR',      		'type' : 'STRING',		'size' : '50',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'N'	},
{ 'name' : 'REMARKS',      			'type' : 'STRING',		'size' : '100',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'N'	},
{ 'name' : 'IS_KTP_VERIFIED',		'type' : 'CHAR',				            'febaType':'FEBAUnboundChar',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PLAFOND_1', 		'type' : 'AMOUNT',		'size' : '18,3', 'febaType':'FEBAAmount','currencyCode':'HOME',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'TENOR_1', 		'type' : 'NUMBER',	'size' : '3',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PLAFOND_2', 		'type' : 'AMOUNT',		'size' : '18,3', 'febaType':'FEBAAmount','currencyCode':'HOME',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'TENOR_2', 		'type' : 'NUMBER',	'size' : '3',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'TENOR_MAX', 		'type' : 'NUMBER',	'size' : '3',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'BANK_CODE',      		'type' : 'STRING',		'size' : '11',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'CR_RESP_CODE', 		'type' : 'NUMBER',	'size' : '5',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'KTP_REJ_CNT', 		'type' : 'NUMBER',	'size' : '5',	'febaType':'FEBAUnboundInt',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'IS_KTP_REUPLOADED',		'type' : 'CHAR',				            'febaType':'FEBAUnboundChar',			'modifiedThroughMaintenance':'Y'	},
{ 'name' : 'PARTNER_ID',      			'type' : 'STRING',		'size' : '50',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'N'	},
{ 'name' : 'SCHEME_CODE',      			'type' : 'STRING',		'size' : '100',   	'febaType':'FEBAUnboundString',		'modifiedThroughMaintenance':'N'	},
{ 'name' : 'PAY_LATER_PROCESS_FEE', 		'type' : 'AMOUNT',		'size' : '18,3', 'febaType':'FEBAAmount','currencyCode':'HOME',			'modifiedThroughMaintenance':'Y'	},

]


#----------- Storage ( only for ORACLE DB ) ------
TABLESPACE = 'MASTER'

#----------- Additional Index Details ------------
additional_index =  [	
]
#----------- Maximum Records ----------------------																			
MAX_RECS_IN_BUFF = '50'

#------------Update TABLE ----------------------
UPDATE = 'Y'

#------------Audit Required----------------------

AUDIT = 'Y'

#----------- Shadow Table required or not----------
SHADOW_TABLE_REQUIRED = 'N'

#----------- Table Maintainance required or not----------
TABLE_MAINTENANCE_REQUIRED = 'N'
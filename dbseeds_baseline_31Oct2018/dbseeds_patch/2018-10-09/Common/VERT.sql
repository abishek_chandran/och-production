INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200607,'CUST','001','0','Error while delivering Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200608,'CUST','001','0','To Address Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200609,'CUST','001','0','Subject Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200610,'CUST','001','0','Email body Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);

--COCD entry for Access Scheme

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','AMS','BRC','001','BRI Bank Admin for Divisi Kepatuhan, Manajemen Risiko dan Hukum','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','SEG','BRC','001','BRI Bank Admin for Divisi Kepatuhan, Manajemen Risiko dan Hukum','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','AMS','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','SEG','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);

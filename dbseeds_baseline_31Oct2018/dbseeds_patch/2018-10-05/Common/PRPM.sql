delete from PRPM where PROPERTY_NAME='CUSTOM_NOTIFICATION_CORE_INPUTPATH' and bank_id='01';
delete from PRPM where PROPERTY_NAME='CUSTOM_NOTIFICATION_CORE_OUTPUTPATH' and bank_id='01';
--Added for Notification core batch --
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_INPUTPATH','/BRIEDB_FE/OCH/FebaBatch/input/CoreBatchNotificationEventInput','Notification Core Batch Input file path','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO PRPM (DB_TS,BANK_ID,APP_ID,PROPERTY_NAME,PROPERTY_VAL,PROPERTYDESCRIPTION,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','BWY','CUSTOM_NOTIFICATION_CORE_OUTPUTPATH','/BRIEDB_FE/OCH/FebaBatch/input/CustomBatchNotificationEventOutput','Notification Core Batch Output file path','N','setup',SYSDATE,'setup',SYSDATE);
--Added for Notification core batch --
commit;
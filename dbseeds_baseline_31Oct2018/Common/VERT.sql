SET DEFINE OFF;
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) VALUES (1,'01',200601,'CUST','001','0','Initialize data',NULL,NULL,NULL,'N','setup',SYSDATE,'setup',SYSDATE,NULL);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) VALUES (1,'01',200602,'CUST','001','0','Incorrect HP No.',NULL,NULL,NULL,'N','setup',SYSDATE,'setup',SYSDATE,NULL);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) VALUES (1,'01',200603,'CUST','001','0','Non-technical errors',NULL,NULL,NULL,'N','setup',SYSDATE,'setup',SYSDATE,NULL);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211041,'CUST','001','0','Card number is invalid.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211042,'CUST','001','0','Expiry Date entered is incorrect.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211043,'CUST','001','0','Card is not active.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211044,'CUST','001','0','CAMS host call failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211045,'CUST','001','0','Whitelist Sahabat host call failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211046,'CUST','001','0','Whitelist Pinang host call failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211047,'CUST','001','0','Silverlake Account Inquiry host call failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211048,'CUST','001','0','Silverlake CIF Inquiry host call failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211049,'CUST','001','0','Account is not open.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211050,'CUST','001','0','Please enter KTP Number.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211051,'CUST','001','0','Entered KTP number is not correct.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211033,'CUST','001','0','Product Code is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211034,'CUST','001','0','Product Category is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211035,'CUST','001','0','Product SubCategory is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211036,'CUST','001','0','Config Type is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211037,'CUST','001','0','Loan Purpose is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211038,'CUST','001','0','Tenor is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211039,'CUST','001','0','Interest is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211040,'CUST','001','0','Minimum Amount Should be less than Maximum Amount',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

--standardText Changes by pratik
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
values (1,'01',211024,'CUST','001','0','Text Type is mandatory',null,null,null,'N','BPD',sysdate,'BPD',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
values (1,'01',211032,'CUST','001','0','Text Type is invalid',null,null,null,'N','BPD',sysdate,'BPD',sysdate,null);


--contentManagment
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211030,'CUST','001','0','Content Type is mandatory',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211031,'CUST','001','0','Content Type is invalid',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);


--loansimulation
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211025,'CUST','001','0','Merchant Id is mandatory',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211026,'CUST','001','0','Payment Amount is mandatory',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211028,'CUST','001','0','Merchant Id is Invalid',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values
(1,'01',211029,'CUST','001','0','Loan Tenure is Invalid',null,null,null,'N','BPD',
sysdate,'BPD',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211053,'CUST','001','0','Application details, Payroll details and Document details are mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) values (1,'01',211052,'CUST','001','0','Table update failed for Credit Score callback.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
commit;

Insert into VERT values (1,'01',211054,'CUST','001','0','Company Name is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211055,'CUST','001','0','Company Address is mandatory',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT values (1,'01',211056,'CUST','001','0','Face not found',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211057,'CUST','001','0','Face does not match',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211058,'CUST','001','0','Invalid Privy Id',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211059,'CUST','001','0','Invalid OTP',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
commit ;

--Vidhi 19th Sept
Insert into VERT values (1,'01',211060,'CUST','001','0','Invalid or Expired Token',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
commit;

--Added by Pratik_shah07 for Standard Text customization
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
values (1,'01',211070,'CUST','001','0','Display Order should be a numeric value.',null,null,null,'N','BPD',sysdate,'BPD',sysdate,null);

--Added by Prasad for OTP generate vaidation 20 Sep 2018-----
Insert into VERT values (1,'01',211080,'CUST','001','0','Invalid Toke',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

--Vidhi 24Sept Loan Bal Inquiry start
Insert into VERT values (1,'01',211081,'CUST','001','0','User Id is empty',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211082,'CUST','001','0','Invalid User ID',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
Insert into VERT values (1,'01',211083,'CUST','001','0','Account Id is empty',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
--Vidhi 24Sept Loan Bal Inquiry end

---Added by sahana-- start -05 Oct

Insert into VERT ("db_ts","bank_id","err_code","error_namespace","lang_id","err_type","err_desc","err_hlp","err_img_html","err_snd_html","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time","err_msg_type") values (1,'01',211081,'CUST','001','0','CIF Updation failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT ("db_ts","bank_id","err_code","error_namespace","lang_id","err_type","err_desc","err_hlp","err_img_html","err_snd_html","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time","err_msg_type") values (1,'01',211084,'CUST','001','0','User Phone Number is mandatory.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
---Added by sahana-- end -05 Oct

---Added by vidhi 05 oct Loan History start
Insert into VERT values (1,'01',211085,'CUST','001','0','If date is provided then both start and end date should be provided',null,null,null,'N','setup',sysdate,'setup',sysdate,null); 
---Added by vidhi 05 oct Loan History end
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200605,'CUST','001','0','Property configuration to fetch loan application documents types empty.',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200606,'CUST','001','0','Loan Application documents not found',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200607,'CUST','001','0','Error while delivering Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200608,'CUST','001','0','To Address Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200609,'CUST','001','0','Subject Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',200610,'CUST','001','0','Email body Mandatory to send Email',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);

INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',211087,'CUST','001','0','The status cannot be updated',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);

---Added for Loan Origination start
Insert into VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE)
values (1,'01',211086,'CUST','001','0','Max 30 days date range is only allowed',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

COMMIT;
---Added for Loan Origination end

--Added for Merchant Payment
INSERT INTO VERT (DB_TS,BANK_ID,ERR_CODE,ERROR_NAMESPACE,LANG_ID,ERR_TYPE,ERR_DESC,ERR_HLP,ERR_IMG_HTML,ERR_SND_HTML,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME,ERR_MSG_TYPE) 
VALUES (1,'01',211090,'CUST','001','0','Mandatory Field not provided : $FIELD_ID$',null,null,null,'N','setup',SYSDATE,'setup',SYSDATE,null);
--Added for Merchant Payment

Insert into VERT values (1,'01',211091,'CUST','001','0','Maximum allowed tried are exceeded. Please try after some time',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
COMMIT;

Insert into VERT values (1,'01',211302,'CUST','001','0','Face verification failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT values (1,'01',211303,'CUST','001','0','Error in Date format in Balance details.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT values (1,'01',211304,'CUST','001','0','Error in Fetching in Balance details.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);

Insert into VERT values (1,'01',211305,'CUST','001','0','Call Back Failed.',null,null,null,'N','setup',sysdate,'setup',sysdate,null);
COMMIT;



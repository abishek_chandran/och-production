SET DEFINE OFF;
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CLOANP','FORMSGROUP_ID=CustomLoanProductMaintenanceResource',null,null,'N','N','ALL|',
'Custom Standard Text Maintenance',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CATMIN','FORMSGROUP_ID=CustomATMInquiryCallResource',null,null,'N','N','ALL|','Custom ATM Inquiry Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);

Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','ACOM','FORMSGROUP_ID=CustomContentMgmtFG&Call_Mode=1',null,null,'N','N','ALL|','Content Management',null,null,'NFIN',null,null,null,null,'setup',sysdate,'setup',sysdate,null,null,null);

Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values 
(1,'01','N','LNCOM','FORMSGROUP_ID=CustomLoanMaintenenceListFG&Call_Mode=0',null,null,'N','N','ALL|','Loan configuration maintainence',null,'W','NFIN','N',null,null,null,null,
'setup',sysdate,'setup',sysdate,'N',null,null);

update vmot set fg_link_url='FORMSGROUP_ID=CustomLoanProdMaintenanceFG&Call_Mode=0' where mnu_id='LNCOM' and bank_id='01';

commit;

Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CLACT','FORMSGROUP_ID=CustomLoanApplicationResource',null,null,'N','N','ALL|','Custom Loan Creation',null,'W','NFIN','Y',null,null,null,'SETUP',sysdate,'SETUP',sysdate,null,null,null);
commit;




--standardText Changes by pratik

Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CSTXT','FORMSGROUP_ID=CustomStandardTextMaintenanceResource',null,null,'N','N','ALL|','Custom Standard Text Maintenance',null,'W','NFIN','Y',null,null,null,'SETUP',to_date('20-AUG-18 10:31:45','DD-MON-RR HH24:MI:SS'),'SETUP',to_date('20-AUG-18 10:31:45','DD-MON-RR HH24:MI:SS'),null,null,null);

--contentManagement
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CCONMG','FORMSGROUP_ID=CustomContentManagementResource',null,null,'N','N','ALL|',
'Custom Content Management Resource',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

--PROTIK / Credit Score - 18/09/2018
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CRSCOR','FORMSGROUP_ID=CustomCreditScoringCallbackResource',null,null,'N','N','ALL|','Custom Credit Scoring Callback Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);


SET DEFINE OFF;
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CCMPD','FORMSGROUP_ID=CustomCompanyDetailsManagerResource',null,null,'N','N','ALL|','Custom Company Details Manager',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',to_date('10-SEP-18','DD-MON-RR'),'SETUP',to_date('10-SEP-18','DD-MON-RR'),null,null,null);
COMMIT;

--loansimulation
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CLNSIM','FORMSGROUP_ID=CustomLoanSimulationResource',null,null,'N','N','ALL|',
'Custom Loan Simulation',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

--Added for loan application list inquiry by Ambili on 20-09-2018 : start
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CLAIT','FORMSGROUP_ID=CustomLoanApplicationListInquiryResource',null,null,'N','N','ALL|','Custom Loan application list inquiry',null,'W','NFIN','Y',null,null,null,'SETUP',sysdate,'SETUP',sysdate,null,null,null);
commit;
--Added for loan application list inquiry by Ambili on 20-09-2018 : end

--Added by Prasad for OTP generate resource 20 Sep 2018-----
insert into vmot values (1,'01','N','COTP','FORMSGROUP_ID=CustomGenerateOtpManagerResource',NULL,NULL,'N','N','ALL|','Custom Generate OTP Manager Resource',NULL,'W','NFIN','Y',NULL,NULL,NULL,NULL,'SETUP',sysdate,'SETUP',sysdate,NULL,NULL,NULL);
--Added by Prasad for OTP generate resource 20 Sep 2018-----

---Added by Prasad for OTP verify resource 20 Sep 2018------
insert into vmot values (1,'01','N','CVOTP','FORMSGROUP_ID=CustomVerifyOtpManagerResource',NULL,NULL,'N','N','ALL|','Custom Verify OTP Manager Resource',NULL,'W','NFIN','Y',NULL,NULL,NULL,NULL,'SETUP',sysdate,'SETUP',sysdate,NULL,NULL,NULL);
---Added by Prasad for OTP verify resource 20 Sep 2018------


--Vidhi 24Sept Loan balance start
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','LONBAL','FORMSGROUP_ID=CustomLoanInquiryResource',null,null,'N','N','ALL|','Custom Loan Balance Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);
--Vidhi 24Sept Loan balance end

--Vidhi 26Sept Standard text
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','STMINQ','FORMSGROUP_ID=CustomStandardTextResource',null,null,'N','N','ALL|','Custom Standard Text Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);

--Vidhi 26Sept Standard textend 

--PRATIK_SHAH07 ChangePassword 26-08-2018
DELETE FROM VMOT WHERE MNU_ID='FCPWD' AND BANK_ID='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','FCPWD','FORMSGROUP_ID=CustomPasswordRetailResource',null,null,'N','N','ALL|',
'Custom Password Maintenance',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

--Login Signon changes Supraja
update VMOT set fg_link_url = 'FORMSGROUP_ID=CustomSignInInterface' where mnu_id ='SIGN' and bank_id ='01';

--Vidhi 27 sept Photo Maintenance
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','PHOVAL','FORMSGROUP_ID=CustomValidatePhotoMaintenanceResource',null,null,'N','N','ALL|','Custom Photo Validation Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);



--Pratik_shah07 for UserProfile Changes
DELETE FROM VMOT WHERE MNU_ID='RCPL' AND BANK_ID='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','RCPL','FORMSGROUP_ID=CustomUserProfileDetailsResource',null,null,'N','N','ALL|',
'Custom Password Maintenance',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

--Pratik_shah07 for Notification status update
DELETE FROM VMOT WHERE MNU_ID='CNSUP' AND BANK_ID='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CNSUP','FORMSGROUP_ID=CustomNotificationMaintenanceResource',null,null,'N','N','ALL|',
'Custom Notification Maintenance',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

---PRASAD for Loan Repayment FETCH 04 oct----
DELETE FROM VMOT WHERE MNU_ID='CFLD' AND BANK_ID='01';
SET DEFINE OFF;
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CFLD','FORMSGROUP_ID=CustomFetchLoanRepaymentScheduleResource',null,null,'N','N','ALL|','Custom Fetch Repayment List Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);
---PRASAD for Loan Repayment FETCH 04 oct----

---Added by sahana -- start 05 Oct
delete from vmot where mnu_id='DEVREG' and bank_id='01';
Insert into VMOT ("db_ts","bank_id","del_flg","mnu_id","fg_link_url","reg_link_url","promo_link_url","promotion_flag","user_acceptance_required","ac_type","mnu_txt","additional_mnu_txt","menu_type","type_of_activity","rm_plus_enabled","highlight_flag","highlight_date","highlight_text","product_code","r_cre_id","r_cre_time","r_mod_id","r_mod_time","is_secure_menu_id","auth_reqd_on_entry","adaptive_auth_reqd") values (1,'01','N','DEVREG','FORMSGROUP_ID=CustomDeviceRegistrationRetailResource',null,null,'N','N','ALL|','Device registration',null,'W','NFIN','Y',null,null,null,null,'setup',sysdate,'setup',sysdate,null,null,null);


delete from vmot where mnu_id='DEVUSR' and bank_id='01';
Insert into VMOT ("db_ts","bank_id","del_flg","mnu_id","fg_link_url","reg_link_url","promo_link_url","promotion_flag","user_acceptance_required","ac_type","mnu_txt","additional_mnu_txt","menu_type","type_of_activity","rm_plus_enabled","highlight_flag","highlight_date","highlight_text","product_code","r_cre_id","r_cre_time","r_mod_id","r_mod_time","is_secure_menu_id","auth_reqd_on_entry","adaptive_auth_reqd") values (1,'01','N','DEVUSR','FORMSGROUP_ID=CustomDeviceRegUpdateHPNumberResource',null,null,'N','N','ALL|','Device registration, update HP Number',null,'W','NFIN','Y',null,null,null,null,'setup',sysdate,'setup',sysdate,null,null,null);
---Added by sahana -- end 05 Oct

---Added by vidhi 05 oct Loan History start
delete from vmot where mnu_id='LNHIST' and bank_id='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','LNHIST','FORMSGROUP_ID=CustomFetchLoanHistoryResource',null,null,'N','N','ALL|',
'Custom Loan Fetch Loan History',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);
---Added by vidhi 05 oct Loan History end

--Added by Ambili for loan Payment SR:: start
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CLNPR','FORMSGROUP_ID=CustomLoanPaymentResource',null,null,'N','N','ALL|',
'Loan payment request',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);
--Added by Ambili for loan Payment SR:: start

INSERT INTO VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) 
VALUES (1,'01','N','CLADF','FORMSGROUP_ID=CustomLoanApplicationDocumentListResource',NULL,NULL,'N','N','ALL|','Custom Loan Application Document Fetch',NULL,'W','NFIN','Y',NULL,NULL,NULL,NULL,'SETUP',SYSDATE,'SETUP',SYSDATE,NULL,NULL,NULL);

--Added by Vidhi for loan creation 9 Oct :: start
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CRELON','FORMSGROUP_ID=CustomCreateLoanResource',null,null,'N','N','ALL|','Custom Create Loan Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);
--Added by Vidhi for loan creation 9 Oct :: end


DELETE FROM VMOT WHERE MNU_ID='CMPAY' AND BANK_ID='01';
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,
AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,
R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD)
values (1,'01','N','CMPAY','FORMSGROUP_ID=CustomMerchantPaymentResource',null,null,'N','N','ALL|',
'Custom Merchant Payment Resource',null,'W','NFIN','Y',null,null,null,'SETUP',
sysdate,'SETUP',sysdate,null,null,null);

--Added by Prasad for Loan Balance Details 16 OCt---
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CALD','FORMSGROUP_ID=CustomAccountListInquiryResource',null,null,'N','N','ALL|','Custom Fetch Balance Details Inq',null,'W','NFIN','Y',null,null,null, null,'SETUP',sysdate,'SETUP',sysdate,null,null,null);
COMMIT;
--Added by Prasad for Loan Balance Details 16 OCt---

--Added for Loan Origination Start---
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) 
values
(1,'01','N','LNORG','FORMSGROUP_ID=CustomLoanOriginationFG&Call_Mode=0',null,null,'N','N','ALL|','Loan Origination',null,'W','NFIN','N',null,null,null,null,'setup',sysdate,'setup',sysdate,'N',null,null);

COMMIT;
--Added for Loan Origination end---

--Added for limit inquiry resource
Insert into VMOT (DB_TS,BANK_ID,DEL_FLG,MNU_ID,FG_LINK_URL,REG_LINK_URL,PROMO_LINK_URL,PROMOTION_FLAG,USER_ACCEPTANCE_REQUIRED,AC_TYPE,MNU_TXT,ADDITIONAL_MNU_TXT,MENU_TYPE,TYPE_OF_ACTIVITY,RM_PLUS_ENABLED,HIGHLIGHT_FLAG,HIGHLIGHT_DATE,HIGHLIGHT_TEXT,PRODUCT_CODE,R_CRE_ID,R_CRE_TIME,R_MOD_ID,R_MOD_TIME,IS_SECURE_MENU_ID,AUTH_REQD_ON_ENTRY,ADAPTIVE_AUTH_REQD) values (1,'01','N','CLIDT','FORMSGROUP_ID=CustomLoanLimitInquiryResource',null,null,'N','N','ALL|','Custom Loan Limit Inquiry Resource',null,'W','NFIN','Y',null,null,null, EMPTY_CLOB(),'SETUP',sysdate,'SETUP',sysdate,null,null,null);
--Added for limit inquiry resource

--for merchant refund
Insert into VMOT ("db_ts","bank_id","del_flg","mnu_id","fg_link_url","reg_link_url","promo_link_url","promotion_flag","user_acceptance_required","ac_type","mnu_txt","additional_mnu_txt","menu_type","type_of_activity","rm_plus_enabled","highlight_flag","highlight_date","highlight_text","product_code","r_cre_id","r_cre_time","r_mod_id","r_mod_time","is_secure_menu_id","auth_reqd_on_entry","adaptive_auth_reqd") values (1,'01','N','CMREF','FORMSGROUP_ID=CustomMerchantRefundResource',null,null,'N','N','ALL|','Custom Merchant Refund Resource',null,'W','NFIN','Y',null,null,null,null,'SETUP',sysdate,'SETUP',sysdate,null,null,null);
--for merchant refund
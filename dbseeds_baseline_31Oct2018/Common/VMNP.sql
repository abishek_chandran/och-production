SET DEFINE OFF;
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLOANP','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLOANP','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CATMIN','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CATMIN','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','ACOM','RRMGR','!','PARENT',15,'Y','setup',sysdate,'setup',sysdate);
commit;

Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLACT','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLACT','RUSER','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','LNCOM','RRMGR','GENADM','CHILD',19,'Y','setup',sysdate,'setup',sysdate);
commit;


--standardText Changes by pratik

Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CSTXT','RRMGR','!','PARENT',1,'N','setup',to_date('24-AUG-18 17:44:36','DD-MON-RR HH24:MI:SS'),'setup',to_date('24-AUG-18 17:44:36','DD-MON-RR HH24:MI:SS'));

--contentMangement
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CCONMG','RRMGR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CCONMG','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

--PROTIK / Credit Score - 18/09/2018
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CRSCOR','RRMGR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

SET DEFINE OFF;
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CCMPD','RRMGR','!','PARENT',1,'N','SETUP',to_date('10-SEP-18','DD-MON-RR'),'SETUP',to_date('10-SEP-18','DD-MON-RR'));
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CCMPD','DEF_RET','!','PARENT',1,'N','SETUP',to_date('10-SEP-18','DD-MON-RR'),'SETUP',to_date('10-SEP-18','DD-MON-RR'));
COMMIT;

--loansimulation
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLNSIM','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLNSIM','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

--Added for loan application list inquiry by Ambili on 20-09-2018 : start

Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLAIT','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLAIT','RUSER','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
commit;

--Added for loan application list inquiry by Ambili on 20-09-2018 : end

--Added by Prasad for OTP generate resource 20 Sep 2018-----

insert into vmnp values (1,'01','N','COTP','DEF_RET','!','PARENT','1','N','SETUP',sysdate,'SETUP',sysdate);
insert into vmnp values (1,'01','N','COTP','RRMGR','!','PARENT','1','N','SETUP',sysdate,'SETUP',sysdate);
--Added by Prasad for OTP generate resource 20 Sep 2018-----

---Added by Prasad for OTP verify resource 20 Sep 2018------
insert into vmnp values (1,'01','N','CVOTP','DEF_RET','!','PARENT','1','N','SETUP',sysdate,'SETUP',sysdate);
insert into vmnp values (1,'01','N','CVOTP','RRMGR','!','PARENT','1','N','SETUP',sysdate,'SETUP',sysdate);
---Added by Prasad for OTP verify resource 20 Sep 2018------

--Vidhi 24Sept Loan Bal Inquiry start
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','LONBAL','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','LONBAL','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
--Vidhi 24Sept Loan Bal Inquiry end

--VIdhi 26Sept Standatd text
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','STMINQ','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','STMINQ','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

--Vidhi 26sEpt Standatd text end

--PRATIK_SHAH07 ChangePassword 26-08-2018
DELETE FROM VMNP WHERE MNU_ID='FCPWD' AND BANK_ID='01';
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','FCPWD','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','FCPWD','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

--Added by Tushar
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','SIGN','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate); 

--Vidhi 27Sept Photo Maintenance
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','PHOVAL','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','PHOVAL','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);


--Pratik_Shah07 for UserProfileChanges

DELETE FROM VMNP WHERE MNU_ID='RCPL' AND BANK_ID='01';
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','RCPL','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','RCPL','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);



--Pratik_shah07 for Notification Status update API
DELETE FROM VMNP WHERE MNU_ID='CNSUP' AND BANK_ID='01';
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CNSUP','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CNSUP','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

---Prasad for Loan Repayment FETCH 04 oct---
DELETE FROM VMNP WHERE MNU_ID='CFLD' AND BANK_ID='01';
SET DEFINE OFF;
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CFLD','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CFLD','RRMGR','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
---Prasad for Loan Repayment FETCH 04 oct---

---Added by sahana--start Oct 5
DELETE FROM VMNP WHERE MNU_ID='DEVUSR' AND BANK_ID='01';
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','DEVUSR','CUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','DEVUSR','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','DEVUSR','RRMGR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','DEVUSR','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
---Added by sahana--end Oct 5


---Added by vidhi 05 oct Loan History start
delete from VMNP where mnu_id='LNHIST' and bank_id='01';
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','LNHIST','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','LNHIST','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

---Added by vidhi 05 oct Loan History end

--Added by Ambili for loan Payment SR:: start
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLNPR','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLNPR','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
commit;
--Added by Ambili for loan Payment SR:: end
INSERT INTO VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','N','CLADF','RUSER','!','PARENT',1,'N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','N','CLADF','DEF_RET','!','PARENT',1,'N','setup',SYSDATE,'setup',SYSDATE);

--Added by Vidhi for loan creation 9 Oct :: start
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CRELON','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CRELON','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
--Added by Vidhi for loan creation 9 Oct :: end


DELETE FROM VMNP WHERE MNU_ID='CMPAY' AND BANK_ID='01';
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CMPAY','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CMPAY','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

--Added by Prasad for Loan Balance Details 16 OCt---
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CALD','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CALD','RRMGR','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
COMMIT;
--Added by Prasad for Loan Balance Details 16 OCt---

Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','CSTXT','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
COMMIT;

--Added for Loan Origination Start---
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','N','LNORG','RRMGR','GENADM','CHILD',20,'Y','setup',sysdate,'setup',sysdate);
COMMIT;
--Added for Loan Origination end---

--Added for limit inquiry resource
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLIDT','DEF_RET','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CLIDT','RRMGR','!','PARENT',1,'N','SETUP',sysdate,'SETUP',sysdate);
--Added for limit inquiry resource

Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order","is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','DEVREG','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);


--VTUSER VMNP
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','N','CCONMG','VTUSR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','N','CSTXT','VTUSR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
values (1,'01','N','ORONSE','VTUSR','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);

Insert into VMNP ("db_ts","bank_id","del_flg","mnu_id","mnu_prf_cd","parent_mnu_id","mnu_id_type","mnu_id_order",
"is_part_of_menu_tree","r_mod_id","r_mod_time","r_cre_id","r_cre_time") values (1,'01','N','FORPWD','VTUSR',
'!','PARENT',1,'N','setup',sysdate,
'setup',sysdate);

--For merchant refund
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CMREF','RUSER','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
Insert into VMNP (DB_TS,BANK_ID,DEL_FLG,MNU_ID,MNU_PRF_CD,PARENT_MNU_ID,MNU_ID_TYPE,MNU_ID_ORDER,IS_PART_OF_MENU_TREE,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','N','CMREF','DEF_RET','!','PARENT',1,'N','setup',sysdate,'setup',sysdate);
--For merchant refund

drop table TRADE_STD_TEXT_EXT;

drop synonym TSTX;

create table TRADE_STD_TEXT_EXT
(
	DB_TS number(5,0),
	BANK_ID nvarchar2(11) NOT NULL,
	CORP_ID nvarchar2(32) NOT NULL,
	USER_TYPE nvarchar2(3) NOT NULL,
	TEXT_TYPE nvarchar2(3) NOT NULL,
	TEXT_NAME nvarchar2(35) NOT NULL,
	DISPLAY_ORDER numeric(15),
	DEL_FLG char(1),
	R_CRE_ID nvarchar2(65),
	R_CRE_TIME date,
	R_MOD_ID nvarchar2(65),
	R_MOD_TIME date 
) 
TABLESPACE MASTER;

create synonym TSTX for TRADE_STD_TEXT_EXT;

create unique index IDX_TSTX on TRADE_STD_TEXT_EXT( CORP_ID, TEXT_TYPE, TEXT_NAME, USER_TYPE, BANK_ID ) TABLESPACE IDX_MASTER;


drop table S_TRADE_STD_TEXT_EXT;
drop synonym S_TSTX;

create table S_TRADE_STD_TEXT_EXT
(
	DB_TS number(5,0),
	BANK_ID nvarchar2(11) NOT NULL,
	CORP_ID nvarchar2(32) NOT NULL,
	USER_TYPE nvarchar2(3) NOT NULL,
	TEXT_TYPE nvarchar2(3) NOT NULL,
	TEXT_NAME nvarchar2(35) NOT NULL,
	DISPLAY_ORDER numeric(15) ,
	DEL_FLG char(1),
	R_CRE_ID nvarchar2(65),
	R_CRE_TIME date,
	R_MOD_ID nvarchar2(65),
	R_MOD_TIME date,
	MKCT_SRL_NO numeric(10,0),
	FUNC_CODE char(1)
) 
TABLESPACE MASTER;

create synonym S_TSTX for S_TRADE_STD_TEXT_EXT;

create unique index IDX_S_TSTX on S_TRADE_STD_TEXT_EXT( CORP_ID, TEXT_TYPE, TEXT_NAME, USER_TYPE, BANK_ID, DISPLAY_ORDER ) TABLESPACE IDX_MASTER;

create  index IDX_S_TSTX_MKCT on S_TRADE_STD_TEXT_EXT( MKCT_SRL_NO) TABLESPACE IDX_MASTER;

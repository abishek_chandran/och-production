SET DEFINE OFF;
INSERT INTO STCT (BANK_ID,STR_ID,LANG_ID,STR_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES ('01','SMS_REG','001','Kode Otorisasi untuk registrasi akun Pinang Anda adalah <OTP>. Kode Anda berlaku 60 menit.','N','system',SYSDATE,'system',SYSDATE);

INSERT INTO STCT (BANK_ID,STR_ID,LANG_ID,STR_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES ('01','SMS_LOGIN','001','Kode Otorisasi untuk login ke aplikasi Pinang Anda adalah <OTP>. Kode Anda berlaku 60 menit.','N','system',SYSDATE,'system',SYSDATE);

COMMIT;
insert into COCD values (1, '01', 'PCO', 'PNG', '001', 'Pinang Loan', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
insert into COCD values (1, '01', 'PCA', 'PNG', '001', 'Pinang Loan', 'N', 'setup', SYSDATE, 'setup', SYSDATE);
INSERT INTO COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','DOC','DTRM','001','Format Syarat dan Ketentuan Pinjaman PINANG v0.1','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','DOC','DAOF','001','Permohonan Fasilitas PINANG v1.0','N','setup',SYSDATE,'setup',SYSDATE);
INSERT INTO COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) 
VALUES (1,'01','DOC','DBTL','001','Surat Pengakuan Hutang PINANG v0.1','N','setup',SYSDATE,'setup',SYSDATE);

REM INSERTING into COCD
SET DEFINE OFF;
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','DTPE','DBTL','001','CustomSuratPengakuanHutang|SPH','N','setup',sysdate,'setup',sysdate);
Insert into COCD (DB_TS,BANK_ID,CODE_TYPE,CM_CODE,LANG_ID,CD_DESC,DEL_FLG,R_MOD_ID,R_MOD_TIME,R_CRE_ID,R_CRE_TIME) values (1,'01','DTPE','DAOF','001','CustomPermohonanFasilitasPinang|FPINANG','N','setup',sysdate,'setup',sysdate);


--COCD entry for Access Scheme pratik_shah07

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','AMS','BRC','001','BRI Bank Admin for Divisi Kepatuhan, Manajemen Risiko dan Hukum','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','SEG','BRC','001','BRI Bank Admin for Divisi Kepatuhan, Manajemen Risiko dan Hukum','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','AMS','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);

Insert into ececuser.COCD ("db_ts","bank_id","code_type","cm_code","lang_id","cd_desc","del_flg","r_mod_id","r_mod_time","r_cre_id","r_cre_time") 
values (1,'01','SEG','BRD','001','BRI Bank Admin for Desk Bisnis Konsumer','N','setup',sysdate,'setup',sysdate);
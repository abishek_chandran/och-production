package com.infosys.custom.ebanking.rest.general.v1.pojo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Credit Scoring Callback")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomCreditScoringCallbackReferencesVO {

	@ApiModelProperty(value = "status")
	private String status;

	@ApiModelProperty(value = "uid")
	private String uid;

	@ApiModelProperty(value = "score")
	private String score;

	@ApiModelProperty(value = "grade")
	private String grade;
	
	@ApiModelProperty(value = "approval")
	private List<CustomCreditScoringCallbackApprovalVO>  approval;
	
	@ApiModelProperty(value = "response Code")
	private String resp_code;
	
//	@ApiModelProperty(value = "valid payroll date")
//	private int valid_payroll_date;
//	
//	@ApiModelProperty(value = "valid payroll net amount")
//	private String valid_pendapatan_netto;
//	
//	@ApiModelProperty(value = "valid start date")
//	private String valid_start_date;
//	
//	@ApiModelProperty(value = "valid end date")
//	private String valid_end_date;
	
	@ApiModelProperty(value = "desc")
	private String desc;

	@ApiModelProperty(value = "statusCode")
	private String statusCode;
	
	@ApiModelProperty(value = "applicationStatus")
	private String applicationStatus;
	
	@ApiModelProperty(value = "tenor_max")
	private String tenor_max;

	public CustomCreditScoringCallbackReferencesVO() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<CustomCreditScoringCallbackApprovalVO> getApproval() {
		return approval;
	}

	public void setApproval(List<CustomCreditScoringCallbackApprovalVO> approval) {
		this.approval = approval;
	}

	public String getResp_code() {
		return resp_code;
	}

	public void setResp_code(String resp_code) {
		this.resp_code = resp_code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getTenor_max() {
		return tenor_max;
	}

	public void setTenor_max(String tenor_max) {
		this.tenor_max = tenor_max;
	}
	



}

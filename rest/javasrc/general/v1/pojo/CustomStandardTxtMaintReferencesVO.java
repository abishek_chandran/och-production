package com.infosys.custom.ebanking.rest.general.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "Standard Text Maintenance Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomStandardTxtMaintReferencesVO {

	@ApiModelProperty(value = "Text Name")
	private String textName;

	@ApiModelProperty(value = "Text Description")
	private String textDescription;
	
	@ApiModelProperty(value = "Display Order")
	private String displayOrder;

	public String getTextName() {
		return textName;
	}
	
	public String getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}
	public void setTextName(String textName) {
		this.textName = textName;
	}
	public String getTextDescription() {
		return textDescription;
	}
	public void setTextDescription(String textDescription) {
		this.textDescription = textDescription;
	}
	
	public CustomStandardTxtMaintReferencesVO(){
		super();
	}
	public CustomStandardTxtMaintReferencesVO(String textName, String textDescription, String displayOrder) {
		super();
		this.textName = textName;
		this.textDescription = textDescription;
		this.displayOrder = displayOrder;
	}
	
	
}

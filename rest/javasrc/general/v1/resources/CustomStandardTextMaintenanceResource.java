package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomStandardTxtMaintReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomStandardTxtMaintReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomStandardTextDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomStandardTextEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ServiceInfo(moduleName = "general")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Standard Text Maintenance")
@ApiResponses(value = {
		@ApiResponse(code=400, message= ResourcePaths.HTTP_ERROR_400 , response = Header.class),
		@ApiResponse(code=401, message= ResourcePaths.HTTP_ERROR_401 , response = Header.class),
		@ApiResponse(code=403, message= ResourcePaths.HTTP_ERROR_403 , response = Header.class),
		@ApiResponse(code=405, message= ResourcePaths.HTTP_ERROR_405 , response = Header.class),
		@ApiResponse(code=415, message= ResourcePaths.HTTP_ERROR_415 , response = Header.class),
		@ApiResponse(code=422, message= ResourcePaths.HTTP_ERROR_422 , response = Header.class),
		@ApiResponse(code=500, message= ResourcePaths.HTTP_ERROR_500 , response = Header.class),
})
public class CustomStandardTextMaintenanceResource extends ResourceAuthenticator{
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;

	/**
	 * Authenticates if the corpid and bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the corpid and bankid is authenticated it returns false
	 * @throws Exception
	 */
	@Override
	protected boolean isUnAuthenticatedRequest() throws Exception {


		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();

		String bankId = pathParams.getFirst("bankid");

		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);

		
		boolean isInValid;

		if (bank.getValue().equals(bankId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}

		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}

		return isInValid;
	}



	@GET
	@MethodInfo(uri = CustomResourcePaths.STANDARD_TEXT_MAINTENANCE_URL, auditFields = {"textName : Standard Text Maintenance text Name input",
			"textType: Standard Text Maintenance text Type input"
	})
	@ApiOperation(value="Fetch Standard Text Maintenance details based on text name and text type" , 
	response=CustomStandardTxtMaintReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "Successfull Retrieval of Standard Text Maintenance Details" , response = CustomStandardTxtMaintReferencesResponseVO.class),
			@ApiResponse(code=404, message= "Standard Text Details do not exist"),
			@ApiResponse(code=500, message= "Internal Server Error")
	})
	public Response fetchRequest(
			@ApiParam(value = "Standard Text Maintenance text type input" ,required=true) @QueryParam("textType") String textType,
			@ApiParam(value = "Standard Text Maintenance text name input") @QueryParam("textName") String textName
			){
		CustomStandardTxtMaintReferencesResponseVO responseVO = new CustomStandardTxtMaintReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
			List<CustomStandardTxtMaintReferencesVO> outputVO = fetchStandardTextDetails(textType,textName, context);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO	, request, 0);

		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request,response, e, restResponseErrorCodeMapping());
		}
		return Response.ok().entity(responseVO).build();
	}

	public List<CustomStandardTxtMaintReferencesVO> fetchStandardTextDetails(String textType, String textName, IOpContext context)
			throws FEBAException {
		List<CustomStandardTxtMaintReferencesVO> outputVO = null;

		CustomStandardTextEnquiryVO enquiryVO = (CustomStandardTextEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomStandardTextEnquiryVO);
		if(null!=textType){
			enquiryVO.getCriteria().setTextType(textType);
		}
		if(null!=textName){
			enquiryVO.getCriteria().setTextName(textName);
		}
		outputVO = new ArrayList<>();
		final IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomStandardTextMaintenanceService"));
		enquiryVO = (CustomStandardTextEnquiryVO)serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		FEBAArrayList<CustomStandardTextDetailsVO> detailsListVO = enquiryVO.getResultList();

		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomStandardTextDetailsVO detailsVO = detailsListVO.get(i);
			CustomStandardTxtMaintReferencesVO stdtVO = new CustomStandardTxtMaintReferencesVO();
			stdtVO.setTextDescription(detailsVO.getTextDescription().toString());
			stdtVO.setTextName(detailsVO.getTextName().toString());
			stdtVO.setDisplayOrder(detailsVO.getDisplayOrder().getValue());
			outputVO.add(stdtVO);
		}
		return outputVO;
	}


	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();

		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211030, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211031, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(100239, "BE", 401));

		
		return restResponceErrorCode;
	}


}


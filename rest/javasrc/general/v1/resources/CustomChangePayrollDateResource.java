package com.infosys.custom.ebanking.rest.general.v1.resources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;

import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomOfflineKTPFetchReferencesResponseVO;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomPayrollDateChangeRequest;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomPayrollDateChangeStatusDetail;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomPayrollDateChangeStatusList;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomPayrollDateChangeStatusReqpojo;
import com.infosys.custom.ebanking.rest.general.v1.pojo.CustomPayrollDateInput;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocBinaryListVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPayrollDateChangeVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPhotoValidationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentUploadUtil;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBADate;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "common")
@Api(value = "Payroll Date change API")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@ApiResponses(value = { 
		@ApiResponse(code = 200, message = "Successful processing"),
		@ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })

public class CustomChangePayrollDateResource extends ResourceAuthenticator {


	@Context  
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;

	final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomPayrollDateService"));

	@POST
	@ApiOperation(value = "Update PayrollDate")
	@MethodInfo(uri = CustomResourcePaths.CHANGE_PAYROLL_DATE_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful processing") })
	public Response changePayrollDate(@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId,CustomPayrollDateChangeRequest dateChangeRequestPojo) {

		IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");

		CustomPayrollDateChangeStatusList resposnepojo = new CustomPayrollDateChangeStatusList();
		resposnepojo.setStatusList(new ArrayList<>());
		try {
			FEBAArrayList<CustomPayrollDateChangeVO> inputList = new FEBAArrayList<CustomPayrollDateChangeVO>();
			for (CustomPayrollDateInput detail : dateChangeRequestPojo.getCustomerList()) {
				CustomPayrollDateChangeVO customPayrollDateChangeVO = (CustomPayrollDateChangeVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomPayrollDateChangeVO);

				customPayrollDateChangeVO.setUserId(detail.getMobileNo());
				customPayrollDateChangeVO.setApplicationId(detail.getApplicationId());
				customPayrollDateChangeVO.setNewPayrollDate(new FEBADate(new SimpleDateFormat("dd/MM/yyyy").parse(detail.getPayrollDate())));

				try {
					customPayrollDateChangeVO.setMode("V");
					customPayrollDateChangeVO = (CustomPayrollDateChangeVO) serviceStub.callService(context,
							customPayrollDateChangeVO, new FEBAUnboundString("validate"));
					if (!customPayrollDateChangeVO.getStatus().getValue().equalsIgnoreCase("FAILURE")) {

						customPayrollDateChangeVO.setMode("C");
						customPayrollDateChangeVO = (CustomPayrollDateChangeVO) serviceStub.callService(context,
								customPayrollDateChangeVO, new FEBAUnboundString("change"));
						try{

							documentSign(context, customPayrollDateChangeVO.getUserId().getValue(),
									customPayrollDateChangeVO.getApplicationId());
						}catch (Exception e) {
							customPayrollDateChangeVO.setStatusDesc(e.getMessage());
						}
					} 
						customPayrollDateChangeVO = (CustomPayrollDateChangeVO) serviceStub.callService(context,
								customPayrollDateChangeVO, new FEBAUnboundString("statusUpdate"));
				} catch (Exception e) {
					customPayrollDateChangeVO.setStatus("FAILURE");
					customPayrollDateChangeVO.setStatusDesc(e.getMessage());
					customPayrollDateChangeVO = (CustomPayrollDateChangeVO) serviceStub.callService(context,
							customPayrollDateChangeVO, new FEBAUnboundString("statusUpdate"));
				}
				
				CustomPayrollDateChangeStatusDetail respdetail=new CustomPayrollDateChangeStatusDetail();
				respdetail.setApplicationId(String.valueOf(customPayrollDateChangeVO.getApplicationId().getValue()));
				respdetail.setMobileNo(customPayrollDateChangeVO.getUserId().getValue());
				respdetail.setNewPayrollDate(customPayrollDateChangeVO.getNewPayrollDate().getValue());
				respdetail.setOldPayrollDate(customPayrollDateChangeVO.getOldPayrollDate().getValue());
				respdetail.setStatus(customPayrollDateChangeVO.getStatus().getValue());
				respdetail.setStatusDesc(customPayrollDateChangeVO.getStatusDesc().getValue());
				resposnepojo.getStatusList().add(respdetail);
			}
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();

			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request,response, 0);
		} catch (Throwable e) {
			e.printStackTrace();
			LogManager.logError(context, e);
		} 
		return Response.ok().entity(resposnepojo).build();

	}

	@POST
	@ApiOperation(value = "Check PayrollDate change Status", response = CustomOfflineKTPFetchReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CHANGE_PAYROLL_DATE_STATUS_URL)
	@Path("/status")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful processing", response = CustomPayrollDateChangeStatusList.class) })
	public Response fetchPayrollDateChanegStatus(
			@ApiParam(value = "bank id", required = true) @PathParam("bankId") String bankId,
			@RequestBody CustomPayrollDateChangeStatusReqpojo inputPojo) {
		CustomPayrollDateChangeStatusList resposnepojo = new CustomPayrollDateChangeStatusList();
		IOpContext context = (IOpContext) request.getAttribute("OP_CONTEXT");
		try {

			CustomPayrollDateChangeEnquiryVO dateChangeEnquiryVO = (CustomPayrollDateChangeEnquiryVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPayrollDateChangeEnquiryVO);

			CustomPayrollDateChangeCriteriaVO criteriaVO = (CustomPayrollDateChangeCriteriaVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPayrollDateChangeCriteriaVO);
			if (RestCommonUtils.isNotNullorEmpty(inputPojo.getMobileNo())) {
				criteriaVO.setUserId(inputPojo.getMobileNo());
			}
			if (RestCommonUtils.isNotNullorEmpty(inputPojo.getFromDate())) {
				criteriaVO.setFromDate(new FEBADate(new SimpleDateFormat("dd/MM/yyyy").parse(inputPojo.getFromDate())));
			}
			if (RestCommonUtils.isNotNullorEmpty(inputPojo.getToDate())) {
				criteriaVO.setToDate(new FEBADate(new SimpleDateFormat("dd/MM/yyyy").parse(inputPojo.getToDate())));
			}
			dateChangeEnquiryVO.setCriteria(criteriaVO);
			dateChangeEnquiryVO = (CustomPayrollDateChangeEnquiryVO) serviceStub.callService(context,
					dateChangeEnquiryVO, new FEBAUnboundString("statusFetch"));

			FEBAArrayList<CustomPayrollDateChangeVO> resultList = dateChangeEnquiryVO.getResultList();
			resposnepojo.setStatusList(new ArrayList<>());
			for (CustomPayrollDateChangeVO customPayrollDateChangeVO : resultList) {
				CustomPayrollDateChangeStatusDetail detail=new CustomPayrollDateChangeStatusDetail();
				detail.setApplicationId(String.valueOf(customPayrollDateChangeVO.getApplicationId().getValue()));
				detail.setMobileNo(customPayrollDateChangeVO.getUserId().getValue());
				detail.setNewPayrollDate(customPayrollDateChangeVO.getNewPayrollDate().getValue());
				detail.setOldPayrollDate(customPayrollDateChangeVO.getOldPayrollDate().getValue());
				detail.setStatus(customPayrollDateChangeVO.getStatus().getValue());
				detail.setStatusDesc(customPayrollDateChangeVO.getStatusDesc().getValue());
				resposnepojo.getStatusList().add(detail);
			}
			
		} catch (Throwable e) {
			LogManager.logError(context, e);
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		} 
		return Response.ok().entity(resposnepojo).build();

	}

	@SuppressWarnings("rawtypes")
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		return restResponceErrorCode;
	}

	public void documentSign(IOpContext context, String userId, ApplicationNumber applicationId) throws Exception {
		CustomPrivyRegistrationVO regEnquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);
		regEnquiryVO.setUserId(userId);

		final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
		regEnquiryVO = (CustomPrivyRegistrationVO) serviceStub.callService(context, regEnquiryVO,
				new FEBAUnboundString("tokenCreate"));

		FEBAUnboundString token = regEnquiryVO.getToken();

		if (token != null) {
			CustomPhotoValidationDetailsVO detailsVO = (CustomPhotoValidationDetailsVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPhotoValidationDetailsVO);
			detailsVO.setApplicationId(String.valueOf(applicationId));
			detailsVO.setToken(token);
			detailsVO.setMerchantKey(PropertyUtil.getProperty("PRIVY_MERCHANT_KEY", context));
			callCreatePDF(context, detailsVO);

		} else {
			throw new BusinessException(true, context, CustomEBankingIncidenceCodes.TOKEN_NOT_FOUND, "Invalid Token",
					null, CustomEBankingErrorCodes.INVALID_TOKEN, null);
		}

	}

	public String callCreatePDF(IOpContext context, CustomPhotoValidationDetailsVO detailsVO) throws Exception {

		CustomApplicationDocEnquiryVO enquiryVO = (CustomApplicationDocEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomApplicationDocEnquiryVO);
		enquiryVO.getCriteria().setApplicationId(new ApplicationNumber(detailsVO.getApplicationId().toString()));
		enquiryVO.getCriteria().setMerchantKey(detailsVO.getMerchantKey());
		enquiryVO.getCriteria().setToken(detailsVO.getToken());

		final IClientStub serviceCStub = ServiceUtil
				.getService(new FEBAUnboundString("CustomDocumentDelegateSigningService"));

		try {
			serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("generatePDF"));

			FEBAArrayList binaryList = enquiryVO.getBinaryList();

			CustomDocumentInputDetailsVO documentDetails = new CustomDocumentInputDetailsVO();
			documentDetails.setApplicationId(Long.valueOf(detailsVO.getApplicationId().toString()));

			for (int i = 0; i < binaryList.size(); i++) {
				CustomApplicationDocBinaryListVO binaryVO = (CustomApplicationDocBinaryListVO) binaryList.get(i);
				documentDetails.setFile(binaryVO.getDocString().toString());
				documentDetails.setFileName(binaryVO.getDocTitle().toString() + ".pdf");

				documentDetails = new CustomDocumentUploadUtil()
						.createFileInSharedDirAndUploadEncryptedFile(documentDetails, context, request);

				CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnDocumentsDetailsVO);
				docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
				docDetailsVO.setDocType(new DocumentType(binaryVO.getDocumentId().toString()));

				docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
				docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());

				final IClientStub customDocDetailsService = ServiceUtil
						.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));

				customDocDetailsService.callService(context, docDetailsVO, new FEBAUnboundString("createOrUpdate"));
			}
			return ("DOCUMENT_SIGNED");

		} catch (CriticalException | BusinessException | BusinessConfirmation | FEBATypeSystemException e) {
			throw e;
		}

	}
}

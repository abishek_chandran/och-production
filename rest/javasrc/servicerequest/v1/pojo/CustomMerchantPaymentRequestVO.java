package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.CommonHeaderRetailDetailsVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value="Loan payment request details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomMerchantPaymentRequestVO extends CommonHeaderRetailDetailsVO {

private CustomMerchantPaymentDetailsInOutVO data;

/**
 * @return the data
 */
public CustomMerchantPaymentDetailsInOutVO getData() {
	return data;
}

/**
 * @param data the data to set
 */
public void setData(CustomMerchantPaymentDetailsInOutVO data) {
	this.data = data;
}

}

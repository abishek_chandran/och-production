package com.infosys.custom.ebanking.rest.servicerequest.v1.pojo;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value="Loan payment details")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomLoanPaymentDetailsInputVO {
	@ApiModelProperty(value = "The loan account id")
	private String accountId;



	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	/*@ApiModelProperty(value = "The loan amount")
	private String transactionAmount;
	
	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}*/

}

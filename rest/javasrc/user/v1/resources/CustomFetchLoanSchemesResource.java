package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAmountVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomFeesReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomFetchLoanSchemesReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomFetchLoanSchemesReferencesVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSchmCodeDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSchmCodeFeesDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanSchmCodeListEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSchemeCodeWLDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSchemeCodeWLEnquiryVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.hif.EBHostInvoker;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Loan Scheme codes")
@Path("/v1/banks/{bankid}/users/{userid}/custom/loanSchemeCodes")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomFetchLoanSchemesResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;

	@Context
	UriInfo uriInfo;


	final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoanSchemeCodeInquiryService"));
	final IClientStub serviceSchmCodeWLStub = ServiceUtil.getService(new FEBAUnboundString("CustomSchemeCodeWLService"));

	
	@Override
	/**
	 * Authenticates if the bankid who is trying to access the resource is same as the corpid and bankid which is associated with the context.
	 *
	 * @return boolean if the bankid is authenticated it returns false
	 * @throws Exception
	 */
	protected boolean isUnAuthenticatedRequest() throws Exception {

		IOpContext opcontext = (OpContext) request.getAttribute("OP_CONTEXT");
		MultivaluedMap<String, String> pathParams = uriInfo.getPathParameters();
		String bankId = pathParams.getFirst("bankid");
		// Fetch the user details from the context.
		BankId bank = (BankId) opcontext.getFromContextData(EBankingConstants.BANK_ID);
		boolean isInValid;
		if (bank.getValue().equals(bankId)) {
			isInValid = false;
		} else {
			isInValid = true;
		}
		if(isInValid) {
			throw new BusinessException(opcontext,
					EBIncidenceCodes.USER_NOT_AUTHORIZED_TO_CONFIRM,
					"The user is not authorized to perform the action.",
					FBAErrorCodes.UNATHORIZED_USER);
		}
		return isInValid;
	}

	@GET
	@MethodInfo(uri = CustomResourcePaths.LOAN_SCHEME_CODES_FETCH_URL)
	@ApiOperation(value = "Loan scheme code Details", response = CustomFetchLoanSchemesReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Loan scheme codes", response = CustomFetchLoanSchemesReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Loan details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })

	public Response fetchRequest(@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId, @QueryParam("partnerId") String partnerId ) {

		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		CustomFetchLoanSchemesReferencesResponseVO responseVO = new CustomFetchLoanSchemesReferencesResponseVO();
		try {
			isUnAuthenticatedRequest();
			CustomLoanSchmCodeListEnquiryVO enquiryVO = (CustomLoanSchmCodeListEnquiryVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanSchmCodeListEnquiryVO");
			// Default request ID for scheme codes fetch
			enquiryVO.getCriteria().setRequestId("LoanSchmCodeSearcher");
			//If partner ID is not null
			if (RestCommonUtils.isNotNull(partnerId)){
				enquiryVO.getCriteria().setPartnerId(partnerId);
			}
			List<CustomFetchLoanSchemesReferencesVO> outputVO = fetchLoanSchemeCodes(opcontext,enquiryVO);
			responseVO.setData(outputVO);
		} catch (Exception e) {
			LogManager.logError(null, e);
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());

		}
		return Response.ok().entity(responseVO).build();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<CustomFetchLoanSchemesReferencesVO> fetchLoanSchemeCodes(IOpContext context, CustomLoanSchmCodeListEnquiryVO enquiryVO) throws Exception {
		List<CustomFetchLoanSchemesReferencesVO> outputVO = new ArrayList<>();
		String currency="IDR";
		try{
				enquiryVO = (CustomLoanSchmCodeListEnquiryVO) serviceStub.callService(context, enquiryVO,
					new FEBAUnboundString("fetch"));
				System.out.println("After calling scheme codes fetch service - enquiryVO - "+enquiryVO);
				FEBAArrayList<CustomLoanSchmCodeDetailsVO> schemeCodeDetailsListVO = enquiryVO.getResultList();
				FEBAArrayList<CustomLoanSchmCodeFeesDetailsVO> feeDetailsListVO = enquiryVO.getFeesList();
				List<CustomFeesReferencesVO> feeList = new ArrayList<>();
				boolean schemeCodeMatches = false;
				int k = 0;
				if(schemeCodeDetailsListVO.size() == 0){
					
					throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, "No records fetched",
							EBankingErrorCodes.NO_RECORDS_FOUND);
				}
				// Calling white list API for fetching the scheme codes - start
				CustomSchemeCodeWLEnquiryVO schmCodeWLInqVO = (CustomSchemeCodeWLEnquiryVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSchemeCodeWLEnquiryVO");
				FEBAArrayList<CustomSchemeCodeWLDetailsVO> schemeCodesList = null;
				
				if(FEBATypesUtility.isNotNullOrBlank(enquiryVO.getCriteria().getPartnerId())){
					schmCodeWLInqVO.getCriteria().setPartnerId(enquiryVO.getCriteria().getPartnerId());
					schmCodeWLInqVO = (CustomSchemeCodeWLEnquiryVO) serviceSchmCodeWLStub.callService(context, schmCodeWLInqVO,
							new FEBAUnboundString("fetch"));
					
					schemeCodesList = schmCodeWLInqVO.getResultList();
				}				
				// Calling white list API for fetching the scheme codes - end
			
			if(null != schemeCodeDetailsListVO && schemeCodeDetailsListVO.size()>0){
				
				for (int i = 0; i < schemeCodeDetailsListVO.size(); i++) {
					
					System.out.println("Inside loop number - "+i+" schemeCodeDetailsListVO - "+schemeCodeDetailsListVO.get(i));
					
					CustomLoanSchmCodeDetailsVO detailsVO = schemeCodeDetailsListVO.get(i);
										
					// initialize the value to zero for loop every iteration.
					k = 0;
					
					// Logic added to skip adding the scheme code details in Response object If scheme code is not present in White list Scheme codes - start
					if(null != schemeCodesList){
						for (k = 0; k < schemeCodesList.size(); k++) {
							CustomSchemeCodeWLDetailsVO schmCodeWLDetailsVO = schemeCodesList.get(k);
							System.out.println("Inside loop number - "+i+" schemeCodesList - "+schemeCodesList.get(k));
							System.out.println("schmCodeWLDetailsVO.getSchemeCode():: "+schmCodeWLDetailsVO.getSchemeCode());
							System.out.println("detailsVO.getSchemeCode().getValue():: "+detailsVO.getSchemeCode().getValue());
							if(FEBATypesUtility.isNotNullOrBlank(schmCodeWLDetailsVO.getSchemeCode()) 
									&& schmCodeWLDetailsVO.getSchemeCode().getValue().equals(detailsVO.getSchemeCode().getValue())){
								schemeCodeMatches = true;
								break;
							}
						}
						if(schemeCodeMatches == true){
							schemeCodeMatches = false;
							
						}else{
							continue;
						}
					}

					
					// Logic added to skip adding the scheme code details in Response object If scheme code is not present in White list Scheme codes - end
					
					CustomFetchLoanSchemesReferencesVO pojoVO = new CustomFetchLoanSchemesReferencesVO();
					CustomFeesReferencesVO feePojoVO = new CustomFeesReferencesVO();
					
					System.out.println("detailsVO - "+detailsVO);
					
					/* Reading Scheme code LIST details VO - start */
					pojoVO.setSchemeCode(detailsVO.getSchemeCode().getValue());
					pojoVO.setSchemeDesc(detailsVO.getSchemeDescription().getValue());
					
					// Set minimum loan amount for SCHEME CODE details
					CustomAmountVO amntVO = new CustomAmountVO();
					amntVO.setAmount(
							new BigDecimal(detailsVO.getMinLoanAmount().toString()).setScale(0, RoundingMode.HALF_UP).toString());
					amntVO.setCurrency(currency);
					pojoVO.setMinLoanAmount(amntVO);
					
					// Set maximum loan amount for SCHEME CODE details
					CustomAmountVO amntVO1 = new CustomAmountVO();
					amntVO1.setAmount(
							new BigDecimal(detailsVO.getMaxLoanAmount().toString()).setScale(0, RoundingMode.HALF_UP).toString());
					amntVO1.setCurrency(currency);
					pojoVO.setMaxLoanAmount(amntVO1);
					
					pojoVO.setMinRepayMonths(Integer.parseInt(detailsVO.getMinRepayMonths().getValue()));
					pojoVO.setMinRepayDays(Integer.parseInt(detailsVO.getMinRepayDays().getValue()));
					pojoVO.setMaxRepayMonths(Integer.parseInt(detailsVO.getMaxRepayMonths().getValue()));
					pojoVO.setMaxRepayDays(Integer.parseInt(detailsVO.getMaxRepayDays().getValue()));
					pojoVO.setNormalInterest(Double.valueOf(detailsVO.getNormalInterest().getValue()));
					pojoVO.setPenalInterest(Double.valueOf(detailsVO.getPenalInterest().getValue()));
					/* Reading Scheme code LIST details VO - End */
					
					System.out.println("feeDetailsListVO.size():: "+feeDetailsListVO.size());
					
					int processFeeCntInt = Integer.parseInt(detailsVO.getProsfFeeCount().getValue());
					pojoVO.setFeesCount(processFeeCntInt);

					System.out.println("processFeeCntInt:: "+processFeeCntInt);

					if(null != feeDetailsListVO && feeDetailsListVO.size() > 0 && processFeeCntInt > 0 ){
					
						/* Reading FEE LIST details VO - start */
						for (int j = 0; j < feeDetailsListVO.size(); j++) {
							
							CustomLoanSchmCodeFeesDetailsVO feeDetailsVO = feeDetailsListVO.get(j);
							feePojoVO = new CustomFeesReferencesVO();
							
							System.out.println("IF:: feeDetailsVO:: "+feeDetailsVO);
							
							// Set minimum amount for FEEdetails
							CustomAmountVO amntVO2 = new CustomAmountVO();
							amntVO2.setAmount(
									new BigDecimal(feeDetailsVO.getMinimumAmount().toString()).setScale(0, RoundingMode.HALF_UP).toString());
							amntVO2.setCurrency(currency);
							feePojoVO.setMinAmount(amntVO2);
							
							// Set maximum amount for FEEdetails
							CustomAmountVO amntVO3 = new CustomAmountVO();
							amntVO3.setAmount(
									new BigDecimal(feeDetailsVO.getMaximumAmount().toString()).setScale(0, RoundingMode.HALF_UP).toString());
							amntVO3.setCurrency(currency);
							feePojoVO.setMaxAmount(amntVO3);
							
							// Set Fee amount for FEEdetails
							CustomAmountVO amntVO4 = new CustomAmountVO();
							amntVO4.setAmount(
									new BigDecimal(feeDetailsVO.getFeeAmount().toString()).setScale(0, RoundingMode.HALF_UP).toString());
							amntVO4.setCurrency(currency);
							feePojoVO.setFeeAmount(amntVO4);
							
							feeList.add(feePojoVO);
							
							System.out.println("IF :: feeList:: "+feeList);
							
						}
						/* Reading FEE LIST details VO - end */
					}else {
						
						feePojoVO = new CustomFeesReferencesVO();
						
						CustomAmountVO amntVO2 = new CustomAmountVO();
						CustomAmountVO amntVO3 = new CustomAmountVO();
						CustomAmountVO amntVO4 = new CustomAmountVO();
						System.out.println("Else :: amntVO2:: "+amntVO2);
						System.out.println("Else :: amntVO3:: "+amntVO3);
						System.out.println("Else :: amntVO4:: "+amntVO4);
						
						feePojoVO.setMinAmount(amntVO2);
						feePojoVO.setMaxAmount(amntVO3);
						feePojoVO.setFeeAmount(amntVO4);
						
						feeList.add(feePojoVO);
						
						System.out.println("Else :: feeDetailsVO:: "+feePojoVO);
						System.out.println("Else :: feeList:: "+feeList);
					}
					pojoVO.setFeeDetailsList(feeList);
					feeList = new ArrayList<>();
					outputVO.add(pojoVO);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return outputVO;
	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(103321, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211085, "BE", 400));
		return restResponceErrorCode;
	}
}



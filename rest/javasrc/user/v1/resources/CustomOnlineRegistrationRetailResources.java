package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.common.v1.RestResourceConstants;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationOutputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationRetailInputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.OnlineRegistractionRetailInputDetailsVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.OnlineRegistractionRetailResponseOutputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.OnlineRegistractionRetailResponseVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.OnlineRetailUserInputChannelVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.RetailOnlineOfflineAuthenticationDetailsVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.RetailOnlineOfflineSetPasswordDetailsVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.RetailUserOutputDetailsVO;
import com.infosys.ebanking.rest.retail.user.v1.resources.OnlineRetailRegistrationBaseResources;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.ebanking.types.valueobjects.ORCompleteRegistrationVO;
import com.infosys.feba.framework.cache.AppDataConstants;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.FEBAIncidenceCodes;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.AdditionalParam;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.BusinessExceptionVO;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.AuthorizationInputVO;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.FrameworkTypesCatalogueConstants;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.lists.FEBAHashList;
import com.infosys.feba.framework.types.primitives.ChannelId;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.MobileNumber;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.primitives.YNFlag;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.feba.framework.types.valueobjects.GenericFieldVO;
import com.infosys.feba.tools.common.util.Logger;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.fentbase.common.FBAIncidenceCodes;
import com.infosys.fentbase.common.FBATransactionContext;
import com.infosys.fentbase.types.entityreferences.PrincipalIDER;
import com.infosys.fentbase.types.primitives.LAFRequestType;
import com.infosys.fentbase.types.primitives.Password;
import com.infosys.fentbase.types.valueobjects.DBNavListVO;
import com.infosys.fentbase.types.valueobjects.DBSectionVO;
import com.infosys.fentbase.types.valueobjects.ForgotPasswordOfflineVO;
import com.infosys.fentbase.types.valueobjects.LoginAltFlowVO;
import com.infosys.fentbase.types.valueobjects.PasswordVO;
import com.infosys.fentbase.types.valueobjects.UserChannelLinkageDetailsVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Palani_Tamilarasan
 *
 *         CustomOnlineRegistrationRetailResources.class the resource create
 *         online registration of retail users ..
 */
@ServiceInfo(moduleName = "USER")
@Path("/v1/banks/{bankid}/custom/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Retail Online Registration")
public class CustomOnlineRegistrationRetailResources extends OnlineRetailRegistrationBaseResources {

	@Context
	protected HttpServletRequest request;

	@Context
	protected HttpServletResponse response;

	DateFormat dateFormat;
	String dtFmt;
	ArrayList channellist = new ArrayList();
	public static final String USER_PRINCIPAL = "USER_PRINCIPAL";
	public static final String USERPHONENUMBER = "USERPHONENUMBER";
	public static final String CUSTOMRMLOGINALTFLOWSERVICE = "CustomRMLoginAltFlowService";
	public static final String CHANNEL_ID = "CHANNEL_ID";

	/**
	 * This method is used to create retail online Registration.
	 * 
	 * @param inputDetails
	 * @return Response
	 */
	@MethodInfo(uri = "/v1/banks/{bankid}/custom/users")
	@POST
	@ApiOperation(value = "To create retail online registration", response = OnlineRegistractionRetailResponseOutputVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Successful created retail online registration", response = OnlineRegistractionRetailResponseOutputVO.class),
			@ApiResponse(code = 401, message = "The autherization and user details Required", response = OnlineRegistractionRetailResponseOutputVO.class) })
	public Response createRetailOnlineRegistration(@PathParam("bankid") String bankId,
			OnlineRegistractionRetailInputDetailsVO inputDetails) {

		OnlineRegistractionRetailResponseVO wrapper = new OnlineRegistractionRetailResponseVO();
		OnlineRegistractionRetailResponseOutputVO data = new OnlineRegistractionRetailResponseOutputVO();
		DeviceRegistrationOutputVO deviceRegistration = new DeviceRegistrationOutputVO();
		Header header = new Header();
		int responseStatus = 201;

		IOpContext opcontext = null;
		try {
			opcontext = (OpContext) request.getAttribute("OP_CONTEXT");

			dtFmt = PropertyUtil.getProperty(FBAConstants.DBUSER_DATE_FORMAT, opcontext);
			dateFormat = new SimpleDateFormat(dtFmt);

			opcontext.setInContextData("REQUEST_TYPE",
					new LAFRequestType(RestResourceConstants.RETAIL_ONLINE_REGSISTRATION_REQST_ID));
			String mode = RestResourceConstants.RETAIL_ONLINE_REGSISTRATION_REQST_ID;
			String rmAprovelReqired = PropertyUtil.getProperty(RestResourceConstants.RM_APPR_REQD_OR, opcontext);

			if (rmAprovelReqired.equals(EBankingConstants.YES)) {
				mode = RestResourceConstants.RETAIL_ONLINE_RM_APPROVEL_REGSISTRATION_REQST_ID;
			}
			String navigation = mode + "Navigation.xml";

			LoginAltFlowVO loginAltflowVO = (LoginAltFlowVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.LoginAltFlowVO);
			loginAltflowVO.setNavigationSource(navigation);
			loginAltflowVO = getNavigationSections(loginAltflowVO, opcontext);

			DBNavListVO dbNavListVO = loginAltflowVO.getLoginAltFlowNavListVO();

			FEBAArrayList navigatinList = getNavigationList(dbNavListVO.getNavList());

			if (inputDetails != null) {
				if (inputDetails.getUserDetail() != null) {
					loginAltflowVO = fetchFormFieldDetails(opcontext,
							RestResourceConstants.ONLINE_RETAIL_REGISTRATION_USER_DETAILS);
					loginAltflowVO = populateUserDetails(loginAltflowVO, inputDetails);
					loginAltflowVO = validateUserDetails(loginAltflowVO, opcontext);
				} else {
					throw new BusinessException(opcontext, EBIncidenceCodes.INVALID_PHONE_NUMBER,
							"Enter the phone number.", EBankingErrorCodes.PLEASE_SPECIFY_PHONENUMBER);
				}

				if (inputDetails.getPasswordDetails() != null) {
					loginAltflowVO = validatePasswordDetails(loginAltflowVO, opcontext,
							inputDetails.getPasswordDetails());
				}

				if (inputDetails.getAuthenticationDetais() != null) {
					validateAuthenticationDetails(navigatinList, inputDetails.getAuthenticationDetais(), opcontext);
				} else {
					responseStatus = 401;
					if (inputDetails.getPasswordDetails() != null) {
						AuthorizationInputVO authmodeDetails = getAutherizationDetails(navigatinList, loginAltflowVO,
								opcontext);
						header.setAuthorization(authmodeDetails);
						wrapper.setHeader(header);
					}
				}

				if (responseStatus != 401 && rmAprovelReqired.equals(EBankingConstants.YES)) {
					submitRequestDetails(loginAltflowVO, opcontext);
				} else if (responseStatus != 401) {
					if (inputDetails.getDeviceDetails() != null) {
						isValidDeviceDetails(opcontext, inputDetails.getDeviceDetails());
					}
					loginAltflowVO = submitCreateRequestDetails(loginAltflowVO, opcontext, channellist);
					String userid = opcontext.getFromContextData(USER_PRINCIPAL).toString();

					if (userid != null && inputDetails.getDeviceDetails() != null) {
						try{
							deviceRegistration = submitDeviceDetails(opcontext, userid, inputDetails.getDeviceDetails());
						}catch(Exception e){
							e.printStackTrace();
						}
						if(RestCommonUtils.isNotNullorEmpty(deviceRegistration.getDeviceId())){
							data.setDeviceRegistrationDetails(deviceRegistration);
						}
						
					}
				}
			} else {
				throw new BusinessException(opcontext, EBIncidenceCodes.INVALID_PHONE_NUMBER, "Enter the phone number.",
						EBankingErrorCodes.PLEASE_SPECIFY_PHONENUMBER);
			}

			onlineRegistrationCorporateOutputDetail(opcontext, data, loginAltflowVO, responseStatus);

		} catch (Exception ex) {
			Logger.logError("Exception e" + ex);
			RestResourceManager.handleFatalErrorOutput(request, response, ex, restResponseErrorCodeMapping());
		}

		if (responseStatus == 401) {
			return Response.status(responseStatus).entity(data).build();
		} else {
			RestResourceManager.updateHeaderFooterResponseData(wrapper, request, response, 0);
			return Response.created(null).entity(data).build();
		}

	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();

	}

	protected LoginAltFlowVO populateUserDetails(LoginAltFlowVO loginAltflowVO,
			OnlineRegistractionRetailInputDetailsVO data) throws BusinessException {

		DBSectionVO userDBSectionVO = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();

		FEBAHashList genericFieldList = userDBSectionVO.getBasicFields();

		GenericFieldVO genericFieldVOObj = (GenericFieldVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.GenericFieldVO);

		if (data.getUserDetail() != null) {
			String fieldValue = null;
			if (RestCommonUtils.isNotNullorEmpty(data.getUserDetail().getUserPhoneNumber())) {
				fieldValue = data.getUserDetail().getUserPhoneNumber();
			} else {
				fieldValue = EBankingConstants.BLANK;
			}
			genericFieldVOObj.initialize();
			genericFieldVOObj.setSName(USERPHONENUMBER);
			genericFieldVOObj.setSValue(fieldValue);
			genericFieldList.cloneAndPut(USERPHONENUMBER, genericFieldVOObj);

		}
		userDBSectionVO.setBasicFields(genericFieldList);
		userDBSectionVO.setSectionId(RestResourceConstants.ONLINE_RETAIL_REGISTRATION_USER_DETAILS);

		loginAltflowVO.getForgotPasswordOfflineVO().setFpSectionVO(userDBSectionVO);
		return loginAltflowVO;
	}

	/**
	 * This method is used to get Navigation section online Registration.
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmations
	 * @throws BusinessException
	 * @throws CriticalException
	 */

	@Override
	protected LoginAltFlowVO validateUserDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		ForgotPasswordOfflineVO frtPwdOfflineVO = loginAltflowVO.getForgotPasswordOfflineVO();

		FEBAHashList hashList = frtPwdOfflineVO.getFpSectionVO().getBasicFields();
		MobileNumber phoneNumber = null;
		if (hashList != null && hashList.size() > 0) {
			phoneNumber = new MobileNumber(((GenericFieldVO) hashList.get(USERPHONENUMBER)).getSValue().toString());
		}
		loginAltflowVO.getForgotPasswordOfflineVO().setMobileNumber(phoneNumber);

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMRMLOGINALTFLOWSERVICE));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("validateUserInputs"));

		return loginAltflowVO;
	}

	/**
	 * This method to validate code type and CM code.
	 * 
	 * @param code_type,cmcode
	 *
	 * @return DBSectionVO
	 */

	public void checkValidTransactionType(IOpContext opcontext, String codeType, String cmcode)
			throws BusinessException {

		IFEBAType codeDescription = null;

		try {
			codeDescription = AppDataManager.getValue(opcontext, AppDataConstants.COMMONCODE_CACHE,
					AppDataConstants.COLUMN_CODE_TYPE + EBankingConstants.EQUAL_TO + codeType
							+ AppDataConstants.SEPERATOR + AppDataConstants.COLUMN_CM_CODE + EBankingConstants.EQUAL_TO
							+ cmcode);
		} catch (CriticalException e) {

			throw new FatalException(opcontext, FEBAIncidenceCodes.UNEXPECTED_EXCEPTION_IN_ERVALIDATOR,
					"Exception while fetching transaction type", e.getCause());

		}

		if (codeDescription == null) {
			throw new BusinessException(opcontext, EBIncidenceCodes.INVALID_COCD_VALUE, "Record does not exist in COCD",
					EBankingErrorCodes.INVALID_CODE_TYPE, new AdditionalParam("CODE_TYPE", cmcode));
		}

	}

	public AuthorizationInputVO getAutherizationDetails(FEBAArrayList navigationList, LoginAltFlowVO loginAltflowVO,
			IOpContext opcontext) throws CriticalException, BusinessException, BusinessConfirmation {
		AuthorizationInputVO authmodeDetails = new AuthorizationInputVO();
		ArrayList authenticationList = new ArrayList();
		for (int section = 0; section < navigationList.size() - 1; section++) {
			if (navigationList.get(section).toString().equals("CLGOTP")
					&& loginAltflowVO.getChangePwdVO().getSignOnFlg().toString().equals("Y")) {
				authenticationList.add(navigationList.get(section).toString());
				generateOTP(opcontext, navigationList.get(section).toString(), loginAltflowVO);
			} else if (navigationList.get(section).toString()
					.equals(RestResourceConstants.ONLINE_RETAIL_REGISTRATION_CREDIT_CARD_SECTIONID)) {
				authenticationList.add(navigationList.get(section).toString());
			} else if (navigationList.get(section).toString()
					.equals(RestResourceConstants.ONLINE_RETAIL_REGISTRATION_DEBIT_CARD_SECTIONID)) {
				authenticationList.add(navigationList.get(section).toString());
			} else if (navigationList.get(section).toString().equals("ORSP")) {
				authenticationList.add(navigationList.get(section).toString());
			}
		}

		if (authenticationList.size() == 1) {
			authmodeDetails.setFirstAuthorizationMode(authenticationList.get(0).toString());
		} else {
			authmodeDetails.setFirstAuthorizationMode(authenticationList.get(0).toString());
			authmodeDetails.setSecondAuthorizationMode(authenticationList.get(1).toString());
		}
		authmodeDetails.setWorkFlowRequired('N');
		authmodeDetails.setUserIdRequired('N');
		return authmodeDetails;
	}

	public void generateOTP(IOpContext opcontext, String sectionId, LoginAltFlowVO loginAltflowVO)
			throws CriticalException, BusinessException, BusinessConfirmation {

		PrincipalIDER principalidER = (PrincipalIDER) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.PrincipalIDER);

		ForgotPasswordOfflineVO frtPwdOfflineVO = loginAltflowVO.getForgotPasswordOfflineVO();
		FEBAHashList hashList = frtPwdOfflineVO.getFpSectionVO().getBasicFields();
		MobileNumber phoneNumber = null;
		if (hashList != null && hashList.size() > 0) {
			phoneNumber = new MobileNumber(((GenericFieldVO) hashList.get(USERPHONENUMBER)).getSValue().toString());
			loginAltflowVO.getForgotPasswordOfflineVO().setMobileNumber(phoneNumber);
			principalidER.setCorpId(phoneNumber.toString());
			principalidER.setUserId(phoneNumber.toString());
		}

		loginAltflowVO.getLoginAltFlowUserDetailsVO().setPrincipalIDER(principalidER);
		loginAltflowVO.getForgotPasswordOfflineVO().getFpFieldDetailsVO().setSectionId(sectionId);

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMRMLOGINALTFLOWSERVICE));
		serviceCStub.callService(opcontext, loginAltflowVO, new FEBAUnboundString("generateAuthAccessCode"));
	}

	@Override
	public void validateAuthenticationDetails(FEBAArrayList navigationList,
			RetailOnlineOfflineAuthenticationDetailsVO authenticationDetais, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		for (int section = 0; section < navigationList.size() - 1; section++) {
			if (navigationList.get(section).toString().equals("CLGOTP")) {
				autherizationOTPSection(navigationList.get(section).toString(), authenticationDetais, opcontext);
			} else if (navigationList.get(section).toString()
					.equals(RestResourceConstants.ONLINE_RETAIL_REGISTRATION_CREDIT_CARD_SECTIONID)) {
				autherizationCreditSection(navigationList.get(section).toString(), authenticationDetais, opcontext);
			} else if (navigationList.get(section).toString()
					.equals(RestResourceConstants.ONLINE_RETAIL_REGISTRATION_DEBIT_CARD_SECTIONID)) {
				autherizationDebitCardSection(navigationList.get(section).toString(), authenticationDetais, opcontext);
			}
		}
	}

	@Override
	public void autherizationOTPSection(String sectionId,
			RetailOnlineOfflineAuthenticationDetailsVO authenticationDetais, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {
		LoginAltFlowVO loginAltflowVO = fetchFormFieldDetails(opcontext, sectionId);
		DBSectionVO userDBSectionVO = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();
		FEBAHashList genericFieldList = userDBSectionVO.getBasicFields();

		GenericFieldVO genericFieldVOObj = (GenericFieldVO) FEBAAVOFactory
				.createInstance(FrameworkTypesCatalogueConstants.GenericFieldVO);

		String fieldValue = null;
		if (RestCommonUtils.isNotNullorEmpty(authenticationDetais.getOneTimePassword())) {
			fieldValue = authenticationDetais.getOneTimePassword();
		} else {
			fieldValue = EBankingConstants.BLANK;
		}
		genericFieldVOObj.initialize();
		genericFieldVOObj.setSName("OTP");
		genericFieldVOObj.setSValue(fieldValue);
		genericFieldList.cloneAndPut("OTP", genericFieldVOObj);

		userDBSectionVO.setBasicFields(genericFieldList);
		userDBSectionVO.setSectionId(sectionId);

		loginAltflowVO.getForgotPasswordOfflineVO().setFpSectionVO(userDBSectionVO);
		getAuthenticateDetails(loginAltflowVO, opcontext);
	}

	protected LoginAltFlowVO submitRequestDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		loginAltflowVO = createLAFRValidateUser(loginAltflowVO, opcontext);
		String userid = opcontext.getFromContextData(USER_PRINCIPAL).toString();

		opcontext.setInContextData("USER_ID", new UserId(userid));
		opcontext.setInContextData("CORPORATE_ID", new CorporateId(userid));
		loginAltflowVO = updateLAFRStatusValidateUser(loginAltflowVO, opcontext);
		return loginAltflowVO;
	}

	/**
	 * This method to generate OTP authorization details based on PRPM is Y.
	 * 
	 * @param authenticationDetails,opcontext
	 *
	 * @return DBSectionVO
	 */

	private void onlineRegistrationCorporateOutputDetail(IOpContext opcontext,
			OnlineRegistractionRetailResponseOutputVO data, LoginAltFlowVO loginAltflowVO, int requestStatus) {

		DBSectionVO userDetails = loginAltflowVO.getForgotPasswordOfflineVO().getFpSectionVO();

		RetailUserOutputDetailsVO userDetail = new RetailUserOutputDetailsVO();

		if (userDetails != null) {
			FEBAHashList hashList = userDetails.getBasicFields();

			GenericFieldVO mobileObj = (GenericFieldVO) hashList.get(USERPHONENUMBER);
			if (mobileObj != null && FEBATypesUtility.isNotNullOrBlank(mobileObj.getSValue())) {
				userDetail.setUserPhoneNumber(mobileObj.getSValue().getValue());
			}

			String userid = opcontext.getFromContextData(USER_PRINCIPAL).toString();
			if (requestStatus != 401 && RestCommonUtils.isNotNullorEmpty(userid)) {
				userDetail.setLoginUserId(userid);
			}

			data.setUserDetail(userDetail);
		}

		if (FEBATypesUtility.isNotNullOrBlank(loginAltflowVO.getForgotPasswordOfflineVO().getRequestID())) {
			data.setReferenceId(loginAltflowVO.getForgotPasswordOfflineVO().getRequestID().getValue());
		}
		if (FEBATypesUtility.isNotNullOrBlank(loginAltflowVO.getForgotPasswordOfflineVO().getRequestStatus())) {
			data.setRequestStatus(
					RestCommonUtils.getCodeReferences(opcontext, RestResourceConstants.ONLINE_REG_REQUEST_STATUS,
							loginAltflowVO.getForgotPasswordOfflineVO().getRequestStatus().getValue()));
		}

	}

	public LoginAltFlowVO validatePasswordDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext,
			RetailOnlineOfflineSetPasswordDetailsVO passwordDetails)
			throws CriticalException, BusinessException, BusinessConfirmation {

		PasswordVO passwordVO = loginAltflowVO.getChangePwdVO();

		if (passwordDetails != null) {
			if (passwordDetails.getPreferredChannels() != null && !passwordDetails.getPreferredChannels().isEmpty()) {
				for (int index = 0; index < passwordDetails.getPreferredChannels().size(); index++) {
					OnlineRetailUserInputChannelVO channnelVO = passwordDetails.getPreferredChannels().get(index);
					if (RestCommonUtils.isNotNullorEmpty(String.valueOf(channnelVO.getChannelId()))) {
						checkValidTransactionType(opcontext, EBankingConstants.CHANNEL_CODE,
								String.valueOf(channnelVO.getChannelId()));
						channellist.add(channnelVO.getChannelId());
					}
				}

			}
			if (RestCommonUtils.isNotNullorEmpty(String.valueOf(passwordDetails.getSignOnPasswordFlag()))) {
				if ((String.valueOf(passwordDetails.getSignOnPasswordFlag()).equals(EBankingConstants.YES))
						|| (String.valueOf(passwordDetails.getSignOnPasswordFlag()).equals(EBankingConstants.NO))) {
					passwordVO.setSignOnFlg(new YNFlag(passwordDetails.getSignOnPasswordFlag()));
				}
				if (!(passwordDetails.getSignOnPassword().trim()
						.equals(passwordDetails.getReTypesignOnPassword().trim()))) {
					throw new BusinessException(opcontext, FBAIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
							"error", "changePwdVO.signOnNewPwd", FBAErrorCodes.CRP_ERR_SIGN_ON_PASSWORD_DO_NOT_MATCH,
							null);

				}
			} else {
				throw new BusinessException(opcontext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
						"Enter the sign on flag not valid.", EBankingErrorCodes.BLANK_SIGNON_PASSWORD_VALIDATION);
			}

			if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getSignOnPassword())) {
				passwordVO.setSignOnNewPwd(passwordDetails.getSignOnPassword());
				passwordVO.setTxnNewPwd(passwordDetails.getSignOnPassword());
			}
			if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getReTypesignOnPassword())) {
				passwordVO.setSignOnDupNewPwd(passwordDetails.getReTypesignOnPassword());
				passwordVO.setTxnDupNewPwd(passwordDetails.getReTypesignOnPassword());
			}
			passwordVO.setTxnFlg(new YNFlag(passwordDetails.getTransactionPasswordFlag()));
		} else {
			throw new BusinessException(opcontext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
					"Enter the sign on password.", EBankingErrorCodes.BLANK_SIGNON_PASSWORD_VALIDATION);
		}
//	custom pin validation check--start
		if (passwordDetails!=null) {
			if (RestCommonUtils.isNotNullorEmpty(passwordDetails.getSignOnPassword())) {
				customCheckPinforPattern(passwordDetails.getSignOnPassword(), opcontext);
			}
		}
//		custom pin validation check--send
		loginAltflowVO.setChangePwdVO(passwordVO);

		return loginAltflowVO;
	}

	public DeviceRegistrationOutputVO submitDeviceDetails(IOpContext opContext, String userId,
			DeviceRegistrationRetailInputVO deviceDetails)
			throws CriticalException, BusinessException, BusinessConfirmation {

		DeviceRegistrationOutputVO detailsVO = new DeviceRegistrationOutputVO();
		DeviceDetailsVO deviceDetailsVO = (DeviceDetailsVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.DeviceDetailsVO);
		if (deviceDetails != null) {
			deviceDetailsVO.setUserID(userId);
			if (RestCommonUtils.isNotNullorEmpty(deviceDetails.getDeviceName())) {
				deviceDetailsVO.setDeviceName(deviceDetails.getDeviceName());
			}
			if (RestCommonUtils.isNotNullorEmpty(deviceDetails.getDeviceId())) {
				deviceDetailsVO.setDeviceId(deviceDetails.getDeviceId());
			}
			if (RestCommonUtils.isNotNullorEmpty(deviceDetails.getDeviceId())) {
				detailsVO.setDeviceId(deviceDetails.getDeviceId());
			}
			if (RestCommonUtils.isNotNullorEmpty(deviceDetails.getDeviceName())) {
				detailsVO.setDeviceName(deviceDetails.getDeviceName());
			}
		}

		IClientStub mPinService = ServiceUtil.getService(new FEBAUnboundString("CustomDeviceRegistrationService"));
		mPinService.callService(opContext, deviceDetailsVO, new FEBAUnboundString("create"));
		return detailsVO;
	}

	public boolean isValidDeviceDetails(IOpContext opContext, DeviceRegistrationRetailInputVO deviceDetails)
			throws BusinessException {
		if (deviceDetails.getMobileNo() == null || deviceDetails.getMobileNo() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
					"Enter the mobile number.", EBankingErrorCodes.CRAP_MOBILE_NO_MANDATORY);
		}
		if (deviceDetails.getDeviceId() == null || deviceDetails.getDeviceId() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.NEW_AND_RETYPE_SIGNON_PWDS_DONOT_MATCH,
					"Enter the device id.", EBankingErrorCodes.DEVICE_ID_MANDATORY);

		}
		return true;
	}

	/**
	 * This method is used to get Navigation section online Registration.
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@Override
	protected LoginAltFlowVO createUser(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomLoginAltFlowService"));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("createORUser"));

		return loginAltflowVO;
	}

	/**
	 * This method is used to get Navigation section online Registration.
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@SuppressWarnings("deprecation")
	@Override
	protected LoginAltFlowVO submitCreateRequestDetails(LoginAltFlowVO loginAltflowVO, IOpContext opcontext,
			ArrayList channelList) throws CriticalException, BusinessException, BusinessConfirmation {
		boolean channelLinkageReq = false;

		ORCompleteRegistrationVO onlineReg = (ORCompleteRegistrationVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.ORCompleteRegistrationVO);

		FEBAArrayList<UserChannelLinkageDetailsVO> chanlist = new FEBAArrayList<>();

		String prpmChannel = PropertyUtil.getProperty("ONLINE_REG_CHANNEL_LIST", opcontext);

		if (prpmChannel.contains("|")) {
			String[] channelLists = prpmChannel.split("|");
			for (int i = 0; i < channelLists.length; i++) {
				channelList.add(channelLists[i]);
			}
		} else {
			channelList.add(prpmChannel);
		}

		if (channelList.isEmpty()) {
			opcontext.setInContextData(CHANNEL_ID, new ChannelId("I"));
		} else if (channelList.size() == 1) {
			opcontext.setInContextData(CHANNEL_ID, new ChannelId(channelList.get(0).toString()));
		} else if (channelList.size() > 1) {
			opcontext.setInContextData(CHANNEL_ID, new ChannelId(channelList.get(0).toString()));
			channelLinkageReq = true;
		}
		loginAltflowVO = createLAFRValidateUser(loginAltflowVO, opcontext);
		String userid = opcontext.getFromContextData(USER_PRINCIPAL).toString();

		opcontext.setInContextData("USER_ID", new UserId(userid));
		opcontext.setInContextData("CORPORATE_ID", new CorporateId(userid));
		loginAltflowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().setUserId(userid);
		loginAltflowVO.getLoginAltFlowUserDetailsVO().getPrincipalIDER().setCorpId(userid);
		loginAltflowVO = updateLAFRStatusValidateUser(loginAltflowVO, opcontext);
		loginAltflowVO = createUser(loginAltflowVO, opcontext);
		for (int channel = 1; channel < channelList.size(); channel++) {
			UserChannelLinkageDetailsVO channelLinkageVo = (UserChannelLinkageDetailsVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.UserChannelLinkageDetailsVO);
			channelLinkageVo.setChannel(new ChannelId(channelList.get(channel).toString()));
			channelLinkageVo.setChannelUserId(userid);
			chanlist.addObject(channelLinkageVo);
		}
		onlineReg.setChannelList(chanlist);
		if (channelLinkageReq) {
			onlineCompletecreation(onlineReg, opcontext);
		}

		return loginAltflowVO;
	}

	/**
	 * This method is used to get Navigation section online Registration.
	 * 
	 * @param loginAltflowVO,opcontext
	 *
	 * @return LoginAltFlowVO
	 * @throws FEBATypeSystemException
	 * @throws BusinessConfirmation
	 * @throws BusinessException
	 * @throws CriticalException
	 */
	@Override
	protected LoginAltFlowVO createLAFRValidateUser(LoginAltFlowVO loginAltflowVO, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation {

		opcontext.setOperatingMode(new FEBAOperatingMode("C"));
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMRMLOGINALTFLOWSERVICE));
		loginAltflowVO = (LoginAltFlowVO) serviceCStub.callService(opcontext, loginAltflowVO,
				new FEBAUnboundString("validateUserInputs"));

		return loginAltflowVO;
	}
	
//	added for custom pin validation -start
	private void customCheckPinforPattern(String password,IOpContext opcontext)
			throws BusinessException {
		if (password.matches("\\d+")) {
			int first = Integer.valueOf(String.valueOf(password.charAt(0)));
			String patern;
			if (first>0) {
				patern = String.valueOf(first * 111111);
			}else{
				patern = "000000";
			}
			if (password.equalsIgnoreCase(patern)) {
				System.out.println("same pin");
				throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
						"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
			}

			for (int i = 1; i < password.length(); i++) {
				if (Integer.valueOf(String.valueOf(password.charAt(i))) == (first + 1)) {
					first++;
				} else {
					return;
				}
			}			
			throw new BusinessException(opcontext, FBAIncidenceCodes.EXCLUDED_NUMBERS_IN_PWD,
					"Password Should not have these letters:"+password, FBAErrorCodes.EXCLUDED_NUMBERS_IN_PWD);
		}
	}
//	added for custom pin validation -end
}

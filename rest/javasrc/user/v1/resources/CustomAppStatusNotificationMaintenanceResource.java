package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAppStatusNotificationListReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAppStatusNotificationListReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAppStatusNotificationOutputVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomAppStatusNotificationResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomNotificationOutputVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomAppStatusNotificationDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomAppStatusNotificationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomNotificationInOutVO;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.retail.user.v1.pojo.ForcePasswordOutputVO;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.logging.LogManager;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 *
 * This is the Resource through which application status notification maintenance is done.
 *
 */
@ServiceInfo(moduleName = "USER")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Custom Application Notification Maintenance")
public class CustomAppStatusNotificationMaintenanceResource extends ResourceAuthenticator {
	
	public static final String MESSAGE = "No records fetched";

	@Context  
	protected HttpServletRequest request;
	

	@GET
	@ApiOperation(value = "Application status Fetch Notification List", response = CustomAppStatusNotificationListReferencesResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_APP_STATUS_NOTIFICATION_LIST_RESOURCE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Notification List", response = CustomAppStatusNotificationListReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Notification List do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchNotificationList (
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId) {

		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		
		CustomAppStatusNotificationListReferencesResponseVO responseVO = new CustomAppStatusNotificationListReferencesResponseVO();
		
		try {		
			isUnAuthenticatedRequest();

			List <CustomAppStatusNotificationListReferencesVO> outputVO = fetchAppStatusNotificationListOutput (opcontext);
			responseVO.setList(outputVO);
		}
		catch (Exception e) {
			LogManager.logError(opcontext, e);
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}
		
		return Response.ok().entity(responseVO).build();
	}
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List <CustomAppStatusNotificationListReferencesVO> fetchAppStatusNotificationListOutput (IOpContext context) throws FEBAException {
	
		List <CustomAppStatusNotificationListReferencesVO> outputVO= new ArrayList();
		
		CustomAppStatusNotificationEnquiryVO enquiryVO = (CustomAppStatusNotificationEnquiryVO) FEBAAVOFactory.createInstance(CustomTypesCatalogueConstants.CustomAppStatusNotificationEnquiryVO);
		
		final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomAppStatusNotificationMaintenanceListService"));
		enquiryVO = (CustomAppStatusNotificationEnquiryVO) serviceStub.callService(context, enquiryVO, new FEBAUnboundString("fetch"));
		
		FEBAArrayList<CustomAppStatusNotificationDetailsVO> detailsListVO = enquiryVO.getResultList();
		for (int i = 0; i < detailsListVO.size(); i++) {
			CustomAppStatusNotificationDetailsVO detailsVO = detailsListVO.get(i);
			CustomAppStatusNotificationListReferencesVO pojoVO = new CustomAppStatusNotificationListReferencesVO ();
			
			if (null == detailsVO) {
				throw new BusinessException(context, CustomEBankingIncidenceCodes.NO_RECORDS_FETCHED, MESSAGE,
						EBankingErrorCodes.NO_RECORDS_FOUND);
			}
			
			if(null != detailsVO){
				
				pojoVO.setAppNotificationStatus(detailsVO.getAppNotificationStatus().toString());
				if(FEBATypesUtility.isNotBlankDate(detailsVO.getNotificationDate())){
					pojoVO.setNotificationDate(formatDateBahasa(detailsVO.getNotificationDate().toString().substring(0,10)));
				}
				
				pojoVO.setNotificationHeader(detailsVO.getAppNotificationHeader().toString());
				pojoVO.setShortDescription(detailsVO.getShortDescription().getValue());
				pojoVO.setIsNew(detailsVO.getIsNew().toString());
				pojoVO.setNotificationId(detailsVO.getNotificationId().toString());
				pojoVO.setAppNotificationType(detailsVO.getAppNotificationType().getValue());
				pojoVO.setAppNotificationStatus(detailsVO.getAppNotificationStatus().getValue());
			}
			
			outputVO.add(pojoVO);
		}
		return outputVO;
	}
	
	
	/**
	 * This method is used for updating notification status
	 * @return Response
	 */
	@PUT
	@ApiOperation(value = "Update notification Table based on notification id and user id", response = ForcePasswordOutputVO.class)
	@MethodInfo(uri = CustomResourcePaths.CUSTOM_APP_STATUS_NOTIFICATION_UPDATE_RESOURCE, auditFields = {"notificationId : Notification Id input","userId : User Id input" })
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Record Updated successfully.", response = ForcePasswordOutputVO.class) })
	public Response updateRequest(@ApiParam(value = "The ID of the user", required = true) @Size(min=0,max=32) @PathParam("userid") String userid, 
			@ApiParam(value = "The notification ID", required = true) @Size(min=0,max=32) @PathParam("notificationId") String notificationId) {

		CustomAppStatusNotificationResponseVO responseVO = new CustomAppStatusNotificationResponseVO();

		CustomAppStatusNotificationOutputVO output = null;
		

		IOpContext opcontext = (OpContext) request
				.getAttribute("OP_CONTEXT");
		
		try {
			isUnAuthenticatedRequest();
			
			CustomAppStatusNotificationDetailsVO inOutVO = (CustomAppStatusNotificationDetailsVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomAppStatusNotificationDetailsVO);
			inOutVO.setNotificationId(notificationId);
			updateNotificationStatus(inOutVO, opcontext);
			
			output = new CustomAppStatusNotificationOutputVO();
			output.setMessage("Notification Status updated Successfully");
			output.setStatus(true);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request, response, 0);
		} catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, null);
		}finally {
			if (opcontext != null) {
				opcontext.cleanup();
			}
		}

		return Response.ok().entity(output).build();
	}
	private CustomAppStatusNotificationDetailsVO updateNotificationStatus(CustomAppStatusNotificationDetailsVO inOutVO, IOpContext context) throws CriticalException, BusinessException, BusinessConfirmation{
		
		IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomAppStatusNotificationMaintenanceService"));
		inOutVO = (CustomAppStatusNotificationDetailsVO) serviceCStub.callService(context, inOutVO, new FEBAUnboundString("update"));
		
		return inOutVO;
		
	}
	public static String formatAmount(String amount){
		String formattedAmount = "";

		try{
			amount = amount.substring(0, amount.indexOf("."));
			Long longVal = Long.parseLong(amount);
			Locale localIDR = new Locale("in","ID");//100.0
			DecimalFormat formatIDR = (DecimalFormat)NumberFormat.getInstance(localIDR);
			formattedAmount = formatIDR.format(longVal);

		}catch(Exception e){
			e.printStackTrace();
		}
		return formattedAmount;
	}
	public static String formatDateBahasa(String date){
		String formattedDate="";
		Date tempDate = new Date();
		Locale locale = new Locale("id", "ID");
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy",locale);

		try{
			tempDate=df.parse(date);
		}catch(Exception e){
			return date;	
		}
		df.applyPattern("dd MMM yyyy");

		formattedDate=df.format(tempDate);

		return formattedDate;
	}
}

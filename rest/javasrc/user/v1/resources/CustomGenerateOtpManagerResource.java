package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.commonweb.context.OpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomGenerateOtpReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomGenerateOtpReferencesVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCompanyDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingConstants;
import com.infosys.ebanking.hif.camel.beans.Header;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "Generate OTP")
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/generateOtp")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })
public class CustomGenerateOtpManagerResource extends ResourceAuthenticator{
	
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse httpResponse;
	
	@Context
	UriInfo uriInfo;
	
	@GET
	@MethodInfo(uri = CustomResourcePaths.OTP_GENERATION_URL, auditFields = {"privyId : Privyd Id"})
	@ApiOperation(value="Privy Token Creation" , response=CustomGenerateOtpReferencesVO.class)
	@ApiResponses(value = {
			@ApiResponse(code=200, message= "Create Token Success" , response = CustomGenerateOtpReferencesVO.class),
			@ApiResponse(code=402, message= "Failed :Invalid PrivyId"),
			@ApiResponse(code=500, message= "Internal Server Error")
	})
	public Response fetchByUserId(
			@ApiParam(value = "Bank Id input", required = true) @PathParam("bankid") String bankId,
			@ApiParam(value = "User Id input", required = true) @PathParam("userid") String userId,
			@ApiParam(value = "Application Id input", required = true) @PathParam("applicationId") String applicationId){
		     
		IOpContext opcontext = (IOpContext) request.getAttribute("OP_CONTEXT");
		
		 CustomGenerateOtpReferencesVO outputVO = null;
		try {
			isUnAuthenticatedRequest();
			
			CustomPrivyRegistrationVO enquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);
			enquiryVO.setUserId(userId);
			
			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
			enquiryVO = (CustomPrivyRegistrationVO) serviceStub.callService(opcontext, enquiryVO, new FEBAUnboundString("tokenCreate"));
			
			
			FEBAUnboundString token=enquiryVO.getToken();
			
	        if (token !=null) {
	        	ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
	        	enquiryVO = (CustomPrivyRegistrationVO) serviceStub.callService(opcontext, enquiryVO, new FEBAUnboundString("OTPRequest"));
	        }
	        else {
	        	throw new BusinessException(
						true,
						opcontext,
						CustomEBankingIncidenceCodes.TOKEN_NOT_FOUND,
						"Invalid Token",
						null,
						CustomEBankingErrorCodes.INVALID_TOKEN,
						null);
	        }
	         outputVO= fetchTokenDetails(enquiryVO,opcontext);
			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request, 0);
		}
		catch (Exception e) {
			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}
		
		return Response.ok().entity(outputVO).build();
	}
	
	public CustomGenerateOtpReferencesVO fetchTokenDetails(CustomPrivyRegistrationVO enquiryVO ,IOpContext context) throws FEBAException{
		
		CustomGenerateOtpReferencesVO outputVO=new CustomGenerateOtpReferencesVO();
		 
		
		if(enquiryVO.getCode()!=null){
			outputVO.setCode(enquiryVO.getCode().toString());;
		}
		if(enquiryVO.getStatus()!=null){
			outputVO.setStatus(enquiryVO.getStatus().toString());
		}
		
		if(enquiryVO.getMessage()!=null){
			outputVO.setMessage(enquiryVO.getMessage().toString());
		}
	    
		if(enquiryVO.getToken()!=null){
			outputVO.setToken(enquiryVO.getToken().toString());
		}
		if(enquiryVO.getRequestId()!=null){
			outputVO.setRequestId(enquiryVO.getRequestId().toString());
		}
		
		
	    return outputVO;
	    
	    
	}
	
	
	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<RestResponseErrorCodeMapping>();



		return restResponceErrorCode;
	}
    
}

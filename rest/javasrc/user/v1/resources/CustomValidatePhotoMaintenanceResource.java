package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDocumentInputDetailsVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPhotoValidationReferenceResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomPhotoValidationReferenceVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocBinaryListVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnDocumentsDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPhotoValidationDetailsVO;
//import com.infosys.custom.ebanking.types.valueobjects.CustomApplicationDocEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.user.util.CustomDocumentUploadUtil;
import com.infosys.custom.ebanking.user.util.CustomLoanHistoryUpdateUtil;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.types.primitives.ApplicationNumber;
import com.infosys.ebanking.types.primitives.DocumentType;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Api(value = "Photo Validation")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/v1/banks/{bankid}/users/{userid}/custom/applications/{applicationId}/valdiatePhoto")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class)
})

public class CustomValidatePhotoMaintenanceResource extends ResourceAuthenticator {

	@Context
	HttpServletRequest request1;
	@Context
	HttpServletResponse httpResponse;

	public static final String OP_CONTEXT = "OP_CONTEXT";


	@POST
	@ApiOperation(value = "Photo Validation", response = CustomPhotoValidationReferenceResponseVO.class)
	@MethodInfo(uri = CustomResourcePaths.PHOTO_MAINTENANCE_URL)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "The request has been succeeded", response = CustomPhotoValidationReferenceResponseVO.class),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response hitCallback(CustomPhotoValidationReferenceVO photoVO,	
			@ApiParam(value = "bankid", required = true) @PathParam("bankid") String bankid,
			@ApiParam(value = "userid", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "applicationId", required = true) @PathParam("applicationId") String applicationId){
		CustomPhotoValidationReferenceResponseVO responseVO = new CustomPhotoValidationReferenceResponseVO();
		FEBATransactionContext objTxnContext = null;
		try {
			isUnAuthenticatedRequest();
			CustomPhotoValidationDetailsVO detailsVO = (CustomPhotoValidationDetailsVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPhotoValidationDetailsVO);

			detailsVO.setApplicationId(new FEBAUnboundString(applicationId));
			detailsVO.setUserId(new FEBAUnboundString(userid));									
			detailsVO.setSelfie1(photoVO.getLiveImage1().getFileData().replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", ""));
			detailsVO.setSelfie2(photoVO.getLiveImage2().getFileData().replaceAll("(?:\\r\\n|\\n\\r|\\n|\\r)", ""));
			detailsVO.setToken(photoVO.getToken());

			responseVO = getSingleResponse(detailsVO, "validate");

			RestCommonSuccessMessageHandler outputHeader = new RestCommonSuccessMessageHandler();
			RestResourceManager.updateHeaderFooterResponseData(outputHeader, request1, 0);

			IOpContext opContext = (IOpContext) request1.getAttribute(OP_CONTEXT);
			objTxnContext = RestCommonUtils.getTransactionContext(opContext);

			new CustomLoanHistoryUpdateUtil().updateHistory(new ApplicationNumber(applicationId), responseVO.getData().get(0).getStatus(), "",
					objTxnContext);
		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		}finally{
			if(objTxnContext != null){
				try {
					objTxnContext.cleanup();
				} catch (CriticalException e) {
					e.printStackTrace();
				}
			}
		}
		return Response.ok().entity(responseVO).build();
	}

	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211056, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211057, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211058, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211059, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211060, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211302, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211094, "BE", 400));

		return restResponceErrorCode;
	}

	private CustomPhotoValidationReferenceResponseVO getSingleResponse(
			CustomPhotoValidationDetailsVO detVO, String method) throws FEBATypeSystemException, Exception {
		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
		final IClientStub serviceCStub = ServiceUtil
				.getService(new FEBAUnboundString("CustomPhotoValidateService"));
		return getResponsePOJO((CustomPhotoValidationDetailsVO) serviceCStub.callService(context, detVO,
				new FEBAUnboundString(method)));							
	}



	private CustomPhotoValidationReferenceResponseVO getResponsePOJO(
			CustomPhotoValidationDetailsVO detailsVO) throws Exception{
		IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");					

		String failed="FAILED";
		int tries=Integer.parseInt(PropertyUtil.getProperty("CUST_MAX_TOKEN_TRIES", context));
		int count=1;

		CustomPhotoValidationReferenceResponseVO referencesResponseVO = new CustomPhotoValidationReferenceResponseVO();
		CustomPhotoValidationReferenceVO referencesVO = new CustomPhotoValidationReferenceVO();

		List<CustomPhotoValidationReferenceVO> outputVO = null;
			
		// Changes have done for CPU high utilization issue in production start.
		while(count<=tries) {		
			
			int responseCode = detailsVO.getCode().getValue();
			
			if (responseCode == 200)
			{						
				referencesVO.setStatusAfterPhotoCompare("COMP_DONE");
				referencesVO.setStatus("COMP_DONE");
				break;
			} else if(responseCode == 403)						
			 {   	
				if(count == tries)  
					break;	
				else
					count++; 
 
				CustomPrivyRegistrationVO regVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO");

				final IClientStub serviceCStub3 = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));	
				regVO.setUserId(detailsVO.getUserId());	
				serviceCStub3.callService(context, regVO, new FEBAUnboundString("tokenCreate"));			
				detailsVO.setToken(regVO.getToken());			
				final IClientStub serviceCStub2 = ServiceUtil.getService(new FEBAUnboundString("CustomPhotoValidateService"));
				serviceCStub2.callService(context, detailsVO, new FEBAUnboundString("validate"));
				
			}else{
				
				if(count == tries)  
					break;	
				else
					count++;
			}
		}
		
		/*while(true) {			
			if (detailsVO.getCode().toString().equals("200"))
			{						
				referencesVO.setStatusAfterPhotoCompare("COMP_DONE");
				referencesVO.setStatus("COMP_DONE");
				break;
			}
			else if(detailsVO.getCode().toString().equals("403"))						
			{   	
				if(count>tries) 			
					break;			
				else 
					count++;

				CustomPrivyRegistrationVO regVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO");

				final IClientStub serviceCStub3 = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));	
				regVO.setUserId(detailsVO.getUserId());	
				serviceCStub3.callService(context, regVO, new FEBAUnboundString("tokenCreate"));			
				detailsVO.setToken(regVO.getToken());			
				final IClientStub serviceCStub2 = ServiceUtil.getService(new FEBAUnboundString("CustomPhotoValidateService"));
				serviceCStub2.callService(context, detailsVO, new FEBAUnboundString("validate"));							
			}
		}*/

		int responseCode = detailsVO.getCode().getValue();
		
		if(count<=tries && responseCode != 200)
		{			
			referencesVO.setStatusAfterPhotoCompare(failed);
			throw new BusinessException(true, context, "PRIVAL01",			  
					"Face verification failed", null, 211302, null);
		}		
		// Changes have done for CPU high utilization issue in production end.


		CustomLoanApplnMasterDetailsVO statusVO = (CustomLoanApplnMasterDetailsVO) FEBAAVOFactory.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO");        	


		try {
			referencesVO.setStatusAfterDocCreation(callCreatePDF(context, detailsVO));
		} catch (Exception e) {
			referencesVO.setStatusAfterDocCreation(failed);
			throw e;
		}
		if(!referencesVO.getStatusAfterDocCreation().equalsIgnoreCase(failed))
		{
			referencesVO.setStatus(referencesVO.getStatusAfterDocCreation());
		}

		if(!referencesVO.getStatus().isEmpty())		
		{ 
			final IClientStub serviceCStub4 = ServiceUtil.getService(new FEBAUnboundString("CustomLoanApplicationService"));
			statusVO.setApplicationId(new ApplicationNumber(detailsVO.getApplicationId().toString()));
			statusVO.setApplicationStatus(referencesVO.getStatus());
			serviceCStub4.callService(context, statusVO, new FEBAUnboundString("modify"));	
		} 

		referencesVO.setToken(detailsVO.getToken().toString());
		referencesVO.setApplicationId(detailsVO.getApplicationId().toString());
		referencesVO.setMerchantKey(detailsVO.getMerchantKey().toString());
		outputVO = new ArrayList<>();
		outputVO.add(referencesVO);
		referencesResponseVO.setData(outputVO);
		return referencesResponseVO;
	}

	public String callCreatePDF(IOpContext context, CustomPhotoValidationDetailsVO detailsVO) throws Exception {

		CustomApplicationDocEnquiryVO enquiryVO = (CustomApplicationDocEnquiryVO) FEBAAVOFactory
				.createInstance(CustomTypesCatalogueConstants.CustomApplicationDocEnquiryVO);
		enquiryVO.getCriteria().setApplicationId(new ApplicationNumber(detailsVO.getApplicationId().toString()));
		enquiryVO.getCriteria().setMerchantKey(detailsVO.getMerchantKey());
		enquiryVO.getCriteria().setToken(detailsVO.getToken());

		final IClientStub serviceCStub = ServiceUtil
				.getService(new FEBAUnboundString("CustomApplicationDocumentSigningService"));


		try {
			serviceCStub.callService(context, enquiryVO, new FEBAUnboundString("generatePDF"));

			FEBAArrayList binaryList = enquiryVO.getBinaryList();

			CustomDocumentInputDetailsVO documentDetails = new CustomDocumentInputDetailsVO();
			documentDetails.setApplicationId(Long.valueOf(detailsVO.getApplicationId().toString()));

			for (int i = 0; i < binaryList.size(); i++) {
				CustomApplicationDocBinaryListVO binaryVO = (CustomApplicationDocBinaryListVO) binaryList.get(i);
				documentDetails.setFile(binaryVO.getDocString().toString());
				documentDetails.setFileName(binaryVO.getDocTitle().toString() + ".pdf");

				documentDetails = new CustomDocumentUploadUtil()
						.createFileInSharedDirAndUploadEncryptedFile(documentDetails, context, request1);

				CustomLoanApplnDocumentsDetailsVO docDetailsVO = (CustomLoanApplnDocumentsDetailsVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomLoanApplnDocumentsDetailsVO);
				docDetailsVO.setApplicationId(new ApplicationNumber(documentDetails.getApplicationId()));
				docDetailsVO.setDocType(new DocumentType(binaryVO.getDocumentId().toString()));

				docDetailsVO.setDocStorePath(documentDetails.getFileUploadPath());
				docDetailsVO.setFileSeqNo(documentDetails.getFileSeqNo());

				final IClientStub customDocDetailsService = ServiceUtil
						.getService(new FEBAUnboundString("CustomLoanApplicationDocDetailsService"));

				customDocDetailsService.callService(context, docDetailsVO, new FEBAUnboundString("createOrUpdate"));
			}
			return("DOCUMENT_SIGNED");

		} catch (CriticalException | BusinessException | BusinessConfirmation | FEBATypeSystemException e) {
			throw e;
		}

	}
}

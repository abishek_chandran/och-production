package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.common.v1.CustomResourcePaths;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomATMInquiryCallReferencesResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomATMInquiryCallReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomATMInquiryCallRequestVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCAMSCallReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomSilverLakeAcctInquiryReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomSilverLakeCIFInquiryReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomWhitelistInquiryReferencesVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCustomerDedupCheckEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqCriteriaVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomWhiteListDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomWhiteListEnquiryVO;
import com.infosys.ebanking.hif.camel.beans.Header;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.feba.framework.cache.AppDataManager;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.types.FEBATypesUtility;
import com.infosys.feba.framework.types.IFEBAType;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "ATM Inquiry Call")
@Path("/v1/banks/{bankid}/users/{userid}/custom/atminquiry")
@ApiResponses(value = { @ApiResponse(code = 400, message = ResourcePaths.HTTP_ERROR_400, response = Header.class),
		@ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 403, message = ResourcePaths.HTTP_ERROR_403, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 422, message = ResourcePaths.HTTP_ERROR_422, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class), })

public class CustomATMInquiryCallResource extends ResourceAuthenticator {

	final IClientStub camsServiceCStub = ServiceUtil.getService(new FEBAUnboundString("CustomCAMSCallService"));
	final IClientStub whitelistServiceCStub = ServiceUtil
			.getService(new FEBAUnboundString("CustomWhiteListCallService"));
	final IClientStub slakeAcctServiceCStub = ServiceUtil
			.getService(new FEBAUnboundString("CustomSilverlakeAcctCallService"));
	final IClientStub slakeCIFServiceCStub = ServiceUtil
			.getService(new FEBAUnboundString("CustomSilverlakeCIFCallService"));
	final IClientStub restAPILimitService = ServiceUtil.getService(new FEBAUnboundString("CustomRestAPILimitService"));
	final IClientStub customerDedupCheckService = ServiceUtil.getService(new FEBAUnboundString("CustomATMInquiryService"));

	@Context
	HttpServletRequest request1;

	@Context
	HttpServletResponse httpResponse1;

	@Context
	UriInfo uriInfo1;

	@POST
	@MethodInfo(uri = CustomResourcePaths.CAMS_CALL_URL, auditFields = { "ktp : CAMS call KTP input",
			"cardNumber: CAMS call card number input" })
	@ApiOperation(value = "Fetch branch code and expiry date along with account number based on ktp card number and expiry date", response = CustomATMInquiryCallReferencesResponseVO.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Retrieval of Card Details", response = CustomATMInquiryCallReferencesResponseVO.class),
			@ApiResponse(code = 404, message = "Card details do not exist"),
			@ApiResponse(code = 500, message = "Internal Server Error") })
	public Response fetchRequest(CustomATMInquiryCallRequestVO customATMInquiryCallRequestVO) {
		
		CustomATMInquiryCallReferencesResponseVO responseVO = new CustomATMInquiryCallReferencesResponseVO();
		
		String ktp = customATMInquiryCallRequestVO.getKtp();
		String cardNumber = customATMInquiryCallRequestVO.getCardNumber();
		String expiryDate = customATMInquiryCallRequestVO.getExpiryDate();	
		String bankCode = customATMInquiryCallRequestVO.getBankCode();
		String productCode = customATMInquiryCallRequestVO.getProductCode();
		
		FEBATransactionContext objTxnContext = null;
		try {
			isUnAuthenticatedRequest();
			IOpContext context = (IOpContext) request1.getAttribute("OP_CONTEXT");
			
			//Added Service Call for Rest API Tracking Util
			CustomRestAPILimitInOutVO inOutVO = (CustomRestAPILimitInOutVO) FEBAAVOFactory
					.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomRestAPILimitInOutVO");
			inOutVO.setApplicationKey("ATM_INQUIRY");
			restAPILimitService.callService(context, inOutVO,
						new FEBAUnboundString("checkInvocation"));
			//Added Service Call for Rest API Tracking Util
			
			List<CustomATMInquiryCallReferencesVO> outputVO = fetchDetails(ktp, cardNumber, expiryDate, bankCode, productCode, context);
			responseVO.setData(outputVO);
			RestResourceManager.updateHeaderFooterResponseData(responseVO, request1, 0);

		} catch (Exception e) {
			e.printStackTrace();
			RestResourceManager.handleFatalErrorOutput(request1, response, e, restResponseErrorCodeMapping());
		} 
		return Response.ok().entity(responseVO).build();
	}
	
	@SuppressWarnings("unchecked")
	public void customerDedupCheck(String ktp, IOpContext context) throws FEBAException {
		try{

			String isCustomerDedupCheckEnabled = PropertyUtil.getProperty("IS_CUSTOMER_DEDUP_CHECK_ENABLED", context);
			System.out.println("isCustomerDedupCheckEnabled - "+isCustomerDedupCheckEnabled);
			System.out.println("ktp - "+ktp);
			isCustomerDedupCheckEnabled="Y";
			if(isCustomerDedupCheckEnabled.equalsIgnoreCase("Y")){
				CustomCustomerDedupCheckEnquiryVO enqVO = (CustomCustomerDedupCheckEnquiryVO) FEBAAVOFactory
						.createInstance(CustomTypesCatalogueConstants.CustomCustomerDedupCheckEnquiryVO);
				enqVO.getCriteria().setPanOrNationalId(ktp);
				enqVO.getCriteria().setUserId(context.getFromContextData("USER_ID").toString());
				System.out.println("enqVO After set criteria - "+enqVO);
			enqVO = (CustomCustomerDedupCheckEnquiryVO)customerDedupCheckService.callService(context, enqVO,
						new FEBAUnboundString("dedupCheck"));
				
				System.out.println("enqVO After service call- "+enqVO.getResultList().size());

				if(null != enqVO.getResultList()){
					throw new BusinessException(true, context, CustomEBankingIncidenceCodes.CUSTOMER_DEDUP_CHECK_FAILD,
							"Customer Failed de-dup check", null, CustomEBankingErrorCodes.CUSTOMER_DEDUP_CHECK_FAILD, null);
				}
				
			}
		
		}catch(FEBAException fe){
			fe.printStackTrace();
			if(fe.getErrorCode()!=100465){
				throw fe;
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	public List<CustomATMInquiryCallReferencesVO> fetchDetails(String ktp, String cardNumber, String expiryDate, String bankCode, 
			String productCode, IOpContext context) throws FEBAException {

		List<CustomATMInquiryCallReferencesVO> outputVO = null;
		String branchCode = "";
		CustomCAMSCallEnquiryVO enquiryVO = (CustomCAMSCallEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO");

		if (null != ktp) {
			enquiryVO.getCriteria().setKtp(ktp);
		}
		if (null != cardNumber) {
			enquiryVO.getCriteria().setCardNumber(cardNumber);
		}
		if (null != expiryDate) {
			enquiryVO.getCriteria().setExpiryDate(expiryDate);
		}
		
		// Added for BRI AGRO payroll
		if (null != bankCode) {
			enquiryVO.getCriteria().setBankCode(bankCode);
		}
		if (null != productCode) {
			enquiryVO.getCriteria().setProductCode(productCode);
		}

		if (ktp == null || cardNumber == null || ktp.equals("") || cardNumber.equals("")) {
			throw new BusinessException(true, context, CustomEBankingIncidenceCodes.KTP_CARD_NUMBER_EMPTY,
					"KTP and Card Number empty.", null, CustomEBankingErrorCodes.KTP_CARD_NUMBER_EMPTY, null);
		}
		
		customerDedupCheck(ktp,context);

		outputVO = new ArrayList<>();
		// Cams Host Call
		enquiryVO = (CustomCAMSCallEnquiryVO) camsServiceCStub.callService(context, enquiryVO,
				new FEBAUnboundString("fetchAccountNumber"));
		CustomCAMSCallDetailsVO detailsVO = enquiryVO.getDetails();

		CustomATMInquiryCallReferencesVO camsVO = new CustomATMInquiryCallReferencesVO();
		CustomCAMSCallReferencesVO camsCallVO = new CustomCAMSCallReferencesVO();
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccountNumber())) {
			camsCallVO.setAccountNumber(detailsVO.getAccountNumber().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getBranchCode())) {
			camsCallVO.setBranchCode(detailsVO.getBranchCode().toString());
		}
		camsVO.setCAMSCallReferenceVO(camsCallVO);

		/*
		 * commented below code for BRI Agro payroll CR
		 * 
		 * // Whitelist Inquiry Host Call
		CustomWhiteListCriteriaVO criteriaVO = (CustomWhiteListCriteriaVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomWhiteListCriteriaVO");
		CustomWhiteListEnquiryVO customWhiteListVO = (CustomWhiteListEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomWhiteListEnquiryVO");
		CustomWhitelistInquiryReferencesVO whitelistInquiryReferencesVO = null;
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccountNumber())) {
			criteriaVO.setAccountNumber(detailsVO.getAccountNumber());
		}
		customWhiteListVO.setCriteria(criteriaVO);
		whitelistInquiryReferencesVO = whitelistInquiry(context, customWhiteListVO);
		camsVO.setWhitelistInquiryReferenceVO(whitelistInquiryReferencesVO);
		*/

		// Silverlake Account Inquiry Host Call
		CustomSilverlakeAccountInqCriteriaVO accountInqCriteriaVO = (CustomSilverlakeAccountInqCriteriaVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqCriteriaVO");
		CustomSilverlakeAccountInqEnquiryVO accountInqEnquiryVO = (CustomSilverlakeAccountInqEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeAccountInqEnquiryVO");
		CustomSilverLakeAcctInquiryReferencesVO acctInquiryReferencesVO = null;
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccountNumber())) {
			accountInqCriteriaVO.setAccountNumber(detailsVO.getAccountNumber());
			branchCode = detailsVO.getAccountNumber().subString(0, 4);
		}
		accountInqCriteriaVO.setBranch(branchCode);
		String appProductId = PropertyUtil.getProperty(CustomEBConstants.INSTALLATION_PRODUCT_ID, context);
		String branchCodeSuffix = PropertyUtil.getProperty(CustomEBConstants.BRANCH_CODE_SUFFIX, context);
		accountInqCriteriaVO.setRequestBy(appProductId);
		accountInqCriteriaVO.setUserId(branchCode + branchCodeSuffix);
		
		// Added for BRI AGRO payroll
		accountInqCriteriaVO.setBankCode(bankCode);
		accountInqCriteriaVO.setProductCode(productCode);
		
		accountInqEnquiryVO.setCriteria(accountInqCriteriaVO);
		acctInquiryReferencesVO = silverLakeAcctInquiry(context, accountInqEnquiryVO);
		camsVO.setSilverLakeAcctInquiryReferenceVO(acctInquiryReferencesVO);

		// Silverlake CIF Inquiry Host Call
		CustomSilverlakeCIFInqCriteriaVO cifInqCriteriaVO = (CustomSilverlakeCIFInqCriteriaVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqCriteriaVO");
		CustomSilverlakeCIFInqEnquiryVO cifInqEnquiryVO = (CustomSilverlakeCIFInqEnquiryVO) FEBAAVOFactory
				.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomSilverlakeCIFInqEnquiryVO");
		CustomSilverLakeCIFInquiryReferencesVO cifInquiryReferencesVO = null;

		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getAccountNumber())) {
			cifInqCriteriaVO.setAccountNumber(detailsVO.getAccountNumber());
		}

		if (acctInquiryReferencesVO.getCifNo() != null || acctInquiryReferencesVO.getCifNo().equals("")) {
			cifInqCriteriaVO.setCifID(acctInquiryReferencesVO.getCifNo());
		}

		if (ktp != null || ktp.equals("")) {
			cifInqCriteriaVO.setKtpNumber(ktp);
		}
		if (branchCode != null) {
			cifInqCriteriaVO.setBranch(branchCode);
		}
		cifInqCriteriaVO.setRequestBy(appProductId);
		cifInqCriteriaVO.setUserId(branchCode + branchCodeSuffix);
		// Added for BRI AGRO payroll
		cifInqCriteriaVO.setBankCode(bankCode);
		cifInqCriteriaVO.setProductCode(productCode);
		cifInqEnquiryVO.setCriteria(cifInqCriteriaVO);
		cifInquiryReferencesVO = silverLakeCIFInquiry(context, cifInqEnquiryVO);
		camsVO.setSilverLakeCIFInquiryReferenceVO(cifInquiryReferencesVO);
		outputVO.add(camsVO);

		return outputVO;
	}

	public CustomWhitelistInquiryReferencesVO whitelistInquiry(IOpContext context,
			CustomWhiteListEnquiryVO customWhiteListVO)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomWhitelistInquiryReferencesVO customWhitelistInquiryReferencesVO = new CustomWhitelistInquiryReferencesVO();
		customWhiteListVO = (CustomWhiteListEnquiryVO) whitelistServiceCStub.callService(context, customWhiteListVO,
				new FEBAUnboundString("inquiry"));
		CustomWhiteListDetailsVO detailsVO = customWhiteListVO.getDetails();
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getPayrollDate())) {
			if (detailsVO.getPayrollDate().getLength() == 1) {
				customWhitelistInquiryReferencesVO
						.setPayrollDate("1900-01-0" + detailsVO.getPayrollDate().toString() + "T00:00:00");
			} else if (detailsVO.getPayrollDate().getLength() == 2) {
				customWhitelistInquiryReferencesVO.setPayrollDate(detailsVO.getPayrollDate().toString());
				customWhitelistInquiryReferencesVO
						.setPayrollDate("1900-01-" + detailsVO.getPayrollDate().toString() + "T00:00:00");
			}
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getCompanyName())) {
			customWhitelistInquiryReferencesVO.setCompanyName(detailsVO.getCompanyName().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getEmployeeContractStartDate())) {
			customWhitelistInquiryReferencesVO
					.setEmployeeContractStartDate(detailsVO.getEmployeeContractStartDate().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getEmployeeContractEndDate())) {
			customWhitelistInquiryReferencesVO
					.setEmployeeContractEndDate(detailsVO.getEmployeeContractEndDate().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getCompanyReferralCode())) {
			customWhitelistInquiryReferencesVO.setCompanyReferralCode(detailsVO.getCompanyReferralCode().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getAverageSavingAmount())) {
			customWhitelistInquiryReferencesVO.setAverageSavingAmount(detailsVO.getAverageSavingAmount().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getFreqMutasiCredit3())) {
			customWhitelistInquiryReferencesVO.setFreqMutasiCredit3(detailsVO.getFreqMutasiCredit3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getFreqMutasiCredit12())) {
			customWhitelistInquiryReferencesVO.setFreqMutasiCredit12(detailsVO.getFreqMutasiCredit12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getFreqMutasiDeb3())) {
			customWhitelistInquiryReferencesVO.setFreqMutasiDeb3(detailsVO.getFreqMutasiDeb3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getFreqMutasiDeb6())) {
			customWhitelistInquiryReferencesVO.setFreqMutasiDeb6(detailsVO.getFreqMutasiDeb6().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getFreqMutasiDeb12())) {
			customWhitelistInquiryReferencesVO.setfreqMutasiDeb12(detailsVO.getFreqMutasiDeb12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiCredit3())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiCredit3(detailsVO.getTotalMutasiCredit3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiCredit6())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiCredit6(detailsVO.getTotalMutasiCredit6().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiCredit12())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiCredit12(detailsVO.getTotalMutasiCredit12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiDeb3())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiDeb3(detailsVO.getTotalMutasiDeb3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiDeb6())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiDeb6(detailsVO.getTotalMutasiDeb6().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getTotalMutasiDeb12())) {
			customWhitelistInquiryReferencesVO.setTotalMutasiDeb12(detailsVO.getTotalMutasiDeb12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiCredit3())) {
			customWhitelistInquiryReferencesVO.setRataMutasiCredit3(detailsVO.getRataMutasiCredit3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiCredit6())) {
			customWhitelistInquiryReferencesVO.setRataMutasiCredit6(detailsVO.getRataMutasiCredit6().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiCredit12())) {
			customWhitelistInquiryReferencesVO.setRataMutasiCredit12(detailsVO.getRataMutasiCredit12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiDeb3())) {
			customWhitelistInquiryReferencesVO.setRataMutasiDeb3(detailsVO.getRataMutasiDeb3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiDeb6())) {
			customWhitelistInquiryReferencesVO.setRataMutasiDeb6(detailsVO.getRataMutasiDeb6().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getRataMutasiDeb12())) {
			customWhitelistInquiryReferencesVO.setRataMutasiDeb12(detailsVO.getRataMutasiDeb12().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getMobileBanking())) {
			customWhitelistInquiryReferencesVO.setMobileBanking(detailsVO.getMobileBanking().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getInetBanking())) {
			customWhitelistInquiryReferencesVO.setInetBanking(detailsVO.getInetBanking().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getSimpananBritama())) {
			customWhitelistInquiryReferencesVO.setSimpananBritama(detailsVO.getSimpananBritama().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getWaktuKepemilikanSimpananBritama())) {
			customWhitelistInquiryReferencesVO
					.setWaktuKepemilikanSimpananBritama(detailsVO.getWaktuKepemilikanSimpananBritama().toString());
		}
		if (FEBATypesUtility.isNotNull(detailsVO.getPrimaryPhone())) {
			customWhitelistInquiryReferencesVO.setPrimaryPhone(detailsVO.getPrimaryPhone().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getSecondaryPhone())) {
			customWhitelistInquiryReferencesVO.setSecondaryPhone(detailsVO.getSecondaryPhone().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getKoperasiName())) {
			customWhitelistInquiryReferencesVO.setKoperasiName(detailsVO.getKoperasiName().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getPersonelName())) {
			customWhitelistInquiryReferencesVO.setPersonelName(detailsVO.getPersonelName().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getPersonelNumber())) {
			customWhitelistInquiryReferencesVO.setPersonelNumber(detailsVO.getPersonelNumber().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(detailsVO.getPayrollAmount())) {
			customWhitelistInquiryReferencesVO.setPayrollAmount(detailsVO.getPayrollAmount().toString());
		}
		return customWhitelistInquiryReferencesVO;
	}

	public CustomSilverLakeAcctInquiryReferencesVO silverLakeAcctInquiry(IOpContext context,
			CustomSilverlakeAccountInqEnquiryVO accountInqEnquiryVO)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomSilverLakeAcctInquiryReferencesVO acctInquiryReferencesVO = new CustomSilverLakeAcctInquiryReferencesVO();

		accountInqEnquiryVO = (CustomSilverlakeAccountInqEnquiryVO) slakeAcctServiceCStub.callService(context,
				accountInqEnquiryVO, new FEBAUnboundString("acctInquiry"));
		CustomSilverlakeAccountInqDetailsVO accountInqDetailsVO = accountInqEnquiryVO.getDetails();
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getCifNo())) {
			System.out.println("accountInqDetailsVO.getCifNo() : " + accountInqDetailsVO.getCifNo());
			acctInquiryReferencesVO.setCifNo(accountInqDetailsVO.getCifNo().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getStatusRekening())) {
			acctInquiryReferencesVO.setStatusRekening(accountInqDetailsVO.getStatusRekening().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(accountInqDetailsVO.getSavingsAmount())) {
			acctInquiryReferencesVO.setSavingsAmount(accountInqDetailsVO.getSavingsAmount().toString());
		}
		
		return acctInquiryReferencesVO;
	}

	public CustomSilverLakeCIFInquiryReferencesVO silverLakeCIFInquiry(IOpContext context,
			CustomSilverlakeCIFInqEnquiryVO cifInqEnquiryVO)
			throws CriticalException, BusinessException, BusinessConfirmation {
		CustomSilverLakeCIFInquiryReferencesVO cifInquiryReferencesVO = new CustomSilverLakeCIFInquiryReferencesVO();

		cifInqEnquiryVO = (CustomSilverlakeCIFInqEnquiryVO) slakeCIFServiceCStub.callService(context, cifInqEnquiryVO,
				new FEBAUnboundString("cIFInquiry"));
		CustomSilverlakeCIFInqDetailsVO cifInqDetailsVO = cifInqEnquiryVO.getDetails();
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddress())) {
			cifInquiryReferencesVO.setAddress(cifInqDetailsVO.getAddress().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddress2())) {
			cifInquiryReferencesVO.setAddress2(cifInqDetailsVO.getAddress2().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddress3())) {
			cifInquiryReferencesVO.setAddress3(cifInqDetailsVO.getAddress3().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddress4())) {
			cifInquiryReferencesVO.setAddress4(cifInqDetailsVO.getAddress4().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddressRT())) {
			cifInquiryReferencesVO.setAddressRT(cifInqDetailsVO.getAddressRT().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAddressRW())) {
			cifInquiryReferencesVO.setAddressRW(cifInqDetailsVO.getAddressRW().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getAgama())) {
			cifInquiryReferencesVO.setAgama(cifInqDetailsVO.getAgama().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getBirthDate())) {
			cifInquiryReferencesVO.setBirthDate(cifInqDetailsVO.getBirthDate().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getCifNo())) {
			cifInquiryReferencesVO.setCifNo(cifInqDetailsVO.getCifNo().toString());
		}
		
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getIdNo())) {
			cifInquiryReferencesVO.setIdNo(cifInqDetailsVO.getIdNo().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getIdType())) {
			cifInquiryReferencesVO.setIdType(cifInqDetailsVO.getIdType().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getKecamatan())) {
			cifInquiryReferencesVO.setKecamatan(cifInqDetailsVO.getKecamatan().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getKelurahan())) {
			cifInquiryReferencesVO.setKelurahan(cifInqDetailsVO.getKelurahan().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getNama())) {
			cifInquiryReferencesVO.setNama(cifInqDetailsVO.getNama().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getPlaceOfBirth())) {
			cifInquiryReferencesVO.setPlaceOfBirth(cifInqDetailsVO.getPlaceOfBirth().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getProvince())) {
			IFEBAType stateCode = AppDataManager
					.getValue(context, "CommonCodeCache",
							"CODE_TYPE=HSCM|CM_CODE=" + cifInqDetailsVO.getProvince());
			if(null != stateCode){
				cifInquiryReferencesVO.setProvince(stateCode.toString());
			}
			if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getCity())) {
				IFEBAType cityCode = AppDataManager
						.getValue(context, "CommonCodeCache",
								"CODE_TYPE=HCCM|CM_CODE=" + stateCode+"|"+cifInqDetailsVO.getCity());
				if(null != cityCode){
					cifInquiryReferencesVO.setCity(cityCode.toString());
				}
			}
		}
		
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getSex())) {
			cifInquiryReferencesVO.setSex(cifInqDetailsVO.getSex().toString());
		}
		if (FEBATypesUtility.isNotNullOrBlank(cifInqDetailsVO.getZipCode())) {
			cifInquiryReferencesVO.setZipCode(cifInqDetailsVO.getZipCode().toString());
		}
		return cifInquiryReferencesVO;
	}
	
	private List restResponseErrorCodeMapping() {
		List<RestResponseErrorCodeMapping> restResponceErrorCode = new ArrayList<>();
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211041, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211042, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211043, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211049, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211050, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211051, "BE", 400));
		restResponceErrorCode.add(new RestResponseErrorCodeMapping(211099, "BE", 400));
		return restResponceErrorCode;
	}
}

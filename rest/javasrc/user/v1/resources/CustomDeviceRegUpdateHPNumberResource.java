package com.infosys.custom.ebanking.rest.user.v1.resources;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.infosys.custom.ebanking.common.CustomEBConstants;
import com.infosys.custom.ebanking.common.CustomEBankingErrorCodes;
import com.infosys.custom.ebanking.common.CustomEBankingIncidenceCodes;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomCAMSCallReferencesVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDeviceRegistrationResponseVO;
import com.infosys.custom.ebanking.rest.user.v1.pojo.CustomDeviceRegistrationRetailInputVO;
import com.infosys.custom.ebanking.types.CustomTypesCatalogueConstants;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomDeviceDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplicationEnquiryVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanApplnMasterDetailsVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomLoanStatusVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyRegistrationVO;
import com.infosys.custom.ebanking.types.valueobjects.CustomPrivyUpdateMobileNumVO;
import com.infosys.ebanking.common.EBIncidenceCodes;
import com.infosys.ebanking.common.EBankingErrorCodes;
import com.infosys.ebanking.rest.common.v1.ResourceAuthenticator;
import com.infosys.ebanking.rest.common.v1.ResourcePaths;
import com.infosys.ebanking.rest.common.v1.RestCommonUtils;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationOutputVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationResponseVO;
import com.infosys.ebanking.rest.retail.user.v1.pojo.DeviceRegistrationRetailInputVO;
import com.infosys.ebanking.types.TypesCatalogueConstants;
import com.infosys.ebanking.types.valueobjects.DeviceDetailsVO;
import com.infosys.feba.framework.common.MethodInfo;
import com.infosys.feba.framework.common.ServiceInfo;
import com.infosys.feba.framework.common.exception.BusinessConfirmation;
import com.infosys.feba.framework.common.exception.BusinessException;
import com.infosys.feba.framework.common.exception.CriticalException;
import com.infosys.feba.framework.common.exception.FEBAException;
import com.infosys.feba.framework.common.exception.FatalException;
import com.infosys.feba.framework.common.util.resource.PropertyUtil;
import com.infosys.feba.framework.commontran.context.FEBATransactionContext;
import com.infosys.feba.framework.commonweb.context.IOpContext;
import com.infosys.feba.framework.interceptor.rest.pojo.AuthorizationInputVO;
import com.infosys.feba.framework.interceptor.rest.pojo.Header;
import com.infosys.feba.framework.interceptor.rest.pojo.RestCommonSuccessMessageHandler;
import com.infosys.feba.framework.interceptor.rest.pojo.RestResponseErrorCodeMapping;
import com.infosys.feba.framework.interceptor.rest.service.RestResourceManager;
import com.infosys.feba.framework.service.ServiceUtil;
import com.infosys.feba.framework.service.stub.client.IClientStub;
import com.infosys.feba.framework.transaction.common.TransactionConstants;
import com.infosys.feba.framework.transaction.util.TransactionContextFactory;
import com.infosys.feba.framework.types.FEBATypeSystemException;
import com.infosys.feba.framework.types.lists.FEBAArrayList;
import com.infosys.feba.framework.types.primitives.BankId;
import com.infosys.feba.framework.types.primitives.CorporateId;
import com.infosys.feba.framework.types.primitives.FEBAOperatingMode;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;
import com.infosys.feba.framework.types.primitives.UserId;
import com.infosys.feba.framework.types.valueobjects.FEBAAVOFactory;
import com.infosys.fentbase.common.FBAConstants;
import com.infosys.fentbase.types.primitives.OTP;
import com.infosys.fentbase.types.valueobjects.AuthenticationVO;
import com.infosys.fentbase.types.valueobjects.UserProfileVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ServiceInfo(moduleName = "user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Api(value = "HP Number Updation")

@ApiResponses(value = { @ApiResponse(code = 401, message = ResourcePaths.HTTP_ERROR_401, response = Header.class),
		@ApiResponse(code = 404, message = ResourcePaths.HTTP_ERROR_404, response = Header.class),
		@ApiResponse(code = 405, message = ResourcePaths.HTTP_ERROR_405, response = Header.class),
		@ApiResponse(code = 415, message = ResourcePaths.HTTP_ERROR_415, response = Header.class),
		@ApiResponse(code = 500, message = ResourcePaths.HTTP_ERROR_500, response = Header.class) })
public class CustomDeviceRegUpdateHPNumberResource extends ResourceAuthenticator {

	@Context
	protected HttpServletRequest request;

	@Context
	UriInfo uriInformation;

	RestCommonSuccessMessageHandler headerResponse = new RestCommonSuccessMessageHandler();

	public static final String USER_PHONE_NUM_SERVICE = "CustomUserPhoneNumService";
	public static final String FETCH = "fetch";
	public static final String LOAN_APPLICATION_SERVICE = "CustomLoanApplicationService";

	public static final String DEVICE_REGISTRATION_SERVICE = "CustomDeviceRegistrationService";
	public static final String MODIFY = "modify";
	public static final String CAMS_CALLS_SERVICE = "CustomCAMSCallService";
	
	public static final String USERPHONENUMBER = "USERPHONENUMBER";
	public static final String CUSTOMUSERMOBILENUMBERSERVICE = "CustomUserMobileNumberService";
	
	String status = "";

	@PUT
	@ApiOperation(value = "HP Number updation,when Mobile number is changed", response = DeviceRegistrationResponseVO.class)
	@MethodInfo(uri = "/v1/banks/{bankid}/users/{userid}/custom/deviceregistration/updateHpNumber", auditFields = {})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "User details updated successfully.", response = DeviceRegistrationResponseVO.class),
			@ApiResponse(code = 401, message = "The authorization and user details Required", response = DeviceRegistrationResponseVO.class) })

	public Response updateUserDetails(
			@ApiParam(value = "Retail user id", required = true) @PathParam("userid") String userId,
			CustomDeviceRegistrationRetailInputVO inputVO) {

		CustomDeviceRegistrationResponseVO wrapper = new CustomDeviceRegistrationResponseVO();

		DeviceRegistrationOutputVO detailsVO = new DeviceRegistrationOutputVO();

		int responseStatus = 200;
		Header header = new Header();

		AuthorizationInputVO authmodeDetails = new AuthorizationInputVO();
		try {
			isUnAuthenticatedRequest();

			IOpContext opContext = (IOpContext) request.getAttribute("OP_CONTEXT");

			String pinangOrSahabat = PropertyUtil.getProperty("INSTALLATION_PRODUCT_ID", opContext);
			FEBATransactionContext txnContext = null;
			txnContext = TransactionContextFactory.getInstance(TransactionConstants.EB_TXN_CONTEXT);

			txnContext.setBankId((BankId) opContext.getFromContextData(FBAConstants.STRING_BANK_ID));
			txnContext.setUserId(new UserId(userId));
			UserProfileVO userProfileVO = (UserProfileVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.UserProfileVO);
			DeviceDetailsVO deviceDetailsVO = (DeviceDetailsVO) FEBAAVOFactory
					.createInstance(TypesCatalogueConstants.DeviceDetailsVO);
			
			/*
			 *	Add the validate user method here 
			 */
			DeviceDetailsVO validateDeviceDetailsVO=null;
			if (inputVO.getUserDetails() != null){
				validateDeviceDetailsVO = (DeviceDetailsVO) FEBAAVOFactory
						.createInstance(TypesCatalogueConstants.DeviceDetailsVO);
				validateDeviceDetailsVO.setMobileNumber(inputVO.getUserDetails().getUserPhoneNumber());
				validateUserDetails(validateDeviceDetailsVO,opContext);
			}
			

			if (inputVO.getDeviceDetails() == null && inputVO.getAuthorizationDetails() == null) {
				/*
				 * Change to update the userid to new mobile number
				 */
				deviceDetailsVO.setUserID(userId);
				if(null!=validateDeviceDetailsVO){
					deviceDetailsVO.getPrincipalIDER().setUserPrincipal(validateDeviceDetailsVO.getMobileNumber().getValue());
				}
				deviceDetailsVO.getPrincipalIDER().setUserId(userId);
				authmodeDetails.setFirstAuthorizationMode("OTP");
				authmodeDetails.setWorkFlowRequired('N');
				authmodeDetails.setUserIdRequired('N');
				header.setAuthorization(authmodeDetails);
				wrapper.setHeader(header);
				generateOTP(opContext, deviceDetailsVO);
				responseStatus = 401;
			}
			if (inputVO.getDeviceDetails() == null && inputVO.getAuthorizationDetails() != null) {
				validateOTP(opContext, deviceDetailsVO, inputVO.getAuthorizationDetails().getOneTimePassword());

				if (inputVO.getUserDetails().getUserPhoneNumber() == null
						|| inputVO.getUserDetails().getUserPhoneNumber() == "") {
					throw new BusinessException(true, opContext, "User details mandatory", "error", null,
							CustomEBankingErrorCodes.USER_PHONE_NUM_MANDATORY, null);
				}

				if (inputVO.getUserDetails() != null
						&& RestCommonUtils.isNotNullorEmpty(inputVO.getUserDetails().getUserPhoneNumber())) {

					userProfileVO.getPrincipalIDER().setUserId(userId);
					userProfileVO.getPrincipalIDER().setCorpId(userId);
					userProfileVO.setPrincipalId(inputVO.getUserDetails().getUserPhoneNumber());
					userProfileVO.setMobileNo(inputVO.getUserDetails().getUserPhoneNumber());
					

					// Added for Privy Phone No Update
					if (responseStatus != 401) {
						updatePrivyNumber(opContext, inputVO, userId);
						final IClientStub customUserService = ServiceUtil
								.getService(new FEBAUnboundString(USER_PHONE_NUM_SERVICE));
						customUserService.callService(opContext, userProfileVO, new FEBAUnboundString(FETCH));
					}

				}

				if (inputVO.getUserDetails() != null) {
					detailsVO.setUserId(inputVO.getUserDetails().getUserPhoneNumber());
				} else {
					detailsVO.setUserId(userId);
				}
			}
			if (inputVO.getDeviceDetails() != null) {
				/*
				 * Change to update the userid to new mobile number
				 */
				deviceDetailsVO.setUserID(userId);
				deviceDetailsVO.getPrincipalIDER().setUserPrincipal(inputVO.getUserDetails().getUserPhoneNumber());
				deviceDetailsVO.getPrincipalIDER().setUserId(userId);
				if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceDetails().getDeviceId())) {
					deviceDetailsVO.setDeviceId(inputVO.getDeviceDetails().getDeviceId());

				}

				CustomLoanApplicationEnquiryVO enquiryVO = (CustomLoanApplicationEnquiryVO) FEBAAVOFactory
						.createInstance(CustomLoanApplicationEnquiryVO.class.getName());
				CustomLoanStatusVO statusVO = (CustomLoanStatusVO) FEBAAVOFactory
						.createInstance(CustomLoanStatusVO.class.getName());

				final IClientStub customLoanInquiryService = ServiceUtil
						.getService(new FEBAUnboundString(LOAN_APPLICATION_SERVICE));
				enquiryVO.getCriteria().setApplicationStatus(CustomEBConstants.LOAN_CREATED);
				customLoanInquiryService.callService(opContext, enquiryVO, new FEBAUnboundString("fetchForInquiry"));
				FEBAArrayList<CustomLoanApplnMasterDetailsVO> loanDetailsList = enquiryVO.getResultList();

				if (inputVO.getAuthorizationDetails() != null) {

					if (inputVO.getAuthorizationDetails().getCardDetails() != null) {

						validateCardDetails(opContext, inputVO.getAuthorizationDetails().getCardDetails());

						CustomCAMSCallEnquiryVO camsEnqVO = (CustomCAMSCallEnquiryVO) FEBAAVOFactory.createInstance(
								"com.infosys.custom.ebanking.types.valueobjects.CustomCAMSCallEnquiryVO");
						camsEnqVO.getCriteria()
						.setCardNumber(inputVO.getAuthorizationDetails().getCardDetails().getCardNumber());
						camsEnqVO.getCriteria()
						.setExpiryDate(inputVO.getAuthorizationDetails().getCardDetails().getExpiryDate());
						camsEnqVO.getCriteria().setKtp(inputVO.getAuthorizationDetails().getCardDetails().getKtp());
						
						// Added for AGRO payroll
						camsEnqVO.getCriteria().setBankCode(inputVO.getAuthorizationDetails().getCardDetails().getBankCode());
						camsEnqVO.getCriteria().setProductCode(inputVO.getAuthorizationDetails().getCardDetails().getProductCode());

						final IClientStub camsServiceCStub = ServiceUtil
								.getService(new FEBAUnboundString(CAMS_CALLS_SERVICE));
						camsEnqVO = (CustomCAMSCallEnquiryVO) camsServiceCStub.callService(opContext, camsEnqVO,
								new FEBAUnboundString("fetchAccountNumber"));
						CustomCAMSCallDetailsVO camsDetVO = camsEnqVO.getDetails();

						if (camsDetVO != null) {

							mapDeviceDetails(inputVO.getDeviceDetails(), deviceDetailsVO, opContext);
						}
					} else {
						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)) {

							validateOTP(opContext, deviceDetailsVO,
									inputVO.getAuthorizationDetails().getOneTimePassword());

						}
						mapDeviceDetails(inputVO.getDeviceDetails(), deviceDetailsVO, opContext);
					}

				} else {

					if (loanDetailsList.size() > 0) {
						try {
							final IClientStub customLoanStatusService = ServiceUtil
									.getService(new FEBAUnboundString("CustomLoanStatusService"));
							customLoanStatusService.callService(opContext, statusVO,
									new FEBAUnboundString("fetchStatus"));
							status = CustomEBConstants.WITH_FACILITY;
						} catch (FEBAException e) {
							if(e.getErrorCode()==8504){
								status = CustomEBConstants.WITHOUT_FACILITY;
							}else{
								throw new BusinessException(opContext, EBIncidenceCodes.HIF_HOST_NOT_AVAILABLE, "The host does not exist.",
										EBankingErrorCodes.HOST_NOT_AVAILABLE);
							}
						}

						responseStatus = 401;
						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)
								&& status.equals(CustomEBConstants.WITH_FACILITY)) {
							authmodeDetails.setFirstAuthorizationMode("KTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
						} else {
							authmodeDetails.setFirstAuthorizationMode("OTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
							generateOTP(opContext, deviceDetailsVO);

							responseStatus = 401;
						}

					} else {
						if (pinangOrSahabat.equals(CustomEBConstants.PINANG)
								|| pinangOrSahabat.equals(CustomEBConstants.SAHABAT)) {
							status = CustomEBConstants.WITHOUT_FACILITY;
							authmodeDetails.setFirstAuthorizationMode("OTP");
							authmodeDetails.setWorkFlowRequired('N');
							authmodeDetails.setUserIdRequired('N');
							header.setAuthorization(authmodeDetails);
							wrapper.setHeader(header);
							generateOTP(opContext, deviceDetailsVO);

							responseStatus = 401;
						}
					}
				}

				if (inputVO.getUserDetails().getUserPhoneNumber() == null
						|| inputVO.getUserDetails().getUserPhoneNumber() == "") {
					throw new BusinessException(true, opContext, "User details mandatory", "error", null,
							CustomEBankingErrorCodes.USER_PHONE_NUM_MANDATORY, null);
				}

				if (inputVO.getUserDetails() != null
						&& RestCommonUtils.isNotNullorEmpty(inputVO.getUserDetails().getUserPhoneNumber())) {

					userProfileVO.getPrincipalIDER().setUserId(userId);
					userProfileVO.getPrincipalIDER().setCorpId(userId);
					userProfileVO.setPrincipalId(inputVO.getUserDetails().getUserPhoneNumber());
					userProfileVO.setMobileNo(inputVO.getUserDetails().getUserPhoneNumber());
					// Added for Privy Phone No Update
					if (responseStatus != 401) {
						updatePrivyNumber(opContext, inputVO, userId);
						final IClientStub customUserService = ServiceUtil
								.getService(new FEBAUnboundString(USER_PHONE_NUM_SERVICE));
						customUserService.callService(opContext, userProfileVO, new FEBAUnboundString(FETCH));
					}

				}

				if (inputVO.getUserDetails() != null) {
					detailsVO.setUserId(inputVO.getUserDetails().getUserPhoneNumber());
				} else {
					detailsVO.setUserId(userId);
				}
			}
			if (inputVO.getDeviceDetails() != null) {
				detailsVO.setMobileNo(inputVO.getDeviceDetails().getMobileNo());
				detailsVO.setDeviceType(inputVO.getDeviceDetails().getDeviceType());
				detailsVO.setDeviceName(inputVO.getDeviceDetails().getDeviceName());
				detailsVO.setDeviceId(inputVO.getDeviceDetails().getDeviceId());
				detailsVO.setEncryptedLicense(deviceDetailsVO.getEncryptedLicense().toString());
			}

		} catch (Exception e) {

			RestResourceManager.handleFatalErrorOutput(request, response, e, restResponseErrorCodeMapping());
		}

		RestResourceManager.updateHeaderFooterResponseData(wrapper, request, response, 0);

		return Response.status(responseStatus).entity(detailsVO).build();

	}

	protected List<RestResponseErrorCodeMapping> restResponseErrorCodeMapping() {

		return new ArrayList<>();
	}
	 
	protected DeviceDetailsVO validateUserDetails(DeviceDetailsVO deviceDetails, IOpContext opcontext)
			throws CriticalException, BusinessException, BusinessConfirmation
			 {

				IClientStub serviceCStub = ServiceUtil.getService(new FEBAUnboundString(CUSTOMUSERMOBILENUMBERSERVICE));
				deviceDetails = (DeviceDetailsVO) serviceCStub.callService(opcontext, deviceDetails,
				new FEBAUnboundString("validate"));

		return deviceDetails;
	}
	protected static void mapDeviceDetails(DeviceRegistrationRetailInputVO inputVO, DeviceDetailsVO deviceDetailsVO,
			IOpContext opContext) throws CriticalException, BusinessException, BusinessConfirmation {

		
		if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceName())) {
			deviceDetailsVO.setDeviceName(inputVO.getDeviceName());

		}
		if (RestCommonUtils.isNotNullorEmpty(inputVO.getDeviceId())) {
			deviceDetailsVO.setDeviceId(inputVO.getDeviceId());

		}

		final IClientStub customDeviceRegService = ServiceUtil
				.getService(new FEBAUnboundString(DEVICE_REGISTRATION_SERVICE));

		customDeviceRegService.callService(opContext, deviceDetailsVO, new FEBAUnboundString(MODIFY));

	}

	public boolean validateCardDetails(IOpContext opContext, CustomCAMSCallReferencesVO cardDetails)
			throws BusinessException {
		if (cardDetails.getCardNumber() == null || cardDetails.getCardNumber() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.IDENTIFICATION_CARD_MANDATORY,
					"Enter the card number.", EBankingErrorCodes.CARD_NO_MANDATORY);
		}
		if (cardDetails.getExpiryDate() == null || cardDetails.getExpiryDate() == "") {
			throw new BusinessException(opContext, EBIncidenceCodes.INVALID_EXPIRY_DATE, "Enter the Card Expiry date",
					EBankingErrorCodes.ENTER_EXP_DATE);
		}
		if (cardDetails.getKtp() == null || cardDetails.getKtp() == "") {
			throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.KTP_CARD_NUMBER_EMPTY,
					"KTP Number empty.", null, CustomEBankingErrorCodes.KTP_CARD_NUMBER_EMPTY, null);
		}
		return true;
	}

	/**
	 * This method generateOTP autherization mode.
	 *
	 * @param IOpContext
	 * @return TransactionAuthorizationModeVO
	 *
	 * @throws BusinessException,CriticalException,BusinessConfirmation,
	 *             FEBATypeSystemException
	 *
	 */

	public AuthenticationVO generateOTP(IOpContext opContext, DeviceDetailsVO detailsVO)
			throws CriticalException, BusinessException, BusinessConfirmation {
		
		AuthenticationVO authenticationVO = (AuthenticationVO) FEBAAVOFactory
				.createInstance(TypesCatalogueConstants.AuthenticationVO);
		// Populate authentication VO
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setUserPrincipal(detailsVO.getPrincipalIDER().getUserPrincipal().toString());
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setUserId((UserId)opContext.getFromContextData(FBAConstants.USER_ID));
		authenticationVO.getUserSignOnVO().getUserProfileVO().getPrincipalIDER().setCorpId((CorporateId)opContext.getFromContextData(FBAConstants.CORP_ID));
		authenticationVO.setBankID((BankId) opContext.getFromContextData(FBAConstants.STRING_BANK_ID));

		opContext.setOperatingMode(new FEBAOperatingMode("C"));

		if(status !=null){
			if(status.equals(CustomEBConstants.WITH_FACILITY)){
				authenticationVO.setIsFallbackRequired("Y");
			}
			else{
				authenticationVO.setIsFallbackRequired("N");
			}
		}
		final IClientStub service = ServiceUtil.getService(new FEBAUnboundString("CustomRMAuthenticationService"));
		authenticationVO = (AuthenticationVO) service.callService(opContext, authenticationVO,
				new FEBAUnboundString("generateOTP"));
		
		return authenticationVO;

	}

	public void validateOTP(IOpContext opContext, DeviceDetailsVO detailsVO, String userEnteredOTP)
			throws BusinessException, CriticalException, BusinessConfirmation {

		CustomDeviceDetailsVO customVO = (CustomDeviceDetailsVO) detailsVO.getExtensionVO();
		customVO.setOtp(new OTP(userEnteredOTP));

		final IClientStub service = ServiceUtil.getService(new FEBAUnboundString(DEVICE_REGISTRATION_SERVICE));
		service.callService(opContext, detailsVO, new FEBAUnboundString("validateOTP"));

	}
	// Added for Privy Phone No Update
	public void updatePrivyNumber(IOpContext opContext, CustomDeviceRegistrationRetailInputVO inputVO, String userId)
			throws BusinessException, CriticalException, BusinessConfirmation, FEBATypeSystemException {
		try {
			CustomPrivyRegistrationVO regEnquiryVO = (CustomPrivyRegistrationVO) FEBAAVOFactory
					.createInstance(CustomTypesCatalogueConstants.CustomPrivyRegistrationVO);
			regEnquiryVO.setUserId(userId);

			final IClientStub serviceStub = ServiceUtil.getService(new FEBAUnboundString("CustomPrivyService"));
			regEnquiryVO = (CustomPrivyRegistrationVO) serviceStub.callService(opContext, regEnquiryVO,
					new FEBAUnboundString("tokenCreate"));

			FEBAUnboundString token = regEnquiryVO.getToken();

			if (token != null) {
				CustomPrivyUpdateMobileNumVO privyUpdadeVO = (CustomPrivyUpdateMobileNumVO) FEBAAVOFactory
						.createInstance("com.infosys.custom.ebanking.types.valueobjects.CustomPrivyUpdateMobileNumVO");
				privyUpdadeVO.setNewMobileNum(inputVO.getUserDetails().getUserPhoneNumber());
				privyUpdadeVO.setToken(token);
				IClientStub privyserviceStub = ServiceUtil
						.getService(new FEBAUnboundString("CustomPrivyMobileNumService"));
				privyUpdadeVO = (CustomPrivyUpdateMobileNumVO) privyserviceStub.callService(opContext, privyUpdadeVO,
						new FEBAUnboundString("update"));
				if (!privyUpdadeVO.getCode().toString().equalsIgnoreCase("200")) {
					throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.PRIVY_MOBILE_UPDATE,
							privyUpdadeVO.getMessage().toString(), null, CustomEBankingErrorCodes.PRIVY_MOBILE_UPDATE,
							null);
				}
			} else {
				throw new BusinessException(true, opContext, CustomEBankingIncidenceCodes.TOKEN_NOT_FOUND,
						"Invalid Token", null, CustomEBankingErrorCodes.INVALID_TOKEN, null);
			}
		} catch (FatalException e) {
			if (!e.getAdditionalMessage().contains("PRYTOKCR0001")) {
				throw e;
			}
		}
	}

}

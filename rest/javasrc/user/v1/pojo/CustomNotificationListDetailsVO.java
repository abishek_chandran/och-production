package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Notification List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomNotificationListDetailsVO {

	@ApiModelProperty(value = "Message")
	private String message;
	
	@ApiModelProperty(value = "First Amount")
	private String amount1;
	
	@ApiModelProperty(value = "Second Amount")
	private String amount2;
	
	@ApiModelProperty(value = "Third Amount")
	private String amount3;
	
	@ApiModelProperty(value = "First Date")
	private String date1;
	
	@ApiModelProperty(value = "Second Date")
	private String date2;
	
	@ApiModelProperty(value = "Third Date")
	private String date3;
	
	public CustomNotificationListDetailsVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomNotificationListDetailsVO(String message, String amount1, String amount2, String amount3, String date1,
			String date2, String date3, String otherInfo1, String otherInfo2, String otherInfo3) {
		super();
		this.message = message;
		this.amount1 = amount1;
		this.amount2 = amount2;
		this.amount3 = amount3;
		this.date1 = date1;
		this.date2 = date2;
		this.date3 = date3;
		this.otherInfo1 = otherInfo1;
		this.otherInfo2 = otherInfo2;
		this.otherInfo3 = otherInfo3;
	}

	@ApiModelProperty(value = "Other Information 1")
	private String otherInfo1;
	
	@ApiModelProperty(value = "Other Information 2")
	private String otherInfo2;
	
	@ApiModelProperty(value = "Other Information 3")
	private String otherInfo3;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAmount1() {
		return amount1;
	}

	public void setAmount1(String amount1) {
		this.amount1 = amount1;
	}

	public String getAmount2() {
		return amount2;
	}

	public void setAmount2(String amount2) {
		this.amount2 = amount2;
	}

	public String getAmount3() {
		return amount3;
	}

	public void setAmount3(String amount3) {
		this.amount3 = amount3;
	}

	public String getDate1() {
		return date1;
	}

	public void setDate1(String date1) {
		this.date1 = date1;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public String getDate3() {
		return date3;
	}

	public void setDate3(String date3) {
		this.date3 = date3;
	}

	public String getOtherInfo1() {
		return otherInfo1;
	}

	public void setOtherInfo1(String otherInfo1) {
		this.otherInfo1 = otherInfo1;
	}

	public String getOtherInfo2() {
		return otherInfo2;
	}

	public void setOtherInfo2(String otherInfo2) {
		this.otherInfo2 = otherInfo2;
	}

	public String getOtherInfo3() {
		return otherInfo3;
	}

	public void setOtherInfo3(String otherInfo3) {
		this.otherInfo3 = otherInfo3;
	}
	
	
}

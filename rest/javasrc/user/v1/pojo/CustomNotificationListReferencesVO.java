package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Notification List")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomNotificationListReferencesVO {
	
	
	@ApiModelProperty(value = "Notification Event")
	private String notificationEvent;
	
	@ApiModelProperty(value = "Notification Message Short Description")
	private String shortDescription;
	
	@ApiModelProperty(value = "Notification Message Date")
	private String notificationDate;
	
	@ApiModelProperty(value = "Notification Header")
	private String notificationHeader;
	
	@ApiModelProperty(value = "Is New")
	private String isNew;
	
	@ApiModelProperty(value = "notification id")
	private String notificationId;
	
	public String getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(String notificationId) {
		this.notificationId = notificationId;
	}

	public CustomNotificationListReferencesVO() {
		super();
	}

	public String getIsNew() {
		return isNew;
	}

	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}

	public CustomNotificationListReferencesVO(String notificationEvent, String shortDescription,
			String notificationDate, String notificationHeader, CustomNotificationListDetailsVO details) {
		super();
		this.notificationEvent = notificationEvent;
		this.shortDescription = shortDescription;
		this.notificationDate = notificationDate;
		this.notificationHeader = notificationHeader;
		this.details = details;
	}

	@ApiModelProperty(value = "Details VO")
	private CustomNotificationListDetailsVO details;
	
	public CustomNotificationListDetailsVO getDetails() {
		return details;
	}

	public void setDetails(CustomNotificationListDetailsVO details) {
		this.details = details;
	}

	public String getNotificationEvent() {
		return notificationEvent;
	}

	public void setNotificationEvent(String notificationEvent) {
		this.notificationEvent = notificationEvent;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(String notificationDate) {
		this.notificationDate = notificationDate;
	}

	public String getNotificationHeader() {
		return notificationHeader;
	}

	public void setNotificationHeader(String notificationHeader) {
		this.notificationHeader = notificationHeader;
	}

	
	

}

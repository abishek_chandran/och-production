package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value= "ATM Inquiry Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomATMInquiryCallReferencesVO {

	@ApiModelProperty(value = "CAMS Call")
	private CustomCAMSCallReferencesVO camsCallReference;
	
	@ApiModelProperty(value = "Silverlake Account Inquiry")
	private CustomSilverLakeAcctInquiryReferencesVO silverLakeAcctInquiry;
	
	@ApiModelProperty(value = "Silverlake CIF Inquiry")
	private CustomSilverLakeCIFInquiryReferencesVO silverLakeCIFInquiry;
	
	//@ApiModelProperty(value = "Whitelist Inquiry")
	//private CustomWhitelistInquiryReferencesVO whitelistInquiry;
	
	public CustomCAMSCallReferencesVO getCAMSCallReferenceVO() {
		return camsCallReference;
	}
	public void setCAMSCallReferenceVO(CustomCAMSCallReferencesVO camsCallVO) {
		this.camsCallReference = camsCallVO;
	}
	
	public CustomSilverLakeAcctInquiryReferencesVO getSilverLakeAcctInquiryReferenceVO() {
		return silverLakeAcctInquiry;
	}
	public void setSilverLakeAcctInquiryReferenceVO(CustomSilverLakeAcctInquiryReferencesVO silverLakeAcctInquiryVO) {
		this.silverLakeAcctInquiry = silverLakeAcctInquiryVO;
	}
	
	public CustomSilverLakeCIFInquiryReferencesVO getSilverLakeCIFInquiryReferenceVO() {
		return silverLakeCIFInquiry;
	}
	public void setSilverLakeCIFInquiryReferenceVO(CustomSilverLakeCIFInquiryReferencesVO silverLakeCIFInquiryVO) {
		this.silverLakeCIFInquiry = silverLakeCIFInquiryVO;
	}
	
	/*	public CustomWhitelistInquiryReferencesVO getWhitelistInquiryReferenceVO() {
		return whitelistInquiry;
	}
	public void setWhitelistInquiryReferenceVO(CustomWhitelistInquiryReferencesVO whitelistInquiryVO) {
		this.whitelistInquiry = whitelistInquiryVO;
	}*/
}

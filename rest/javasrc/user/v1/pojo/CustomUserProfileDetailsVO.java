package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.ebanking.rest.common.v1.pojo.CodeReferencesVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@XmlRootElement
@ApiModel(value = "The profile details of the user")
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class CustomUserProfileDetailsVO {

	public CustomUserProfileDetailsVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ApiModelProperty(value="The Id of the bank") 
	private String bankId;
	
	@ApiModelProperty(value="The Id of the corporate to which the user belongs to. For Example - INFOSYS, AIRTEL, etc") 
	private String orgId;
	
	@ApiModelProperty(value="The master Id of the user") 
	private String userId;
	
	@ApiModelProperty(value="The channel specific login Id of the user") 
	private String principalId;
	
	@ApiModelProperty(value="The  type of the user. For Example - Retail, Corporate, Bank admin") 
	private CodeReferencesVO userType;
	
	@ApiModelProperty(value="The primary customer Id of the user") 
	private String customerId;
	
	@ApiModelProperty(value="The salutation of the user") 
	private CodeReferencesVO salutation;
	
	@ApiModelProperty(value="The nickname of the user") 
	private String userNickName;
	
	@ApiModelProperty(value="The first name of the user") 
	private String firstName;
	
	@ApiModelProperty(value="The middle name of the user") 
	private String middleName;
	
	@ApiModelProperty(value="The last name of the user") 
	private String lastName;
	
	@ApiModelProperty(value="The primary branch of the bank to which the user belongs to") 
	private String primaryBranchId;
	
	@ApiModelProperty(value="The primary division to which the user is mapped to. For Example - Accounts, HR, Operations etc. This field is applicable when division indicator is Local") 
	private String primaryDivisionId;
	
	@ApiModelProperty(value="The flag to indicate whether user has access to all divisions. Valid Values - Local (L), Global (G)") 
	private CodeReferencesVO divisionAccessIndicator;
	
	@ApiModelProperty(value="The preferred primary account set by the user which gets defaulted for all transactions initiated") 
	private String primaryAccountId;
	
	@ApiModelProperty(value="The last transaction date initiated by the user") 
	private Date lastTransactionDate;
	
	@ApiModelProperty(value="The last successful login date by the user") 
	private Date loginDate;
	
	@ApiModelProperty(value="The last successful log off date by the user") 
	private Date logoffDate;
	
	@ApiModelProperty(value="The session Id as assigned to the specified user for the duration of that session") 
	private String sessionId;
	
	@ApiModelProperty(value="Indicates if the multi currency transaction is allowed. Valid Values - Yes(Y), No(N)") 
	private CodeReferencesVO multiCurrencyTxnAllowed;
	
	@ApiModelProperty(value="The gender of the user") 
	private CodeReferencesVO gender;
	
	@ApiModelProperty(value="The marital status of the user") 
	private CodeReferencesVO maritalStatus;
	
	@ApiModelProperty(value="The category code of the user") 
	private CodeReferencesVO categoryCode;
	
	@ApiModelProperty(value="The limit scheme attached to the user where transaction (consumption) limits are defined") 
	private CodeReferencesVO limitScheme;
	
	@ApiModelProperty(value="The limit scheme attached to the user where transaction (Entry and Approval) limits are defined") 
	private CodeReferencesVO rangeLimitScheme;
	
	@ApiModelProperty(value="The residential status of the user") 
	private CodeReferencesVO customerResidentialStatus;
	
	@ApiModelProperty(value="The flag to indicate if login is allowed. Valid Values - Yes (Y), No(N)") 
	private CodeReferencesVO loginAllowed;
	
	@ApiModelProperty(value="The flag to indicate if transaction is allowed. Valid Values - Yes (Y), No(N)") 
	private CodeReferencesVO transactionAllowed;
			
	@ApiModelProperty(value="The language preference of the user") 
	private CodeReferencesVO language;
	
	@ApiModelProperty(value="The preferred account format set by the user. For Example - NickName(Currency)-AccountId, AccountName") 
	private CodeReferencesVO accountFormat;
	
	@ApiModelProperty(value="The segment to which user is attached. For Example - Privilege, HNI") 
	private CodeReferencesVO segmentName;
	
	@ApiModelProperty(value="The access scheme to which user is attached. For Example - Salary Privilege, Privilege, Super HNI") 
	private CodeReferencesVO accessScheme;
	
	@ApiModelProperty(value="The transaction authorization scheme to which user is attached") 
	private CodeReferencesVO transactionAuthorizationScheme;
	
	@ApiModelProperty(value="The last successful  login channel of the user") 
	private CodeReferencesVO loginChannel;
	
	@ApiModelProperty(value="The last unsuccessful  login channel of the user") 
	private CodeReferencesVO lastUnsuccessfullLoginChannel;
	
	@ApiModelProperty(value="The date format as preferred by the user. For Example - yyyy-MM-dd, MM/dd/yyyy etc") 
	private CodeReferencesVO dateFormat;
	
	@ApiModelProperty(value="The preferred calendar type of the user. For Example - Gregorian, Hijri") 
	private CodeReferencesVO calendarType;
	
	@ApiModelProperty(value="The field defines whether multiple logins are mapped or not to the user. Valid Values - Yes (Y), No(N)") 
	private CodeReferencesVO unifiedLoginUser;
	
	@ApiModelProperty(value="The last successful login time of the user") 
	private Date lastSuccessfullLoginTime;
	
	@ApiModelProperty(value="The last unsuccessful login time of the user") 
	private Date lastUnsuccessfullLoginTime;

	@ApiModelProperty(value="Mobile number") 
	private String MobileNumber;

	@ApiModelProperty(value="Phone Number") 
	private String PhoneNumber;
	
	@ApiModelProperty(value="email Id") 
	private String emailId;
	
	@ApiModelProperty(value="Payroll Account Number") 
	private String payrollAccountNumber;	
	
	@ApiModelProperty(value="Collectability Status") 
	private String collectabilityStatus;
	
	public String getCollectabilityStatus() {
		return collectabilityStatus;
	}

	public void setCollectabilityStatus(String collectabilityStatus) {
		this.collectabilityStatus = collectabilityStatus;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPayrollAccountNumber() {
		return payrollAccountNumber;
	}

	public void setPayrollAccountNumber(String payrollAccountNumber) {
		this.payrollAccountNumber = payrollAccountNumber;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public CodeReferencesVO getUserType() {
		return userType;
	}

	public void setUserType(CodeReferencesVO userType) {
		this.userType = userType;
	}

	public CodeReferencesVO getSalutation() {
		return salutation;
	}

	public void setSalutation(CodeReferencesVO salutation) {
		this.salutation = salutation;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPrimaryBranchId() {
		return primaryBranchId;
	}

	public void setPrimaryBranchId(String primaryBranchId) {
		this.primaryBranchId = primaryBranchId;
	}

	public String getPrimaryDivisionId() {
		return primaryDivisionId;
	}

	public void setPrimaryDivisionId(String primaryDivisionId) {
		this.primaryDivisionId = primaryDivisionId;
	}

	public String getPrimaryAccountId() {
		return primaryAccountId;
	}

	public void setPrimaryAccountId(String primaryAccountId) {
		this.primaryAccountId = primaryAccountId;
	}

	public Date getLastTransactionDate() {
		return lastTransactionDate;
	}

	public void setLastTransactionDate(Date lastTransactionDate) {
		this.lastTransactionDate = lastTransactionDate;
	}

	public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public Date getLogoffDate() {
		return logoffDate;
	}

	public void setLogoffDate(Date logoffDate) {
		this.logoffDate = logoffDate;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public CodeReferencesVO getMultiCurrencyTxnAllowed() {
		return multiCurrencyTxnAllowed;
	}

	public void setMultiCurrencyTxnAllowed(CodeReferencesVO multiCurrencyTxnAllowed) {
		this.multiCurrencyTxnAllowed = multiCurrencyTxnAllowed;
	}

	public CodeReferencesVO getGender() {
		return gender;
	}

	public void setGender(CodeReferencesVO gender) {
		this.gender = gender;
	}

	public CodeReferencesVO getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(CodeReferencesVO maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public CodeReferencesVO getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(CodeReferencesVO categoryCode) {
		this.categoryCode = categoryCode;
	}

	public CodeReferencesVO getLimitScheme() {
		return limitScheme;
	}

	public void setLimitScheme(CodeReferencesVO limitScheme) {
		this.limitScheme = limitScheme;
	}

	public CodeReferencesVO getRangeLimitScheme() {
		return rangeLimitScheme;
	}

	public void setRangeLimitScheme(CodeReferencesVO rangeLimitScheme) {
		this.rangeLimitScheme = rangeLimitScheme;
	}

	public CodeReferencesVO getCustomerResidentialStatus() {
		return customerResidentialStatus;
	}

	public void setCustomerResidentialStatus(CodeReferencesVO customerResidentialStatus) {
		this.customerResidentialStatus = customerResidentialStatus;
	}

	public CodeReferencesVO getLoginAllowed() {
		return loginAllowed;
	}

	public void setLoginAllowed(CodeReferencesVO loginAllowed) {
		this.loginAllowed = loginAllowed;
	}

	public CodeReferencesVO getTransactionAllowed() {
		return transactionAllowed;
	}

	public void setTransactionAllowed(CodeReferencesVO transactionAllowed) {
		this.transactionAllowed = transactionAllowed;
	}

	public String getUserNickName() {
		return userNickName;
	}

	public void setUserNickName(String userNickName) {
		this.userNickName = userNickName;
	}

	public CodeReferencesVO getLanguage() {
		return language;
	}

	public void setLanguage(CodeReferencesVO language) {
		this.language = language;
	}

	public CodeReferencesVO getAccountFormat() {
		return accountFormat;
	}

	public void setAccountFormat(CodeReferencesVO accountFormat) {
		this.accountFormat = accountFormat;
	}

	public CodeReferencesVO getSegmentName() {
		return segmentName;
	}

	public void setSegmentName(CodeReferencesVO segmentName) {
		this.segmentName = segmentName;
	}

	public CodeReferencesVO getAccessScheme() {
		return accessScheme;
	}

	public void setAccessScheme(CodeReferencesVO accessScheme) {
		this.accessScheme = accessScheme;
	}

	public CodeReferencesVO getTransactionAuthorizationScheme() {
		return transactionAuthorizationScheme;
	}

	public void setTransactionAuthorizationScheme(CodeReferencesVO transactionAuthorizationScheme) {
		this.transactionAuthorizationScheme = transactionAuthorizationScheme;
	}

	public CodeReferencesVO getLoginChannel() {
		return loginChannel;
	}

	public void setLoginChannel(CodeReferencesVO loginChannel) {
		this.loginChannel = loginChannel;
	}

	public CodeReferencesVO getLastUnsuccessfullLoginChannel() {
		return lastUnsuccessfullLoginChannel;
	}

	public void setLastUnsuccessfullLoginChannel(CodeReferencesVO lastUnsuccessfullLoginChannel) {
		this.lastUnsuccessfullLoginChannel = lastUnsuccessfullLoginChannel;
	}

	public CodeReferencesVO getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(CodeReferencesVO dateFormat) {
		this.dateFormat = dateFormat;
	}

	public CodeReferencesVO getCalendarType() {
		return calendarType;
	}

	public void setCalendarType(CodeReferencesVO calendarType) {
		this.calendarType = calendarType;
	}

	public Date getLastUnsuccessfullLoginTime() {
		return lastUnsuccessfullLoginTime;
	}

	public void setLastUnsuccessfullLoginTime(Date lastUnsuccessfullLoginTime) {
		this.lastUnsuccessfullLoginTime = lastUnsuccessfullLoginTime;
	}

	public CodeReferencesVO getDivisionAccessIndicator() {
		return divisionAccessIndicator;
	}

	public void setDivisionAccessIndicator(CodeReferencesVO divisionAccessIndicator) {
		this.divisionAccessIndicator = divisionAccessIndicator;
	}

	public CodeReferencesVO getUnifiedLoginUser() {
		return unifiedLoginUser;
	}

	public void setUnifiedLoginUser(CodeReferencesVO unifiedLoginUser) {
		this.unifiedLoginUser = unifiedLoginUser;
	}

	public Date getLastSuccessfullLoginTime() {
		return lastSuccessfullLoginTime;
	}

	public void setLastSuccessfullLoginTime(Date lastSuccessfullLoginTime) {
		this.lastSuccessfullLoginTime = lastSuccessfullLoginTime;
	}

	public String getPrincipalId() {
		return principalId;
	}

	public void setPrincipalId(String principalId) {
		this.principalId = principalId;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public CustomUserProfileDetailsVO(String bankId, String orgId, String userId, String principalId,
			CodeReferencesVO userType, String customerId, CodeReferencesVO salutation, String userNickName,
			String firstName, String middleName, String lastName, String primaryBranchId, String primaryDivisionId,
			CodeReferencesVO divisionAccessIndicator, String primaryAccountId, Date lastTransactionDate, Date loginDate,
			Date logoffDate, String sessionId, CodeReferencesVO multiCurrencyTxnAllowed, CodeReferencesVO gender,
			CodeReferencesVO maritalStatus, CodeReferencesVO categoryCode, CodeReferencesVO limitScheme,
			CodeReferencesVO rangeLimitScheme, CodeReferencesVO customerResidentialStatus,
			CodeReferencesVO loginAllowed, CodeReferencesVO transactionAllowed, CodeReferencesVO language,
			CodeReferencesVO accountFormat, CodeReferencesVO segmentName, CodeReferencesVO accessScheme,
			CodeReferencesVO transactionAuthorizationScheme, CodeReferencesVO loginChannel,
			CodeReferencesVO lastUnsuccessfullLoginChannel, CodeReferencesVO dateFormat, CodeReferencesVO calendarType,
			CodeReferencesVO unifiedLoginUser, Date lastSuccessfullLoginTime, Date lastUnsuccessfullLoginTime,
			String mobileNumber, String phoneNumber, String emailId, String payrollAccountNumber,
			String collectabilityStatus) {
		super();
		this.bankId = bankId;
		this.orgId = orgId;
		this.userId = userId;
		this.principalId = principalId;
		this.userType = userType;
		this.customerId = customerId;
		this.salutation = salutation;
		this.userNickName = userNickName;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.primaryBranchId = primaryBranchId;
		this.primaryDivisionId = primaryDivisionId;
		this.divisionAccessIndicator = divisionAccessIndicator;
		this.primaryAccountId = primaryAccountId;
		this.lastTransactionDate = lastTransactionDate;
		this.loginDate = loginDate;
		this.logoffDate = logoffDate;
		this.sessionId = sessionId;
		this.multiCurrencyTxnAllowed = multiCurrencyTxnAllowed;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.categoryCode = categoryCode;
		this.limitScheme = limitScheme;
		this.rangeLimitScheme = rangeLimitScheme;
		this.customerResidentialStatus = customerResidentialStatus;
		this.loginAllowed = loginAllowed;
		this.transactionAllowed = transactionAllowed;
		this.language = language;
		this.accountFormat = accountFormat;
		this.segmentName = segmentName;
		this.accessScheme = accessScheme;
		this.transactionAuthorizationScheme = transactionAuthorizationScheme;
		this.loginChannel = loginChannel;
		this.lastUnsuccessfullLoginChannel = lastUnsuccessfullLoginChannel;
		this.dateFormat = dateFormat;
		this.calendarType = calendarType;
		this.unifiedLoginUser = unifiedLoginUser;
		this.lastSuccessfullLoginTime = lastSuccessfullLoginTime;
		this.lastUnsuccessfullLoginTime = lastUnsuccessfullLoginTime;
		MobileNumber = mobileNumber;
		PhoneNumber = phoneNumber;
		this.emailId = emailId;
		this.payrollAccountNumber = payrollAccountNumber;
		this.collectabilityStatus = collectabilityStatus;
	}

	
	
	
}

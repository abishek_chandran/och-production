package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.ebanking.rest.common.v1.pojo.CommonHeaderRetailDetailsVO;

public class CustomLoanApplicationRequestVO  extends CommonHeaderRetailDetailsVO {

private CustomLoanApplicationMasterDetailsVO data;

/**
 * @return the data
 */
public CustomLoanApplicationMasterDetailsVO getData() {
	return data;
}

/**
 * @param data the data to set
 */
public void setData(CustomLoanApplicationMasterDetailsVO data) {
	this.data = data;
}


}

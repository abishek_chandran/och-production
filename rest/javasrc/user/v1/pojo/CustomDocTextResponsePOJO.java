package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.types.primitives.FEBAUnboundInt;
import com.infosys.feba.framework.types.primitives.FEBAUnboundString;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Document Text Maintenance Details")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomDocTextResponsePOJO {

	@ApiModelProperty(value = "row number")
	/** Holds the row number */
	private int rowNumber;
	@ApiModelProperty(value = "text code")
	/** Holds the text name */
	private String textName;
	@ApiModelProperty(value = "text")
	/** Holds the text value */
	private String text;
	@ApiModelProperty(value = "text serial number")
	/** Holds the display serial number of returned value */
	private String srlNumber;
	@ApiModelProperty(value = "text type")
	/** Holds the text type from values (TEXT|HEADING|TABLE) */
	private String textType;
	@ApiModelProperty(value = "parent text code")
	/** Holds the text name of the containing text */
	private String parentTextName;
	@ApiModelProperty(value = "indentation level for the text")
	/** Holds the indentation level for the text */
	private int indentLevel;
	@ApiModelProperty(value = "table")
	/** Holds the table value */	
	private CustomDocTextTablePOJO table;

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getTextName() {
		return textName;
	}

	public void setTextName(String textName) {
		this.textName = textName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSrlNumber() {
		return srlNumber;
	}

	public void setSrlNumber(String srlNumber) {
		this.srlNumber = srlNumber;
	}

	public String getTextType() {
		return textType;
	}

	public void setTextType(String textType) {
		this.textType = textType;
	}

	public String getParentTextName() {
		return parentTextName;
	}

	public void setParentTextName(String parentTextName) {
		this.parentTextName = parentTextName;
	}

	public int getIndentLevel() {
		return indentLevel;
	}

	public void setIndentLevel(int indentLevel) {
		this.indentLevel = indentLevel;
	}

	public CustomDocTextTablePOJO getTable() {
		return table;
	}

	public void setTable(CustomDocTextTablePOJO table) {
		this.table = table;
	}

}

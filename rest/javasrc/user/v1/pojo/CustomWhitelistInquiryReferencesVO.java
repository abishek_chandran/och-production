package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ATM Inquiry Whitelist Inquiry Call")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomWhitelistInquiryReferencesVO {

	@ApiModelProperty(value = "Payroll Date")
	private String payrollDate;

	@ApiModelProperty(value = "Company Name")
	private String companyName;

	@ApiModelProperty(value = "Employee Contract Start Date")
	private String employeeContractStartDate;

	@ApiModelProperty(value = "Employee Contract End Date")
	private String employeeContractEndDate;

	@ApiModelProperty(value = "Company Referral Code")
	private String companyReferralCode;

	@ApiModelProperty(value = "Average Saving Amount")
	private String averageSavingAmount;

	@ApiModelProperty(value = "Frequency Mutasi Credit 3")
	private String freqMutasiCredit3;

	@ApiModelProperty(value = "Frequency Mutasi Credit 6")
	private String freqMutasiCredit6;

	@ApiModelProperty(value = "Frequency Mutasi Credit 12")
	private String freqMutasiCredit12;

	@ApiModelProperty(value = "Frequency Mutasi Debit 3")
	private String freqMutasiDeb3;

	@ApiModelProperty(value = "Frequency Mutasi Debit 6")
	private String freqMutasiDeb6;

	@ApiModelProperty(value = "Frequency Mutasi Debit 12")
	private String freqMutasiDeb12;

	@ApiModelProperty(value = "Total Mutasi Credit 3")
	private String totalMutasiCredit3;

	@ApiModelProperty(value = "Total Mutasi Credit 6")
	private String totalMutasiCredit6;

	@ApiModelProperty(value = "Total Mutasi Credit 12")
	private String totalMutasiCredit12;

	@ApiModelProperty(value = "Total Mutasi Debit 3")
	private String totalMutasiDeb3;

	@ApiModelProperty(value = "Total Mutasi Debit 6")
	private String totalMutasiDeb6;

	@ApiModelProperty(value = "Total Mutasi Debit 12")
	private String totalMutasiDeb12;

	@ApiModelProperty(value = "Rata Mutasi Credit 3")
	private String rataMutasiCredit3;

	@ApiModelProperty(value = "Rata Mutasi Credit 6")
	private String rataMutasiCredit6;

	@ApiModelProperty(value = "Rata Mutasi Credit 12")
	private String rataMutasiCredit12;

	@ApiModelProperty(value = "Rata Mutasi Debit 3")
	private String rataMutasiDeb3;

	@ApiModelProperty(value = "Rata Mutasi Debit 6")
	private String rataMutasiDeb6;

	@ApiModelProperty(value = "Rata Mutasi Debit 12")
	private String rataMutasiDeb12;

	@ApiModelProperty(value = "Mobile Banking")
	private String mobileBanking;

	@ApiModelProperty(value = "iNet Banking")
	private String inetBanking;

	@ApiModelProperty(value = "Simpanan Britama")
	private String simpananBritama;

	@ApiModelProperty(value = "Waktu Kepemilikan Simpanan Britama")
	private String waktuKepemilikanSimpananBritama;

	@ApiModelProperty(value = "Primary Phone")
	private String primaryPhone;

	@ApiModelProperty(value = "Secondary Phone")
	private String secondaryPhone;

	@ApiModelProperty(value = "Koperasi Name")
	private String koperasiName;
	
	@ApiModelProperty(value = "Personel Name")
	private String personelName;
	
	@ApiModelProperty(value = "Personel Number")
	private String personelNumber;
	
	@ApiModelProperty(value = "Payroll Amount")
	private String payrollAmount;
	
	public String getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(String payrollDate) {
		this.payrollDate = payrollDate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getEmployeeContractStartDate() {
		return employeeContractStartDate;
	}

	public void setEmployeeContractStartDate(String employeeContractStartDate) {
		this.employeeContractStartDate = employeeContractStartDate;
	}

	public String getEmployeeContractEndDate() {
		return employeeContractEndDate;
	}

	public void setEmployeeContractEndDate(String employeeContractEndDate) {
		this.employeeContractEndDate = employeeContractEndDate;
	}

	public String getCompanyReferralCode() {
		return companyReferralCode;
	}

	public void setCompanyReferralCode(String companyReferralCode) {
		this.companyReferralCode = companyReferralCode;
	}

	public String getAverageSavingAmount() {
		return averageSavingAmount;
	}

	public void setAverageSavingAmount(String averageSavingAmount) {
		this.averageSavingAmount = averageSavingAmount;
	}

	public String getFreqMutasiCredit3() {
		return freqMutasiCredit3;
	}

	public void setFreqMutasiCredit3(String freqMutasiCredit3) {
		this.freqMutasiCredit3 = freqMutasiCredit3;
	}

	public String getFreqMutasiCredit6() {
		return freqMutasiCredit6;
	}

	public void setFreqMutasiCredit6(String freqMutasiCredit6) {
		this.freqMutasiCredit6 = freqMutasiCredit6;
	}

	public String getFreqMutasiCredit12() {
		return freqMutasiCredit12;
	}

	public void setFreqMutasiCredit12(String freqMutasiCredit12) {
		this.freqMutasiCredit12 = freqMutasiCredit12;
	}

	public String getFreqMutasiDeb3() {
		return freqMutasiDeb3;
	}

	public void setFreqMutasiDeb3(String freqMutasiDeb3) {
		this.freqMutasiDeb3 = freqMutasiDeb3;
	}

	public String getFreqMutasiDeb6() {
		return freqMutasiDeb6;
	}

	public void setFreqMutasiDeb6(String freqMutasiDeb6) {
		this.freqMutasiDeb6 = freqMutasiDeb6;
	}

	public String getFreqMutasiDeb12() {
		return freqMutasiDeb12;
	}

	public void setfreqMutasiDeb12(String freqMutasiDeb12) {
		this.freqMutasiDeb12 = freqMutasiDeb12;
	}

	public String getTotalMutasiCredit3() {
		return totalMutasiCredit3;
	}

	public void setTotalMutasiCredit3(String totalMutasiCredit3) {
		this.totalMutasiCredit3 = totalMutasiCredit3;
	}

	public String getTotalMutasiCredit6() {
		return totalMutasiCredit6;
	}

	public void setTotalMutasiCredit6(String totalMutasiCredit6) {
		this.totalMutasiCredit6 = totalMutasiCredit6;
	}

	public String getTotalMutasiCredit12() {
		return totalMutasiCredit12;
	}

	public void setTotalMutasiCredit12(String totalMutasiCredit12) {
		this.totalMutasiCredit12 = totalMutasiCredit12;
	}

	public String getTotalMutasiDeb3() {
		return totalMutasiDeb3;
	}

	public void setTotalMutasiDeb3(String totalMutasiDeb3) {
		this.totalMutasiDeb3 = totalMutasiDeb3;
	}

	public String getTotalMutasiDeb6() {
		return totalMutasiDeb6;
	}

	public void setTotalMutasiDeb6(String totalMutasiDeb6) {
		this.totalMutasiDeb6 = totalMutasiDeb6;
	}

	public String getTotalMutasiDeb12() {
		return totalMutasiDeb12;
	}

	public void setTotalMutasiDeb12(String totalMutasiDeb12) {
		this.totalMutasiDeb12 = totalMutasiDeb12;
	}

	public String getRataMutasiCredit3() {
		return rataMutasiCredit3;
	}

	public void setRataMutasiCredit3(String rataMutasiCredit3) {
		this.rataMutasiCredit3 = rataMutasiCredit3;
	}

	public String getRataMutasiCredit6() {
		return rataMutasiCredit6;
	}

	public void setRataMutasiCredit6(String rataMutasiCredit6) {
		this.rataMutasiCredit6 = rataMutasiCredit6;
	}

	public String getRataMutasiCredit12() {
		return rataMutasiCredit12;
	}

	public void setRataMutasiCredit12(String rataMutasiCredit12) {
		this.rataMutasiCredit12 = rataMutasiCredit12;
	}

	public String getRataMutasiDeb3() {
		return rataMutasiDeb3;
	}

	public void setRataMutasiDeb3(String rataMutasiDeb3) {
		this.rataMutasiDeb3 = rataMutasiDeb3;
	}

	public String getRataMutasiDeb6() {
		return rataMutasiDeb6;
	}

	public void setRataMutasiDeb6(String rataMutasiDeb6) {
		this.rataMutasiDeb6 = rataMutasiDeb6;
	}

	public String getRataMutasiDeb12() {
		return rataMutasiDeb12;
	}

	public void setRataMutasiDeb12(String rataMutasiDeb12) {
		this.rataMutasiDeb12 = rataMutasiDeb12;
	}

	public String getMobileBanking() {
		return mobileBanking;
	}

	public void setMobileBanking(String mobileBanking) {
		this.mobileBanking = mobileBanking;
	}

	public String getInetBanking() {
		return inetBanking;
	}

	public void setInetBanking(String inetBanking) {
		this.inetBanking = inetBanking;
	}

	public String getSimpananBritama() {
		return simpananBritama;
	}

	public void setSimpananBritama(String simpananBritama) {
		this.simpananBritama = simpananBritama;
	}

	public String getWaktuKepemilikanSimpananBritama() {
		return waktuKepemilikanSimpananBritama;
	}

	public void setWaktuKepemilikanSimpananBritama(String waktuKepemilikanSimpananBritama) {
		this.waktuKepemilikanSimpananBritama = waktuKepemilikanSimpananBritama;
	}

	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	public String getKoperasiName() {
		return koperasiName;
	}

	public void setKoperasiName(String koperasiName) {
		this.koperasiName = koperasiName;
	}

	public String getPersonelName() {
		return personelName;
	}

	public void setPersonelName(String personelName) {
		this.personelName = personelName;
	}

	public String getPersonelNumber() {
		return personelNumber;
	}

	public void setPersonelNumber(String personelNumber) {
		this.personelNumber = personelNumber;
	}
		
	public String getPayrollAmount() {
		return payrollAmount;
	}

	public void setPayrollAmount(String payrollAmount) {
		this.payrollAmount = payrollAmount;
	}

	public void setFreqMutasiDeb12(String freqMutasiDeb12) {
		this.freqMutasiDeb12 = freqMutasiDeb12;
	}

	public CustomWhitelistInquiryReferencesVO() {
		super();
	}

	public CustomWhitelistInquiryReferencesVO(String payrollDate, String companyName, String employeeContractStartDate,
			String employeeContractEndDate, String companyReferralCode, String averageSavingAmount,
			String freqMutasiCredit3, String freqMutasiCredit6, String freqMutasiCredit12, String freqMutasiDeb3,
			String freqMutasiDeb6, String freqMutasiDeb12, String totalMutasiCredit3, String totalMutasiCredit6,
			String totalMutasiCredit12, String totalMutasiDeb3, String totalMutasiDeb6, String totalMutasiDeb12,
			String rataMutasiCredit3, String rataMutasiCredit6, String rataMutasiCredit12, String rataMutasiDeb3,
			String rataMutasiDeb6, String rataMutasiDeb12, String mobileBanking, String inetBanking,
			String simpananBritama, String waktuKepemilikanSimpananBritama, String primaryPhone,
			String secondaryPhone,String payrollAmount) {
		super();
		this.payrollDate = payrollDate;
		this.companyName = companyName;
		this.employeeContractStartDate = employeeContractStartDate;
		this.employeeContractEndDate = employeeContractEndDate;
		this.companyReferralCode = companyReferralCode;
		this.averageSavingAmount = averageSavingAmount;
		this.freqMutasiCredit3 = freqMutasiCredit3;
		this.freqMutasiCredit6 = freqMutasiCredit6;
		this.freqMutasiCredit12 = freqMutasiCredit12;
		this.freqMutasiDeb3 = freqMutasiDeb3;
		this.freqMutasiDeb6 = freqMutasiDeb6;
		this.freqMutasiDeb12 = freqMutasiDeb12;
		this.totalMutasiCredit3 = totalMutasiCredit3;
		this.totalMutasiCredit6 = totalMutasiCredit6;
		this.totalMutasiCredit12 = totalMutasiCredit12;
		this.totalMutasiDeb3 = totalMutasiDeb3;
		this.totalMutasiDeb6 = totalMutasiDeb6;
		this.totalMutasiDeb12 = totalMutasiDeb12;
		this.rataMutasiCredit3 = rataMutasiCredit3;
		this.rataMutasiCredit6 = rataMutasiCredit6;
		this.rataMutasiCredit12 = rataMutasiCredit12;
		this.rataMutasiDeb3 = rataMutasiDeb3;
		this.rataMutasiDeb6 = rataMutasiDeb6;
		this.rataMutasiDeb12 = rataMutasiDeb12;
		this.mobileBanking = mobileBanking;
		this.inetBanking = inetBanking;
		this.simpananBritama = simpananBritama;
		this.waktuKepemilikanSimpananBritama = waktuKepemilikanSimpananBritama;
		this.primaryPhone = primaryPhone;
		this.secondaryPhone = secondaryPhone;
		this.koperasiName = koperasiName;
		this.personelName = personelName;
		this.personelNumber = personelNumber;
		this.payrollAmount = payrollAmount;

	}
}

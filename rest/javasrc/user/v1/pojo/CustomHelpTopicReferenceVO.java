package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class CustomHelpTopicReferenceVO {

	@ApiModelProperty(value = "Help Topic")
	private String helpTopic;

	@ApiModelProperty(value = "List of Sub Topics")
	private List<CustomHelpSubTopicVO> subTopics;

	public String getHelpTopic() {
		return helpTopic;
	}

	public void setHelpTopic(String helpTopic) {
		this.helpTopic = helpTopic;
	}

	public List<CustomHelpSubTopicVO> getSubTopics() {
		return subTopics;
	}

	public void setSubTopics(List<CustomHelpSubTopicVO> subTopics) {
		this.subTopics = subTopics;
	}

}

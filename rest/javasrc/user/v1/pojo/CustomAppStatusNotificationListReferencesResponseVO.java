package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomAppStatusNotificationListReferencesResponseVO extends RestHeaderFooterHandler {
	
	
	
	public CustomAppStatusNotificationListReferencesResponseVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private List<CustomAppStatusNotificationListReferencesVO> list;

	public List<CustomAppStatusNotificationListReferencesVO> getList() {
		return list;
	}

	public CustomAppStatusNotificationListReferencesResponseVO(List<CustomAppStatusNotificationListReferencesVO> list) {
		super();
		this.list = list;
	}

	public void setList(List<CustomAppStatusNotificationListReferencesVO> list) {
		this.list = list;
	}

}

package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomLoanApplicationResponseVO extends RestHeaderFooterHandler {
	
	private CustomLoanApplicationMasterDetailsVO data = null;
	
	public void setData(CustomLoanApplicationMasterDetailsVO data) {
		this.data = data;
	}

	public CustomLoanApplicationMasterDetailsVO getData() {
		return data;
	}

}

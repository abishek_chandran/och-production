package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomFetchLoanSchemesReferencesResponseVO extends RestHeaderFooterHandler {
	private List<CustomFetchLoanSchemesReferencesVO> data;

	public List<CustomFetchLoanSchemesReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomFetchLoanSchemesReferencesVO> data) {
		this.data = data;
	}
}

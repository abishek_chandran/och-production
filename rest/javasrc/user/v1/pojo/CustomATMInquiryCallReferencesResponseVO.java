package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomATMInquiryCallReferencesResponseVO extends RestHeaderFooterHandler{
	private List<CustomATMInquiryCallReferencesVO> data;
	
	public List<CustomATMInquiryCallReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomATMInquiryCallReferencesVO> data) {
		this.data = data;
	}

	
	
}

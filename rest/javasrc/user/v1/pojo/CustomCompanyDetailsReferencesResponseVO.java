package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

import io.swagger.annotations.ApiModel;

@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
@ApiModel(value="Company Details Response")
public class CustomCompanyDetailsReferencesResponseVO extends RestHeaderFooterHandler{
	
	private List<CustomCompanyDetailsInOutVO> data;

	public List<CustomCompanyDetailsInOutVO> getData() {
		return data;
	}

	public void setData(List<CustomCompanyDetailsInOutVO> data) {
		this.data = data;
	}

}

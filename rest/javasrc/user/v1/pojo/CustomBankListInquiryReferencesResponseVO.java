package com.infosys.custom.ebanking.rest.user.v1.pojo;

import java.util.List;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomBankListInquiryReferencesResponseVO extends RestHeaderFooterHandler{
	
	private List<CustomBankListInquiryReferencesVO> data;

	public List<CustomBankListInquiryReferencesVO> getData() {
		return data;
	}

	public void setData(List<CustomBankListInquiryReferencesVO> data) {
		this.data = data;
	}


	

}

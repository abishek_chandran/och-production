package com.infosys.custom.ebanking.rest.user.v1.pojo;

import com.infosys.feba.framework.interceptor.rest.pojo.RestHeaderFooterHandler;

public class CustomVerifyPayrollAcctReferencesResponseVO extends RestHeaderFooterHandler {

	private CustomVerifyPayrollAcctReferencesVO data;

	public CustomVerifyPayrollAcctReferencesVO getData() {
		return data;
	}

	public void setData(CustomVerifyPayrollAcctReferencesVO data) {
		this.data = data;
	}
	
	
}

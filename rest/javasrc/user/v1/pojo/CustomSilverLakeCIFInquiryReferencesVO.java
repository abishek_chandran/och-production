package com.infosys.custom.ebanking.rest.user.v1.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ATM Inquiry Silverlake CIF Inquiry Call")
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomSilverLakeCIFInquiryReferencesVO {

	@ApiModelProperty(value = "CIF No")
	private String cifNo;

	@ApiModelProperty(value = "Address")
	private String address;

	@ApiModelProperty(value = "Address2")
	private String address2;

	@ApiModelProperty(value = "Address3")
	private String address3;

	@ApiModelProperty(value = "Address4")
	private String address4;

	@ApiModelProperty(value = "AddressRT")
	private String addressRT;

	@ApiModelProperty(value = "AddressRW")
	private String addressRW;

	@ApiModelProperty(value = "Agama")
	private String agama;

	@ApiModelProperty(value = "Birth Date")
	private String birthDate;

	@ApiModelProperty(value = "City")
	private String city;

	@ApiModelProperty(value = "id No")
	private String idNo;

	@ApiModelProperty(value = "id Type")
	private String idType;

	@ApiModelProperty(value = "Kecamatan")
	private String kecamatan;

	@ApiModelProperty(value = "Kelurahan")
	private String kelurahan;

	@ApiModelProperty(value = "Nama")
	private String nama;

	@ApiModelProperty(value = "Place Of Birth")
	private String placeOfBirth;

	@ApiModelProperty(value = "Province")
	private String province;

	@ApiModelProperty(value = "Sex")
	private String sex;

	@ApiModelProperty(value = "ZipCode")
	private String zipCode;

	public String getCifNo() {
		return cifNo;
	}

	public void setCifNo(String cifNo) {
		this.cifNo = cifNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getAddressRT() {
		return addressRT;
	}

	public void setAddressRT(String addressRT) {
		this.addressRT = addressRT;
	}

	public String getAddressRW() {
		return addressRW;
	}

	public void setAddressRW(String addressRW) {
		this.addressRW = addressRW;
	}

	public String getAgama() {
		return agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public CustomSilverLakeCIFInquiryReferencesVO() {
		super();
	}

	public CustomSilverLakeCIFInquiryReferencesVO(String cifNo, String address, String address2, String address3,
			String address4, String addressRT, String addressRW, String agama, String birthDate, String city,
			String idNo, String idType, String kecamatan, String kelurahan, String nama, String placeOfBirth,
			String province, String sex, String zipCode) {
		super();
		this.cifNo = cifNo;
		this.address = address;
		this.address2 = address2;
		this.address3 = address3;
		this.address4 = address4;
		this.addressRT = addressRT;
		this.addressRW = addressRW;
		this.agama = agama;
		this.birthDate = birthDate;
		this.city = city;
		this.idNo = idNo;
		this.idType = idType;
		this.kecamatan = kecamatan;
		this.kelurahan = kelurahan;
		this.nama = nama;
		this.placeOfBirth = placeOfBirth;
		this.province = province;
		this.sex = sex;
		this.zipCode = zipCode;

	}

}
